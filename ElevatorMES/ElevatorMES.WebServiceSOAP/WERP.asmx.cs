﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using ElevatorMES.Data;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.WebServiceSOAP.Log;

namespace ElevatorMES.WebServiceSOAP
{
    /// <summary>
    /// Интерфейс взаимодействия с ERP
    /// </summary>
    [WebService(Namespace = "http://ElevatorMES.ru/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Чтобы разрешить вызывать веб-службу из скрипта с помощью ASP.NET AJAX, раскомментируйте следующую строку. 
    [System.Web.Script.Services.ScriptService]
    public class WERP : System.Web.Services.WebService
    {
        private UnitOfWork uof = new UnitOfWork(new DatabaseContext());
        private TaskManager TaskManager => new TaskManager(uof);
        private DictManager DictManager => new DictManager(uof);
        private SyncManager SyncManager => new SyncManager(uof);

        public static readonly ILog Logger = new NlogLogger();

        [WebMethod]
        public string Ping()
        {
            //var result = Shipping(new ShippingArg()
            //{
            //    DocId = "2191fbca-bce2-11ea-81f6-0050569bd0d4",
            //    VehicleRegNum = "M979MA161RUS",
            //    Quantity = 50,
            //    NomenclatureCode1C = "8f33e8b8-0111-11e0-8080-002481e8860a",
            //    SourceUnitCode1C = "f39e2b0d-f20c-11e8-81ae-00505680688f",
            //    OrganizationCode1C = "46cd8af4-e2b1-11db-828d-00306e222164",
            //    ExecuteAt = DateTime.Now,
            //    User = "ASU_elevator",
            //    Comment = "3"
            //}
            //);
            return TryCatchLog(() =>
            {
                return DateTime.Now.ToString();
            });
        }

        [WebMethod(Description = "Поступление информации о ТС от ERP")]
        public string RegVehicle(RegVehicleArg regVehicleArg)
        {
            return TryCatchLog(() =>
            {
                return TaskManager.RegVehicle(regVehicleArg);
            });
       }

        /// <summary>
        /// Поступление задания на приемку сырья от 1С
        /// </summary>
        /// <param name="rowAcceptArg"></param>
        [WebMethod(Description = "Поступление задания на приемку сырья от ERP")]
        public string Accept(RowAcceptArg rowAcceptArg)
        {
            return TryCatchLog(() =>
            {
                rowAcceptArg.IsQualityControl = rowAcceptArg.IsGood;
                return TaskManager.RowAccept(rowAcceptArg);
            });
        }

        ///// <summary>
        ///// Отгрузка сырья на КЗ
        ///// </summary>
        ///// <param name="plantMovingArg"></param>
        //[WebMethod(Description = "Отгрузка сырья на КЗ")]
        //public bool PlantMoving(PlantMovingArg plantMovingArg)
        //{
        //    TaskManager.PlantMoving(plantMovingArg);
        //    return true;
        //}

        /// <summary>
        /// Отгрузка сырья в рукава
        /// </summary>
        /// <param name="sleeveArg"></param>
        [WebMethod(Description = "Отгрузка сырья в рукава")]
        public string Sleeve(SleeveArg sleeveArg)
        {
            return TryCatchLog(() =>
            {
                return TaskManager.Sleeve(sleeveArg);
            });
        }

        /// <summary>
        /// Отгрузка сырья внешним потребителям
        /// </summary>
        /// <param name="shippingArg"></param>
        [WebMethod(Description = "Отгрузка сырья внешним потребителям")]
        public string Shipping(ShippingArg shippingArg)
        {
            return TryCatchLog(() =>
            {
                return TaskManager.Shipping(shippingArg);
            });
        }


        /// <summary>
        /// Синхронизируемые с 1С таблицы
        /// </summary>
        /// <returns></returns>
        [WebMethod(Description = "Запрос списка синхронизируемых таблиц")]
        public List<string> GetSyncEntities()
        {
            return SyncManager.GetSyncEntities().ToList();
        }

        /// <summary>
        /// Синхронизировать
        /// </summary>
        /// <param name="dictSyncArg"></param>
        [WebMethod(Description = "Синхронизиция таблиц с ERP")]
        public bool Sync(DictSyncArg dictSyncArg)
        {
            TryCatchLog(() =>
            {
                SyncManager.Sync(dictSyncArg);
                return "True";
            });
            return true;
            
        }

        /// <summary>
        /// Приоритет
        /// </summary>
        /// <param name="dictSyncArg"></param>
        [WebMethod(Description = "Задать приоритет загрузки сырья")]
        public string Priority(PriorityArg priorityArg)
        {
            return TryCatchLog(() =>
            {
                return TaskManager.UpdatePriority(priorityArg);
            });
        }

        /// <summary>
        /// Логи от 1С
        /// </summary>
        /// <param name="dictSyncArg"></param>
        [WebMethod(Description = "Добавить логи из 1С")]
        public string AddLogEvent1C(LogEvent1CArg logEvent1CArg)
        {
            return TryCatchLog(() =>
            {
                return SyncManager.AddLogEvent1C(logEvent1CArg);
            });
        }

        #region privateMethods
        private string TryCatchLog(Func<string> action)
        {
            try
            {
                return action();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                throw;
            }
        }
        #endregion
    }
}
