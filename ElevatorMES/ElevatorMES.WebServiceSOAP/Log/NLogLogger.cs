﻿using ElevatorMES.Domain;
using NLog;

namespace ElevatorMES.WebServiceSOAP.Log
{
    public class NlogLogger : ILog
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public void Trace(string text)
        {
            Logger.Trace(text);
        }
        public void Info(string text)
        {
            Logger.Info(text);
        }

        public void Error(string text)
        {
            Logger.Error(text);
        }
    }
}