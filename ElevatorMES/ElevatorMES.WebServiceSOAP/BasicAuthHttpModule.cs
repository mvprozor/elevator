﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace ElevatorMES.WebServiceSOAP
{
    public class BasicAuthHttpModule : IHttpModule
    {
        private static Regex regex = new Regex(@"(?<=<title>)[\w\s\r\n]*?(?=</title)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public void Dispose()
        {
        }

        public void Init(HttpApplication application)
        {
            application.EndRequest += OnEndRequest;
            application.BeginRequest += LogRequest;
        }
        private void LogRequest(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;
            var request = app.Context.Request;

            byte[] buffer = new byte[request.InputStream.Length];
            request.InputStream.Read(buffer, 0, buffer.Length);
            request.InputStream.Position = 0;

            string soapMessage = Encoding.ASCII.GetString(buffer);
            WERP.Logger.Info($"HTTP IP: {request.UserHostAddress}. RawUrl:{request.RawUrl} Request: {soapMessage}");

        }

        public void OnEndRequest(object source, EventArgs eventArgs)
        {
            if (HttpContext.Current.Response.StatusCode == 401)
            {
                HttpContext context = HttpContext.Current;
                context.Response.StatusCode = 401;
                context.Response.AddHeader("WWW-Authenticate", "Basic Realm");
            }

            WERP.Logger.Trace($"------------------------ End Request [{HttpContext.Current.Response.StatusCode}] -------------------------- ");
        }
      
        private void DenyAccess(HttpApplication app)
        {
            app.Response.StatusCode = 401;
            app.Response.StatusDescription = "Access Denied";
            app.Response.Write("401 Access Denied");
            app.CompleteRequest();
        }
    }
  
}