﻿namespace ElevatorMES.WinControl
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mesUserControl1 = new ElevatorMES.WinControl.MESUserControl();
            this.SuspendLayout();
            // 
            // mesUserControl1
            // 
            this.mesUserControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mesUserControl1.Location = new System.Drawing.Point(0, 0);
            this.mesUserControl1.Name = "mesUserControl1";
            this.mesUserControl1.Size = new System.Drawing.Size(1054, 715);
            this.mesUserControl1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 715);
            this.Controls.Add(this.mesUserControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MESUserControl mesUserControl1;
    }
}

