﻿namespace ElevatorMES.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _3 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.VehicleActivities");
            CreateTable(
                "dbo.UnitInventories",
                c => new
                    {
                        UnitId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        NomenclatureId = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 3),
                        Comment = c.String(maxLength: 2000),
                        UserId = c.Int(),
                    })
                .PrimaryKey(t => new { t.UnitId, t.CreatedAt })
                .ForeignKey("dbo.Nomenclatures", t => t.NomenclatureId, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UnitId)
                .Index(t => t.NomenclatureId)
                .Index(t => t.UserId);
            
            AddColumn("dbo.Tasks", "IsQualityControl", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TaskExecMoves", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.Units", "Capacity", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.Tasks", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.UnitLayers", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.VehicleActivities", "Weight", c => c.Decimal(precision: 18, scale: 3));
            AddPrimaryKey("dbo.VehicleActivities", new[] { "VehicleId", "CreatedAt" });
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UnitInventories", "UserId", "dbo.Users");
            DropForeignKey("dbo.UnitInventories", "UnitId", "dbo.Units");
            DropForeignKey("dbo.UnitInventories", "NomenclatureId", "dbo.Nomenclatures");
            DropIndex("dbo.UnitInventories", new[] { "UserId" });
            DropIndex("dbo.UnitInventories", new[] { "NomenclatureId" });
            DropIndex("dbo.UnitInventories", new[] { "UnitId" });
            DropPrimaryKey("dbo.VehicleActivities");
            AlterColumn("dbo.VehicleActivities", "Weight", c => c.Decimal(precision: 8, scale: 0));
            AlterColumn("dbo.UnitLayers", "Quantity", c => c.Decimal(nullable: false, precision: 8, scale: 0));
            AlterColumn("dbo.Tasks", "Quantity", c => c.Decimal(nullable: false, precision: 8, scale: 0));
            AlterColumn("dbo.Units", "Capacity", c => c.Decimal(nullable: false, precision: 8, scale: 0));
            AlterColumn("dbo.TaskExecMoves", "Quantity", c => c.Decimal(nullable: false, precision: 8, scale: 0));
            DropColumn("dbo.Tasks", "IsQualityControl");
            DropTable("dbo.UnitInventories");
            AddPrimaryKey("dbo.VehicleActivities", new[] { "CreatedAt", "VehicleId" });
        }
    }
}
