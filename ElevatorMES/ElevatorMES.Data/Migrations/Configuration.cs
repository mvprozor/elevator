﻿namespace ElevatorMES.Data.Migrations
{
    using ElevatorMES.Domain.Data;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ElevatorMES.Data.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ElevatorMES.Data.DatabaseContext";
        }

        protected override void Seed(ElevatorMES.Data.DatabaseContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            context.Equipments.AddRange(Equipment.Seed());
            context.SaveChanges();
        }
    }
}
