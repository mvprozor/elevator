﻿namespace ElevatorMES.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contragents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Deleted = c.Boolean(nullable: false),
                        DisplayName = c.String(nullable: false, maxLength: 255),
                        Code1C = c.String(nullable: false, maxLength: 36),
                        Type1C = c.String(maxLength: 36),
                        CodeParent1C = c.String(maxLength: 36),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code1C, unique: true);
            
            CreateTable(
                "dbo.LogEvent1C",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocId = c.String(maxLength: 36),
                        EventType = c.String(),
                        Message = c.String(),
                        DateMessage = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Lots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LotNo = c.String(nullable: false, maxLength: 64),
                        NomenclatureId = c.Int(nullable: false),
                        OrganizationId = c.Int(),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nomenclatures", t => t.NomenclatureId, cascadeDelete: true)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId)
                .Index(t => t.LotNo, unique: true)
                .Index(t => t.NomenclatureId)
                .Index(t => t.OrganizationId);
            
            CreateTable(
                "dbo.Nomenclatures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Deleted = c.Boolean(nullable: false),
                        DisplayName = c.String(nullable: false, maxLength: 255),
                        Code1C = c.String(nullable: false, maxLength: 36),
                        Type1C = c.String(maxLength: 36),
                        CodeParent1C = c.String(maxLength: 36),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code1C, unique: true);
            
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Deleted = c.Boolean(nullable: false),
                        DisplayName = c.String(nullable: false, maxLength: 255),
                        Code1C = c.String(nullable: false, maxLength: 36),
                        Type1C = c.String(maxLength: 36),
                        CodeParent1C = c.String(maxLength: 36),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code1C, unique: true);
            
            CreateTable(
                "dbo.NomenclaturePriorities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocId = c.String(maxLength: 36),
                        NomenclatureId = c.Int(nullable: false),
                        Priority = c.Int(nullable: false),
                        UserId = c.Int(),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nomenclatures", t => t.NomenclatureId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.NomenclatureId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Deleted = c.Boolean(nullable: false),
                        DisplayName = c.String(nullable: false, maxLength: 255),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaskExecMoves",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskExecId = c.Int(nullable: false),
                        LotId = c.Int(nullable: false),
                        SourceUnitId = c.Int(),
                        SourceVehicleId = c.Int(),
                        DestUnitId = c.Int(),
                        DestVehicleId = c.Int(),
                        MovedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Quantity = c.Decimal(nullable: false, precision: 8, scale: 0),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Units", t => t.DestUnitId)
                .ForeignKey("dbo.Vehicles", t => t.DestVehicleId)
                .ForeignKey("dbo.Lots", t => t.LotId, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.SourceUnitId)
                .ForeignKey("dbo.Vehicles", t => t.SourceVehicleId)
                .ForeignKey("dbo.TaskExecs", t => t.TaskExecId, cascadeDelete: true)
                .Index(t => t.TaskExecId)
                .Index(t => t.LotId)
                .Index(t => t.SourceUnitId)
                .Index(t => t.SourceVehicleId)
                .Index(t => t.DestUnitId)
                .Index(t => t.DestVehicleId);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Capacity = c.Decimal(nullable: false, precision: 8, scale: 0),
                        Deleted = c.Boolean(nullable: false),
                        DisplayName = c.String(nullable: false, maxLength: 255),
                        Code1C = c.String(nullable: false, maxLength: 36),
                        Type1C = c.String(maxLength: 36),
                        CodeParent1C = c.String(maxLength: 36),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code1C, unique: true);
            
            CreateTable(
                "dbo.Vehicles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VehicleTypeId = c.Int(nullable: false),
                        RegNum = c.String(nullable: false, maxLength: 36),
                        LeftAt = c.DateTime(storeType: "date"),
                        DocId = c.String(maxLength: 36),
                        UserId = c.Int(),
                        Sync1CAt = c.DateTime(precision: 7, storeType: "datetime2"),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.VehicleTypes", t => t.VehicleTypeId, cascadeDelete: true)
                .Index(t => t.VehicleTypeId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.VehicleTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Class = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DisplayName = c.String(nullable: false, maxLength: 255),
                        Code1C = c.String(nullable: false, maxLength: 36),
                        Type1C = c.String(maxLength: 36),
                        CodeParent1C = c.String(maxLength: 36),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code1C, unique: true);
            
            CreateTable(
                "dbo.TaskExecs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskId = c.Int(nullable: false),
                        Start = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        End = c.DateTime(precision: 7, storeType: "datetime2"),
                        Status = c.Int(nullable: false),
                        Comment = c.String(maxLength: 2000),
                        UserId = c.Int(),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.TaskId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.TaskId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocId = c.String(maxLength: 36),
                        Type = c.Int(nullable: false),
                        NomenclatureId = c.Int(),
                        SourceLotId = c.Int(),
                        SourceUnitId = c.Int(),
                        DestLotId = c.Int(),
                        DestUnitId = c.Int(),
                        OrganizationId = c.Int(),
                        VehicleId = c.Int(),
                        IsDrying = c.Boolean(nullable: false),
                        IsScalping = c.Boolean(nullable: false),
                        IsSeparating = c.Boolean(nullable: false),
                        ExecuteAt = c.DateTime(storeType: "date"),
                        Quantity = c.Decimal(nullable: false, precision: 8, scale: 0),
                        Status = c.Int(nullable: false),
                        Comment = c.String(maxLength: 2000),
                        UserId = c.Int(),
                        Sync1CAt = c.DateTime(precision: 7, storeType: "datetime2"),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lots", t => t.DestLotId)
                .ForeignKey("dbo.Units", t => t.DestUnitId)
                .ForeignKey("dbo.Nomenclatures", t => t.NomenclatureId)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId)
                .ForeignKey("dbo.Lots", t => t.SourceLotId)
                .ForeignKey("dbo.Units", t => t.SourceUnitId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Vehicles", t => t.VehicleId)
                .Index(t => t.NomenclatureId)
                .Index(t => t.SourceLotId)
                .Index(t => t.SourceUnitId)
                .Index(t => t.DestLotId)
                .Index(t => t.DestUnitId)
                .Index(t => t.OrganizationId)
                .Index(t => t.VehicleId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UnitLayers",
                c => new
                    {
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UnitId = c.Int(nullable: false),
                        LotId = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 8, scale: 0),
                    })
                .PrimaryKey(t => new { t.CreatedAt, t.UnitId })
                .ForeignKey("dbo.Lots", t => t.LotId, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .Index(t => t.UnitId)
                .Index(t => t.LotId);
            
            CreateTable(
                "dbo.UnitSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UnitId = c.Int(nullable: false),
                        NomenclatureId = c.Int(),
                        UpdatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nomenclatures", t => t.NomenclatureId)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .Index(t => new { t.UnitId, t.NomenclatureId }, unique: true, name: "IX_InitId_NomenclatureId");
            
            CreateTable(
                "dbo.VehicleActivities",
                c => new
                    {
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        VehicleId = c.Int(nullable: false),
                        Action = c.Int(nullable: false),
                        NomenclatureId = c.Int(),
                        UnitId = c.Int(),
                        Weight = c.Decimal(precision: 8, scale: 0),
                        Comment = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => new { t.CreatedAt, t.VehicleId })
                .ForeignKey("dbo.Nomenclatures", t => t.NomenclatureId)
                .ForeignKey("dbo.Units", t => t.UnitId)
                .ForeignKey("dbo.Vehicles", t => t.VehicleId, cascadeDelete: true)
                .Index(t => t.VehicleId)
                .Index(t => t.NomenclatureId)
                .Index(t => t.UnitId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VehicleActivities", "VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.VehicleActivities", "UnitId", "dbo.Units");
            DropForeignKey("dbo.VehicleActivities", "NomenclatureId", "dbo.Nomenclatures");
            DropForeignKey("dbo.UnitSettings", "UnitId", "dbo.Units");
            DropForeignKey("dbo.UnitSettings", "NomenclatureId", "dbo.Nomenclatures");
            DropForeignKey("dbo.UnitLayers", "UnitId", "dbo.Units");
            DropForeignKey("dbo.UnitLayers", "LotId", "dbo.Lots");
            DropForeignKey("dbo.TaskExecMoves", "TaskExecId", "dbo.TaskExecs");
            DropForeignKey("dbo.TaskExecs", "UserId", "dbo.Users");
            DropForeignKey("dbo.TaskExecs", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.Tasks", "VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.Tasks", "UserId", "dbo.Users");
            DropForeignKey("dbo.Tasks", "SourceUnitId", "dbo.Units");
            DropForeignKey("dbo.Tasks", "SourceLotId", "dbo.Lots");
            DropForeignKey("dbo.Tasks", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.Tasks", "NomenclatureId", "dbo.Nomenclatures");
            DropForeignKey("dbo.Tasks", "DestUnitId", "dbo.Units");
            DropForeignKey("dbo.Tasks", "DestLotId", "dbo.Lots");
            DropForeignKey("dbo.TaskExecMoves", "SourceVehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.TaskExecMoves", "SourceUnitId", "dbo.Units");
            DropForeignKey("dbo.TaskExecMoves", "LotId", "dbo.Lots");
            DropForeignKey("dbo.TaskExecMoves", "DestVehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.Vehicles", "VehicleTypeId", "dbo.VehicleTypes");
            DropForeignKey("dbo.Vehicles", "UserId", "dbo.Users");
            DropForeignKey("dbo.TaskExecMoves", "DestUnitId", "dbo.Units");
            DropForeignKey("dbo.NomenclaturePriorities", "UserId", "dbo.Users");
            DropForeignKey("dbo.NomenclaturePriorities", "NomenclatureId", "dbo.Nomenclatures");
            DropForeignKey("dbo.Lots", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.Lots", "NomenclatureId", "dbo.Nomenclatures");
            DropIndex("dbo.VehicleActivities", new[] { "UnitId" });
            DropIndex("dbo.VehicleActivities", new[] { "NomenclatureId" });
            DropIndex("dbo.VehicleActivities", new[] { "VehicleId" });
            DropIndex("dbo.UnitSettings", "IX_InitId_NomenclatureId");
            DropIndex("dbo.UnitLayers", new[] { "LotId" });
            DropIndex("dbo.UnitLayers", new[] { "UnitId" });
            DropIndex("dbo.Tasks", new[] { "UserId" });
            DropIndex("dbo.Tasks", new[] { "VehicleId" });
            DropIndex("dbo.Tasks", new[] { "OrganizationId" });
            DropIndex("dbo.Tasks", new[] { "DestUnitId" });
            DropIndex("dbo.Tasks", new[] { "DestLotId" });
            DropIndex("dbo.Tasks", new[] { "SourceUnitId" });
            DropIndex("dbo.Tasks", new[] { "SourceLotId" });
            DropIndex("dbo.Tasks", new[] { "NomenclatureId" });
            DropIndex("dbo.TaskExecs", new[] { "UserId" });
            DropIndex("dbo.TaskExecs", new[] { "TaskId" });
            DropIndex("dbo.VehicleTypes", new[] { "Code1C" });
            DropIndex("dbo.Vehicles", new[] { "UserId" });
            DropIndex("dbo.Vehicles", new[] { "VehicleTypeId" });
            DropIndex("dbo.Units", new[] { "Code1C" });
            DropIndex("dbo.TaskExecMoves", new[] { "DestVehicleId" });
            DropIndex("dbo.TaskExecMoves", new[] { "DestUnitId" });
            DropIndex("dbo.TaskExecMoves", new[] { "SourceVehicleId" });
            DropIndex("dbo.TaskExecMoves", new[] { "SourceUnitId" });
            DropIndex("dbo.TaskExecMoves", new[] { "LotId" });
            DropIndex("dbo.TaskExecMoves", new[] { "TaskExecId" });
            DropIndex("dbo.NomenclaturePriorities", new[] { "UserId" });
            DropIndex("dbo.NomenclaturePriorities", new[] { "NomenclatureId" });
            DropIndex("dbo.Organizations", new[] { "Code1C" });
            DropIndex("dbo.Nomenclatures", new[] { "Code1C" });
            DropIndex("dbo.Lots", new[] { "OrganizationId" });
            DropIndex("dbo.Lots", new[] { "NomenclatureId" });
            DropIndex("dbo.Lots", new[] { "LotNo" });
            DropIndex("dbo.Contragents", new[] { "Code1C" });
            DropTable("dbo.VehicleActivities");
            DropTable("dbo.UnitSettings");
            DropTable("dbo.UnitLayers");
            DropTable("dbo.Tasks");
            DropTable("dbo.TaskExecs");
            DropTable("dbo.VehicleTypes");
            DropTable("dbo.Vehicles");
            DropTable("dbo.Units");
            DropTable("dbo.TaskExecMoves");
            DropTable("dbo.Users");
            DropTable("dbo.NomenclaturePriorities");
            DropTable("dbo.Organizations");
            DropTable("dbo.Nomenclatures");
            DropTable("dbo.Lots");
            DropTable("dbo.LogEvent1C");
            DropTable("dbo.Contragents");
        }
    }
}
