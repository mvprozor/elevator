﻿namespace ElevatorMES.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Equipments", "UnitId", "dbo.Units");
            DropIndex("dbo.Equipments", new[] { "UnitId" });
            CreateTable(
                "dbo.UnitEquipmentLinks",
                c => new
                    {
                        UnitId = c.Int(nullable: false),
                        EquipmentId = c.Int(nullable: false),
                        LinkType = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => new { t.UnitId, t.EquipmentId })
                .ForeignKey("dbo.Equipments", t => t.EquipmentId, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .Index(t => t.UnitId)
                .Index(t => t.EquipmentId);
            
            DropColumn("dbo.Equipments", "UnitId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Equipments", "UnitId", c => c.Int());
            DropForeignKey("dbo.UnitEquipmentLinks", "UnitId", "dbo.Units");
            DropForeignKey("dbo.UnitEquipmentLinks", "EquipmentId", "dbo.Equipments");
            DropIndex("dbo.UnitEquipmentLinks", new[] { "EquipmentId" });
            DropIndex("dbo.UnitEquipmentLinks", new[] { "UnitId" });
            DropTable("dbo.UnitEquipmentLinks");
            CreateIndex("dbo.Equipments", "UnitId");
            AddForeignKey("dbo.Equipments", "UnitId", "dbo.Units", "Id");
        }
    }
}
