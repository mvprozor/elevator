USE [ElevatorMES]
GO
INSERT INTO [dbo].[Units]([Type],[Capacity],[Deleted],[DisplayName],[Code1C],[Type1C],[CodeParent1C],[CreatedAt]) VALUES
(5, 100.0, 0, 'Бункер 4.1.1', '411', NULL, NULL, GETDATE())
,(5, 100.0, 0, 'Бункер 4.2.1', '421', NULL, NULL, GETDATE())
,(5, 100.0, 0, 'Бункер 4.3.1', '431', NULL, NULL, GETDATE())
,(5, 100.0, 0, 'Бункер 3.1.1', '311', NULL, NULL, GETDATE())
,(5, 100.0, 0, 'Бункер 3.2.1', '321', NULL, NULL, GETDATE())
,(5, 100.0, 0, 'Бункер 3.3.1', '331', NULL, NULL, GETDATE())
,(5, 100.0, 0, 'Бункер 3.4.1', '341', NULL, NULL, GETDATE())
,(5, 100.0, 0, 'Бункер 3.5.1', '351', NULL, NULL, GETDATE())
,(5, 100.0, 0, 'Бункер 3.6.1', '361', NULL, NULL, GETDATE())
,(7, 100.0, 0, 'Сушилка 6.1.1', '611', NULL, NULL, GETDATE())
,(7, 100.0, 0, 'Сушилка 6.2.1', '621', NULL, NULL, GETDATE())
,(7, 100.0, 0, 'Сушилка 6.3.1', '631', NULL, NULL, GETDATE())



