﻿namespace ElevatorMES.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.TaskExecMoves", "LotId", "dbo.Lots");
            //DropIndex("dbo.TaskExecMoves", new[] { "LotId" });
            CreateTable(
                "dbo.Equipments",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    EquipmentType = c.Int(nullable: false),
                    SysName = c.String(nullable: false, maxLength: 128),
                    DisplayName = c.String(nullable: false, maxLength: 128),
                    CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                })
                .PrimaryKey(t => t.Id);

            //CreateTable(
            //    "dbo.TransmitLogs",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            xMessage = c.String(),
            //            MessageType = c.Int(nullable: false),
            //            MessageTypeRus = c.String(),
            //            Result = c.String(),
            //            IsSuccess = c.Boolean(),
            //            CountError = c.Int(),
            //            DateTransmit = c.DateTime(precision: 7, storeType: "datetime2"),
            //            LastTryDate = c.DateTime(precision: 7, storeType: "datetime2"),
            //            NextTryDate = c.DateTime(precision: 7, storeType: "datetime2"),
            //            ErrorType = c.Int(),
            //            CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
            //        })
            //    .PrimaryKey(t => t.Id);

            //AddColumn("dbo.Tasks", "ContragentId", c => c.Int());
            //AlterColumn("dbo.TaskExecMoves", "LotId", c => c.Int());
            //CreateIndex("dbo.TaskExecMoves", "LotId");
            //CreateIndex("dbo.Tasks", "ContragentId");
            //AddForeignKey("dbo.Tasks", "ContragentId", "dbo.Contragents", "Id");
            //AddForeignKey("dbo.TaskExecMoves", "LotId", "dbo.Lots", "Id");
        }

        public override void Down()
        {
            //DropForeignKey("dbo.TaskExecMoves", "LotId", "dbo.Lots");
            //DropForeignKey("dbo.Tasks", "ContragentId", "dbo.Contragents");
            //DropForeignKey("dbo.Equipments", "UnitId", "dbo.Units");
            //DropIndex("dbo.Tasks", new[] { "ContragentId" });
            //DropIndex("dbo.TaskExecMoves", new[] { "LotId" });
            //DropIndex("dbo.Equipments", new[] { "UnitId" });
            //AlterColumn("dbo.TaskExecMoves", "LotId", c => c.Int(nullable: false));
            //DropColumn("dbo.Tasks", "ContragentId");
            //DropTable("dbo.TransmitLogs");
            DropTable("dbo.Equipments");
            //CreateIndex("dbo.TaskExecMoves", "LotId");
            //AddForeignKey("dbo.TaskExecMoves", "LotId", "dbo.Lots", "Id", cascadeDelete: true);
        }
    }
}
