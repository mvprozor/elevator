﻿# Если из оболочки VisualStudio, то так:
# 1. Запустить PS от администратора!
# 2. Set-ExecutionPolicy Unrestricted -force
# 3. cd f:\0work\Elevator\ElevatorMES\ElevatorMES.Data\SQLScripts

$strFileName = "script.sql"
If (Test-Path $strFileName){
	Remove-Item $strFileName
}
Get-ChildItem -include *.sql -exclude $strFileName -rec | sort $_.CreationTime | ForEach-Object {gc $_; ""}  |  out-file $strFileName

<#
    Get-Content ..\Tables\*.sql | Add-Content $strFileName
    Get-Content .\*.sql | Add-Content $strFileName
#>

