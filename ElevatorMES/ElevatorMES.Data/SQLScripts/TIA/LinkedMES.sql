﻿USE RUSAGRO
IF NOT EXISTS(SELECT TOP 1 1 FROM sys.servers WHERE [name] = N'192.168.0.58')
BEGIN
	EXEC master.dbo.sp_addlinkedserver @server = N'192.168.0.58', @srvproduct=N'SQL Server'
	EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'192.168.0.58',@useself=N'False',@locallogin=NULL,@rmtuser=N'RealLinked',@rmtpassword='Qazwsx123-+!'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'collation compatible', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'data access', @optvalue=N'true'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'dist', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'pub', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'rpc', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'rpc out', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'sub', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'connect timeout', @optvalue=N'0'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'collation name', @optvalue=null
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'lazy schema validation', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'query timeout', @optvalue=N'0'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'use remote collation', @optvalue=N'true'
	EXEC master.dbo.sp_serveroption @server=N'192.168.0.58', @optname=N'remote proc transaction promotion', @optvalue=N'true'
END

IF OBJECT_ID('MESSilosStates','v') IS NOT NULL
	DROP VIEW MESSilosStates;
GO

CREATE VIEW MESSilosStates
AS
SELECT 
	un.Id,
	un.Code1C,
	un.DisplayName,
	us.NomenclatureId,
	nm.Code1C NomenclatureCode1C,
	nm.DisplayName NomenclatureDisplayName,
	un.Capacity,
	ISNULL(uq.Quantity, 0.0) Quantity,
	CAST(ROUND(ISNULL(uq.Quantity*100.000/NULLIF(un.Capacity,0.0), 0.0),3) AS DECIMAL(18,3)) FillPercent
FROM [192.168.0.58].ElevatorMES.dbo.Units un
LEFT JOIN [192.168.0.58].ElevatorMES.dbo.UnitSettings us ON us.UnitId=un.Id
LEFT JOIN [192.168.0.58].ElevatorMES.dbo.Nomenclatures nm ON nm.Id=us.NomenclatureId
LEFT JOIN
(
	SELECT 
		ul.UnitId,
		SUM(Quantity) Quantity
	FROM [192.168.0.58].ElevatorMES.dbo.UnitLayers ul
	GROUP BY ul.UnitId
) uq ON uq.UnitId = un.Id
WHERE un.Type=1
