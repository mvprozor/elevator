﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetSiloHistoryReport') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetSiloHistoryReport
GO

CREATE PROCEDURE dbo.GetSiloHistoryReport
(
	@StateAt		DATETIME = NULL
)
AS
BEGIN
	SET @StateAt = ISNULL(@StateAt, GETDATE())

	SELECT
		u.Id UnitId,
		u.DisplayName UnitDisplayName,
		nm.Id NomenclatureId,
		nm.DisplayName NomenclatureDisplayName,
		inv.CreatedAt InvAt,
		inv.UserDisplayName InvUser,
		inv.Comment InvComment,
		inv.Quantity InvQuantity,
		inc.Quantity Income,
		outc.Quantity Outcome,
		ISNULL(inv.Quantity,0.0)+ISNULL(inc.Quantity,0.0)-ISNULL(outc.Quantity,0.0) Quantity
	FROM dbo.Units u
	LEFT JOIN dbo.UnitSettings us ON us.UnitId=u.Id
	OUTER APPLY
	(
		SELECT TOP 1 
			uinv.NomenclatureId,
			uinv.UserId,
			uinv.CreatedAt,
			uinv.Comment,
			uinv.Quantity,
			usr.DisplayName UserDisplayName
		FROM dbo.UnitInventories uinv
		LEFT JOIN dbo.Users usr ON usr.Id=uinv.UserId
		WHERE uinv.UnitId=u.Id AND uinv.CreatedAt<=@StateAt
		ORDER BY uinv.CreatedAt DESC
	) inv
	OUTER APPLY
	(
		SELECT 
			SUM(m.Quantity) Quantity
		FROM dbo.TaskExecMoves m 
		WHERE m.DestUnitId=u.Id AND m.CreatedAt<=@StateAt 
			AND (inv.CreatedAt IS NULL OR m.CreatedAt>=inv.CreatedAt) 
	) inc
	OUTER APPLY
	(
		SELECT 
			SUM(m.Quantity) Quantity
		FROM dbo.TaskExecMoves m 
		WHERE m.SourceUnitId=u.Id AND m.CreatedAt<=@StateAt 
			AND (inv.CreatedAt IS NULL OR m.CreatedAt>=inv.CreatedAt) 
	) outc
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id=ISNULL(inv.NomenclatureId, us.NomenclatureId)
	WHERE (u.Type & 1)=1 AND u.Deleted=0
	ORDER BY u.DisplayName

END
GO
GRANT EXECUTE ON dbo.GetSiloHistoryReport TO [RealOperatorRole]
GRANT EXECUTE ON dbo.GetSiloHistoryReport TO [RealReportReader]
GO