﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetVehicleReport') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetVehicleReport
GO

CREATE PROCEDURE dbo.GetVehicleReport
(
	@Begin				DATETIME = NULL,
	@End				DATETIME = NULL,
	@RegNum				NVARCHAR(16) = NULL,
	@IsWithin			BIT = 1
)
AS
BEGIN
	SELECT
		v.Id,
		v.CreatedAt,
		v.DocId,
		v.RegNum,
		v.VehicleTypeId,
		vt.DisplayName VehicleTypeName,
		vt.Class VehicleClass,
		STUFF((SELECT 
			' / - '
			+CASE va.Action
				WHEN 1 THEN 'Зарегистрировано'
				WHEN 2 THEN 'Взвешено'
				WHEN 3 THEN 'Выгружено'
				WHEN 4 THEN 'Погружено'
				WHEN 5 THEN 'Покинуло'
				ELSE CAST(va.Action AS NVARCHAR(12))
			END
			+' '
			+FORMAT(va.CreatedAt,'dd MMM HH:mm', 'ru-RU')
			+ISNULL(', '+FORMAT(va.Weight,'0.0', 'ru-RU')+' т.', '')
			+ISNULL(', '+nl.DisplayName, '')
			+ISNULL(', '+un.DisplayName, '')
		FROM dbo.VehicleActivities va
		LEFT JOIN dbo.Nomenclatures nl ON nl.Id=va.NomenclatureId
		LEFT JOIN dbo.Units un ON un.Id=va.UnitId
		WHERE va.VehicleId=v.Id
		ORDER BY va.CreatedAt ASC
		FOR XML PATH(''))
		,1,3,'') Activities,
		CAST(IIF((SELECT TOP 1 rawr.NomenclatureId FROM dbo.VehicleActivities rawr
		WHERE rawr.VehicleId=v.Id AND rawr.Action=1 ORDER BY rawr.CreatedAt DESC) IS NULL, 0, 1) AS BIT) IsWithRaw
	FROM dbo.Vehicles v
	INNER JOIN dbo.VehicleTypes vt ON vt.Id=v.VehicleTypeId
	WHERE (@Begin IS NULL OR v.CreatedAt>=@Begin)
		AND (@End IS NULL OR v.CreatedAt<=@End)
		AND (@RegNum IS NULL OR v.RegNum LIKE '%'+@RegNum+'%')
		AND (ISNULL(@IsWithin,0)=0 OR v.LeftAt IS NULL)
END
GO
GRANT EXECUTE ON dbo.GetVehicleReport TO [RealReportReader]
GO