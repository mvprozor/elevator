﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetSiloStateReport') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetSiloStateReport
GO

CREATE PROCEDURE dbo.GetSiloStateReport
(
	@SiloId				INT
)
AS
BEGIN
	SELECT
		ul.CreatedAt,
		ul.LotId,
		ul.Quantity,
		l.LotNo,
		l.NomenclatureId,
		nc.DisplayName NomenclatureName,
		l.OrganizationId,
		org.DisplayName OrganizationName
	FROM dbo.UnitLayers ul
	INNER JOIn dbo.Lots l ON l.Id=ul.LotId
	INNER JOIN dbo.Nomenclatures nc ON nc.Id=l.NomenclatureId
	INNER JOIN dbo.Organizations org ON org.Id=l.OrganizationId
	WHERE ul.UnitId = @SiloId
	ORDER BY ul.CreatedAt DESC
END
GO
GRANT EXECUTE ON dbo.GetSiloStateReport TO [RealReportReader]
GRANT EXECUTE ON dbo.GetSiloStateReport TO [RealOperatorRole]
GO