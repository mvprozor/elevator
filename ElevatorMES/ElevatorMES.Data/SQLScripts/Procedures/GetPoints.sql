﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetPoints') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetPoints
GO

CREATE PROCEDURE dbo.GetPoints
AS
BEGIN
	SELECT
		u.Id UnitId,
		u.DisplayName UnitDisplayName,
		u.Type UnitType,
		srceq.*,
		dsteq.*
	FROM dbo.Units u
	LEFT JOIN dbo.UnitSettings us ON us.UnitId=u.Id
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id=us.NomenclatureId
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType SourceEquipmentType,
			eq.DisplayName SourceEquipmentName,
			eq.Id SourceEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=u.Id AND uel.LinkType & 2 = 2
		ORDER BY eq.Id
	) srceq
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType DestinationEquipmentType,
			eq.DisplayName DestinationEquipmentName,
			eq.Id DestinationEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=u.Id AND uel.LinkType & 4 = 4
		ORDER BY eq.Id
	) dsteq
	WHERE u.Type IN (48, 80, 128) AND u.Deleted=0
	ORDER BY u.DisplayName
END
GO
GRANT EXECUTE ON dbo.GetPoints TO [RealReportReader]
GRANT EXECUTE ON dbo.GetPoints TO [RealOperatorRole]
GO