﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetEquipmentStats') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetEquipmentStats
GO

CREATE PROCEDURE dbo.GetEquipmentStats
(
	@EquipmentId		NVARCHAR(255) = NULL,
	@Begin				DATETIME = NULL,
	@End				DATETIME = NULL
)
AS
BEGIN
	DECLARE @xEquipmentId XML
	IF @EquipmentId IS NOT NULL
		SET @xEquipmentId = CONVERT(xml,'<i>' + REPLACE(@EquipmentId,',','</i><i>') + '</i>');
	
	SET @End = ISNULL(@End, GETDATE())
	SET @Begin = ISNULL(@Begin, CAST(@End AS DATE))

	SELECT
		src.EquipmentId,
		eq.DisplayName EquipmentName,
		src.RunFrom,
		src.RunTo,
		DATEDIFF(SECOND, src.RunFrom, src.RunTo) RunSeconds,
		CAST(src.FullPowerConsumption*DATEDIFF(SECOND, src.RunFrom, src.RunTo)/NULLIF(src.FullRunSeconds,0) AS DECIMAL(18,3)) PowerConsumption,
		t.Type TaskType,
		t.IsDrying,
		t.IsScalping,
		t.IsSeparating,
		nm.DisplayName NomenclatureName,
		(
			SELECT	
				SUM(tem.Quantity)
			FROM dbo.TaskExecs te
			INNER JOIN dbo.TaskExecMoves tem ON tem.TaskExecId = te.Id 
			WHERE te.TaskId=t.Id
		) QuantityActual
	FROM
	(
	SELECT
		IIF(DATEADD(SECOND, -mh.MotorHours, mh.DateTime)<@Begin, @Begin, DATEADD(SECOND, -mh.MotorHours, mh.DateTime)) RunFrom,
		IIF(mh.DateTime>@End, @End, DateTime) RunTo,
		mh.DevId EquipmentId,
		mh.TaskId,
		mh.MotorHours FullRunSeconds,
		mh.Power FullPowerConsumption
	FROM [TIA].RUSAGRO.dbo.MH mh
	WHERE (@EquipmentId IS NULL OR mh.DevId IN (SELECT T.c.value('.','nvarchar(300)') FROM  @xEquipmentId.nodes('/i') T(c)))
		AND mh.DateTime>@Begin AND DATEADD(SECOND, -mh.MotorHours, mh.DateTime)<@End
	) src
	LEFT JOIN dbo.Equipments eq ON eq.Id=src.EquipmentId
	LEFT JOIN dbo.Tasks t ON t.Id=src.TaskId
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id=t.NomenclatureId
	ORDER BY eq.DisplayName, src.RunFrom

END
GO
GRANT EXECUTE ON dbo.GetEquipmentStats TO [RealReportReader]
GRANT EXECUTE ON dbo.GetEquipmentStats TO [RealOperatorRole]
GO

