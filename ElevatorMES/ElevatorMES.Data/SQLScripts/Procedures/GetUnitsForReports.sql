﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetUnitsForReports') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetUnitsForReports
GO

CREATE PROCEDURE dbo.GetUnitsForReports
(
	@Type	INT = NULL
)
AS
BEGIN
	SELECT 
		u.Id,
		u.Type,
		u.Code1C,
		u.DisplayName,
		u.CreatedAt,
		u.Capacity
	FROM dbo.Units u
	WHERE @Type IS NULL OR u.Type=@Type
END
GO
GRANT EXECUTE ON dbo.GetUnitsForReports TO [RealReportReader]
GO