﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetEquipments') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetEquipments
GO

CREATE PROCEDURE dbo.GetEquipments
(
	@EquipmentName		NVARCHAR(255) = NULL
)
AS
BEGIN
	SELECT
	*
	FROM dbo.Equipments eq
	WHERE @EquipmentName IS NULL OR eq.DisplayName LIKE '%'+@EquipmentName+'%'
	ORDER BY eq.DisplayName
END
GO
GRANT EXECUTE ON dbo.GetEquipments TO [RealReportReader]
GRANT EXECUTE ON dbo.GetEquipments TO [RealOperatorRole]
GO