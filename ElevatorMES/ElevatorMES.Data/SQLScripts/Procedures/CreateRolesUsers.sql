﻿USE [ElevatorMES]
IF DATABASE_PRINCIPAL_ID('RealReportReader') IS NULL
	CREATE ROLE RealReportReader
GO

IF DATABASE_PRINCIPAL_ID('RealOperatorRole') IS NULL
	CREATE ROLE RealOperatorRole
	GRANT SELECT ON SCHEMA :: [dbo] TO RealReportReader
GO

IF DATABASE_PRINCIPAL_ID('RealReport') IS NULL
BEGIN
	CREATE LOGIN [RealReport] WITH PASSWORD=N'Qazwsx123++', DEFAULT_DATABASE=[ElevatorMES], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
	CREATE USER [RealReport] FOR LOGIN [RealReport]
END
GO
ALTER ROLE [RealReportReader] ADD MEMBER [RealReport]
GO

IF DATABASE_PRINCIPAL_ID('RealOperator') IS NULL
BEGIN
	CREATE LOGIN [RealOperator] WITH PASSWORD=N'Qazwsx123--', DEFAULT_DATABASE=[ElevatorMES], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
	CREATE USER [RealOperator] FOR LOGIN [RealOperator]
END
GO
ALTER ROLE [RealOperatorRole] ADD MEMBER [RealOperator]
GO

IF DATABASE_PRINCIPAL_ID('RealLinked') IS NULL
BEGIN
	CREATE LOGIN [RealLinked] WITH PASSWORD=N'Qazwsx123-+!', DEFAULT_DATABASE=[ElevatorMES], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
	CREATE USER [RealLinked] FOR LOGIN [RealLinked]
	ALTER ROLE [db_datareader] ADD MEMBER [RealLinked]
END
GO