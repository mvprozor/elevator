﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetTask') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetTask
GO

CREATE PROCEDURE dbo.GetTask
(
	@TaskId				INT = NULL,
	@TaskStatus			INT = 0, -- 0 - все, 1 - в очереди,  2 - активные, 3 - история
	@Begin				DATETIME = NULL,
	@End				DATETIME = NULL,
	@TextFilter			NVARCHAR(300) = NULL
)
AS
BEGIN
	SET @TaskId = NULLIF(@TaskId,0)

	SELECT * FROM
	(SELECT 
		t.Id ,
		t.DocId ,
		t.[Type] ,
		t.CreatedAt,
		t.NomenclatureId ,
			nm.DisplayName AS NomenclatureDisplayName,
			nm.Code1C AS NomenclatureCode1C,
		np.Priority NomenclaturePriority,
		t.SourceLotId ,
			sl.LotNo AS SourceLotNo,		
		t.SourceUnitId,
		su.DisplayName SourceUnitName,
		t.DestUnitId,
		du.DisplayName DestUnitName,
		t.ExternalUnitId,
		eu.DisplayName ExternalUnitName,
		t.VehicleId,
		ISNULL(vh.RegNum, '') + ISNULL(', '+org.DisplayName, '') + ISNULL(', '+ctr.DisplayName, '') + ISNULL(', '+eu.DisplayName,'') SuppCons,
		t.IsDrying ,
		t.IsScalping ,
		t.IsSeparating ,
		t.IsQualityControl,
		t.ExecuteAt ,
		t.[Status] , -- { Queued = 1, Deleted = 2, Running = 3, Completed = 4, Failed = 5, Pending = 6, Suspended = 7 }
		t.Comment ,
		t.UserId ,
			us.DisplayName AS [UserDisplayName],
		lus.Id LastUserId,
		lus.DisplayName LastUserName,
		te.StartedAt,
		te.EndAt,
		t.Quantity AS [QuantityPlanned],
		ISNULL(te.QuantityActual, 0.0) AS QuantityActual,

		vh.RegNum,
		su.Code1C as SourceUnitCode1C,
		du.Code1C as DestUnitCode1C,
		ctr.Code1C as ContragentCode1C,
		org.Code1C as OrganizationCode1C,

		srceq.*,
		dsteq.*,

		asurep.*

		/*STUFF((
			SELECT
				', '+ISNULL(eq.DisplayName, CAST(src.DevId AS NVARCHAR(12)))
			FROM
			(
				SELECT DISTINCT mh.DevId
				FROM [TIA].RUSAGRO.dbo.MH mh
				WHERE mh.TaskId=t.Id
			) src
			LEFT JOIN dbo.Equipments eq ON eq.Id=src.DevId
			ORDER BY src.DevId
			FOR XML PATH('')), 1, 2, '') Equipments*/
	FROM dbo.Tasks t
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id = t.NomenclatureId
	LEFT JOIN dbo.NomenclaturePriorities np ON np.NomenclatureId=t.NomenclatureId
	LEFT JOIN dbo.Lots sl ON sl.Id = t.SourceLotId
	LEFT JOIN dbo.Lots dl ON dl.Id = t.DestLotId
	LEFT JOIN dbo.Vehicles vh ON vh.Id = t.VehicleId
	LEFT JOIN dbo.Organizations org ON org.Id = t.OrganizationId
	LEFT JOIN dbo.Contragents ctr ON ctr.Id = t.ContragentId
	LEFT JOIN dbo.Units su ON su.Id = t.SourceUnitId
	LEFT JOIN dbo.Units du ON du.Id = t.DestUnitId
	LEFT JOIN dbo.Units eu ON eu.Id = t.ExternalUnitId
	LEFT JOIN dbo.Users us ON us.Id = t.UserId
	OUTER APPLY (
		SELECT 
			MIN(te.[Start]) AS StartedAt,
			MAX(ISNULL(te.[End], GETDATE())) AS EndAt,
			SUM(tem.Quantity) AS QuantityActual,
			MAX(te.Id) AS MaxExecId
		FROM dbo.TaskExecs te
		LEFT JOIN dbo.TaskExecMoves tem ON tem.TaskExecId = te.Id 
		GROUP BY te.TaskId
		HAVING te.TaskId = t.Id
	) te
	LEFT JOIN dbo.TaskExecs tel ON tel.Id=te.MaxExecId
	LEFT JOIN dbo.Users lus ON lus.Id=tel.UserId
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType SourceEquipmentType,
			eq.DisplayName SourceEquipmentName,
			eq.Id SourceEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=t.SourceUnitId AND uel.LinkType & 2 = 2
		ORDER BY eq.Id
	) srceq
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType DestinationEquipmentType,
			eq.DisplayName DestinationEquipmentName,
			eq.Id DestinationEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=t.DestUnitId AND uel.LinkType & 4 = 4
		ORDER BY eq.Id
	) dsteq
	OUTER APPLY
	(
		SELECT 
			MAX(rep.AllDevs) Equipments,
			SUM(rep.[Power]) [Power]
		FROM [TIA].RUSAGRO.dbo.REPORT rep 
		WHERE rep.TaskId=t.Id
	) asurep
	WHERE 
		(@TaskId IS NULL OR t.Id = @TaskId)
		AND ( @TaskStatus = 0 -- все, 
			OR @TaskStatus = 1 AND t.[Status] IN (1) -- в очереди 
			OR @TaskStatus = 2 AND t.[Status] IN (3, 6, 7) -- активные  
			OR @TaskStatus = 3 AND t.[Status] IN (2, 4, 5) -- история
		)
		AND (t.ExecuteAt >= ISNULL(@begin,'19171107'))
		AND (t.ExecuteAt <= ISNULL(@end,'26661231'))) t 
	WHERE @TextFilter IS NULL 
	OR EXISTS -- Фильтр @TextFilter по всем полям
		(SELECT * FROM
			( SELECT t.* FOR XML PATH, TYPE) X(C) -- Turn all columns into XML
				CROSS APPLY
				X.C.nodes('//*/.') E(V) -- Get all XML elements
				WHERE E.V.value('./text()[1]', 'VARCHAR(MAX)') LIKE '%' + @TextFilter + '%' -- Filter on value of the XML element
			)
			
	END
GO
GRANT EXECUTE ON dbo.GetTask TO [RealReportReader]
GRANT EXECUTE ON dbo.GetTask TO [RealOperatorRole]
GO
GO