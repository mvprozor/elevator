﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetTaskForMatching') AND o.[Type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetTaskForMatching
GO

CREATE PROCEDURE dbo.GetTaskForMatching
(
	@TaskId				INT = NULL,
	@DocId				NVARCHAR(36) = NULL,
	@IsFromERP			BIT = NULL,
	@TaskGenStatus		INT = 0, -- 0 - все, 1 - в очереди,  2 - активные, 3 - история
	@TaskType			INT = NULL,
	@NomenclatureId		INT = NULL,
	@TextFilter			NVARCHAR(255) = NULL,
	@TaskCount			INT = NULL
)
AS
BEGIN
	SET @TaskCount = ISNULL(@TaskCount, 10)

	SELECT TOP (@TaskCount)
		* 
	FROM (SELECT 
		t.Id ,
		t.DocId ,
		t.[Type] ,
		t.CreatedAt,
		t.NomenclatureId ,
		nm.DisplayName AS NomenclatureDisplayName,
		t.SourceLotId ,
		sl.LotNo AS SourceLotNo,		
		t.SourceUnitId,
		su.DisplayName SourceUnitName,
		t.DestUnitId,
		du.DisplayName DestUnitName,
		t.VehicleId,
		ISNULL(vh.RegNum, '') + ISNULL(', '+org.DisplayName, '') + ISNULL(', '+ctr.DisplayName, '') SuppCons,
		t.IsDrying ,
		t.IsScalping ,
		t.IsSeparating ,
		t.ExecuteAt ,
		t.[Status] , -- { Queued = 1, Deleted = 2, Running = 3, Completed = 4, Failed = 5, Pending = 6, Suspended = 7 }
		t.Comment ,
		t.UserId ,
			us.DisplayName AS [UserDisplayName],
		te.StartedAt,
		te.EndAt,
		t.Quantity AS [QuantityPlanned],
		ISNULL(te.QuantityActual, 0.0) AS QuantityActual

	FROM dbo.Tasks t
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id = t.NomenclatureId
	LEFT JOIN dbo.NomenclaturePriorities np ON np.NomenclatureId=t.NomenclatureId
	LEFT JOIN dbo.Lots sl ON sl.Id = t.SourceLotId
	LEFT JOIN dbo.Lots dl ON dl.Id = t.DestLotId
	LEFT JOIN dbo.Vehicles vh ON vh.Id = t.VehicleId
	LEFT JOIN dbo.Organizations org ON org.Id = t.OrganizationId
	LEFT JOIN dbo.Contragents ctr ON ctr.Id = t.ContragentId
	LEFT JOIN dbo.Units su ON su.Id = t.SourceUnitId
	LEFT JOIN dbo.Units du ON du.Id = t.DestUnitId
	LEFT JOIN dbo.Users us ON us.Id = t.UserId
	OUTER APPLY (
		SELECT 
			MIN(te.[Start]) AS StartedAt,
			MAX(te.[End]) AS EndAt,
			SUM(tem.Quantity) AS QuantityActual
		FROM dbo.TaskExecs te
		INNER JOIN dbo.TaskExecMoves tem ON tem.TaskExecId = te.Id 
		GROUP BY te.TaskId
		HAVING te.TaskId = t.Id
	) te
	WHERE 
		(@TaskId IS NULL OR t.Id = @TaskId)
		AND (@DocId IS NULL OR t.DocId = @DocId)
		AND (@IsFromERP IS NULL OR @IsFromERP=1 AND t.Sync1CAt IS NOT NULL OR @IsFromERP=0 AND t.Sync1CAt IS NULL)
		AND (@TaskType IS NULL OR t.Type=@TaskType)
		AND (@NomenclatureId IS NULL OR t.NomenclatureId = @NomenclatureId)
		AND ( @TaskGenStatus = 0 -- все, 
			OR @TaskGenStatus = 1 AND t.[Status] IN (1) -- в очереди 
			OR @TaskGenStatus = 2 AND t.[Status] IN (3, 6, 7) -- активные  
			OR @TaskGenStatus = 3 AND t.[Status] IN (2, 4, 5)) -- история
	) t 
	WHERE @TextFilter IS NULL 
	OR EXISTS -- Фильтр @TextFilter по всем полям
		(SELECT * FROM
			( SELECT t.* FOR XML PATH, Type) X(C) -- Turn all columns into XML
				CROSS APPLY
				X.C.nodes('//*/.') E(V) -- Get all XML elements
				WHERE E.V.value('./text()[1]', 'VARCHAR(MAX)') LIKE '%' + @TextFilter + '%' -- Filter on value of the XML element
			)
	ORDER BY t.CreatedAt DESC		
END
GO
GRANT EXECUTE ON dbo.GetTaskForMatching TO [RealReportReader]
GRANT EXECUTE ON dbo.GetTaskForMatching TO [RealOperatorRole]
GO