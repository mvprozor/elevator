﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetStorages') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetStorages
GO

CREATE PROCEDURE dbo.GetStorages
AS
BEGIN
	SELECT
		u.Id UnitId,
		u.DisplayName UnitDisplayName,
		u.Type UnitType,
		u.Capacity,
		us.NomenclatureId,
		nm.DisplayName NomenclatureDisplayName,
		(SELECT SUM(ul.Quantity) FROM dbo.UnitLayers ul WHERE ul.UnitId=u.Id) Quantity,
		srceq.*,
		dsteq.*
	FROM dbo.Units u
	LEFT JOIN dbo.UnitSettings us ON us.UnitId=u.Id
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id=us.NomenclatureId
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType SourceEquipmentType,
			eq.DisplayName SourceEquipmentName,
			eq.Id SourceEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=u.Id AND uel.LinkType & 2 = 2
		ORDER BY eq.Id
	) srceq
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType DestinationEquipmentType,
			eq.DisplayName DestinationEquipmentName,
			eq.Id DestinationEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=u.Id AND uel.LinkType & 4 = 4
		ORDER BY eq.Id
	) dsteq
	WHERE (u.Type & 1)=1 AND u.Deleted=0
	ORDER BY u.DisplayName
END
GO
GRANT EXECUTE ON dbo.GetStorages TO [RealReportReader]
GRANT EXECUTE ON dbo.GetStorages TO [RealOperatorRole]
GO