﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetRecvReport') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetRecvReport
GO

CREATE PROCEDURE dbo.GetRecvReport
(
	@Begin				DATETIME = NULL,
	@End				DATETIME = NULL,
	@TextFilter			NVARCHAR(300) = NULL
)
AS
BEGIN
	SELECT * FROM
	(SELECT 
		t.Id ,
		t.DocId ,
		t.NomenclatureId ,
		nm.DisplayName AS NomenclatureDisplayName,
		nm.Code1C AS NomenclatureCode1C,
		t.SourceLotId ,
		sl.LotNo AS SourceLotNo,
		t.OrganizationId,
		org.DisplayName AS OrganizationDisplayName,
		org.Code1C OrganizationCode1C,
		vt.DisplayName + ' ' + vh.RegNum VehicleInfo,
		t.IsDrying ,
		t.IsScalping ,
		t.IsSeparating,
		t.ExecuteAt ,
		t.[Status] , -- { Queued = 1, Deleted = 2, Running = 3, Completed = 4, Failed = 5, Cancelled = 6, Suspended = 7 }
		t.Comment,
		t.UserId ,
		us.DisplayName AS UserDisplayName,
		te.StartedAt,
		te.EndAt,
		te.QuantityActual
	FROM dbo.Tasks t
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id = t.NomenclatureId
	LEFT JOIN dbo.Lots sl ON sl.Id = t.SourceLotId
	LEFT JOIN dbo.Vehicles vh ON vh.Id = t.VehicleId
	LEFT JOIN dbo.VehicleTypes vt ON vt.Id = vh.VehicleTypeId
	LEFT JOIN dbo.Organizations org ON org.Id = t.OrganizationId
	LEFT JOIN dbo.Units du ON du.Id = t.DestUnitId
	LEFT JOIN dbo.Users us ON us.Id = t.UserId
	OUTER APPLY (
		SELECT 
			MIN(te.[Start]) AS StartedAt,
			MAX(te.[End]) AS EndAt,
			SUM(tem.Quantity) AS QuantityActual
		FROM dbo.TaskExecs te
		INNER JOIN dbo.TaskExecMoves tem ON tem.TaskExecId = te.Id 
		GROUP BY te.TaskId
		HAVING te.TaskId = t.Id
	) te
	WHERE te.QuantityActual>0
		AND (t.ExecuteAt >= ISNULL(@begin,'19171107'))
		AND (t.ExecuteAt <= ISNULL(@end,'26661231'))) t 
	WHERE @TextFilter IS NULL 
	OR EXISTS -- Фильтр @TextFilter по всем полям
		(SELECT * FROM
			( SELECT t.* FOR XML PATH, TYPE) X(C) -- Turn all columns into XML
				CROSS APPLY
				X.C.nodes('//*/.') E(V) -- Get all XML elements
				WHERE E.V.value('./text()[1]', 'NVARCHAR(MAX)') LIKE '%' + @TextFilter + '%' -- Filter on value of the XML element
			)
			
	END
GO
GRANT EXECUTE ON dbo.GetRecvReport TO [RealReportReader]
GO