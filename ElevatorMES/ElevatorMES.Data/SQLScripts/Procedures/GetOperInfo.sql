﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetOperInfo') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetOperInfo
GO

CREATE PROCEDURE dbo.GetOperInfo
AS
BEGIN
	DECLARE @TodayBegin DATETIME
	DECLARE @TodayEnd DATETIME

	SET @TodayBegin = CAST(GETDATE() AS DATE)
	SET @TodayEnd = DATEADD(DAY, 1, @TodayBegin)

	SELECT 
		(SELECT COUNT(*) FROM dbo.Tasks WHERE Status=1) QueuedTasks,
		(SELECT COUNT(*) FROM dbo.TaskExecs WHERE Status=4
			AND Start>=@TodayBegin AND Start<@TodayEnd) FinishedTasks,
		(SELECT COUNT(*) FROM dbo.Vehicles WHERE LeftAt IS NULL) CarsInside
END
GO
GRANT EXECUTE ON dbo.GetOperInfo TO [RealReportReader]
GRANT EXECUTE ON dbo.GetOperInfo TO [RealOperatorRole]
GO