﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetNomenclaturePriority') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetNomenclaturePriority
GO

CREATE PROCEDURE dbo.GetNomenclaturePriority
AS
BEGIN
	SELECT
		nom.Id NomenclatureId,
		nom.DisplayName NomenclatureDisplayName,
		np.Priority
	FROM dbo.Nomenclatures nom 
	LEFT JOIN dbo.NomenclaturePriorities np ON np.NomenclatureId=nom.Id
	WHERE nom.Deleted=0
	ORDER BY NomenclatureDisplayName
END
GO
GRANT EXECUTE ON dbo.GetNomenclaturePriority TO [RealReportReader]
GRANT EXECUTE ON dbo.GetNomenclaturePriority TO [RealOperatorRole]
GO