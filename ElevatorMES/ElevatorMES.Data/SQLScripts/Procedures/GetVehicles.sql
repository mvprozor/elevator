﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetVehicles') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetVehicles
GO

CREATE PROCEDURE dbo.GetVehicles
(
	@VehicleId			INT = NULL,
	@IncludingLeft		BIT = 0,
	@RegNum				NVARCHAR(255) = NULL
)
AS
BEGIN
	SELECT
		v.Id,
		v.CreatedAt,
		v.DocId,
		v.RegNum,
		v.VehicleTypeId,
		vt.DisplayName VehicleTypeName,
		vt.Class VehicleClass,
		rawd.RawId,
		rawd.RawName,
		rawd.RegisteredAt,
		rawd.RawPriority,
		rawd.RawWeightNet,
		rwb.*,
		unl.*,
		prd.ProdId,
		prd.ProdName,
		prd.LoadedAt,
		prd.ProdWeightNet,
		pwb.*,
		tare.*,
		v.LeftAt,

				(SELECT TOP 1 
			rawg.Weight
		FROM dbo.VehicleActivities rawg
		WHERE rawg.VehicleId=v.Id AND rawg.CreatedAt<unl.UnloadedAt AND rawg.Action=2
		ORDER BY rawg.CreatedAt DESC) RawWeightBrutto2

	FROM dbo.Vehicles v
	INNER JOIN dbo.VehicleTypes vt ON vt.Id=v.VehicleTypeId
	OUTER APPLY
	(
		SELECT TOP 1 
			rawr.CreatedAt RegisteredAt,
			nom.Id RawId,
			nom.DisplayName RawName,
			rawr.Weight RawWeightNet,
			np.Priority RawPriority
		FROM dbo.VehicleActivities rawr
		JOIN dbo.Nomenclatures nom ON nom.Id=rawr.NomenclatureId
		LEFT JOIN dbo.NomenclaturePriorities np ON np.NomenclatureId=nom.Id
		WHERE rawr.VehicleId=v.Id AND rawr.Action=1
		ORDER BY rawr.CreatedAt DESC
	) rawd
	OUTER APPLY
	(
		SELECT TOP 1 
			prd.CreatedAt LoadedAt,
			nom.Id ProdId,
			nom.DisplayName ProdName,
			prd.Weight ProdWeightNet
		FROM dbo.VehicleActivities prd
		JOIN dbo.Nomenclatures nom ON nom.Id=prd.NomenclatureId
		WHERE prd.VehicleId=v.Id AND prd.Action=4
		ORDER BY prd.CreatedAt DESC
	) prd
	OUTER APPLY
	(
		SELECT TOP 1 
			va.CreatedAt UnloadedAt
		FROM dbo.VehicleActivities va
		WHERE va.VehicleId=v.Id AND va.Action=3 ORDER BY va.CreatedAt DESC
	) unl
	OUTER APPLY
	(
		SELECT TOP 1 
			rawg.Weight RawWeightBrutto,
			rawg.CreatedAt RawWeightBruttoAt
		FROM dbo.VehicleActivities rawg
		WHERE rawg.VehicleId=v.Id AND 
			(unl.UnloadedAt IS NULL AND rawd.RawId IS NOT NULL OR
			 rawg.CreatedAt<unl.UnloadedAt AND rawg.Action=2)
		ORDER BY rawg.CreatedAt DESC
	) rwb
	OUTER APPLY
	(
		SELECT TOP 1 
			prdg.Weight ProdWeightBrutto,
			prdg.CreatedAt ProdWeightBruttoAt
		FROM dbo.VehicleActivities prdg
		WHERE prdg.VehicleId=v.Id AND prdg.CreatedAt>prd.LoadedAt AND prdg.Action=2
		ORDER BY prdg.CreatedAt DESC
	) pwb
	OUTER APPLY
	(
		SELECT TOP 1 
			tare.Weight WeightTare,
			tare.CreatedAt WeightTareAt
		FROM dbo.VehicleActivities tare
		WHERE tare.VehicleId=v.Id AND tare.Action=2
			AND (rawd.RawId IS NULL OR tare.CreatedAt>unl.UnloadedAt)
			AND (prd.LoadedAt IS NULL OR tare.CreatedAt<prd.LoadedAt)
		ORDER BY tare.CreatedAt DESC
	) tare
	WHERE (@VehicleId IS NOT NULL AND v.Id=@VehicleId) OR
		(@VehicleId IS NULL AND (ISNULL(@IncludingLeft,0)=1 OR v.LeftAt IS NULL)
		AND (@RegNum IS NULL OR v.RegNum LIKE '%'+@RegNum+'%'))
	ORDER BY v.RegNum, v.CreatedAt
END
GO
GRANT EXECUTE ON dbo.GetVehicles TO [RealReportReader]
GRANT EXECUTE ON dbo.GetVehicles TO [RealOperatorRole]
GO