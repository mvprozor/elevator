﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetLotHistory') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetLotHistory
GO

CREATE PROCEDURE dbo.GetLotHistory
(
	@LotNo	NVARCHAR(64) = 'РЕГИ_ПОДС_2019_220416_210528'
)
AS
BEGIN
	SELECT 
		l.Id,
		l.NomenclatureId,
		n.DisplayName NomenclatureDisplayName,
		m.MovedAt,
		m.Quantity,
		ISNULL(svt.DisplayName+' '+sv.RegNum, su.DisplayName) [Source],
		ISNULL(dvt.DisplayName+' '+dv.RegNum, du.DisplayName) [Dest],
		t.Type TaskType,
		t.Id TaskId
	FROM dbo.Lots l
	LEFT JOIN dbo.Nomenclatures n ON n.Id=l.NomenclatureId
	LEFT JOIN dbo.TaskExecMoves m ON m.LotId=l.Id
	LEFT JOIN dbo.TaskExecs exc ON exc.Id=m.TaskExecId
	LEFT JOIN dbo.Vehicles sv ON sv.Id = m.SourceVehicleId
	LEFT JOIN dbo.VehicleTypes svt ON svt.Id=sv.VehicleTypeId
	LEFT JOIN dbo.Vehicles dv ON dv.Id = m.DestVehicleId
	LEFT JOIN dbo.VehicleTypes dvt ON dvt.Id=dv.VehicleTypeId
	LEFT JOIN dbo.Tasks t ON t.Id = exc.TaskId
	LEFT JOIN dbo.Units su ON su.Id = t.SourceUnitId
	LEFT JOIN dbo.Units du ON du.Id = t.DestUnitId

	
	WHERE l.LotNo=@LotNo
END
GO
GRANT EXECUTE ON dbo.GetLotHistory TO [RealReportReader]
GRANT EXECUTE ON dbo.GetLotHistory TO [RealOperatorRole]
GO