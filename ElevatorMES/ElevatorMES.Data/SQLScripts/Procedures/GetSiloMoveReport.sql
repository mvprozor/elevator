﻿IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetSiloMoveReport') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetSiloMoveReport
GO

CREATE PROCEDURE dbo.GetSiloMoveReport
(
	@SiloId				INT,
	@TimeFrom			DATETIME = NULL,
	@TimeTo				DATETIME = NULL
)
AS
BEGIN
	SELECT
		tem.Id,
		tem.MovedAt,
		tem.Quantity,
		CAST(IIF(tem.DestUnitId=@SiloId, 1, 0) AS BIT) IsIncome,
		tem.LotId,
		l.LotNo,
		l.NomenclatureId,
		nc.DisplayName NomenclatureName,
		l.OrganizationId LotOrganizationId,
		org.DisplayName LotOrganizationName,
		IIF(tem.DestUnitId=@SiloId, 
			COALESCE(vhsrc.RegNum, unsrc.DisplayName),
			COALESCE(vhdest.RegNum, undest.DisplayName)) EntityName
	FROM dbo.TaskExecMoves tem
	INNER JOIn dbo.Lots l ON l.Id=tem.LotId
	INNER JOIN dbo.Nomenclatures nc ON nc.Id=l.NomenclatureId
	INNER JOIN dbo.Organizations org ON org.Id=l.OrganizationId
	LEFT JOIN dbo.Vehicles vhsrc ON vhsrc.Id=tem.SourceVehicleId
	LEFT JOIN dbo.Vehicles vhdest ON vhdest.Id=tem.DestVehicleId
	LEFT JOIN dbo.Units unsrc ON unsrc.Id=tem.SourceUnitId
	LEFT JOIN dbo.Units undest ON undest.Id=tem.DestUnitId
	WHERE @SiloId IN (tem.SourceUnitId, tem.DestUnitId)
		AND (@TimeFrom IS NULL OR tem.MovedAt>=@TimeFrom)
		AND (@TimeTo IS NULL OR tem.MovedAt<@TimeTo)
	ORDER BY tem.MovedAt DESC
END
GO
GRANT EXECUTE ON dbo.GetSiloMoveReport TO [RealReportReader]
GO