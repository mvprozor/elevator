USE [ElevatorMES]
IF DATABASE_PRINCIPAL_ID('RealReportReader') IS NULL
	CREATE ROLE RealReportReader
GO

IF DATABASE_PRINCIPAL_ID('RealOperatorRole') IS NULL
	CREATE ROLE RealOperatorRole
GO

IF DATABASE_PRINCIPAL_ID('RealReport') IS NULL
BEGIN
	CREATE LOGIN [RealReport] WITH PASSWORD=N'Qazwsx123++', DEFAULT_DATABASE=[ElevatorMES], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
	CREATE USER [RealReport] FOR LOGIN [RealReport]
END
GO
ALTER ROLE [RealReportReader] ADD MEMBER [RealReport]
GO

IF DATABASE_PRINCIPAL_ID('RealOperator') IS NULL
BEGIN
	CREATE LOGIN [RealOperator] WITH PASSWORD=N'Qazwsx123--', DEFAULT_DATABASE=[ElevatorMES], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
	CREATE USER [RealOperator] FOR LOGIN [RealOperator]
END
GO
ALTER ROLE [RealOperatorRole] ADD MEMBER [RealOperator]
GO

IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetPoints') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetPoints
GO

CREATE PROCEDURE dbo.GetPoints
AS
BEGIN
	SELECT
		u.Id UnitId,
		u.DisplayName UnitDisplayName,
		u.Type UnitType,
		srceq.*,
		dsteq.*
	FROM dbo.Units u
	LEFT JOIN dbo.UnitSettings us ON us.UnitId=u.Id
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id=us.NomenclatureId
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType SourceEquipmentType,
			eq.DisplayName SourceEquipmentName,
			eq.Id SourceEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=u.Id AND uel.LinkType & 2 = 2
		ORDER BY eq.Id
	) srceq
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType DestinationEquipmentType,
			eq.DisplayName DestinationEquipmentName,
			eq.Id DestinationEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=u.Id AND uel.LinkType & 4 = 4
		ORDER BY eq.Id
	) dsteq
	WHERE u.Type IN (2,3) AND u.Deleted=0
	ORDER BY u.DisplayName
END
GO
GRANT EXECUTE ON dbo.GetPoints TO [RealReportReader]
GRANT EXECUTE ON dbo.GetPoints TO [RealOperatorRole]
GO

IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetRecvReport') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetRecvReport
GO

CREATE PROCEDURE dbo.GetRecvReport
(
	@Begin				DATETIME = NULL,
	@End				DATETIME = NULL,
	@TextFilter			NVARCHAR(300) = NULL
)
AS
BEGIN
	SELECT * FROM
	(SELECT 
		t.Id ,
		t.DocId ,
		t.NomenclatureId ,
		nm.DisplayName AS NomenclatureDisplayName,
		nm.Code1C AS NomenclatureCode1C,
		t.SourceLotId ,
		sl.LotNo AS SourceLotNo,
		t.OrganizationId,
		org.DisplayName AS OrganizationDisplayName,
		org.Code1C OrganizationCode1C,
		vt.DisplayName + ' ' + vh.RegNum VehicleInfo,
		t.IsDrying ,
		t.IsScalping ,
		t.IsSeparating,
		t.ExecuteAt ,
		t.[Status] , -- { Queued = 1, Deleted = 2, Running = 3, Completed = 4, Failed = 5, Cancelled = 6, Suspended = 7 }
		t.Comment,
		t.UserId ,
		us.DisplayName AS UserDisplayName,
		te.StartedAt,
		te.EndAt,
		te.QuantityActual
	FROM dbo.Tasks t
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id = t.NomenclatureId
	LEFT JOIN dbo.Lots sl ON sl.Id = t.SourceLotId
	LEFT JOIN dbo.Vehicles vh ON vh.Id = t.VehicleId
	LEFT JOIN dbo.VehicleTypes vt ON vt.Id = vh.VehicleTypeId
	LEFT JOIN dbo.Organizations org ON org.Id = t.OrganizationId
	LEFT JOIN dbo.Units du ON du.Id = t.DestUnitId
	LEFT JOIN dbo.Users us ON us.Id = t.UserId
	OUTER APPLY (
		SELECT 
			MIN(te.[Start]) AS StartedAt,
			MAX(te.[End]) AS EndAt,
			SUM(tem.Quantity) AS QuantityActual
		FROM dbo.TaskExecs te
		INNER JOIN dbo.TaskExecMoves tem ON tem.TaskExecId = te.Id 
		GROUP BY te.TaskId
		HAVING te.TaskId = t.Id
	) te
	WHERE te.QuantityActual>0
		AND (t.ExecuteAt >= ISNULL(@begin,'19171107'))
		AND (t.ExecuteAt <= ISNULL(@end,'26661231'))) t 
	WHERE @TextFilter IS NULL 
	OR EXISTS -- Фильтр @TextFilter по всем полям
		(SELECT * FROM
			( SELECT t.* FOR XML PATH, TYPE) X(C) -- Turn all columns into XML
				CROSS APPLY
				X.C.nodes('//*/.') E(V) -- Get all XML elements
				WHERE E.V.value('./text()[1]', 'NVARCHAR(MAX)') LIKE '%' + @TextFilter + '%' -- Filter on value of the XML element
			)
			
	END
GO
GRANT EXECUTE ON dbo.GetRecvReport TO [RealReportReader]
GO

IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetSiloMoveReport') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetSiloMoveReport
GO

CREATE PROCEDURE dbo.GetSiloMoveReport
(
	@SiloId				INT,
	@TimeFrom			DATETIME = NULL
)
AS
BEGIN
	SELECT
		tem.Id,
		tem.MovedAt,
		tem.Quantity,
		CAST(IIF(tem.DestUnitId=@SiloId, 1, 0) AS BIT) IsIncome,
		tem.LotId,
		l.LotNo,
		l.NomenclatureId,
		nc.DisplayName NomenclatureName,
		l.OrganizationId LotOrganizationId,
		org.DisplayName LotOrganizationName,
		IIF(tem.DestUnitId=@SiloId, 
			COALESCE(vhsrc.RegNum, unsrc.DisplayName),
			COALESCE(vhdest.RegNum, undest.DisplayName)) EntityName
	FROM dbo.TaskExecMoves tem
	INNER JOIn dbo.Lots l ON l.Id=tem.LotId
	INNER JOIN dbo.Nomenclatures nc ON nc.Id=l.NomenclatureId
	INNER JOIN dbo.Organizations org ON org.Id=l.OrganizationId
	LEFT JOIN dbo.Vehicles vhsrc ON vhsrc.Id=tem.SourceVehicleId
	LEFT JOIN dbo.Vehicles vhdest ON vhdest.Id=tem.DestVehicleId
	LEFT JOIN dbo.Units unsrc ON unsrc.Id=tem.SourceUnitId
	LEFT JOIN dbo.Units undest ON undest.Id=tem.DestUnitId
	WHERE @SiloId IN (tem.SourceUnitId, tem.DestUnitId)
		AND (@TimeFrom IS NULL OR tem.MovedAt>=@TimeFrom)
	ORDER BY tem.MovedAt DESC
END
GO
GRANT EXECUTE ON dbo.GetSiloMoveReport TO [RealReportReader]
GO

IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetSiloStateReport') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetSiloStateReport
GO

CREATE PROCEDURE dbo.GetSiloStateReport
(
	@SiloId				INT
)
AS
BEGIN
	SELECT
		ul.CreatedAt,
		ul.LotId,
		ul.Quantity,
		l.LotNo,
		l.NomenclatureId,
		nc.DisplayName NomenclatureName,
		l.OrganizationId,
		org.DisplayName OrganizationName
	FROM dbo.UnitLayers ul
	INNER JOIn dbo.Lots l ON l.Id=ul.LotId
	INNER JOIN dbo.Nomenclatures nc ON nc.Id=l.NomenclatureId
	INNER JOIN dbo.Organizations org ON org.Id=l.OrganizationId
	WHERE ul.UnitId = @SiloId
	ORDER BY ul.CreatedAt DESC
END
GO
GRANT EXECUTE ON dbo.GetSiloStateReport TO [RealReportReader]
GO

IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetStorages') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetStorages
GO

CREATE PROCEDURE dbo.GetStorages
AS
BEGIN
	SELECT
		u.Id UnitId,
		u.DisplayName UnitDisplayName,
		u.Type UnitType,
		u.Capacity,
		us.NomenclatureId,
		nm.DisplayName NomenclatureDisplayName,
		(SELECT SUM(ul.Quantity) FROM dbo.UnitLayers ul WHERE ul.UnitId=u.Id) Quantity,
		srceq.*,
		dsteq.*
	FROM dbo.Units u
	LEFT JOIN dbo.UnitSettings us ON us.UnitId=u.Id
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id=us.NomenclatureId
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType SourceEquipmentType,
			eq.DisplayName SourceEquipmentName,
			eq.Id SourceEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=u.Id AND uel.LinkType & 2 = 2
		ORDER BY eq.Id
	) srceq
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType DestinationEquipmentType,
			eq.DisplayName DestinationEquipmentName,
			eq.Id DestinationEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=u.Id AND uel.LinkType & 4 = 4
		ORDER BY eq.Id
	) dsteq
	WHERE u.Type=1 AND u.Deleted=0
	ORDER BY u.DisplayName
END
GO
GRANT EXECUTE ON dbo.GetStorages TO [RealReportReader]
GRANT EXECUTE ON dbo.GetStorages TO [RealOperatorRole]
GO

IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetTask') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetTask
GO

CREATE PROCEDURE dbo.GetTask
(
	@TaskId				INT = NULL,
	@TaskStatus			INT = 0, -- 0 - все, 1 - в очереди,  2 - активные, 3 - история
	@Begin				DATETIME = NULL,
	@End				DATETIME = NULL,
	@TextFilter			NVARCHAR(300) = NULL
)
AS
BEGIN
	SELECT * FROM
	(SELECT 
		t.Id ,
		t.DocId ,
		t.[Type] ,
		t.CreatedAt,
		t.NomenclatureId ,
			nm.DisplayName AS NomenclatureDisplayName,
			nm.Code1C AS NomenclatureCode1C,
		t.SourceLotId ,
			sl.LotNo AS SourceLotNo,		
		t.SourceUnitId,
		su.DisplayName SourceUnitName,
		t.DestUnitId,
		du.DisplayName DestUnitName,
		ISNULL(vh.RegNum, '') + ISNULL(', '+org.DisplayName, '') + ISNULL(', '+ctr.DisplayName, '') SuppCons,
		t.IsDrying ,
		t.IsScalping ,
		t.IsSeparating ,
		t.ExecuteAt ,
		t.[Status] , -- { Queued = 1, Deleted = 2, Running = 3, Completed = 4, Failed = 5, Pending = 6, Suspended = 7 }
		t.Comment ,
		t.UserId ,
			us.DisplayName AS [UserDisplayName],
		te.StartedAt,
		te.EndAt,
		t.Quantity AS [QuantityPlanned],
		ISNULL(te.QuantityActual, 0.0) AS QuantityActual,

		vh.RegNum,
		su.Code1C as SourceUnitCode1C,
		du.Code1C as DestUnitCode1C,
		ctr.Code1C as ContragentCode1C,
		org.Code1C as OrganizationCode1C,

		srceq.*,
		dsteq.*

	FROM dbo.Tasks t
	LEFT JOIN dbo.Nomenclatures nm ON nm.Id = t.NomenclatureId
	LEFT JOIN dbo.Lots sl ON sl.Id = t.SourceLotId
	LEFT JOIN dbo.Lots dl ON dl.Id = t.DestLotId
	LEFT JOIN dbo.Vehicles vh ON vh.Id = t.VehicleId
	LEFT JOIN dbo.Organizations org ON org.Id = t.OrganizationId
	LEFT JOIN dbo.Contragents ctr ON ctr.Id = t.ContragentId
	LEFT JOIN dbo.Units su ON su.Id = t.SourceUnitId
	LEFT JOIN dbo.Units du ON du.Id = t.DestUnitId
	LEFT JOIN dbo.Users us ON us.Id = t.UserId
	OUTER APPLY (
		SELECT 
			MIN(te.[Start]) AS StartedAt,
			MAX(te.[End]) AS EndAt,
			SUM(tem.Quantity) AS QuantityActual
		FROM dbo.TaskExecs te
		INNER JOIN dbo.TaskExecMoves tem ON tem.TaskExecId = te.Id 
		GROUP BY te.TaskId
		HAVING te.TaskId = t.Id
	) te
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType SourceEquipmentType,
			eq.DisplayName SourceEquipmentName,
			eq.Id SourceEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=t.SourceUnitId AND uel.LinkType & 2 = 2
		ORDER BY eq.Id
	) srceq
	OUTER APPLY (
		SELECT TOP 1
			eq.EquipmentType DestinationEquipmentType,
			eq.DisplayName DestinationEquipmentName,
			eq.Id DestinationEquipmentId
		FROM dbo.UnitEquipmentLinks uel
		INNER JOIN dbo.Equipments eq ON eq.Id=uel.EquipmentId
		WHERE uel.UnitId=t.DestUnitId AND uel.LinkType & 4 = 4
		ORDER BY eq.Id
	) dsteq
	WHERE 
		(@TaskId IS NULL OR t.Id = @TaskId)
		AND ( @TaskStatus = 0 -- все, 
			OR @TaskStatus = 1 AND t.[Status] IN (1) -- в очереди 
			OR @TaskStatus = 2 AND t.[Status] IN (3, 6, 7) -- активные  
			OR @TaskStatus = 3 AND t.[Status] IN (2, 4, 5) -- история
		)
		AND (t.ExecuteAt >= ISNULL(@begin,'19171107'))
		AND (t.ExecuteAt <= ISNULL(@end,'26661231'))) t 
	WHERE @TextFilter IS NULL 
	OR EXISTS -- Фильтр @TextFilter по всем полям
		(SELECT * FROM
			( SELECT t.* FOR XML PATH, TYPE) X(C) -- Turn all columns into XML
				CROSS APPLY
				X.C.nodes('//*/.') E(V) -- Get all XML elements
				WHERE E.V.value('./text()[1]', 'VARCHAR(MAX)') LIKE '%' + @TextFilter + '%' -- Filter on value of the XML element
			)
			
	END
GO
GRANT EXECUTE ON dbo.GetTask TO [RealReportReader]
GRANT EXECUTE ON dbo.GetTask TO [RealOperatorRole]
GO
GO

IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetUnitsForReports') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetUnitsForReports
GO

CREATE PROCEDURE dbo.GetUnitsForReports
(
	@Type	INT = NULL
)
AS
BEGIN
	SELECT 
		u.Id,
		u.Type,
		u.Code1C,
		u.DisplayName,
		u.CreatedAt,
		u.Capacity
	FROM dbo.Units u
	WHERE @Type IS NULL OR u.Type=@Type
END
GO
GRANT EXECUTE ON dbo.GetUnitsForReports TO [RealReportReader]
GO

IF EXISTS (SELECT * FROM sys.objects o WHERE o.[object_id] = OBJECT_ID('dbo.GetVehicleReport') AND o.[type] IN ('P', 'PC'))
	DROP PROCEDURE dbo.GetVehicleReport
GO

CREATE PROCEDURE dbo.GetVehicleReport
(
	@Begin				DATETIME = NULL,
	@End				DATETIME = NULL,
	@RegNum				NVARCHAR(16) = NULL,
	@IsWithin			BIT = 1
)
AS
BEGIN
	SELECT
		v.Id,
		v.CreatedAt,
		v.DocId,
		v.RegNum,
		v.VehicleTypeId,
		vt.DisplayName VehicleTypeName,
		vt.Class VehicleClass,
		STUFF((SELECT 
			' / - '
			+CASE va.Action
				WHEN 1 THEN 'Зарегистрировано'
				WHEN 2 THEN 'Взвешено'
				WHEN 3 THEN 'Выгружено'
				WHEN 4 THEN 'Погружено'
				ELSE CAST(Action AS NVARCHAR(12))
			END
			+' '
			+FORMAT(va.CreatedAt,'dd MMM HH:mm', 'ru-RU')
			+ISNULL(IIF(va.Action IN (1, 4), ' ('+nl.DisplayName +')',NULL),'')
		FROM dbo.VehicleActivities va
		LEFT JOIN dbo.Nomenclatures nl ON nl.Id=va.NomenclatureId
		WHERE va.VehicleId=v.Id
		ORDER BY va.CreatedAt ASC
		FOR XML PATH(''))
		+IIF(v.LeftAt IS NULL, '', ' / - Выезд '+FORMAT(v.LeftAt,'dd MMM HH:mm', 'ru-RU'))
		,1,3,'') Activities
	FROM dbo.Vehicles v
	INNER JOIN dbo.VehicleTypes vt ON vt.Id=v.VehicleTypeId
	WHERE (@Begin IS NULL OR v.CreatedAt>=@Begin)
		AND (@End IS NULL OR v.CreatedAt<=@End)
		AND (@RegNum IS NULL OR v.RegNum LIKE '%'+@RegNum+'%')
		AND (ISNULL(@IsWithin,0)=0 OR v.LeftAt IS NULL)
END
GO
GRANT EXECUTE ON dbo.GetVehicleReport TO [RealReportReader]
GO

