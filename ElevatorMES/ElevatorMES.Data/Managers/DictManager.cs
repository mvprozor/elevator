﻿using System.Collections.Generic;
using System.Linq;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;

namespace ElevatorMES.Data.Managers
{
    public class DictManager : BaseManager
    {
        public DictManager(IUnitOfWork unitOfWork) => _db = unitOfWork;

        public IEnumerable<Nomenclature> GetNomenclatures(GetDictArg arg = null)
        {
            var isDeleted = arg?.IsDeleted;
            var searchText = arg?.SearchText;
            return _db.NomenclatureRepository.FindBy(m => (!isDeleted.HasValue || isDeleted.Value == m.Deleted)
               && (searchText==null || m.DisplayName.Contains(searchText) 
               || m.Code1C.Contains(searchText) || m.Type1C.Contains(searchText))).ToList();
        }

        public IEnumerable<Unit> GetSilos()
        {
            return _db.UnitRepository.GetAll().Where(r=>r.Type == UnitType.Silos).ToList();
        }

        public IEnumerable<Unit> GetStorages()
        {
            return _db.UnitRepository.GetAll().Where(r => (r.Type & UnitType.Storage)== UnitType.Storage).ToList();
        }

        public IEnumerable<Vehicle> GetVehicles()
        {
            return _db.VehicleRepository.GetAll().ToList();
        }

        public IEnumerable<VehicleType> GetVehicleTypes()
        {
            return _db.VehicleTypeRepository.GetAll().ToList();
        }

        public IEnumerable<Organization> GetOrganizations(GetDictArg arg = null)
        {
            var isDeleted = arg?.IsDeleted;
            return _db.OrganizationRepository.FindBy(m => !isDeleted.HasValue || isDeleted.Value == m.Deleted).ToList();
        }

        public IEnumerable<Contragent> GetContragents(GetDictArg arg = null)
        {
            var isDeleted = arg?.IsDeleted;
            return _db.ContragentsRepository.FindBy(m => !isDeleted.HasValue || isDeleted.Value == m.Deleted).ToList();
        }

        public IEnumerable<User> GetUsers()
        {
            return _db.UserRepository.GetAll().ToList();
        }

        /// <summary>
        /// Обновление (создание) элемента справочника
        /// </summary>
        /// <param name="arg"></param>
        public void UpdateDictionary(UpdateDictArg arg)
        {
            Nomenclature nomenclature = null;
            Contragent contragent = null;
            Organization organization = null;
            if (arg.Id.HasValue)
            {
                switch (arg.Type)
                {
                    case DictionaryType.Nomenclature:
                        nomenclature = _db.NomenclatureRepository.GetById(arg.Id.Value, true);
                        nomenclature.DisplayName = arg.DisplayName;
                        nomenclature.Code1C = arg.Code1C;
                        nomenclature.Type1C = arg.Type1C;
                        nomenclature.CodeParent1C = arg.CodeParent1C;
                        _db.NomenclatureRepository.Update(nomenclature);
                        break;

                    case DictionaryType.Contragent:
                        contragent = _db.ContragentsRepository.GetById(arg.Id.Value, true);
                        contragent.DisplayName = arg.DisplayName;
                        contragent.Code1C = arg.Code1C;
                        contragent.Type1C = arg.Type1C;
                        contragent.CodeParent1C = arg.CodeParent1C;
                        _db.ContragentsRepository.Update(contragent);
                        break;

                    case DictionaryType.Organization:
                        organization = _db.OrganizationRepository.GetById(arg.Id.Value, true);
                        organization.DisplayName = arg.DisplayName;
                        organization.Code1C = arg.Code1C;
                        organization.Type1C = arg.Type1C;
                        organization.CodeParent1C = arg.CodeParent1C;
                        _db.OrganizationRepository.Update(organization);
                        break;
                }
            }
            else
            {
                switch (arg.Type)
                {
                    case DictionaryType.Nomenclature:
                        nomenclature = new Nomenclature()
                        {
                            DisplayName = arg.DisplayName,
                            Code1C = arg.Code1C,
                            Type1C = arg.Type1C,
                            CodeParent1C = arg.CodeParent1C
                        };
                        _db.NomenclatureRepository.Add(nomenclature);
                        break;

                    case DictionaryType.Contragent:
                        contragent = new Contragent()
                        {
                            DisplayName = arg.DisplayName,
                            Code1C = arg.Code1C,
                            Type1C = arg.Type1C,
                            CodeParent1C = arg.CodeParent1C
                        };
                        _db.ContragentsRepository.Add(contragent);
                        break;

                    case DictionaryType.Organization:
                        organization = new Organization()
                        {
                            DisplayName = arg.DisplayName,
                            Code1C = arg.Code1C,
                            Type1C = arg.Type1C,
                            CodeParent1C = arg.CodeParent1C
                        };
                        _db.OrganizationRepository.Add(organization);
                        break;
                }
            }
            _db.SaveChanges();

        }

        /// <summary>
        /// Удаление элемента справочника
        /// </summary>
        /// <param name="arg"></param>
        public void DeleteDictionary(DeleteDictArg arg)
        {
            switch (arg.Type)
            {
                case DictionaryType.Nomenclature:
                    _db.NomenclatureRepository.TryDelete(arg.Id);
                    break;

                case DictionaryType.Contragent:
                    _db.ContragentsRepository.TryDelete(arg.Id);
                    break;

                case DictionaryType.Organization:
                    _db.OrganizationRepository.TryDelete(arg.Id);
                    break;
            }
        }

        /// <summary>
        /// Восстановление элемента справочника
        /// </summary>
        /// <param name="arg"></param>
        public void RestoreDictionary(DeleteDictArg arg)
        {
            switch (arg.Type)
            {
                case DictionaryType.Nomenclature:
                    _db.NomenclatureRepository.Restore(arg.Id);
                    break;

                case DictionaryType.Contragent:
                    _db.ContragentsRepository.Restore(arg.Id);
                    break;

                case DictionaryType.Organization:
                    _db.OrganizationRepository.Restore(arg.Id);
                    break;
            }
            _db.SaveChanges();
        }
    }
}
