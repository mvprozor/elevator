﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Data.Services;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Exceptions;

namespace ElevatorMES.Data.Managers
{

    public class StorageManager : BaseManager
    {
        public StorageManager(IUnitOfWork db) => _db = db;

        public Interact1C Interact1C => new Interact1C();

        /// <summary>
        /// Получение списка складов и их состояний
        /// </summary>
        /// <returns></returns>
        public List<StorageUIItem> GetStorages()
        {
            return _db.ExecuteQuery<StorageUIItem>("EXEC dbo.GetStorages");
        }

        /// <summary>
        /// Изменение назначения склада
        /// </summary>
        /// <returns></returns>
        public void StorageAssign(StorageAssignArg arg)
        {
            var unit = _db.UnitRepository.GetById(arg.UnitId, true);
            if ((unit.Type & UnitType.Storage) != UnitType.Storage)
                throw new LogicException($"Подразделение {unit.DisplayName} не является складом");

            using (var ctx = _db.BeginTransaction())
            {
                if (_db.UnitLayerRepository.FindBy(m => m.UnitId == unit.Id).Any())
                    throw new LogicException($"Склад {unit.DisplayName} не пустой, невозможно назначить новую номенклатуру");

                Nomenclature nomenclature = null;
                if (arg.NomenclatureId.HasValue)
                    nomenclature = _db.NomenclatureRepository.GetById(arg.NomenclatureId.Value, true);

                var unitSetting = _db.UnitSettingRepository.FindBy(m => m.UnitId == unit.Id).Include( m => m.Nomenclature).SingleOrDefault();
                if (unitSetting == null)
                {
                    unitSetting = new UnitSetting()
                    {
                        Unit = unit,
                        Nomenclature = nomenclature,
                        UpdatedAt = DateTime.Now
                    };
                    _db.UnitSettingRepository.Add(unitSetting);
                }
                else
                {
                    unitSetting.Nomenclature = nomenclature;
                    unitSetting.UpdatedAt = DateTime.Now;
                }
                _db.SaveChanges();
                ctx.Commit();
            }
        }

        /// <summary>
        /// Инвентаризация склада
        /// </summary>
        /// <returns></returns>
        public void StorageInventory(StorageInventoryArg arg)
        {
            var unit = _db.UnitRepository.GetById(arg.UnitId, true);
            if ((unit.Type & UnitType.Storage) != UnitType.Storage)
                throw new LogicException($"Подразделение {unit.DisplayName} не является складом");
            var unitSetting = _db.UnitSettingRepository.FindBy(m => m.UnitId == unit.Id).Include(m => m.Nomenclature).FirstOrDefault();
            if (unitSetting?.Nomenclature == null)
                throw new LogicException($"Складу {unit.DisplayName} не назначена номенклатура");
            if (arg.NewQuantity>unit.Capacity)
                throw new LogicException($"Заданное количесво {arg.NewQuantity} превышает емкость склада {unit.Capacity}");

            using (var transaction = _db.BeginTransaction())
            {
                var layers = _db.UnitLayerRepository.FindBy( m => m.UnitId == unit.Id).OrderByDescending(m => m.CreatedAt).ToList();
                var topLayer = layers.FirstOrDefault();
                var bottomLayer = layers.LastOrDefault();

                // проверка того, что склад не успел измениться
                if (topLayer?.CreatedAt != arg.TopLayerCreatedAt
                    || (topLayer?.Quantity ?? decimal.Zero) != arg.TopLayerQuantity
                    || bottomLayer?.CreatedAt != arg.BottomLayerCreatedAt
                    || (bottomLayer?.Quantity ?? decimal.Zero) != arg.BottomLayerQuantity)
                    throw new LogicException($"Состояние склада {unit.DisplayName} было изменено в другом приложении. Повторите инвентаризацию заново.");

                var currQuantity = layers.Sum(m => m.Quantity);
                if (arg.NewQuantity <= 0)
                    _db.UnitLayerRepository.RemoveRange(layers);
                else
                {
                    if (layers.Count == 0)
                    {
                        // создание нового наполнения - создание новой партии
                        // организация - своя
                        var org = _db.OrganizationRepository.FindBy(m => m.DisplayName.Contains("русагро")
                            && m.DisplayName.Contains("приморье")).FirstOrDefault();
                        if (org == null)
                            throw new LogicException("Для создания партии не найдена собственная организация");
                        var lot = new Lot()
                        {
                            Nomenclature = unitSetting.Nomenclature,
                            Organization = org
                        };
                        lot.GenerateLotNo();
                        _db.LotRepository.Add(lot);
                        var layer = new UnitLayer()
                        {
                            Unit = unit,
                            Lot = lot,
                            Quantity = arg.NewQuantity
                        };
                        _db.UnitLayerRepository.Add(layer);
                    }
                    else
                    {
                        switch (arg.Mode)
                        {
                            case InventoryMode.SetBottomLayer:
                                bottomLayer = layers[layers.Count - 1];
                                if (arg.NewQuantity > currQuantity)
                                {
                                    bottomLayer.Quantity += arg.NewQuantity - currQuantity;
                                    _db.UnitLayerRepository.Update(bottomLayer);
                                }
                                else
                                {
                                    while (layers.Count > 0 && currQuantity - layers[layers.Count - 1].Quantity >= arg.NewQuantity)
                                    {
                                        var layer = layers[layers.Count - 1];
                                        currQuantity -= layer.Quantity;
                                        layers.RemoveAt(layers.Count - 1);
                                        _db.UnitLayerRepository.Remove(layer);
                                    }
                                    if (layers.Count > 0)
                                    {
                                        bottomLayer = layers[layers.Count - 1];
                                        bottomLayer.Quantity -= currQuantity - arg.NewQuantity;
                                        _db.UnitLayerRepository.Update(bottomLayer);
                                    }
                                }
                                break;
                            case InventoryMode.SetTopLayer:
                                topLayer = layers[0];
                                if (arg.NewQuantity > currQuantity)
                                {
                                    topLayer.Quantity += arg.NewQuantity - currQuantity;
                                }
                                else
                                {
                                    while (layers.Count > 0 && currQuantity - layers[0].Quantity >= arg.NewQuantity)
                                    {
                                        var layer = layers[0];
                                        currQuantity -= layer.Quantity;
                                        layers.RemoveAt(0);
                                        _db.UnitLayerRepository.Remove(layer);
                                    }
                                    if (layers.Count > 0)
                                    {
                                        topLayer = layers[0];
                                        topLayer.Quantity -= currQuantity - arg.NewQuantity;
                                        _db.UnitLayerRepository.Update(topLayer);
                                    }
                                }
                                break;
                        }
                    }
                }
                var user = _db.UserRepository.GetOrCreateByDisplayName(arg.User);
                _db.UnitInventoryRepository.Add(new UnitInventory()
                {
                    Unit = unit,
                    CreatedAt = DateTime.Now,
                    Nomenclature = unitSetting.Nomenclature,
                    Quantity = arg.NewQuantity,
                    User = user,
                    Comment = arg.Comment
                });

                var transmitLog = Interact1C.GetTransmitLogInventory(guid: Guid.NewGuid().ToString(), 
                    quantity: (float)arg.NewQuantity, date: DateTime.Now, warehouse: unit.Code1C, nomenclatureCode1C: unitSetting.Nomenclature?.Code1C, user: user.DisplayName);
                _db.TransmitLogRepository.Add(transmitLog);

                _db.SaveChanges();
                transaction.Commit();
            }
        }

        /// <summary>
        /// Получение слоев на складе
        /// </summary>
        /// <returns></returns>
        public List<LayerUIItem> GetSiloStateReport(int siloId)
        {
            return _db.ExecuteQuery<LayerUIItem>("EXEC dbo.GetSiloStateReport @SiloId",
                new SqlParameter("@" + nameof(siloId), siloId));
        }

        /// <summary>
        /// Получение движений по складу
        /// </summary>
        /// <returns></returns>
        public List<MoveUIItem> GetSiloMoveReport(int siloId, DateTime? timeFrom)
        {
            return _db.ExecuteQuery<MoveUIItem>("EXEC dbo.GetSiloMoveReport @SiloId, @TimeFrom",
                new SqlParameter("@" + nameof(siloId), siloId),
                new SqlParameter("@" + nameof(timeFrom), timeFrom ?? ObjNull));
        }    
    }
}