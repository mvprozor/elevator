﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using System.Xml.Linq;
using System.Xml.Serialization;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Data.Managers
{
    public class VehicleManager : BaseManager
    {
        public VehicleManager(IUnitOfWork db) => _db = db;

        /// <summary>
        /// Получение списка транспорта
        /// </summary>
        /// <returns></returns>
        public List<VehicleUIItem> GetVehicles(GetVehicleArg arg)
        {
            return _db.ExecuteQuery<VehicleUIItem>("EXEC dbo.GetVehicles @VehicleId, @IncludingLeft, @RegNum",
                new SqlParameter("@"+nameof(arg.VehicleId), ((int?)arg?.VehicleId) ?? ObjNull),
                new SqlParameter("@"+nameof(arg.IncludingLeft), arg.IncludingLeft),
                new SqlParameter("@"+nameof(arg.RegNum), arg.RegNum ?? ObjNull));
        }

        /// <summary>
        /// Получение истории транспорта
        /// </summary>
        /// <returns></returns>
        public List<VehicleActivity> GetVehicleHistory(int vehicleId)
        {
            return _db.VehicleActivityRepository.GetAll().Where(
                r => r.VehicleId == vehicleId)
                    .Include( m => m.Nomenclature)
                    .Include( m => m.Unit)
                    .ToList();
        }

        /// <summary>
        /// Регистрация транспорта
        /// </summary>
        /// <returns></returns>
        public VehicleUIItem RegisterVehicle(VehicleRegisterArg arg)
        {
            if (String.IsNullOrWhiteSpace(arg.VehicleRegNum))
                throw new LogicException("Не указан номер транспорта");
            VehicleType vehicleType = _db.VehicleTypeRepository.GetById(arg.VehicleTypeId, true);
            Nomenclature nomenclature = null;
            if (arg.NomenclatureId.HasValue)
            {
                nomenclature = _db.NomenclatureRepository.GetById(arg.NomenclatureId.Value, true);
            }

            Vehicle vehicle = null;
            using (var ctx = _db.BeginTransaction())
            {

                if (_db.VehicleRepository.FindBy(m => m.RegNum == arg.VehicleRegNum && !m.LeftAt.HasValue).Any())
                {
                    throw new LogicException("Транспорт с указанным номером уже зарегистрирован");
                }
                var user = _db.UserRepository.GetOrCreateByDisplayName(arg.User);
                vehicle = new Vehicle()
                {
                    RegNum = arg.VehicleRegNum,
                    VehicleType = vehicleType,
                    User = user
                };
                _db.VehicleRepository.Add(vehicle);
                var reg = new VehicleActivity()
                {
                    Action = VehicleAction.Registering,
                    Comment = arg.Comment,
                    CreatedAt = arg.RegisteredAt,
                    Nomenclature = nomenclature,
                    Vehicle = vehicle,
                    Weight = arg.Weight
                };
                _db.VehicleActivityRepository.Add(reg);
                _db.SaveChanges();
                ctx.Commit();
            }
            return GetVehicles(new GetVehicleArg() { VehicleId=vehicle.Id}).FirstOrDefault();
        }

        /// <summary>
        /// Действия с автотранспортом
        /// </summary>
        public VehicleUIItem Serve(VehicleServeArg arg)
        {
            var vehicle = _db.VehicleRepository.GetById(arg.VehicleId, true);
            Nomenclature nomenclature = null;
            if (arg.NomenclatureId.HasValue)
            {
                nomenclature = _db.NomenclatureRepository.GetById(arg.NomenclatureId.Value, true);
            }
            else
            {
                if (arg.Action == VehicleAction.Loading)
                    throw new LogicException($"Не указан груз");
            }
            Unit unit = null;
            if (arg.UnitId.HasValue)
            {
                unit = _db.UnitRepository.GetById(arg.UnitId.Value, true);
            }

            using (var ctx = _db.BeginTransaction())
            {
                var activites = _db.VehicleActivityRepository
                    .FindBy(m => m.VehicleId == arg.VehicleId)
                    .OrderBy(m => m.CreatedAt)
                    .Include(m => m.Nomenclature)
                    .Include(m => m.Unit).ToList();
                var reg = activites.FirstOrDefault(m => m.Action == VehicleAction.Registering);
                if (reg == null)
                {
                    if (arg.Action != VehicleAction.Registering)
                        throw new LogicException($"Транспорт сначала необходимо зарегистрировать");
                }
                else
                {
                    if (arg.Action == VehicleAction.Registering)
                        throw new LogicException($"Транспорт уже зарегистрирован");
                    if (arg.ActionAt <= reg.CreatedAt)
                        throw new LogicException($"Действие должно быть позже времени регистрации");
                }
                var lea = activites.FirstOrDefault(m => m.Action == VehicleAction.Leaving);
                if (lea != null)
                {
                    if (arg.Action == VehicleAction.Leaving)
                        throw new LogicException($"Выезд уже зарегистрирован");
                    if (arg.ActionAt >= lea.CreatedAt)
                        throw new LogicException($"Действие должно быть до времени выезда");
                }
                var same = activites.FirstOrDefault(m => m.CreatedAt == arg.ActionAt);
                if (same!=null)
                    throw new LogicException($"На данное время уже зафиксировано действие {same}");
                var lastActivity = activites.LastOrDefault(m => m.CreatedAt < arg.ActionAt);
                if (lastActivity != null)
                {
                    if (lastActivity.Action == VehicleAction.Unloading)
                        nomenclature = null;
                    else if (nomenclature == null)
                        nomenclature = lastActivity.Nomenclature;
                }
                VehicleActivity activity = null;
                switch (arg.Action)
                {
                    case VehicleAction.Unloading:
                        activity = activites.LastOrDefault(m => m.CreatedAt < arg.ActionAt && m.Action != VehicleAction.Weighting);
                        if (activity != null && (activity.Action == VehicleAction.Registering &&
                                activity.NomenclatureId.HasValue || activity.Action == VehicleAction.Loading))
                            break;
                        throw new LogicException($"Выгрузка на данное время невозможна, т.к. согласно предыдущим действиям транспорт пуст");

                    case VehicleAction.Loading:
                        activity = activites.LastOrDefault(m => m.CreatedAt < arg.ActionAt && m.Action != VehicleAction.Weighting);
                        if (activity == null || activity.Action == VehicleAction.Registering &&
                                !activity.NomenclatureId.HasValue || activity.Action == VehicleAction.Unloading)
                            break;
                        throw new LogicException($"Погрузка на данное время невозможна, т.к. согласно предыдущим действиям транспорт загружен");

                    case VehicleAction.Leaving:
                        vehicle.LeftAt = arg.ActionAt;
                        _db.VehicleRepository.Update(vehicle);
                        break;
                }
                activity = new VehicleActivity()
                {
                    CreatedAt = arg.ActionAt,
                    Action = arg.Action,
                    Comment = arg.Comment,
                    Nomenclature = nomenclature,
                    Unit = unit,
                    Vehicle = vehicle,
                    Weight = arg.Weight
                };
                _db.VehicleActivityRepository.Add(activity);
                _db.SaveChanges();
                if (arg.Action == VehicleAction.Weighting)
                {
                    // при взвешивании проверяем, что готов вес тары, нетто и брутто
                    // для отправки в 1С
                    var item = GetVehicles(new GetVehicleArg() { VehicleId = vehicle.Id }).FirstOrDefault();
                    if (item.WeightTare.HasValue && (item.RawWeightBrutto.HasValue || item.ProdWeightBrutto.HasValue))
                    {
                        var brutto = Convert.ToDouble((item.ProdWeightBrutto ?? item.RawWeightBrutto).Value);
                        var bruttoAt = (item.ProdWeightBruttoAt ?? item.RawWeightBruttoAt).Value;
                        var netto = brutto - Convert.ToDouble(item.WeightTare.Value);
                        var tare = Convert.ToDouble(item.WeightTare.Value);
                        var tareAt = item.WeightTareAt.Value;

                        var weightVeh = new ServiceReference1.WeighingOfVehicles()
                        {
                            GUID = item.DocId,
                            NumberVehicles = vehicle.RegNum,
                            TypeWeighting = item.RawId.HasValue,
                            TareWeight = tare,
                            DateTareWeighing = tareAt,
                            GrossWeight = brutto,
                            DateGrossWeighing = bruttoAt,
                            NettoWeight = netto,
                        };
                        var xWeighVeh = weightVeh.ToXElement();
                        if(xWeighVeh != null)
                        {
                            var transmitLogItem = new TransmitLog(xWeighVeh, MessageType.WeightingOfVehicles);
                            _db.TransmitLogRepository.Add(transmitLogItem);
                            _db.SaveChanges();
                        }
                    }
                }
                ctx.Commit();
            }
            return GetVehicles(new GetVehicleArg() { VehicleId = vehicle.Id }).FirstOrDefault();
        }


        /// <summary>
        /// Удаление автотранспорта
        /// </summary>
        public void Delete(int vehicleId)
        {
            var vehicle = _db.VehicleRepository.GetById(vehicleId, true);
            if (!String.IsNullOrWhiteSpace(vehicle.DocId))
                throw new LogicException($"Транспорт зарегистрирован в ERP: {vehicle.DocId}");
            var task = _db.TaskRepository.FindBy(m => m.VehicleId == vehicle.Id).FirstOrDefault();
            if (task!=null)
                throw new LogicException($"Транспорт используется в задании: {task}");
            using (var ctx = _db.BeginTransaction())
            {
                _db.VehicleActivityRepository.RemoveRange(_db.VehicleActivityRepository.FindBy(m => m.VehicleId == vehicleId));
                _db.VehicleRepository.Remove(vehicle);
                _db.SaveChanges();
                ctx.Commit();
            }
        }

        /// <summary>
        /// Удаление действия
        /// </summary>
        public void DeleteHistory(VehicleHistoryDeleteArg arg)
        {
            var activity = _db.VehicleActivityRepository
                .FindBy(m => m.VehicleId == arg.VehicleId && m.CreatedAt == arg.CreatedAt)
                .FirstOrDefault();
            if (activity == null)
                throw new LogicException("Действие не найдено");
            if (activity.Action == VehicleAction.Registering)
                Delete(activity.VehicleId);
            else
            {
                _db.VehicleActivityRepository.Remove(activity);
                _db.SaveChanges();
            }
        }

        public List<VehicleRegNumUIItem> GetRegNums()
        {
            return _db.VehicleRepository.GetAll().Select(m => 
                new VehicleRegNumUIItem()
                {
                    RegNum = m.RegNum,
                    Type = m.VehicleType
                }).Distinct().OrderBy(m => m.RegNum).ToList();
        }
    }
}
