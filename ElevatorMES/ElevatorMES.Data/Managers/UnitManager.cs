﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using System.Xml.Linq;
using System.Xml.Serialization;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;


namespace ElevatorMES.Data.Managers
{
    public class UnitManager : BaseManager
    {
        public UnitManager(IUnitOfWork db) => _db = db;

        /// <summary>
        /// Получение подразделений
        /// </summary>
        /// <returns></returns>
        public List<Unit> GetUnits()
        {
            return _db.UnitRepository.GetAll().Where(m => !m.Deleted).ToList();
        }

        /// <summary>
        /// Получение точек отгрузки и погрузки
        /// </summary>
        /// <returns></returns>
        public List<UnitUIItem> GetPoints()
        {
            return _db.ExecuteQuery<UnitUIItem>("EXEC dbo.GetPoints");
        }

        /// <summary>
        /// Получение связи подразделение-оборудование
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public List<UnitEquipmentUIItem> GetUnitEquipments([FromUri] GetUnitEquipmentsArg arg)
        {
            var all = _db.UnitEquipmentLinkRepository.GetAll()
                .Include(m => m.Unit)
                .Include(m => m.Equipment);
            if (arg != null)
                all.Where(m => !arg.UnitId.HasValue || m.UnitId == arg.UnitId.Value);

            var res = all.Select(m => new UnitEquipmentUIItem()
            {
                UnitId = m.UnitId,
                UnitDisplayName = m.Unit.DisplayName,
                EquipmentId = m.EquipmentId,
                EquipmentDisplayName = m.Equipment.DisplayName,
                IsContainer = (m.LinkType & UnitEquipmentLinkType.Container) == UnitEquipmentLinkType.Container,
                IsRouteSource = (m.LinkType & UnitEquipmentLinkType.RouteSource) == UnitEquipmentLinkType.RouteSource,
                IsRouteDestination = (m.LinkType & UnitEquipmentLinkType.RouteDestination) == UnitEquipmentLinkType.RouteDestination
            }).ToList();
            return res;
        }

        /// <summary>
        /// Обновление связей подразделение-оборудование
        /// </summary>
        /// <param name="arg"></param>
        public void UpdateUnitEquipments(UnitEquipmentUpdateArg arg)
        {
            var link = _db.UnitEquipmentLinkRepository.GetByIds(arg.UnitId, arg.EquipmentId);
            if (link == null)
            {
                if (arg.LinkType != UnitEquipmentLinkType.None)
                {
                    link = new UnitEquipmentLink()
                    {
                        UnitId = arg.UnitId,
                        EquipmentId = arg.EquipmentId,
                        LinkType = arg.LinkType
                    };
                    _db.UnitEquipmentLinkRepository.Add(link);
                }
                else
                    return;
            }
            else
            {
                if (arg.LinkType == UnitEquipmentLinkType.None)
                    _db.UnitEquipmentLinkRepository.Remove(link);
                else
                {
                    link.LinkType = arg.LinkType;
                    _db.UnitEquipmentLinkRepository.Update(link);
                }
            }
            _db.SaveChanges();
        }
    }
}
