﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using System.Xml.Linq;
using System.Xml.Serialization;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Data.Managers
{
    public class EquipmentManager : BaseManager
    {
        public EquipmentManager(IUnitOfWork db) => _db = db;

        /// <summary>
        /// Получение оборудования
        /// </summary>
        /// <returns></returns>
        public List<Equipment> GetEquipments()
        {
            return _db.EquipmentRepository.GetAll().ToList();
        }

        /// <summary>
        /// Получение статистики по работе оборудования
        /// </summary>
        /// <returns></returns>
        public List<EquipmentStatUIItem> GetEquipmentStats(string equipmentId, DateTime begin, DateTime end)
        {
            return _db.ExecuteQuery<EquipmentStatUIItem>("EXEC dbo.GetEquipmentStats @EquipmentId, @Begin, @End",
                new SqlParameter("@" + nameof(equipmentId), equipmentId),
                new SqlParameter("@" + nameof(begin), begin),
                new SqlParameter("@" + nameof(end), end));
        }
    }
}
