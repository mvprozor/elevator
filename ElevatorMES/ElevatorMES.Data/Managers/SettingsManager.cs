﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using System.Xml.Linq;
using System.Xml.Serialization;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Data.Managers
{
    public class SettingsManager : BaseManager
    {
        public SettingsManager(IUnitOfWork db) => _db = db;

        /// <summary>
        /// Получение списка транспорта
        /// </summary>
        /// <returns></returns>
        public List<NomenclaturePriorityUIItem> GetNomenclaturePriority()
        {
            return _db.ExecuteQuery<NomenclaturePriorityUIItem>("EXEC dbo.GetNomenclaturePriority");
        }

        /// <summary>
        /// Обновление приоритета
        /// </summary>
        /// <param name="arg"></param>
        public void UpdateNomenclaturePriority(NomenclaturePriorityUpdateArg arg)
        {
            var nomenclature = _db.NomenclatureRepository.GetById(arg.NomenclatureId, true);
            var priority = _db.NomenclaturePriorityRepository.FindBy(m => m.NomenclatureId == arg.NomenclatureId)
                .FirstOrDefault();
            if (priority == null)
            {
                if (arg.Priority.HasValue)
                {
                    priority = new NomenclaturePriority()
                    {
                        Nomenclature = nomenclature,
                        Priority = arg.Priority.Value
                    };
                    _db.NomenclaturePriorityRepository.Add(priority);
                }
            }
            else
            {
                if (arg.Priority.HasValue)
                {
                    priority.Priority = arg.Priority.Value;
                    _db.NomenclaturePriorityRepository.Update(priority);
                }
                else
                {
                    _db.NomenclaturePriorityRepository.Remove(priority);
                }
            }
            _db.SaveChanges();
        }
    }
}
