﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Globalization;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Data.Repositories;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;



namespace ElevatorMES.Data.Managers
{
    public class TaskManager : BaseManager
    {
        public TaskManager(IUnitOfWork db) => _db = db;

        /// <summary>
        /// Приемка сырья/Поступление информации о ТС от 1С
        /// </summary>
        /// <param name="m"></param>

        public string RegVehicle(RegVehicleArg m)
        {
            // todo Проверить модель на Валидность, чтоб не зависеть от Web.api фильтра ApiValidateModelAttribute
            var item = String.IsNullOrWhiteSpace(m.NomenclatureCode1C) ? null : _db.NomenclatureRepository.GetOrCreateByCode1C(m.NomenclatureCode1C);
            var vehType = _db.VehicleTypeRepository.GetOrCreateByCode1C(m.VehicleTypeCode1C);
            var user = _db.UserRepository.GetOrCreateByDisplayName(m.User);

            var veh = _db.VehicleRepository.FindBy(r =>
                r.RegNum == m.VehicleRegNum
                && r.LeftAt == null /*На территории!*/
                && r.VehicleTypeId == vehType.Id).SingleOrDefault();

            if (veh != null)
            {
                veh.DocId = m.DocId;
                veh.User = user;

                veh.Sync1CAt = DateTime.Now;
            }
            else
            {
                veh = new Vehicle()
                {
                    DocId = m.DocId,
                    RegNum = m.VehicleRegNum,
                    VehicleType = vehType,
                    User = user,
                    Sync1CAt = DateTime.Now
                };
                _db.VehicleRepository.Add(veh);
            }

            var vehActivity = new VehicleActivity()
            {
                Vehicle = veh,
                Action = VehicleAction.Registering,
                Nomenclature = item,
                UnitId = null,
                Weight = null
            };
            _db.VehicleActivityRepository.Add(vehActivity);

            _db.SaveChanges();

            return m.DocId;
        }

        /// <summary>
        /// Приемка сырья/Поступление задания на приемку сырья от 1С
        /// </summary>
        /// <param name="m"></param>
        public string RowAccept(RowAcceptArg m)
        {
            // todo Проверить модель на Валидность, чтоб не зависеть от Web.api фильтра ApiValidateModelAttribute

            var destUnit = _db.UnitRepository.GetByCode1C(m.DestUnitCode1C);
            if (destUnit == null)
            {
                throw new LogicException($"Неизвестный пункт приёма {m.DestUnitCode1C}");
            }

            var item = _db.NomenclatureRepository.GetOrCreateByCode1C(m.NomenclatureCode1C);
            var organization = _db.OrganizationRepository.GetOrCreateByCode1C(m.OrganizationCode1C);
            var user = _db.UserRepository.GetOrCreateByDisplayName(m.User);
            var lot = _db.LotRepository.FindBy(r => r.LotNo == m.LotNo).SingleOrDefault();
            if (lot == null)
            {
                lot = new Lot()
                {
                    Organization = organization,
                    Nomenclature = item,
                    LotNo = m.LotNo ?? $@"{m.VehicleRegNum}_{m.NomenclatureCode1C}_{m.OrganizationCode1C}_{DateTime.Now:ddMMMyy HH:mm:ss}"
                };
                _db.LotRepository.Add(lot);
            }

            var veh = GetCreateVehicle(m.VehicleRegNum, m.DocId, user);

            var tsk = _db.TaskRepository.FindBy(r => r.DocId == m.DocId && m.DocId != null).SingleOrDefault();
            if (tsk == null)
            {
                tsk = new Task()
                {
                    Type = TaskType.Receiving,
                    DocId = m.DocId,
                    Sync1CAt = DateTime.Now,
                    Vehicle = veh,
                    Nomenclature = item,
                    DestUnit = destUnit,
                    SourceLot = lot,
                    DestLot = lot,
                    IsDrying = m.IsDrying,
                    IsQualityControl = m.IsQualityControl,
                    //IsSorting = m.IsSorting,
                    IsScalping = m.IsScalping,
                    IsSeparating = m.IsSeparating,
                    User = user,
                    ExecuteAt = DateTime.Now,
                    Organization = organization,
                    Comment = m.Comment
                };
                _db.TaskRepository.Add(tsk);
            }
            else
                switch (tsk.Status)
                {
                    case TaskStatus.Queued:
                    case TaskStatus.Deleted:
                        tsk.DocId = m.DocId;
                        tsk.Vehicle = veh;
                        tsk.Nomenclature = item;
                        tsk.DestUnit = destUnit;
                        tsk.DestLot = lot;
                        tsk.SourceLot = lot;
                        tsk.ExecuteAt = DateTime.Now;
                        tsk.IsDrying = m.IsDrying;
                        tsk.IsQualityControl = m.IsQualityControl;
                        //tsk.IsSorting = m.IsSorting;
                        tsk.IsScalping = m.IsScalping;
                        tsk.IsSeparating = m.IsSeparating;
                        tsk.User = user;
                        tsk.Status = TaskStatus.Queued;
                        tsk.Sync1CAt = DateTime.Now;
                        tsk.Comment += " " + m.Comment;
                        break;
                    case TaskStatus.Running:
                    case TaskStatus.Completed:
                    case TaskStatus.Failed:
                        throw new LogicException($"Задание DocId={m.DocId} {tsk.Status.GetDescription()}. Обновление запрещено.");
                }

            _db.SaveChanges();
            return m.DocId;
        }

        private Vehicle GetCreateVehicle(string vehicleRegNum, string docId, User user)
        {
            var veh = _db.VehicleRepository.FindBy(r => r.LeftAt == null && r.RegNum == vehicleRegNum).FirstOrDefault();
            if (veh == null)
            {
                // Ищем в истории машину с данным номером
                int? vehTypeId;

                var vehType = _db.VehicleRepository.LastCreated(r => r.RegNum == vehicleRegNum);
                if (vehType != null)
                {
                    vehTypeId = vehType.Id;
                }
                else
                {
                    vehTypeId = _db.VehicleTypeRepository.GetAll().Where(r => r.Class == VehicleClass.Truck)
                        .GroupBy(r => r.Id)
                        .Select(g => new
                        {
                            vehTypeId = g.Key,
                            count = g.Count()
                        }).OrderByDescending(r => r.count).FirstOrDefault()?.vehTypeId;
                    if (vehTypeId == null)
                    {
                        throw new LogicException("В бд отсутствует хотя бы тип автомобиля!");
                    }
                }

                veh = new Vehicle()
                {
                    DocId = docId,
                    RegNum = vehicleRegNum,
                    VehicleTypeId = vehTypeId.Value,
                    User = user,
                    Sync1CAt = DateTime.Now
                };
                _db.VehicleRepository.Add(veh);
            }

            return veh;
        }

        /// <summary>
        /// Создание задания на перемещение между силосами
        /// </summary>
        /// <param name="m"></param>
        public string SilosMoving(SilosMovingArg m)
        {
            // todo Проверить модель на Валидность, чтоб не зависеть от Web.api фильтра ApiValidateModelAttribute
            var sourceUnit = _db.UnitRepository.GetByCode1C(m.SourceUnitCode1C);
            if (sourceUnit == null)
                throw new LogicException($"Не найден источник с SourceUnitCode1C={m.SourceUnitCode1C}");

            var lotId = _db.UnitLayerRepository.FirstCreated(r => r.UnitId == sourceUnit.Id)?.LotId;
            if (lotId == null)
            {
                throw new LogicException($"В силосе {m.SourceUnitCode1C} отсутствуют слои");
            }

            var silosRemain = GetSilosRemain(sourceUnit.Id);

            if (silosRemain < (float)m.Quantity)
            {
                throw new LogicException($"Остаток в силосе {silosRemain} < требуемого для перемещения по заданию {m.Quantity}");
            }

            var destUnit = _db.UnitRepository.GetByCode1C(m.DestUnitCode1C);
            if (destUnit == null)
                throw new LogicException($"Не найден приёмник с DestUnitCode1C={m.DestUnitCode1C}");

            var item = _db.NomenclatureRepository.GetOrCreateByCode1C(m.NomenclatureCode1C);
            var user = _db.UserRepository.GetOrCreateByDisplayName(m.User);

            var tsk = _db.TaskRepository.FindBy(r => r.DocId == m.DocId && m.DocId != null).SingleOrDefault();
            if (tsk == null)
            {
                tsk = new Task()
                {
                    Type = TaskType.SilosMoving,
                    DocId = m.DocId,
                    Sync1CAt = DateTime.Now,
                    Vehicle = null,
                    Nomenclature = item,
                    SourceUnit = sourceUnit,
                    SourceLotId = lotId,
                    DestUnit = destUnit,
                    DestLotId = lotId,
                    IsDrying = false,
                    IsScalping = false,
                    IsSeparating = false,
                    User = user,
                    ExecuteAt = m.ExecuteAt,
                    Organization = null,
                    Comment = m.Comment,
                    Quantity = m.Quantity
                };
                _db.TaskRepository.Add(tsk);
            }
            else
                switch (tsk.Status)
                {
                    case TaskStatus.Queued:
                    case TaskStatus.Deleted:
                        tsk.DocId = m.DocId;
                        tsk.Vehicle = null;
                        tsk.Nomenclature = item;
                        tsk.DestUnit = destUnit;
                        tsk.DestLotId = lotId;
                        tsk.SourceLotId = lotId;
                        tsk.ExecuteAt = m.ExecuteAt;
                        tsk.IsDrying = false;
                        tsk.IsScalping = false;
                        tsk.IsSeparating = false;
                        tsk.User = user;
                        tsk.Status = TaskStatus.Queued;
                        tsk.Sync1CAt = DateTime.Now;
                        tsk.Comment += " " + m.Comment;
                        tsk.Quantity = m.Quantity;
                        break;
                    case TaskStatus.Running:
                    case TaskStatus.Completed:
                    case TaskStatus.Failed:
                        throw new LogicException($"Задание DocId={m.DocId} {tsk.Status.GetDescription()}. Обновление запрещено.");
                }

            _db.SaveChanges();
            return m.DocId;
        }

        /// <summary>
        /// Отгрузка сырья на КЗ
        /// </summary>
        /// <param name="m"></param>
        public string PlantMoving(PlantMovingArg m)
        {
            // todo Проверить модель на Валидность, чтоб не зависеть от Web.api фильтра ApiValidateModelAttribute
            var sourceUnit = _db.UnitRepository.GetByCode1C(m.SourceUnitCode1C);
            if (sourceUnit == null)
                throw new LogicException($"Не найден источник с SourceUnitCode1C={m.SourceUnitCode1C}");

            var item = _db.NomenclatureRepository.GetOrCreateByCode1C(m.NomenclatureCode1C);
            var user = _db.UserRepository.GetOrCreateByDisplayName(m.User);
            var unitPlant = _db.UnitRepository.GetPlant();


            var tsk = _db.TaskRepository.FindBy(r => r.DocId == m.DocId && m.DocId != null).SingleOrDefault();
            if (tsk == null)
            {
                tsk = new Task()
                {
                    Type = TaskType.PlantMoving,
                    DocId = m.DocId,
                    Sync1CAt = DateTime.Now,
                    Vehicle = null,
                    Nomenclature = item,
                    SourceUnit = sourceUnit,
                    SourceLot = null,
                    DestUnit = unitPlant,
                    DestLot = null,
                    IsDrying = false,
                    IsScalping = false,
                    IsSeparating = false,
                    User = user,
                    ExecuteAt = m.ExecuteAt,
                    Organization = null,
                    Comment = m.Comment,
                    Quantity = m.Quantity
                };
                _db.TaskRepository.Add(tsk);
            }
            else
                switch (tsk.Status)
                {
                    case TaskStatus.Queued:
                    case TaskStatus.Deleted:
                        tsk.DocId = m.DocId;
                        tsk.Vehicle = null;
                        tsk.Nomenclature = item;
                        tsk.DestUnit = unitPlant;
                        tsk.DestLot = null;
                        tsk.SourceLot = null;
                        tsk.ExecuteAt = m.ExecuteAt;
                        tsk.IsDrying = false;
                        tsk.IsScalping = false;
                        tsk.IsSeparating = false;
                        tsk.User = user;
                        tsk.Status = TaskStatus.Queued;
                        tsk.Sync1CAt = DateTime.Now;
                        tsk.Comment += " " + m.Comment;
                        tsk.Quantity = m.Quantity;
                        break;
                    case TaskStatus.Running:
                    case TaskStatus.Completed:
                    case TaskStatus.Failed:
                        throw new LogicException($"Задание DocId={m.DocId} {tsk.Status.GetDescription()}. Обновление запрещено.");
                }

            _db.SaveChanges();
            return m.DocId;
        }

        /// <summary>
        /// Отгрузка сырья в рукава
        /// </summary>
        /// <param name="m"></param>
        public string Sleeve(SleeveArg m)
        {
            // todo Проверить модель на Валидность, чтоб не зависеть от Web.api фильтра ApiValidateModelAttribute
            var sourceUnit = _db.UnitRepository.GetByCode1C(m.SourceUnitCode1C);
            if (sourceUnit == null)
                throw new LogicException($"Не найден источник с SourceUnitCode1C={m.SourceUnitCode1C}");

            var destUnit = _db.UnitRepository.GetByCode1C(m.DestUnitCode1C);
            if (destUnit == null)
                throw new LogicException($"Не найден приёмник с destUnitCode1C={m.DestUnitCode1C}");

            var item = _db.NomenclatureRepository.GetOrCreateByCode1C(m.NomenclatureCode1C);
            var user = _db.UserRepository.GetOrCreateByDisplayName(m.User);


            var veh = GetCreateVehicle(m.VehicleRegNum, m.DocId, user);

            var tsk = _db.TaskRepository.FindBy(r => r.DocId == m.DocId && m.DocId != null).SingleOrDefault();
            if (tsk == null)
            {
                tsk = new Task()
                {
                    Type = TaskType.Sleeve,
                    DocId = m.DocId,
                    Sync1CAt = DateTime.Now,
                    Vehicle = veh,
                    Nomenclature = item,
                    SourceUnit = sourceUnit,
                    SourceLot = null,
                    //DestUnit = destUnit,
                    ExternalUnit = destUnit,
                    DestLot = null,
                    IsDrying = false,
                    IsScalping = false,
                    IsSeparating = false,
                    User = user,
                    ExecuteAt = m.ExecuteAt,
                    Organization = null,
                    Comment = m.Comment,
                    Quantity = m.Quantity                    
                };
                _db.TaskRepository.Add(tsk);
            }
            else
                switch (tsk.Status)
                {
                    case TaskStatus.Queued:
                    case TaskStatus.Deleted:
                        tsk.DocId = m.DocId;
                        tsk.Vehicle = veh;
                        tsk.Nomenclature = item;
                        tsk.DestUnit = destUnit;
                        tsk.DestLot = null;
                        tsk.SourceUnit = sourceUnit;
                        tsk.SourceLot = null;
                        tsk.ExecuteAt = m.ExecuteAt;
                        tsk.IsDrying = false;
                        tsk.IsScalping = false;
                        tsk.IsSeparating = false;
                        tsk.User = user;
                        tsk.Status = TaskStatus.Queued;
                        tsk.Sync1CAt = DateTime.Now;
                        tsk.Comment += " " + m.Comment;
                        tsk.Quantity = m.Quantity;
                        break;
                    case TaskStatus.Running:
                    case TaskStatus.Completed:
                    case TaskStatus.Failed:
                        throw new LogicException($"Задание DocId={m.DocId} {tsk.Status.GetDescription()}. Обновление запрещено.");
                }

            _db.SaveChanges();

            return m.DocId;
        }

        /// <summary>
        /// Отгрузка сырья внешним потребителям
        /// </summary>
        /// <param name="m"></param>
        public string Shipping(ShippingArg m)
        {
            // todo Проверить модель на Валидность, чтоб не зависеть от Web.api фильтра ApiValidateModelAttribute
            Unit sourceUnit = null;
            if (!String.IsNullOrEmpty(m.SourceUnitCode1C))
            {
                sourceUnit = _db.UnitRepository.GetByCode1C(m.SourceUnitCode1C);
                if (sourceUnit == null)
                    throw new LogicException($"Не найден источник с SourceUnitCode1C={m.SourceUnitCode1C}");
            }

            if (String.IsNullOrEmpty(m.OrganizationCode1C))
                throw new LogicException($"Не указана организация");

            if (String.IsNullOrEmpty(m.ContragentCode1C))
                throw new LogicException($"Не указан контрагент");


            var item = _db.NomenclatureRepository.GetOrCreateByCode1C(m.NomenclatureCode1C);
            var user = _db.UserRepository.GetOrCreateByDisplayName(m.User);
            var veh = GetCreateVehicle(m.VehicleRegNum, m.DocId, user);
            var organization = _db.OrganizationRepository.GetOrCreateByCode1C(m.OrganizationCode1C);
            var contragent = _db.ContragentsRepository.GetOrCreateByCode1C(m.ContragentCode1C);

            var tsk = _db.TaskRepository.FindBy(r => r.DocId == m.DocId && m.DocId != null).SingleOrDefault();
            if (tsk == null)
            {
                tsk = new Task()
                {
                    Type = TaskType.ShippingExt,
                    DocId = m.DocId,
                    Sync1CAt = DateTime.Now,
                    Vehicle = veh,
                    Nomenclature = item,
                    SourceUnit = sourceUnit,
                    SourceLot = null,
                    DestUnit = null,
                    DestLot = null,
                    IsDrying = false,
                    IsScalping = false,
                    IsSeparating = false,
                    User = user,
                    ExecuteAt = m.ExecuteAt,
                    Organization = organization,
                    Contragent = contragent,
                    Comment = m.Comment,
                    Quantity = m.Quantity
                };
                _db.TaskRepository.Add(tsk);
            }
            else
                switch (tsk.Status)
                {
                    case TaskStatus.Queued:
                    case TaskStatus.Deleted:
                        tsk.DocId = m.DocId;
                        tsk.Vehicle = veh;
                        tsk.Nomenclature = item;
                        tsk.DestUnit = null;
                        tsk.DestLot = null;
                        tsk.SourceUnit = sourceUnit;
                        tsk.SourceLot = null;
                        tsk.ExecuteAt = m.ExecuteAt;
                        tsk.IsDrying = false;
                        tsk.IsScalping = false;
                        tsk.IsSeparating = false;
                        tsk.User = user;
                        tsk.Status = TaskStatus.Queued;
                        tsk.Sync1CAt = DateTime.Now;
                        tsk.Comment += " " + m.Comment;
                        tsk.Quantity = m.Quantity;
                        break;
                    case TaskStatus.Running:
                    case TaskStatus.Completed:
                    case TaskStatus.Failed:
                        throw new LogicException($"Задание DocId={m.DocId} {tsk.Status.GetDescription()}. Обновление запрещено.");
                }

            _db.SaveChanges();

            return m.DocId;
        }

        /// <summary>
        /// Ручное задание
        /// </summary>
        public TaskUIItem CreateManual(TaskManualArg m)
        {
            // сгенерируем DocId
            var docId = Guid.NewGuid().ToString();

            var nomenclature = _db.NomenclatureRepository.GetById(m.NomenclatureId, true);
            var sourceUnit = _db.UnitRepository.GetById(m.UnitSourceId, true);
            var destUnit = _db.UnitRepository.GetById(m.UnitDestId, true);
            var extUnit = m.UnitExternalId == 0 ? null : _db.UnitRepository.GetById(m.UnitExternalId, true);

            // получим пользователя
            var user = _db.UserRepository.GetOrCreateByDisplayName(m.User);

            var task = new Task()
            {
                Type = m.TaskType,
                DocId = docId,
                Nomenclature = nomenclature,
                SourceUnit = sourceUnit,
                DestUnit = destUnit,
                ExternalUnit = extUnit,
                IsDrying = m.IsDrying,
                IsScalping = m.IsScalping,
                IsSeparating = m.IsSeparating,
                IsQualityControl = true,
                User = user,
                ExecuteAt = DateTime.Today,
                Quantity = m.Quantity,
                Comment = m.Comment
            };

            if ((m.TaskType == TaskType.Receiving || m.TaskType == TaskType.ShippingExt || m.TaskType == TaskType.Sleeve)
                && !String.IsNullOrWhiteSpace(m.VehicleRegNum))
            {
                var vehicle = GetCreateVehicle(m.VehicleRegNum, docId, user);
                vehicle.Sync1CAt = null;
                task.Vehicle = vehicle;
			}
            
            if (m.TaskType == TaskType.Receiving || m.TaskType == TaskType.ShippingExt)
            {
                var organization = _db.OrganizationRepository.GetById(m.OrganizationId, true);
                var contragent = _db.ContragentsRepository.GetById(m.ContragentId, true);

                task.Organization = organization;
                task.Contragent = contragent;

                if (m.TaskType == TaskType.Receiving)
                {
                    // сгенерируем номер партии, используя ID
                    var lot = new Lot()
                    {
                        Nomenclature = nomenclature,
                        Organization = organization,
                        //LotNo = $@"{m.VehicleRegNum?.Truncate(20)}_{nomenclature.Id}_{organization.Id}_{DateTime.Now:ddMMMyy HH:mm:ss}"
                    };
                    lot.GenerateLotNo();
                    _db.LotRepository.Add(lot);
                    task.SourceLot = lot;
                    task.DestLot = lot;
                }
            }

            _db.TaskRepository.Add(task);

            _db.SaveChanges();

            return GetTaskUI(new GetTaskArg() { TaskId = task.Id }).FirstOrDefault();
        }

        /// <summary>
        /// Задание из ПЛК
        /// </summary>
        public TaskUIItem CreatePLC(TaskPLCArg m)
        {
            var nomenclature = _db.NomenclatureRepository.GetByCode1C(m.Nomenclature);
            if (nomenclature == null)
                throw new LogicException($"Неизвестный код номенклатуры: {m.Nomenclature}.");
            var unitDest = _db.UnitRepository.FindBy(n => n.Type == UnitType.CompoundFeedPlant).FirstOrDefault();
            if (unitDest == null)
                throw new LogicException($"Комбикормовый завод не найден в подразделениях.");
            var unitSourceSettings = _db.UnitSettingRepository.FindBy(n => n.NomenclatureId == nomenclature.Id).FirstOrDefault();
            if (unitSourceSettings == null)
                unitSourceSettings = _db.UnitSettingRepository.FindBy(n => n.NomenclatureId.HasValue).FirstOrDefault();
            if (unitSourceSettings == null)
                throw new LogicException($"Нет силосов с номенклатурой {nomenclature.DisplayName}.");

            var manual = new TaskManualArg()
            {
                Quantity = m.Quantity,
                NomenclatureId = nomenclature.Id,
                UnitDestId = unitDest.Id,
                UnitSourceId = unitSourceSettings.UnitId,
                TaskType = TaskType.PlantMoving,
                User = m.User
            };
            return CreateManual(manual);
        }

        /// <summary>
        /// Удаление задания
        /// </summary>
        public void Delete(int taskId)
        {
            var task = _db.TaskRepository.GetById(taskId, true);
            if (task.Status != TaskStatus.Queued)
                throw new LogicException($"Текущий статус задания {task} не допускает удаления");
            if (_db.TaskExecMoveRepository.GetAll().Include(m => m.TaskExec).Where(m => m.TaskExec.TaskId == taskId).Any())
                throw new LogicException($"По заданию {task} были фактические перемещения, удаление задания невозможно");
            var sourceLotId = task.SourceLotId;
            var destLotId = task.DestLotId;
            _db.TaskRepository.Remove(task);
            if (sourceLotId.HasValue)
            {
                var lot = _db.LotRepository.GetById(sourceLotId.Value);
                if (!_db.TaskRepository.FindBy(m => m.SourceLotId == sourceLotId.Value && m.Id != taskId).Any())
                    _db.LotRepository.Remove(lot);
            };
            if (destLotId.HasValue && destLotId != sourceLotId)
            {
                var lot = _db.LotRepository.GetById(destLotId.Value);
                if (!_db.TaskRepository.FindBy(m => m.DestLotId == destLotId.Value && m.Id != taskId).Any())
                    _db.LotRepository.Remove(lot);
            };
            /*if (task.VehicleId.HasValue)
            {
                var vehicle = _db.VehicleRepository.GetById(task.VehicleId.Value);
                _db.VehicleRepository.Remove(vehicle);
            }*/
            _db.SaveChanges();
        }

        /// <summary>
        /// Получение списка заданий
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public List<TaskUIItem> GetTaskUI(GetTaskArg arg)
        {
            return _db.ExecuteQuery<TaskUIItem>("EXEC dbo.GetTask @TaskId, @TaskStatus, @Begin, @End, @TextFilter",
                new SqlParameter("@" + nameof(arg.TaskId), (arg?.TaskId) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.TaskStatus), ((int?)arg?.TaskStatus) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.Begin), (arg?.Begin) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.End), (arg?.End) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.TextFilter), (arg?.TextFilter) ?? ObjNull));
        }

        /// <summary>
        /// Постановка задания на выполение
        /// </summary>
        /// <param name="taskStartArg">Параметры постановки</param>
        /// <remark>
        /// Задание фактически не запускается, переводится в статус ожидания,
        /// до получения сигнала от контроллера о запуске маршрута, т.к. запуска
        /// может и не произойти.
        /// При этом также могут быть обновлены точки отгрузки и выгрузки, место выгрузки.
        /// </remark>
        public TaskUIItem Start(TaskStartArg taskStartArg)
        {
            using (var ctx = _db.BeginTransaction())
            {
                var task = _db.TaskRepository.GetById(taskStartArg.TaskId, isThrowNotFound: true);

                if (task.Status != TaskStatus.Queued && task.Status != TaskStatus.Suspended)
                    throw new LogicException($"Задание {task} должно быть в очереди или приостановлено");

                switch (task.Type)
                {
                    case TaskType.Receiving:
                        task.DestUnitId = taskStartArg.DestUnitId ?? task.DestUnitId;
                        if (!task.DestUnitId.HasValue)
                            throw new LogicException($"Не указан уточненный склад-приемник для задания {task}");
                        task.SourceUnitId = taskStartArg.SourceUnitId ?? task.SourceUnitId;
                        if (!task.SourceUnitId.HasValue)
                            throw new LogicException($"Не указано уточненное место разгрузки для задания {task}");
                        break;
                    case TaskType.ShippingExt:
                        task.SourceUnitId = taskStartArg.SourceUnitId ?? task.SourceUnitId;
                        if (!task.SourceUnitId.HasValue)
                            throw new LogicException($"Не указан уточненный склад-источник для задания {task}");
                        task.DestUnitId = taskStartArg.DestUnitId ?? task.DestUnitId;
                        if (!task.DestUnitId.HasValue)
                            throw new LogicException($"Не указано уточненное место погрузки для задания {task}");
                        break;
                    case TaskType.SilosMoving:
                        task.DestUnitId = taskStartArg.DestUnitId ?? task.DestUnitId;
                        if (!task.DestUnitId.HasValue)
                            throw new LogicException($"Не указан уточненный склад-приемник для задания {task}");
                        task.SourceUnitId = taskStartArg.SourceUnitId ?? task.SourceUnitId;
                        if (!task.SourceUnitId.HasValue)
                            throw new LogicException($"Не указан уточненный склад-источник для задания {task}");
                        break;
                    case TaskType.Sleeve:
                    case TaskType.PlantMoving:
                        task.SourceUnitId = taskStartArg.SourceUnitId ?? task.SourceUnitId;
                        if (!task.SourceUnitId.HasValue)
                            throw new LogicException($"Не указан уточненный склад-источник для задания {task}");
                        task.DestUnitId = taskStartArg.DestUnitId ?? task.DestUnitId;
						if (!task.DestUnitId.HasValue)
							throw new LogicException($"Не указано уточненное место погрузки для задания {task}");
						break;

                }
                task.Status = TaskStatus.Pending; // до получения сигнала о запуске маршрута

                var now = DateTime.Now;

                var lastTaskExec = _db.TaskExecRepository.LastCreated(r => r.TaskId == task.Id);
                if (lastTaskExec != null)
                {
                    lastTaskExec.End = now;
                    _db.TaskExecRepository.Update(lastTaskExec);
                }

                _db.TaskRepository.Update(task);

                var taskExec = new TaskExec()
                {
                    Task = task,
                    Status = task.Status,
                    Start = now,
                    User = _db.UserRepository.GetOrCreateByDisplayName(taskStartArg.User)
                };
                _db.TaskExecRepository.Add(taskExec);

                _db.SaveChanges();

                ctx.Commit();
            }
            return _db.ExecuteQuery<TaskUIItem>("EXEC dbo.GetTask @TaskId",
                new SqlParameter("@TaskId", taskStartArg.TaskId)).Single();
        }


        /// <summary>
        /// Отметка о фактическом начале выполнения задания (начало работы маршрута)
        /// </summary>
        /// <param name="taskRunArg">Параметры выполнения</param>
        public TaskUIItem Run(TaskRunArg taskRunArg)
        {
            using (var ctx = _db.BeginTransaction())
            {
                var task = _db.TaskRepository.GetById(taskRunArg.TaskId, isThrowNotFound: true);

                if (task.Status != TaskStatus.Running)
                {

                    if (task.Status != TaskStatus.Suspended && task.Status != TaskStatus.Pending)
                        throw new LogicException($"Задание {task} должно находится в состоянии ожидания или быть приостановлено");

                    task.Status = TaskStatus.Running;

                    var now = DateTime.Now;

                    var lastTaskExec = _db.TaskExecRepository.LastCreated(r => r.TaskId == task.Id);
                    if (lastTaskExec != null)
                    {
                        lastTaskExec.End = now;
                        _db.TaskExecRepository.Update(lastTaskExec);
                    }

                    _db.TaskRepository.Update(task);

                    var taskExec = new TaskExec()
                    {
                        Task = task,
                        Status = task.Status,
                        User = _db.UserRepository.GetOrCreateByDisplayName(taskRunArg.User),
                        Start = now
                    };
                    _db.TaskExecRepository.Add(taskExec);

                    _db.SaveChanges();

                    ctx.Commit();
                }
            }
            return _db.ExecuteQuery<TaskUIItem>("EXEC dbo.GetTask @TaskId",
                new SqlParameter("@TaskId", taskRunArg.TaskId)).Single();
        }

		/// <summary>
		/// Фиксация веса по заданию (для внутреннего использования)
		/// </summary>
		/// <param name="taskExec">Выполнение задания</param>
		/// <param name="weight">Вес</param>
		public void RegWeightInternal(TaskExec taskExec, decimal weight, DateTime at)
		{
			var task = taskExec.Task;
			var lastTaskRun = _db.TaskExecRepository.LastCreated(r => r.TaskId == task.Id 
                && r.Status == TaskStatus.Running && r.End<=taskExec.Start);
			var taskExecMove = new TaskExecMove()
			{
                MovedAt = at,
				Quantity = weight,
				TaskExec = lastTaskRun ?? taskExec,
				SourceUnitId = task.SourceUnitId,
				SourceVehicleId = task.VehicleId,
				DestUnitId = task.DestUnitId
			};

            switch (task.Type)
            {
                case TaskType.Receiving:
                    {
                        var lotId = task.DestLotId.Value;
                        taskExecMove.LotId = lotId;
                        var silos = GetUnit(task.DestUnitId);

                        AddLayerOrIncreaseQuantityOfLastCreatedUnitLayer(silos.Id, lotId, weight);

                        // добавляем выгрузку, если ее еще не было:
                        if (!_db.VehicleActivityRepository.FindBy(va =>
                            va.VehicleId == task.VehicleId && va.Action == VehicleAction.Unloading).Any())
                        {
                            var vehicle = _db.VehicleRepository.GetById(task.VehicleId.Value);
                            _db.VehicleActivityRepository.Add(new VehicleActivity()
                            {
                                Action = VehicleAction.Unloading,
                                Vehicle = vehicle,
                                NomenclatureId = task.NomenclatureId,
                                UnitId = task.DestUnitId,
                                CreatedAt = at
                            });
                        }

                        break;
                    }
                case TaskType.SilosMoving:
                    {
                        var lotId = GetSourceLotIdOrFirstUnitLayerLotId(task);
                        var sourceSilos = GetUnit(task.SourceUnitId);
                        var destSilos = GetUnit(task.DestUnitId);

                        taskExecMove.LotId = lotId;

                        var weightMoved = Move(sourceSilos.Id, destSilos.Id, lotId, weight);
                        taskExecMove.Quantity = weightMoved;
                        break;
                    }
                case TaskType.PlantMoving:
                    {
                        var lotId = GetSourceLotIdOrFirstUnitLayerLotId(task);
                        var sourceSilos = GetUnit(task.SourceUnitId);
                        var plant = _db.UnitRepository.GetPlant();

                        taskExecMove.LotId = lotId;
                        taskExecMove.DestUnitId = plant.Id;

                        var weightMoved = RemoveLayerOrDecreaseQuantityOfFirstCreatedUnitLayer(sourceSilos.Id, lotId, weight);
                        taskExecMove.Quantity = weightMoved;
                        break;
                    }
                case TaskType.Sleeve:
                    {
                        var lotId = GetSourceLotIdOrFirstUnitLayerLotId(task);
                        var sourceSilos = GetUnit(task.SourceUnitId);
                        if (task.DestUnitId.HasValue)
                        {
                            taskExecMove.DestUnitId = task.DestUnitId;
                        }
                        else if (task.VehicleId.HasValue)
                        {
                            taskExecMove.DestVehicleId = task.VehicleId;
                        }
                        else
                        {
                            throw new LogicException($"Для {TaskType.Sleeve.GetDescription()} у задания требуется либо пункт назначения [UnitId] либо транспорт [Vehicle] в [Tasks]");
                        }

                        taskExecMove.LotId = lotId;

                        var weightMoved = RemoveLayerOrDecreaseQuantityOfFirstCreatedUnitLayer(sourceSilos.Id, lotId, weight);
                        taskExecMove.Quantity = weightMoved;
                        break;
                    }
                case TaskType.ShippingExt:
                    {
                        var lotId = GetSourceLotIdOrFirstUnitLayerLotId(task);
                        var sourceSilos = GetUnit(task.SourceUnitId);
                        var weightMoved = RemoveLayerOrDecreaseQuantityOfFirstCreatedUnitLayer(sourceSilos.Id, lotId, weight);

                        taskExecMove.LotId = lotId;
                        taskExecMove.Quantity = weightMoved;
                        taskExecMove.SourceVehicleId = null;
                        taskExecMove.DestVehicleId = task.VehicleId;

                        break;
                    }
                default:
                    {
                        break;
                        //throw new NotImplementedException(task.Type.ToString());
                    }
            }
			_db.TaskExecMoveRepository.Add(taskExecMove);
		}


		/// <summary>
		/// Фиксация веса по заданию
		/// </summary>
		/// <param name="m">Параметры</param>
		public TaskUIItem RegWeight(TaskWeightArg m)
        {
			var taskId = m.TaskId;
			var task = _db.TaskRepository.GetById(taskId, isThrowNotFound: true);
			var lastTaskExec = _db.TaskExecRepository.LastCreated(r => r.TaskId == taskId)
				?? throw new LogicException($"Невозможно найти последнее состояние в журнале выполнения задания {task}");
            RegWeightInternal(lastTaskExec, m.Weight, DateTime.Now);
			_db.SaveChanges();
			var taskUI = _db.ExecuteQuery<TaskUIItem>("EXEC dbo.GetTask @TaskId",
				new SqlParameter("@TaskId", m.TaskId)).Single();
            return taskUI;
		}


		/// <summary>
		/// Снятие задания с выполнения.
		/// </summary>
		/// <param name="m">Параметры</param>
		public TaskUIItem Stop(TaskStopArg m)
        {
            var taskId = m.TaskId;
            var task = _db.TaskRepository.GetById(taskId, isThrowNotFound: true);
            var user = _db.UserRepository.GetOrCreateByDisplayName(m.User);

            if (task.Status != TaskStatus.Pending && task.Status != TaskStatus.Suspended && task.Status != TaskStatus.Running)
                throw new LogicException($"Задание {task} должно быть в состоянии ожидания, приостановлено или запущено");

            #region Выполнение задания - TaskExec
            var lastTaskExec = _db.TaskExecRepository.LastCreated(r => r.TaskId == taskId)
                ?? throw new LogicException($"Невозможно найти последнее состояние в журнале выполнения задания {task}");

			/*if (lastTaskExec.End.HasValue)
                throw new LogicException($"Завершение (приостановка) задания невозможно. Задание уже завершено ранее. Требуется: Отсутствие даты завершения фактического выполнения заданий [TaskExec][End]");

            if (lastTaskExec.Status != TaskStatus.Running)
            {
                throw new LogicException($"Завершение (приостановка) задания невозможно. Задание {lastTaskExec.Status}, а должно быть {TaskStatus.Running.GetDescription()}! В [Task][Status]. Id: {taskId}");
            }*/

			var now = DateTime.Now;

            lastTaskExec.End = now;
            _db.TaskExecRepository.Update(lastTaskExec);

            #endregion           

            if (m.Weight > 0) // && (m.IsCloseTask || m.IsReturn))
            {
                RegWeightInternal(lastTaskExec, m.Weight, now);
				if (task.Type==TaskType.ShippingExt)
                {
					// добавляем погрузку, если ее еще не было:
					if (!_db.VehicleActivityRepository.FindBy(va =>
					   va.VehicleId == task.VehicleId && va.Action == VehicleAction.Loading).Any())
					{
						var vehicle = _db.VehicleRepository.GetById(task.VehicleId.Value);
						_db.VehicleActivityRepository.Add(new VehicleActivity()
						{
							Action = VehicleAction.Loading,
							Vehicle = vehicle,
							NomenclatureId = task.NomenclatureId,
							UnitId = task.DestUnitId,
							CreatedAt = now
						});
					}
				}
			}

            if (m.IsCloseTask)
            {
                if (m.IsFailed)
                {
                    task.Status = TaskStatus.Failed;
                }
                else
                {
                    task.Status = TaskStatus.Completed;
                }
            }
            else
            {
                if (m.IsReturn)
                {
                    task.Status = TaskStatus.Queued;
                }
                else
                {
                    task.Status = TaskStatus.Suspended;
                }
            }

            var taskExec = new TaskExec()
            {
                Task = task,
                Status = task.Status,
                Start = now,
                User = user,
                Comment = m.Comment
            };
            _db.TaskExecRepository.Add(taskExec);

            _db.SaveChanges();

            var taskUI = _db.ExecuteQuery<TaskUIItem>("EXEC dbo.GetTask @TaskId",
                new SqlParameter("@TaskId", m.TaskId)).Single();

            #region Отправка в 1С выполнения задания

            if (m.IsCloseTask)
            {
                //var taskUI = GetTaskUI(new GetTaskArg() { TaskId = taskId }).Single();

                var transmitLog = GetTransmitLog(taskUI, task.Type);
                var isSaveTransmitLog = true;

                if (transmitLog != null)
                {
                    if (task.Type == TaskType.Receiving || task.Type == TaskType.Sleeve || task.Type == TaskType.ShippingExt)
                    {
                        if (task.Sync1CAt == null) // не синхронизировалось
                        {
                            isSaveTransmitLog = false; // Ничего не отправляем
                        }
                    }

                    if (isSaveTransmitLog)
                        _db.TransmitLogRepository.Add(transmitLog);

                    _db.SaveChanges();
                }

            }

            #endregion

            return taskUI;

        }

        private Unit GetUnit(int? unitId)
        {
            return _db.UnitRepository.GetById(unitId.Value);
        }

        private decimal Move(int sourceSilosId, int destSilosId, int lotId, decimal weight)
        {
            var weightRemoved = RemoveLayerOrDecreaseQuantityOfFirstCreatedUnitLayer(sourceSilosId, lotId, weight);
            AddLayerOrIncreaseQuantityOfLastCreatedUnitLayer(destSilosId, lotId, weightRemoved);
            return weight;
        }

        /// <summary>
        /// Возвращает перемещенное количество
        /// </summary>
        private decimal RemoveLayerOrDecreaseQuantityOfFirstCreatedUnitLayer(int silosId, int lotId, decimal value)
        {
            var layer = _db.UnitLayerRepository.FirstCreated(r => r.UnitId == silosId);
            if (layer == null)
            {
                throw new LogicException("В силосе отсутствуют слои");
            }
            if (layer.LotId != lotId)
            {
                throw new LogicException("Партия в силосе отличается от требуемой");
            }

            var layerQuantity = _db.UnitLayerRepository.FindBy(r => r.UnitId == silosId).Sum(r => r.Quantity);
            if (layerQuantity < value)
            {
                _db.UnitLayerRepository.Remove(layer);

                return layerQuantity;
            }
            else
            {
                layer.Quantity -= value;
                _db.UnitLayerRepository.Update(layer);
                return value;
            }
        }

        private void AddLayerOrIncreaseQuantityOfLastCreatedUnitLayer(int silosId, int lotId, decimal weight)
        {
            var layer = _db.UnitLayerRepository.LastCreated(r => r.UnitId == silosId);
            if (layer == null) // Пустой силос
            {
                layer = new UnitLayer(silosId, lotId, weight);
            }
            else
            {
                if (layer.LotId == lotId)
                { // Та же партия
                    layer.Quantity += weight;
                }
                else
                {
                    // Изменилась партия - Новый слой
                    layer = new UnitLayer(silosId, lotId, weight);
                }
            }
            _db.UnitLayerRepository.Update(layer);
        }

        // todo Move to Interact1C class
        #region Создание записи в TransmitLog для передачи в 1C
        private TransmitLog GetTransmitLog(TaskUIItem taskUI, TaskType taskType)
        {
            XElement transmitLogXElement = null;
            MessageType? messageType = null;

            switch (taskType)
            {
                // Выполнение задания на приёмку сырья
                case TaskType.Receiving:
                    messageType = MessageType.CompletingRawMaterial;
                    transmitLogXElement = GetReceivingXElement(taskUI);
                    break;
                // Выполнение задания на перемещение сырья между силосами
                case TaskType.SilosMoving:
                    messageType = MessageType.CompletingMove;
                    transmitLogXElement = GetSilosMovingXElement(taskUI);
                    break;
                // Выполнении задания на отгрузку сырья на КЗ
                case TaskType.PlantMoving:
                    messageType = MessageType.ExecutionTaskShipmentRaw_KZ;
                    transmitLogXElement = GetPlantMovingXElement(taskUI);
                    break;

                // Выполнении задания на отгрузку сырья в рукава
                case TaskType.Sleeve:
                    messageType = MessageType.ExecutionTaskShipmentRaw_Hose;
                    transmitLogXElement = GetSleeveMovingXElement(taskUI);
                    break;

                case TaskType.ShippingExt:
                    messageType = MessageType.ExecutionTaskShipmentRaw_ExternalConsumers;
                    transmitLogXElement = GetShipmentExtXElement(taskUI);
                    break;
            }

            if (transmitLogXElement != null && messageType != null)
            {
                return new TransmitLog(transmitLogXElement, messageType.Value);
            }
            return null;
        }

        private XElement GetSleeveMovingXElement(TaskUIItem t)
        {
            var xMessage = new ServiceReference1.ExecutionTaskShipmentRaw_Hose()
            {
                GUID = t.DocId,
                Raw_Code = t.NomenclatureCode1C,
                SenderWarehouse = t.SourceUnitCode1C,
                SenderWarehouseQuantity = GetSilosRemain(t.SourceUnitId.Value),
                ReceiptWarehouse = GetReceiptWarehouse(t.ExternalUnitId),
                //ReceiptWarehouse = t.DestUnitCode1C,
                NumberVehicles = t.RegNum,
                Quantity = (float)t.QuantityActual,
                DateExecute = DateTime.Now,
                ResponsibleCode = t.LastUserName
            };

            return xMessage.ToXElement();
        }

        private string GetReceiptWarehouse(int? externalUnitId)
        {
            if (externalUnitId == null) return null;
            var q = _db.UnitRepository.GetById(externalUnitId.Value).Code1C;
            return q?.ToString();
        }

        private XElement GetPlantMovingXElement(TaskUIItem t)
        {
            var xMessage = new ServiceReference1.ExecutionTaskShipmentRaw_KZ()
            {
                GUID = t.DocId,
                Raw_Code = t.NomenclatureCode1C,

                Quantity = (float)t.QuantityActual,
                SenderWarehouse = t.SourceUnitCode1C,
                SenderWarehouseQuantity = GetSilosRemain(t.SourceUnitId.Value),


                DateExecute = DateTime.Now,
                ResponsibleCode = t.LastUserName
            };

            return xMessage.ToXElement();
        }

        private XElement GetSilosMovingXElement(TaskUIItem t)
        {
            var xMessage = new ServiceReference1.CompletingMove()
            {
                GUID = t.DocId,
                Raw_Code = t.NomenclatureCode1C,
                Quantity = (float)t.QuantityActual,
                SenderWarehouse = t.SourceUnitCode1C,
                StatusSenderWarehouse = GetSilosRemain(t.SourceUnitId.Value),

                ReceiptWarehouse = t.DestUnitCode1C,
                StatusReceiptWarehouse = GetSilosRemain(t.DestUnitId.Value),

                ExecuteDrying = t.IsDrying,
                ExecuteCleaningScalper = t.IsScalping,
                ExecuteCleaningSeparator = t.IsSeparating,
                ResponsibleCode = t.LastUserName
            };
            return xMessage.ToXElement();
        }

        private float GetSilosRemain(int unitId)
        {
            var q = _db.UnitLayerRepository.FindBy(r => r.UnitId == unitId).Select(r => r.Quantity).DefaultIfEmpty(0m).Sum();
            return (float)q;
        }

        private XElement GetReceivingXElement(TaskUIItem t)
        {
            var xMessage = new ServiceReference1.CompletingRawMaterial()
            {
                GUID = t.DocId,
                //batch_Code = t.SourceLotNo,
                NumberVehicles = t.RegNum,
                Raw_Code = t.NomenclatureCode1C,
                Quantity = (float)t.QuantityActual,
                ReceiptWarehouse = t.DestUnitCode1C,
                ReceiptWarehouseQuantity = GetSilosRemain(t.DestUnitId.Value),
                //QualityControl = t.IsSorting,
                ExecuteDrying = t.IsDrying,
                ExecuteCleaningScalper = t.IsScalping,
                ExecuteCleaningSeparator = t.IsSeparating,
                ResponsibleCode = t.LastUserName
            };
            return xMessage.ToXElement();
        }

        private XElement GetShipmentExtXElement(TaskUIItem t)
        {
            var xMessage = new ServiceReference1.ExecutionTaskShipmentRaw_ExternalConsumers()
            {
                GUID = t.DocId,
                NumberVehicles = t.RegNum,
                Quantity = (float)t.QuantityActual,
                Raw_Code = t.NomenclatureCode1C,
                SenderWarehouse = t.SourceUnitCode1C,
                SenderWarehouseQuantity = GetSilosRemain(t.SourceUnitId.Value),
                Partner = t.ContragentCode1C,
                Organization = t.OrganizationCode1C,
                DateExecute = DateTime.Now,
                ResponsibleCode = t.LastUserName
            };
            return xMessage.ToXElement();
        }
        #endregion

        /// <summary>
        /// Вернуть ID слоя для отгрузки
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        private int GetSourceLotIdOrFirstUnitLayerLotId(Task task)
        {
            if (task.SourceLotId.HasValue) return task.SourceLotId.Value;

            var layers = _db.UnitLayerRepository.GetAll().Where(r => r.UnitId == task.SourceUnitId).ToList();
            var unit = _db.UnitRepository.GetById(task.SourceUnitId.Value);
            var unitDescr = $"{unit.DisplayName} [{unit.Code1C}]";
            if (!layers.Any())
            {
                throw new LogicException($"Отгрузка из источника {unitDescr}] невозможна. Отсутствуют слои в источнике.");
            }
            var layersRemains = layers.Sum(r => r.Quantity);
            if (layersRemains < task.Quantity)
            {
                throw new LogicException($"Отгрузка из {unitDescr}] невозможна. Остаток {layersRemains} < количества по заданию {task.Quantity}.");
            }
            return layers.OrderBy(r => r.CreatedAt).First().LotId;
        }

        public string UpdatePriority(PriorityArg m)
        {
            var item = _db.NomenclatureRepository.GetOrCreateByCode1C(m.NomenclatureCode1C);
            var user = _db.UserRepository.GetOrCreateByDisplayName(m.User);

            var nomenclaturePriority = _db.NomenclaturePriorityRepository.FindBy(r => r.NomenclatureId == item.Id).SingleOrDefault();
            if (nomenclaturePriority == null)
            {
                nomenclaturePriority = new NomenclaturePriority()
                {
                    NomenclatureId = item.Id,
                    DocId = m.DocId,
                    Priority = m.Priority,
                    User = user
                };
                _db.NomenclaturePriorityRepository.Add(nomenclaturePriority);
            }
            else
            {
                nomenclaturePriority.DocId = m.DocId;
                nomenclaturePriority.Nomenclature = item;
                nomenclaturePriority.Priority = m.Priority;
                nomenclaturePriority.User = user;
            }

            _db.SaveChanges();
            return m.DocId;
        }

        /// <summary>
        /// Получение списка заданий для сопоставления
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public List<TaskMatchUIItem> GetTaskForMatching(GetTaskMatchArg arg)
        {
            return _db.ExecuteQuery<TaskMatchUIItem>("EXEC dbo.GetTaskForMatching @TaskId, @DocId, @IsFromERP, @TaskGenStatus, @TaskType, @NomenclatureId, @TextFilter, @TaskCount",
                new SqlParameter("@" + nameof(arg.TaskId), (arg?.TaskId) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.DocId), (arg?.DocId) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.IsFromERP), (arg?.IsFromERP) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.TaskGenStatus), ((int?)arg?.TaskGenStatus) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.TaskType), ((int?)arg?.TaskType) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.NomenclatureId), ((int?)arg?.NomenclatureId) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.TextFilter), (arg?.TextFilter) ?? ObjNull),
                new SqlParameter("@" + nameof(arg.TaskCount), (arg?.TaskCount) ?? ObjNull));
        }

        /// <summary>
        /// Сопоставление заданий
        /// </summary>
        /// <param name="arg">Параметры</param>
        public void MatchTask(TaskMatchArg arg)
        {
            var taskManual = _db.TaskRepository.GetById(arg.TaskManualId, true);
            var taskERP = _db.TaskRepository.GetById(arg.TaskERPId, true);
            if (taskERP.Status != TaskStatus.Queued)
                throw new LogicException("Сопоставляемое задание от 1С должно находиться в очереди.");
            using (var ctx = _db.BeginTransaction())
            {
                taskManual.DocId = taskERP.DocId;
                taskManual.Sync1CAt = taskERP.Sync1CAt.Value.AddMilliseconds(100);
                taskERP.Status = TaskStatus.Deleted;
                _db.SaveChanges();

                // Отправка в 1С выполнения задания
                if (taskManual.Status == TaskStatus.Failed || taskManual.Status == TaskStatus.Completed)
                {
                    var taskUI = GetTaskUI(new GetTaskArg() { TaskId = arg.TaskManualId }).Single();
                    var transmitLog = GetTransmitLog(taskUI, taskUI.Type);
                    if (transmitLog != null)
                    {
                        _db.TransmitLogRepository.Add(transmitLog);
                        _db.SaveChanges();
                    }
                }
                _db.SaveChanges();
                ctx.Commit();
            }
        }

        /// <summary>
        /// Откат выполнения задания
        /// </summary>
        public void Revert(int taskId)
        {
            using (var ctx = _db.BeginTransaction())
            {
                var task = _db.TaskRepository.GetById(taskId, true);
                if (task.Status != TaskStatus.Completed)
                    throw new LogicException($"Текущий статус задания {task} не допускает отката");
                var execMoves = _db.TaskExecRepository.GetAll().Where(m => m.TaskId == taskId).Join(
                    _db.TaskExecMoveRepository.GetAll(), e => e.Id, m => m.TaskExecId,
                        (e, m) => new { Exec = e, Move = m }).OrderByDescending(k => k.Move.CreatedAt).ToArray();
                if (execMoves.Length > 0)
                {
                    var lastTaskExecMove = execMoves.FirstOrDefault();
                    if (task.Type == TaskType.Receiving || task.Type == TaskType.SilosMoving)
                    {
                        var lastDestMove = _db.TaskExecMoveRepository.LastCreated(m => m.DestUnitId == task.DestUnitId);
                        if (lastDestMove?.Id == lastTaskExecMove?.Move?.Id)
                        {
                            var lastTaskDestDateMove = lastTaskExecMove?.Move?.CreatedAt;
                            if (_db.UnitInventoryRepository.GetAll()
                                .Any(m => m.UnitId == task.DestUnitId && m.CreatedAt > lastTaskDestDateMove))
                            {
                                throw new LogicException($"Откат задания {task} невозможен. В силосе-приемнике была проведена инвентаризация.");
                            }
                            var unitDestLayers = _db.UnitLayerRepository.GetAll()
                                .Where(m => m.UnitId == task.DestUnitId).OrderByDescending(m => m.CreatedAt).ToArray();
                            var layerIdx = 0;
                            var moveIdx = 0;
                            while (moveIdx < execMoves.Length && layerIdx < unitDestLayers.Length)
                            {
                                var move = execMoves[moveIdx].Move;
                                var layer = unitDestLayers[layerIdx];
                                if (move.LotId == layer.LotId && layer.Quantity >= move.Quantity)
                                {
                                    layer.Quantity -= move.Quantity;
                                    if (layer.Quantity == 0.0m)
                                        layerIdx++;
                                }
                                else
                                    break;
                                moveIdx++;
                            }
                            if (moveIdx < execMoves.Length)
                                throw new LogicException($"Откат задания {task} невозможен. В силосе-приемнике нарушена структура слоев.");
                            if (layerIdx > 0)
                                _db.UnitLayerRepository.RemoveRange(unitDestLayers.Take(layerIdx));
                            if (task.Type != TaskType.SilosMoving) // перемещения будут удалены после обработки источника
                            {
                                _db.TaskExecMoveRepository.RemoveRange(execMoves.Select(m => m.Move));
                                _db.TaskExecRepository.RemoveRange(execMoves.Select(m => m.Exec).Distinct());
                            }
                        }
                        else
                        {
                            throw new LogicException($"Откат задания {task} невозможен. Состояние силоса-приемника изменилось после выполнения других заданий.");
                        }
                    }
                    if (task.Type == TaskType.SilosMoving || task.Type == TaskType.ShippingExt
                        || task.Type == TaskType.Sleeve || task.Type == TaskType.PlantMoving)
                    {
                        var lastSrcMove = _db.TaskExecMoveRepository.LastCreated(m => m.SourceUnitId == task.SourceUnitId);
                        if (lastSrcMove?.Id == lastTaskExecMove?.Move?.Id)
                        {
                            var lastTaskSrcDateMove = lastTaskExecMove?.Move?.CreatedAt;
                            if (_db.UnitInventoryRepository.GetAll()
                                .Any(m => m.UnitId == task.SourceUnitId && m.CreatedAt > lastTaskSrcDateMove))
                            {
                                throw new LogicException($"Откат задания {task} невозможен. В силосе-источнике была проведена инвентаризация.");
                            }
                            var layer = _db.UnitLayerRepository.GetAll()
                                .Where(m => m.UnitId == task.SourceUnitId).OrderByDescending(m => m.CreatedAt).FirstOrDefault();
                            for(var moveIdx=0; moveIdx<execMoves.Length; moveIdx++)
                            {
                                var move = execMoves[moveIdx].Move;
                                if (layer != null && move.LotId == layer.LotId)
                                    layer.Quantity += move.Quantity;
                                else
                                {
                                    layer = new UnitLayer()
                                    {
                                        CreatedAt = move.CreatedAt,
                                        Lot = move.Lot,
                                        Unit = task.SourceUnit,
                                        Quantity = move.Quantity
                                    };
                                    _db.UnitLayerRepository.Add(layer);
                                }
                            }
                            _db.TaskExecMoveRepository.RemoveRange(execMoves.Select(m => m.Move));
                            _db.TaskExecRepository.RemoveRange(execMoves.Select(m => m.Exec).Distinct());
                        }
                        else
                        {
                            throw new LogicException($"Откат задания {task} невозможен. Состояние силоса-источника изменилось после выполнения других заданий.");
                        }
                    }
                }
                task.Status = TaskStatus.Queued;
                _db.SaveChanges();
                ctx.Commit();
            }
        }
    }
}
