﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Model.UI;

namespace ElevatorMES.Data.Managers
{
    public class UserManager : BaseManager
    {
        public UserManager(IUnitOfWork unitOfWork) => _db = unitOfWork;

        public IEnumerable<UserUI> GetAll()
        {
            var domainUsers = _db.UserRepository.GetAll().ToList();

            var result = domainUsers.Select(r => new UserUI()
            {
                Name = r.DisplayName
            });

            return result;
        }

        public async Task<IEnumerable<UserUI>> GetAllAsync()
        {
            var result = await System.Threading.Tasks.Task.Run(() => GetAll());

            return result;
        }

    }
}
