﻿using System;
using System.Collections.Generic;
using System.Linq;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Utilites.Exceptions;

namespace ElevatorMES.Data.Managers
{
    /// <summary>
    /// Обновление данных в MES из ERP
    /// </summary>
    public class SyncManager : BaseManager
    {
        private readonly string _nomenclatureTableName = $"{nameof(Nomenclature)}s";
        private readonly string _organizationTableName = $"{nameof(Organization)}s";
        private readonly string _contragentTableName = $"{nameof(Contragent)}s";
        private readonly string _unitTableName = $"{nameof(Unit)}s";
        private readonly string _vehicleTypeTableName = $"{nameof(VehicleType)}s";

        public SyncManager(IUnitOfWork unitOfWork) => _db = unitOfWork;

        /// <summary>
        /// Получение списка обновляемых таблиц
        /// </summary>
        /// <returns></returns>
        public List<string> GetSyncEntities() => new[] { _nomenclatureTableName, _organizationTableName, _unitTableName, _vehicleTypeTableName, _contragentTableName, }.ToList();

        /// <summary>
        /// Обновление данных заданной таблицы из ERP
        /// </summary>
        /// <param name="dictSyncArg"></param>
        public void Sync(DictSyncArg dictSyncArg)
        {
            var entityName = dictSyncArg.TableName;

            foreach (var item in dictSyncArg.Items)
            {
                BaseSyncEntity itemUpSerted = null;
                if (entityName == _nomenclatureTableName)
                {
                    itemUpSerted = _db.NomenclatureRepository.GetOrCreateByCode1C(item.Code1C);
                }
                else if (entityName == _organizationTableName)
                {
                    itemUpSerted = _db.OrganizationRepository.GetOrCreateByCode1C(item.Code1C);
                }
                else if (entityName == _unitTableName)
                {
                    var unit = _db.UnitRepository.GetOrCreateByCode1C(item.Code1C);
                    if ("Sleeves" == item.Type1C)
                    {
                        unit.Type = Domain.Enums.UnitType.SleeveStorage;
                    }
                    itemUpSerted = unit;
                }
                else if (entityName == _vehicleTypeTableName)
                {
                    itemUpSerted = _db.VehicleTypeRepository.GetOrCreateByCode1C(item.Code1C);
                }
                else if (entityName == _contragentTableName)
                {
                    itemUpSerted = _db.ContragentsRepository.GetOrCreateByCode1C(item.Code1C);
                }
                else throw new LogicException($"Таблица {entityName} не синхронизируется с 1С");

                if(itemUpSerted != null)
                {
                    itemUpSerted.Deleted = item.Deleted;
                    itemUpSerted.DisplayName = item.DisplayName;
                    itemUpSerted.CodeParent1C = item.CodeParent1C;
                    itemUpSerted.Type1C = item.Type1C;
                }
            }

            _db.SaveChanges();
        }

        public string AddLogEvent1C(LogEvent1CArg m)
        {
            var ev = new LogEvent1C()
            {
                DocId = m.DocId,
                DateMessage = m.DateMessage,
                EventType = m.EventType,
                Message = m.Message
            };
            _db.LogEvent1CRepository.Add(ev);

            _db.SaveChanges();
            return m.DocId;
        }
    }
}
