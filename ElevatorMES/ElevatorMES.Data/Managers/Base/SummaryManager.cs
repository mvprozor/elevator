﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Data.Managers.Base
{
    public class SummaryManager : BaseManager
    {
        public SummaryManager(IUnitOfWork db) => _db = db;

        /// <summary>
        /// Получение оперативной информации
        /// </summary>
        /// <returns></returns>
        public OperInfoUI GetOperInfo()
        {
            return _db.ExecuteQuery<OperInfoUI>("EXEC dbo.GetOperInfo").FirstOrDefault();
        }
    }
}
