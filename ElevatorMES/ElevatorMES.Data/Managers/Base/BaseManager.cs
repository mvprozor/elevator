﻿using ElevatorMES.Domain;
using System;

namespace ElevatorMES.Data.Managers.Base
{
    public class BaseManager
    {
        protected static readonly object ObjNull = DBNull.Value;

        protected IUnitOfWork _db;
    }
}