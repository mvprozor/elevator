﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using ElevatorMES.Data.Managers.Base;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Utilites.Helpers;


namespace ElevatorMES.Data.Managers
{
    public class TransmitLogManager : BaseManager
    {
        public TransmitLogManager(IUnitOfWork db) => _db = db;

        public List<TransmitLog> GetLogsToSend()
        {
            return _db.TransmitLogRepository.GetAll().Where(r => r.NextTryDate < DateTime.Now).ToList();
        }

        public bool Send(TransmitLog msg)
        {
            var isSendError = false;
            try
            {
                var client = GetClient();
                string result = null;

                switch (msg.MessageType)
                {
                    case MessageType.ExecutionTaskShipmentRaw_ExternalConsumers:
                        {
                            var s = msg.xMessageWrapper.FromXElement<ServiceReference1.ExecutionTaskShipmentRaw_ExternalConsumers>();
                            result = client.ExecutionTaskShipmentRaw_ExternalConsumers(s).ToString();
                            break;
                        }
                    case MessageType.ExecutionTaskShipmentRaw_Hose:
                        {
                            var s = msg.xMessageWrapper.FromXElement<ServiceReference1.ExecutionTaskShipmentRaw_Hose>();
                            result = client.ExecutionTaskShipmentRaw_Hose(s).ToString();
                            break;
                        }
                    case MessageType.ExecutionTaskShipmentRaw_KZ:
                        {
                            var s = msg.xMessageWrapper.FromXElement<ServiceReference1.ExecutionTaskShipmentRaw_KZ>();
                            result = client.ExecutionTaskShipmentRaw_KZ(s).ToString();
                            break;
                        }
                    case MessageType.WeightingOfVehicles:
                        {
                            var s = msg.xMessageWrapper.FromXElement<ServiceReference1.WeighingOfVehicles>();//  WeightingOfVehicles>();
                            result = client.WeighingCars(s).ToString();// WeightingOfVehicles(s).ToString();
                            break;
                        }
                    case MessageType.Inventory:
                        {
                            var s = msg.xMessageWrapper.FromXElement<ServiceReference1.Inventory>();
                            result = client.Inventory(s).ToString();
                            break;
                        }
                    case MessageType.CompletingMove:
                        {
                            var s = msg.xMessageWrapper.FromXElement<ServiceReference1.CompletingMove>();
                            result = client.CompletingMove(s).ToString();
                            break;
                        }
                    case MessageType.CompletingRawMaterial:
                        {
                            var s = msg.xMessageWrapper.FromXElement<ServiceReference1.CompletingRawMaterial>();
                            result = client.CompletingRawMaterial(s).ToString();
                            break;
                        }

                }

                msg.DateTransmit = DateTime.Now;
                msg.Result = result;
                msg.IsSuccess = true;
                msg.NextTryDate = null;

            }
            catch (HttpException exp)
            {
                msg.Result = exp.HResult.ToString();
                msg.ErrorType = ErrorType.Logical;

                msg.CountError = (msg.CountError ?? 0) + 1;

                if (msg.CountError < 60)
                {
                    msg.NextTryDate = DateTime.Now.AddMinutes(+1);
                }
                else if (msg.CountError < 70)
                {
                    msg.NextTryDate = DateTime.Now.AddMinutes(+10);
                }
                else
                {
                    msg.NextTryDate = DateTime.Now.AddMinutes(+60);
                }

                msg.LastTryDate = DateTime.Now;
                msg.IsSuccess = false;
            }
            catch (Exception ex)
            {
                isSendError = true;

                msg.Result = ex.ToString();
                msg.ErrorType = ErrorType.Network;
                msg.CountError = (msg.CountError ?? 0) + 1;

                if (msg.CountError < 60)
                {
                    msg.NextTryDate = DateTime.Now.AddMinutes(+1);
                }
                else if (msg.CountError < 70)
                {
                    msg.NextTryDate = DateTime.Now.AddMinutes(+10);
                }
                else
                {
                    msg.NextTryDate = DateTime.Now.AddMinutes(+60);
                }

                msg.LastTryDate = DateTime.Now;
                msg.IsSuccess = false;
            }

            _db.TransmitLogRepository.Update(msg);
            _db.SaveChanges();
            return isSendError;
        }

        /// <summary>
        /// Получение журнала отправки
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public List<TransmitLog> GetTransmitLog(GetTransmitLogArg arg)
        {
            return _db.TransmitLogRepository.GetAll()
                .Where(r => (!arg.Begin.HasValue || r.CreatedAt >= arg.Begin)
                    && (!arg.End.HasValue || r.CreatedAt < arg.End)
                    && (arg.TextFilter==null || r.MessageTypeRus.Contains(arg.TextFilter)
                        || r.Result==null || r.Result.Contains(arg.TextFilter)))
                .ToList();
        }

        ServiceReference1.WebService_ASU_elevatorPortTypeClient _client { get; set; }

        private ServiceReference1.WebService_ASU_elevatorPortTypeClient GetClient()
        {
            #region Для HTTP Basic
            //var client = new ServiceReference1.WebService_ASU_elevatorPortTypeClient("WebService_ASU_elevatorSoap");

            //client.ClientCredentials.UserName.UserName = "ASU_elevator";  // Authorization: Basic QVNVX2VsZXZhdG9yOkFTVV9lbGV2YXRvcjE=
            //client.ClientCredentials.UserName.Password = "ASU_elevator1!";
            #endregion

            #region Для Https NTML
            _client = new ServiceReference1.WebService_ASU_elevatorPortTypeClient();

            if (_client.ClientCredentials != null)
            {
                //_client.ClientCredentials.Windows.ClientCredential = new NetworkCredential(txtUserName.Text, txtPassword.Text/*"TimoshevskiyKO", "iQnZkJ2P7V2E9FF"*/);
                _client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
                _client.ClientCredentials.Windows.ClientCredential = CredentialCache.DefaultNetworkCredentials;
            }
            #endregion

            _client.Open();

            return _client;
        }

        public void Close()
        {
            if(_client != null)
                _client.Close();
        }
    }
}
