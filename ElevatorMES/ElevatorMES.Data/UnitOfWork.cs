﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using ElevatorMES.Data.Repositories;
using ElevatorMES.Domain;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Repositories;
using ElevatorMES.Utilites.Exceptions;

namespace ElevatorMES.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _context;

        #region Закрытые поля

        private volatile Type _dependency;

        #endregion


        public UnitOfWork(DatabaseContext context)
        {
            _context = context;
            _dependency = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
        }

        private ILotRepository _lotRepository; public ILotRepository LotRepository => _lotRepository = _lotRepository ?? new LotRepository(_context);
        private INomenclatureRepository _nomenclatureRepository; public INomenclatureRepository NomenclatureRepository => _nomenclatureRepository = _nomenclatureRepository ?? new NomenclatureRepository(_context);
        private IOrganizationRepository _organizationRepository; public IOrganizationRepository OrganizationRepository => _organizationRepository = _organizationRepository ?? new OrganizationRepository(_context);
        private IContragentsRepository _contragentsRepository; public IContragentsRepository ContragentsRepository => _contragentsRepository = _contragentsRepository ?? new ContragentsRepository(_context);
        private ITaskRepository _taskRepository; public ITaskRepository TaskRepository => _taskRepository = _taskRepository ?? new TaskRepository(_context);
        private ITaskExecRepository _taskExecRepository; public ITaskExecRepository TaskExecRepository => _taskExecRepository = _taskExecRepository ?? new TaskExecRepository(_context);
        private IUnitRepository _unitRepository; public IUnitRepository UnitRepository => _unitRepository = _unitRepository ?? new UnitRepository(_context);
        private IUnitLayerRepository _unitLayerRepository; public IUnitLayerRepository UnitLayerRepository => _unitLayerRepository = _unitLayerRepository ?? new UnitLayerRepository(_context);
        private IUnitSettingRepository _unitSettingRepository; public IUnitSettingRepository UnitSettingRepository => _unitSettingRepository = _unitSettingRepository ?? new UnitSettingRepository(_context);
        private IUnitEquipmentLinkRepository _unitEquipmentLinkRepository; public IUnitEquipmentLinkRepository UnitEquipmentLinkRepository => _unitEquipmentLinkRepository = _unitEquipmentLinkRepository ?? new UnitEquipmentLinkRepository(_context);
        private IEquipmentRepository _equipmentRepository; public IEquipmentRepository EquipmentRepository => _equipmentRepository = _equipmentRepository ?? new EquipmentRepository(_context);
        private IUserRepository _userRepository; public IUserRepository UserRepository => _userRepository = _userRepository ?? new UserRepository(_context);
        private IVehicleRepository _vehicleRepository; public IVehicleRepository VehicleRepository => _vehicleRepository = _vehicleRepository ?? new VehicleRepository(_context);
        private IVehicleActivityRepository _vehicleActivityRepository; public IVehicleActivityRepository VehicleActivityRepository => _vehicleActivityRepository = _vehicleActivityRepository ?? new VehicleActivityRepository(_context);
        private ITaskExecMoveRepository _taskExecMoveRepository; public ITaskExecMoveRepository TaskExecMoveRepository => _taskExecMoveRepository = _taskExecMoveRepository ?? new TaskExecMoveRepository(_context);
        private IVehicleTypeRepository _vehicleTypeRepository; public IVehicleTypeRepository VehicleTypeRepository => _vehicleTypeRepository = _vehicleTypeRepository ?? new VehicleTypeRepository(_context);
        private INomenclaturePriorityRepository _nomenclaturePriorityRepository; public INomenclaturePriorityRepository NomenclaturePriorityRepository => _nomenclaturePriorityRepository = _nomenclaturePriorityRepository ?? new NomenclaturePriorityRepository(_context);
        private ILogEvent1CRepository _logEvent1CRepository; public ILogEvent1CRepository LogEvent1CRepository => _logEvent1CRepository = _logEvent1CRepository ?? new LogEvent1CRepository(_context);
        private ITransmitLogRepository _transmitLogRepository; public ITransmitLogRepository TransmitLogRepository => _transmitLogRepository = _transmitLogRepository ?? new TransmitLogRepository(_context);
        private IUnitInventoryRepository _unitInventoryRepository; public IUnitInventoryRepository UnitInventoryRepository => _unitInventoryRepository = _unitInventoryRepository ?? new UnitInventoryRepository(_context);

        public void ExecuteNonQuery(string query, params object[] parameters)
        {
            _context.Database.ExecuteSqlCommand(query, parameters);
        }

        public List<T> ExecuteQuery<T>(string query, params object[] parameters)
        {
            return _context.Database.SqlQuery<T>(query, parameters).ToList();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public DbContextTransaction BeginTransaction()
        {
            return _context.Database.BeginTransaction();
        }

        public IEnumerable<string> GetValidationErrors()
        {
            return _context.GetValidationErrors().SelectMany(m => m.ValidationErrors.Select(n => n.ToString()));
        }
    }
}
