﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class TaskRepository : BaseRepository<Task>, ITaskRepository
    {
        public TaskRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }

        /*public IEnumerable<TaskUIItem> GetList<TaskUIItem>(string storedProc, GetTaskArg arg)
        {
            if(arg == null) arg = new GetTaskArg();
            var query = $"EXEC dbo.{storedProc} @TaskId, @TaskStatus, @Begin, @End, @TextFilter";
            var pars = new[]
            {
                new SqlParameter("@" + nameof(arg.TaskId), arg.TaskId ?? objNull),
                new SqlParameter("@" + nameof(arg.TaskStatus), (int)arg.TaskStatus),
                new SqlParameter("@" + nameof(arg.Begin), arg.Begin ?? objNull),
                new SqlParameter("@" + nameof(arg.End), arg.End ?? objNull),
                new SqlParameter("@" + nameof(arg.TextFilter), arg.TextFilter ?? objNull),
            };
            return base.Context.Database.SqlQuery<TaskUIItem>(query, pars).ToList();
        }*/

    }
}
