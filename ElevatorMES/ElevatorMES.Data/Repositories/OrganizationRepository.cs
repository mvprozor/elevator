﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class OrganizationRepository : BaseSyncRepository<Organization>, IOrganizationRepository
    {
        public OrganizationRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
