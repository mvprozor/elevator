﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class TransmitLogRepository : BaseRepository<TransmitLog>, ITransmitLogRepository
    {
        public TransmitLogRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
