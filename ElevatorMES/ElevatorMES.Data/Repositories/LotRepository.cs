﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class LotRepository : BaseRepository<Lot>, ILotRepository
    {
        public LotRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
