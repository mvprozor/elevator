﻿using System;
using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;
using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Data.Repositories
{
    public class UnitInventoryRepository : BaseRepository<UnitInventory>, IUnitInventoryRepository
    {
        public UnitInventoryRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }

        public UnitInventory GetByIds(int unitId, DateTime createdAt, bool isThrowNotFound = false)
        {
            var entity = EntitySet.Find(unitId, createdAt);
            if (entity == null && isThrowNotFound)
            {
                var entityName = typeof(UnitInventory).GetClassDescriptionOrName();
                throw new LogicException($"{entityName} отсутствует. UnitId: {unitId}, CreatedAt: {createdAt}");
            }
            return entity;
        }
    }
}