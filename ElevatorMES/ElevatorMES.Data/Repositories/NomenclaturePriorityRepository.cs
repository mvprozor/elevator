﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class NomenclaturePriorityRepository : BaseRepository<NomenclaturePriority>, INomenclaturePriorityRepository
    {
        public NomenclaturePriorityRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
