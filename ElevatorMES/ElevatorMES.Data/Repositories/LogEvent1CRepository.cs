﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class LogEvent1CRepository : BaseRepository<LogEvent1C>, ILogEvent1CRepository
    {
        public LogEvent1CRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
