﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class VehicleActivityRepository : BaseRepository<VehicleActivity>, IVehicleActivityRepository
    {
        public VehicleActivityRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
