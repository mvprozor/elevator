﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Repositories;
using System.Linq;

namespace ElevatorMES.Data.Repositories
{
    public class UnitRepository : BaseSyncRepository<Unit>, IUnitRepository
    {
        public UnitRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }

        public Unit GetPlant()
        {
            var unit = FindBy(r => r.Type == UnitType.CompoundFeedPlant).SingleOrDefault();
            if (unit == null)
            {
                unit = new Unit { DisplayName = "Комбикормовый завод", Type = UnitType.CompoundFeedPlant };
                Add(unit);
            }

            return unit;
        }
    }
}