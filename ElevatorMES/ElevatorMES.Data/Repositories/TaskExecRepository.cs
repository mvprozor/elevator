﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class TaskExecRepository : BaseRepository<TaskExec>, ITaskExecRepository
    {
        public TaskExecRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
