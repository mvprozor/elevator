﻿using System.Linq;
using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }

        public User GetOrCreateByDisplayName(string displayName)
        {
            if (string.IsNullOrWhiteSpace(displayName))
                return null;
            var user = FindBy(r => r.DisplayName == displayName).SingleOrDefault();
            if (user == null)
            {
                user = new User { DisplayName = displayName };
                Add(user);
            }

            return user;
        }
    }
}
