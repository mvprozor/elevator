﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class ContragentsRepository : BaseSyncRepository<Contragent>, IContragentsRepository
    {
        public ContragentsRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
