﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class NomenclatureRepository : BaseSyncRepository<Nomenclature>, INomenclatureRepository
    {
        public NomenclatureRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
