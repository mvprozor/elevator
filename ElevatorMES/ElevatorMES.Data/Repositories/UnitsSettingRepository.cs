﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data
{
    public class UnitSettingRepository : BaseRepository<UnitSetting>, IUnitSettingRepository
    {
        public UnitSettingRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
