﻿using System;
using System.Linq;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Data.Repositories.Base
{
    public abstract class BaseSyncRepository<T> : BaseRepository<T> where T: BaseSyncEntity 
    {
        protected BaseSyncRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public T GetByCode1C(string code1C)
        {
            var obj = FindBy(r => r.Code1C == code1C);
            return obj.SingleOrDefault();
        }

        public T GetOrCreateByCode1C(string code1C)
        {
            var obj = FindBy(r => r.Code1C == code1C);
            if (obj.Any())
            {
                return obj.First();
            }

            var newObj = (T)Activator.CreateInstance(typeof(T));
            newObj.Code1C = code1C;
            newObj.DisplayName = $@"Код {code1C}";
            Add(newObj);
            return newObj;
        }


    }
}