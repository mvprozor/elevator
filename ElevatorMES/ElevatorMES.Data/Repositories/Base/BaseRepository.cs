﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Repositories.Base;
using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Data.Repositories.Base
{
    public abstract class BaseRepository<T> : CommRepository<T>, IBaseRepository<T> where T: class, IBaseEntity
    {
        protected BaseRepository(DatabaseContext databaseContext) : base(databaseContext)
        {

        }

        public T GetById(int id, bool isThrowNotFound = false)
        {
            var entity = EntitySet.FirstOrDefault(e => e.Id == id);
            if (entity == null && isThrowNotFound)
            {
                var entityName = typeof(T).GetClassDescriptionOrName();
                throw new LogicException($"{entityName} отсутствует. Id: {id}");
            }

            return entity;
        }

        public IQueryable<T> GetMany(IEnumerable<int> ids)
        {
            return EntitySet.Where(n => ids.Any(i => i == n.Id));
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return EntitySet.Where(predicate).AsQueryable();
        }

        public T FirstCreated(Expression<Func<T, bool>> predicate)
        {
            return FindBy(predicate).OrderBy(r => r.CreatedAt).FirstOrDefault();
        }

        public T LastCreated(Expression<Func<T, bool>> predicate)
        {
            return FindBy(predicate).OrderByDescending(r => r.CreatedAt).FirstOrDefault();
        }

        public void TryDelete(int id)
        {
            var entity = GetById(id, true);
            try
            {
                EntitySet.Remove(entity);
                Context.SaveChanges();
            }
            catch(Exception ex)
            {
                SqlException sqlEx = null;
                while(true)
                {
                    sqlEx = ex as SqlException;
                    if (sqlEx != null || ex.InnerException==null)
                        break;
                    ex = ex.InnerException;
                }
                if (sqlEx?.Number == 547)
                {
                    var deletable = entity as IDeletable;
                    if (deletable == null)
                        throw new LogicException($"Элемент с ID={id} используется, удаление невозможно.");
                    deletable.Deleted = true;
                    var updateable = entity as IUpdateable;
                    if (updateable != null)
                        updateable.UpdatedAt = DateTime.Now;
                    Context.Entry(entity).State = EntityState.Modified;
                    Context.SaveChanges();
                }
                else
                    throw new LogicException($"Ошибка при удалении элемента с ID={id}. {ex.Message}");
            }
        }

        public void Restore(int id)
        {
            var entity = GetById(id, true);
            var deletable = entity as IDeletable;
            if (deletable == null)
                throw new LogicException($"Элемент не поддерживает восстановление.");
            deletable.Deleted = false;
            Context.SaveChanges();
        }
    }
}
