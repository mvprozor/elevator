﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Repositories.Base;
using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Data.Repositories.Base
{
    public abstract class CommRepository<T> : ICommRepository<T> where T : class
    {
        public object objNull => DBNull.Value;
        protected readonly DatabaseContext Context;

        protected CommRepository(DatabaseContext databaseContext)
        {
            Context = databaseContext;
        }

        protected DbSet<T> EntitySet => Context.Set<T>();

        public void Add(T entity)
        {
            EntitySet.Add(entity);
        }

        public void AddRange(IEnumerable<T> entities)
        {
            EntitySet.AddRange(entities);
        }

        public void Remove(T entity)
        {
            EntitySet.Remove(entity);
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            EntitySet.RemoveRange(entities);
        }

        public void Update(T entity)
        {
            EntitySet.AddOrUpdate(entity);
        }

        public IQueryable<T> GetAll()
        {
            return EntitySet.AsQueryable();
        }
    }
}
