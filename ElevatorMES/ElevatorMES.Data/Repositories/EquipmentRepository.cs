﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;
using System.Linq;

namespace ElevatorMES.Data.Repositories
{
    class EquipmentRepository : BaseRepository<Equipment>, IEquipmentRepository
    {
        public EquipmentRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
