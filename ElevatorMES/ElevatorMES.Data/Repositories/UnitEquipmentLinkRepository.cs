﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;
using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;
using System.Linq;

namespace ElevatorMES.Data.Repositories
{
    class UnitEquipmentLinkRepository : CommRepository<UnitEquipmentLink>, IUnitEquipmentLinkRepository
    {
        public UnitEquipmentLinkRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }

        public UnitEquipmentLink GetByIds(int unitId, int equipmentId, bool isThrowNotFound = false)
        {
            var entity = EntitySet.Find(unitId, equipmentId);
            if (entity == null && isThrowNotFound)
            {
                var entityName = typeof(UnitEquipmentLink).GetClassDescriptionOrName();
                throw new LogicException($"{entityName} отсутствует. UnitId: {unitId}, EquipmentId: {equipmentId}");
            }
            return entity;
        }
    }
}