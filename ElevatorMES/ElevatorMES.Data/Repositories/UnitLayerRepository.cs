﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class UnitLayerRepository : BaseRepository<UnitLayer>, IUnitLayerRepository
    {
        public UnitLayerRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
