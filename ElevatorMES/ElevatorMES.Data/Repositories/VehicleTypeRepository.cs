﻿using ElevatorMES.Data.Repositories.Base;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Data.Repositories
{
    public class VehicleTypeRepository : BaseSyncRepository<VehicleType>, IVehicleTypeRepository
    {
        public VehicleTypeRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }
    }
}
