﻿using System;
using System.Data.Entity;
using System.Linq;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
            : base("ElevatorMESConnection")
        {
            Database.SetInitializer<DatabaseContext>(null);
            //Database.SetInitializer<DatabaseContext>(new DBInitializer());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Nomenclature> Nomenclatures { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Contragent> Contragents { get; set; }
        public DbSet<VehicleType> VehicleTypes { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Lot> Lots { get; set; }
        public DbSet<UnitSetting> UnitSettings { get; set; }
        public DbSet<UnitLayer> UnitLayers { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskExec> TaskExecs { get; set; }
        public DbSet<TaskExecMove> TaskExecMoves { get; set; }
        public DbSet<NomenclaturePriority> NomenclaturePriorities { get; set; }
        public DbSet<LogEvent1C> LogEvent1C { get; set; }
        public DbSet<TransmitLog> TransmitLogs { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<UnitEquipmentLink> UnitEquipmentLinks { get; set; }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            // Configure that all used DateTime should be modeled datetime2:
            const string standardDateTimeType = "datetime2";
            modelBuilder.Properties<DateTime>().Configure(p => p.HasColumnType(standardDateTimeType));

            modelBuilder.Entity<Unit>().Property(unit => unit.Capacity).HasPrecision(18, 3);
            modelBuilder.Entity<UnitLayer>().Property(unit => unit.Quantity).HasPrecision(18, 3);
            modelBuilder.Entity<VehicleActivity>().Property(ent => ent.Weight).HasPrecision(18, 3);
            modelBuilder.Entity<Task>().Property(ent => ent.Quantity).HasPrecision(18, 3);
            modelBuilder.Entity<TaskExecMove>().Property(ent => ent.Quantity).HasPrecision(18, 3);
            modelBuilder.Entity<UnitInventory>().Property(unit => unit.Quantity).HasPrecision(18, 3);

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var modifiedEntities = ChangeTracker.Entries<IUpdateable>().Where(entity => entity.State == EntityState.Modified || entity.State == EntityState.Added).ToList();

            foreach (var modifiedEntity in modifiedEntities)
            {
                modifiedEntity.Entity.UpdatedAt = DateTime.Now;
            }

            return base.SaveChanges();
        }
    }

    public class DBInitializer : CreateDatabaseIfNotExists<DatabaseContext> // DropCreateDatabaseIfModelChanges<DatabaseContext>
    {
        // Отключить соединения в БД:
        // USE [master]; DECLARE @kill varchar(8000) = '';  SELECT @kill = @kill + 'kill ' + CONVERT(varchar(5), session_id) + ';'  FROM sys.dm_exec_sessions WHERE database_id  = db_id('ElevatorMES'); EXEC(@kill);
        protected override void Seed(DatabaseContext context)
        {
            var queries = new[]
            {
                $@"CREATE UNIQUE NONCLUSTERED INDEX IX_{nameof(Task)}s_UQ_{nameof(Task.Sync1CAt)} ON {nameof(Task)}s (Sync1CAt) WHERE {nameof(Task.Sync1CAt)} IS NOT NULL; ",
                //$@"ALTER TABLE {nameof(TaskExecMove)}s ADD CONSTRAINT UQ_{nameof(TaskExecMove)}s_SourceId CHECK ((SourceUnitId IS NULL AND SourceVehicleId  IS NOT NULL) OR (SourceUnitId IS NOT NULL AND SourceVehicleId IS NULL)) ",
                //$@"ALTER TABLE {nameof(TaskExecMove)}s ADD CONSTRAINT UQ_{nameof(TaskExecMove)}s_DestId CHECK ((DestUnitId IS NULL AND DestVehicleId  IS NOT NULL) OR (DestUnitId IS NOT NULL AND DestVehicleId IS NULL)) ",
                $@"ALTER TABLE NomenclaturePriorities ADD CONSTRAINT CK_NomenclaturePriorities_Priority CHECK (Priority <= 5 ) "
            };

            foreach (var query in queries)
            {
                context.Database.ExecuteSqlCommand(query);
            }

            context.Users.AddRange(User.Seed());
            context.Nomenclatures.AddRange(Nomenclature.Seed());
            context.Organizations.AddRange(Organization.Seed());
            context.Contragents.AddRange(Contragent.Seed());
            context.Units.AddRange(Unit.Seed());
            context.VehicleTypes.AddRange(VehicleType.Seed());
            context.Equipments.AddRange(Equipment.Seed());

            context.SaveChanges();
        }
    }

}
