﻿using System;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Data.Services
{
    public class Interact1C
    {
        public TransmitLog GetTransmitLogInventory(string guid, float quantity, DateTime date, string warehouse, string nomenclatureCode1C, string user)
        {
            var xMessage = new ServiceReference1.Inventory()
            {                
                GUID = guid,
                Raw_Code = nomenclatureCode1C,
                Warehouse = warehouse,
                WarehouseQuantity = quantity,
                Date = date,
                ResponsibleCode = user
            };
            var msg = xMessage.ToXElement();
            var transmitLogItem = new TransmitLog(msg, MessageType.Inventory);
            return transmitLogItem;
        }
    }
}