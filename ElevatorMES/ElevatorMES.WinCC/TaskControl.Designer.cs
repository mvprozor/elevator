﻿namespace ElevatorMES.WinCC
{
    partial class TaskControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolTasks = new System.Windows.Forms.ToolStrip();
            this.statusTasks = new System.Windows.Forms.StatusStrip();
            this.gridTasks = new System.Windows.Forms.DataGridView();
            this.statusCurrent = new System.Windows.Forms.ToolStripStatusLabel();
            this.ColId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusTasks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTasks)).BeginInit();
            this.SuspendLayout();
            // 
            // toolTasks
            // 
            this.toolTasks.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolTasks.Location = new System.Drawing.Point(0, 0);
            this.toolTasks.Name = "toolTasks";
            this.toolTasks.Size = new System.Drawing.Size(800, 25);
            this.toolTasks.TabIndex = 0;
            // 
            // statusTasks
            // 
            this.statusTasks.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusCurrent});
            this.statusTasks.Location = new System.Drawing.Point(0, 428);
            this.statusTasks.Name = "statusTasks";
            this.statusTasks.Size = new System.Drawing.Size(800, 22);
            this.statusTasks.TabIndex = 1;
            // 
            // gridTasks
            // 
            this.gridTasks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTasks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColId});
            this.gridTasks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTasks.Location = new System.Drawing.Point(0, 25);
            this.gridTasks.Name = "gridTasks";
            this.gridTasks.RowHeadersVisible = false;
            this.gridTasks.ShowCellErrors = false;
            this.gridTasks.ShowEditingIcon = false;
            this.gridTasks.ShowRowErrors = false;
            this.gridTasks.Size = new System.Drawing.Size(800, 403);
            this.gridTasks.TabIndex = 2;
            // 
            // statusCurrent
            // 
            this.statusCurrent.Name = "statusCurrent";
            this.statusCurrent.Size = new System.Drawing.Size(104, 17);
            this.statusCurrent.Text = "Инициализация...";
            // 
            // ColId
            // 
            this.ColId.HeaderText = "ID";
            this.ColId.Name = "ColId";
            this.ColId.ReadOnly = true;
            // 
            // TaskControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridTasks);
            this.Controls.Add(this.statusTasks);
            this.Controls.Add(this.toolTasks);
            this.Name = "TaskControl";
            this.Size = new System.Drawing.Size(800, 450);
            this.Load += new System.EventHandler(this.TaskControlLoad);
            this.statusTasks.ResumeLayout(false);
            this.statusTasks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTasks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolTasks;
        private System.Windows.Forms.StatusStrip statusTasks;
        private System.Windows.Forms.DataGridView gridTasks;
        private System.Windows.Forms.ToolStripStatusLabel statusCurrent;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColId;
    }
}
