﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElevatorMES.Domain;
using ElevatorMES.Data;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain.Model.Args;

namespace ElevatorMES.WinCC
{
    public partial class TaskControl: UserControl
    {
        private readonly IUnitOfWork _uow;
        private readonly TaskManager _taskManager;
        public TaskControl()
        {
            InitializeComponent();
            var dc = new DatabaseContext()
            {
                
            };
            _uow = new UnitOfWork(dc);
            _taskManager = new TaskManager(_uow);
        }

        private void TaskControlLoad(object sender, EventArgs e)
        {
            var arg = new GetTaskArg();
            var taskUI = _taskManager.GetTaskUI(arg).TaskUIItems.ToList();

        }
    }
}
