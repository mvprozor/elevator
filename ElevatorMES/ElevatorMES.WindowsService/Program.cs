﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ElevetorMES.WindowsService
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                ElevetorMESWindowsService service = new ElevetorMESWindowsService();
                service.TestStartupAndStop(args);
            }
            else
            {
                var serviceToRun = new ServiceBase[]
                {
                    new ElevetorMESWindowsService()
                };

                ServiceBase.Run(serviceToRun);
            }
        }
    }
}
