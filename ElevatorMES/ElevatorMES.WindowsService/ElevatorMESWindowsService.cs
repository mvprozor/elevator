﻿using ElevatorMES.Data;
using ElevatorMES.Domain.Enums;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using ElevatorMES.Utilites.Helpers;
using ElevatorMES.Data.Managers;

namespace ElevetorMES.WindowsService
{
    partial class ElevetorMESWindowsService
    {
        private readonly Timer _timer = new Timer();

        private const string EventSource = "ElevetorMES.WindowsService";
        private static TransmitLogManager TransmitLogManager = new TransmitLogManager(new UnitOfWork(new DatabaseContext()));
        public ElevetorMESWindowsService()
        {
            InitializeComponent();
        }

        public void Log(string s)
        {
            Console.WriteLine($@"{DateTime.Now:HH:mm:ss}: {s}");
            AddEventLogEntry(s, EventLogEntryType.Information);
        }

        protected override void OnStart(string[] args)
        {
            Console.WriteLine(@"Сервис запущен. OnStart " + Environment.NewLine);
            AddEventLogEntry("OnStart", EventLogEntryType.Information);

            try
            {
                string timerElapsed = ConfigurationManager.AppSettings["TimerSeconds"];
                Log("timerElapsed = " + timerElapsed);
                int timerSeconds = int.Parse(timerElapsed);
                _timer.Interval = 1000 * timerSeconds;
                Log($@"Интервал {timerSeconds } секунд");
                _timer.Elapsed += OnElapsed;
                _timer.Enabled = true;
                OnElapsed(this, null);
                _timer.Start();
            }
            catch (Exception exception)
            {
                var message = $"OnStart [{exception}]";
                AddEventLogEntry(message, EventLogEntryType.Error);
                throw;
            }
        }

        protected override void OnStop()
        {
            AddEventLogEntry("OnStop", EventLogEntryType.Information);
            AddEventLogEntry("OnExit", EventLogEntryType.Information);
        }

        private void AddEventLogEntry(String message, EventLogEntryType entryType)
        {
            try
            {
                if (!EventLog.SourceExists(EventSource))
                {
                    EventLog.CreateEventSource(EventSource, EventSource);
                }

                var eventLog = new EventLog
                {
                    Source = EventSource
                };
                eventLog.WriteEntry(message, entryType);
            }
            catch
            {
                // ignore
            }
        }
        private void OnElapsed(Object sender, ElapsedEventArgs e)
        {
            AddEventLogEntry("OnElapsed [Start]", EventLogEntryType.Information);

            try
            {
                _timer.Stop();

                var msgList = TransmitLogManager.GetLogsToSend();

                Log($"К передаче: {msgList.Count} сообщений");
                var now = DateTime.Now;

                var isSendError = false;
                foreach (var msg in msgList)
                {
                    TryAction($"Отправка в 1C сообщения {msg.Id} типа {msg.MessageTypeRus}. {msg.xMessage}", () =>
                    {
                        isSendError = TransmitLogManager.Send(msg);
                    });
                    if (isSendError)
                    {
                        Log("Ошибка канала связи");
                        break;
                    }
                }
            }
            catch (Exception exception)
            {
                var message = $"OnElapsed [{exception}]";
                Console.WriteLine(exception);
                AddEventLogEntry(message, EventLogEntryType.Error);
                //Console.ReadLine();
                //throw;
            }
            finally
            {
                _timer.Start();
            }

            AddEventLogEntry("OnElapsed [Complete]", EventLogEntryType.Information);
        }

        private void TryAction(string actionName, Action action)
        {
            Log($"--- {actionName} ---");
            try
            {
                action();
                Log($"--- Выполнено {actionName} ---");
            }
            catch (Exception ex)
            {
                Log($"--- Ошибка {actionName}. {Environment.NewLine}{ex} ---");
            }
        }

        public void TestStartupAndStop(string[] args)
        {
            OnStart(args);
            Console.ReadLine();
            OnStop();
        }
    }
}
