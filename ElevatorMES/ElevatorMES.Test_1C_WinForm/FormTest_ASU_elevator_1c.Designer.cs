﻿namespace ElevatorMES.Test_1C_WinForm
{
    partial class FormTest_ASU_elevator_1c
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnTestCompletingMove = new System.Windows.Forms.Button();
            this.TxtResult = new System.Windows.Forms.TextBox();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtSenderWarehouse = new System.Windows.Forms.TextBox();
            this.txtStatusSenderWarehouse = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbExecuteDrying = new System.Windows.Forms.CheckBox();
            this.txtStatusReceiptWarehouse = new System.Windows.Forms.NumericUpDown();
            this.cbExecuteCleaningScalper = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbExecuteCleaningSeparator = new System.Windows.Forms.CheckBox();
            this.txtReceiptWarehouse = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cbQualityControl = new System.Windows.Forms.CheckBox();
            this.cbExecuteDryingCompletingRawMaterial = new System.Windows.Forms.CheckBox();
            this.cbExecuteCleaningSeparatorCompletingRawMaterial = new System.Windows.Forms.CheckBox();
            this.BtnTestCompletingRawMaterial = new System.Windows.Forms.Button();
            this.cbExecuteCleaningCompletingRawMaterial = new System.Windows.Forms.CheckBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.BtnInventory = new System.Windows.Forms.Button();
            this.BtnPing = new System.Windows.Forms.Button();
            this.BtnWeighingOfVehicles = new System.Windows.Forms.Button();
            this.BtnExecutionTaskShipmentRaw_KZ = new System.Windows.Forms.Button();
            this.BtnExecutionTaskShipmentRaw_Hose = new System.Windows.Forms.Button();
            this.BtnExecutionTaskShipmentRaw_ExternalConsumers = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNumberVehicles = new System.Windows.Forms.TextBox();
            this.txtResponsibleCode = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRaw_Code = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtGUID = new System.Windows.Forms.TextBox();
            this.txtWarehouse = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPartner = new System.Windows.Forms.TextBox();
            this.txtOrganization = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusSenderWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusReceiptWarehouse)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnTestCompletingMove
            // 
            this.BtnTestCompletingMove.Location = new System.Drawing.Point(424, 136);
            this.BtnTestCompletingMove.Margin = new System.Windows.Forms.Padding(2);
            this.BtnTestCompletingMove.Name = "BtnTestCompletingMove";
            this.BtnTestCompletingMove.Size = new System.Drawing.Size(115, 32);
            this.BtnTestCompletingMove.TabIndex = 0;
            this.BtnTestCompletingMove.Text = "TestCompletingMove";
            this.BtnTestCompletingMove.UseVisualStyleBackColor = true;
            this.BtnTestCompletingMove.Click += new System.EventHandler(this.BtnTestCompletingMove_Click);
            // 
            // TxtResult
            // 
            this.TxtResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtResult.Location = new System.Drawing.Point(2, 15);
            this.TxtResult.Margin = new System.Windows.Forms.Padding(2);
            this.TxtResult.Multiline = true;
            this.TxtResult.Name = "TxtResult";
            this.TxtResult.Size = new System.Drawing.Size(697, 191);
            this.TxtResult.TabIndex = 1;
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.tabPage1);
            this.TabControl1.Controls.Add(this.tabPage2);
            this.TabControl1.Controls.Add(this.tabPage3);
            this.TabControl1.Controls.Add(this.tabPage4);
            this.TabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TabControl1.Location = new System.Drawing.Point(0, 0);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(701, 219);
            this.TabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtSenderWarehouse);
            this.tabPage1.Controls.Add(this.txtStatusSenderWarehouse);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.cbExecuteDrying);
            this.tabPage1.Controls.Add(this.BtnTestCompletingMove);
            this.tabPage1.Controls.Add(this.txtStatusReceiptWarehouse);
            this.tabPage1.Controls.Add(this.cbExecuteCleaningScalper);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.cbExecuteCleaningSeparator);
            this.tabPage1.Controls.Add(this.txtReceiptWarehouse);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(693, 193);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "CompletingMove";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtSenderWarehouse
            // 
            this.txtSenderWarehouse.Location = new System.Drawing.Point(138, 37);
            this.txtSenderWarehouse.Name = "txtSenderWarehouse";
            this.txtSenderWarehouse.Size = new System.Drawing.Size(208, 20);
            this.txtSenderWarehouse.TabIndex = 17;
            // 
            // txtStatusSenderWarehouse
            // 
            this.txtStatusSenderWarehouse.Location = new System.Drawing.Point(493, 39);
            this.txtStatusSenderWarehouse.Name = "txtStatusSenderWarehouse";
            this.txtStatusSenderWarehouse.Size = new System.Drawing.Size(46, 20);
            this.txtStatusSenderWarehouse.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(358, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "StatusSenderWarehouse";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "SenderWarehouse";
            // 
            // cbExecuteDrying
            // 
            this.cbExecuteDrying.AutoSize = true;
            this.cbExecuteDrying.Location = new System.Drawing.Point(138, 74);
            this.cbExecuteDrying.Name = "cbExecuteDrying";
            this.cbExecuteDrying.Size = new System.Drawing.Size(95, 17);
            this.cbExecuteDrying.TabIndex = 12;
            this.cbExecuteDrying.Text = "ExecuteDrying";
            this.cbExecuteDrying.UseVisualStyleBackColor = true;
            // 
            // txtStatusReceiptWarehouse
            // 
            this.txtStatusReceiptWarehouse.Location = new System.Drawing.Point(493, 15);
            this.txtStatusReceiptWarehouse.Name = "txtStatusReceiptWarehouse";
            this.txtStatusReceiptWarehouse.Size = new System.Drawing.Size(46, 20);
            this.txtStatusReceiptWarehouse.TabIndex = 7;
            // 
            // cbExecuteCleaningScalper
            // 
            this.cbExecuteCleaningScalper.AutoSize = true;
            this.cbExecuteCleaningScalper.Location = new System.Drawing.Point(138, 100);
            this.cbExecuteCleaningScalper.Name = "cbExecuteCleaningScalper";
            this.cbExecuteCleaningScalper.Size = new System.Drawing.Size(142, 17);
            this.cbExecuteCleaningScalper.TabIndex = 13;
            this.cbExecuteCleaningScalper.Text = "ExecuteCleaningScalper";
            this.cbExecuteCleaningScalper.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(358, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "StatusReceiptWarehouse";
            // 
            // cbExecuteCleaningSeparator
            // 
            this.cbExecuteCleaningSeparator.AutoSize = true;
            this.cbExecuteCleaningSeparator.Location = new System.Drawing.Point(138, 126);
            this.cbExecuteCleaningSeparator.Name = "cbExecuteCleaningSeparator";
            this.cbExecuteCleaningSeparator.Size = new System.Drawing.Size(152, 17);
            this.cbExecuteCleaningSeparator.TabIndex = 14;
            this.cbExecuteCleaningSeparator.Text = "ExecuteCleaningSeparator";
            this.cbExecuteCleaningSeparator.UseVisualStyleBackColor = true;
            // 
            // txtReceiptWarehouse
            // 
            this.txtReceiptWarehouse.Location = new System.Drawing.Point(138, 12);
            this.txtReceiptWarehouse.Name = "txtReceiptWarehouse";
            this.txtReceiptWarehouse.Size = new System.Drawing.Size(208, 20);
            this.txtReceiptWarehouse.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "ReceiptWarehouse";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cbQualityControl);
            this.tabPage2.Controls.Add(this.cbExecuteDryingCompletingRawMaterial);
            this.tabPage2.Controls.Add(this.cbExecuteCleaningSeparatorCompletingRawMaterial);
            this.tabPage2.Controls.Add(this.BtnTestCompletingRawMaterial);
            this.tabPage2.Controls.Add(this.cbExecuteCleaningCompletingRawMaterial);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(693, 193);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "CompletingRawMaterial";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cbQualityControl
            // 
            this.cbQualityControl.AutoSize = true;
            this.cbQualityControl.Location = new System.Drawing.Point(25, 14);
            this.cbQualityControl.Name = "cbQualityControl";
            this.cbQualityControl.Size = new System.Drawing.Size(91, 17);
            this.cbQualityControl.TabIndex = 36;
            this.cbQualityControl.Text = "QualityControl";
            this.cbQualityControl.UseVisualStyleBackColor = true;
            // 
            // cbExecuteDryingCompletingRawMaterial
            // 
            this.cbExecuteDryingCompletingRawMaterial.AutoSize = true;
            this.cbExecuteDryingCompletingRawMaterial.Location = new System.Drawing.Point(26, 38);
            this.cbExecuteDryingCompletingRawMaterial.Name = "cbExecuteDryingCompletingRawMaterial";
            this.cbExecuteDryingCompletingRawMaterial.Size = new System.Drawing.Size(95, 17);
            this.cbExecuteDryingCompletingRawMaterial.TabIndex = 30;
            this.cbExecuteDryingCompletingRawMaterial.Text = "ExecuteDrying";
            this.cbExecuteDryingCompletingRawMaterial.UseVisualStyleBackColor = true;
            // 
            // cbExecuteCleaningSeparatorCompletingRawMaterial
            // 
            this.cbExecuteCleaningSeparatorCompletingRawMaterial.AutoSize = true;
            this.cbExecuteCleaningSeparatorCompletingRawMaterial.Location = new System.Drawing.Point(26, 90);
            this.cbExecuteCleaningSeparatorCompletingRawMaterial.Name = "cbExecuteCleaningSeparatorCompletingRawMaterial";
            this.cbExecuteCleaningSeparatorCompletingRawMaterial.Size = new System.Drawing.Size(152, 17);
            this.cbExecuteCleaningSeparatorCompletingRawMaterial.TabIndex = 32;
            this.cbExecuteCleaningSeparatorCompletingRawMaterial.Text = "ExecuteCleaningSeparator";
            this.cbExecuteCleaningSeparatorCompletingRawMaterial.UseVisualStyleBackColor = true;
            // 
            // BtnTestCompletingRawMaterial
            // 
            this.BtnTestCompletingRawMaterial.Location = new System.Drawing.Point(238, 49);
            this.BtnTestCompletingRawMaterial.Margin = new System.Windows.Forms.Padding(2);
            this.BtnTestCompletingRawMaterial.Name = "BtnTestCompletingRawMaterial";
            this.BtnTestCompletingRawMaterial.Size = new System.Drawing.Size(152, 32);
            this.BtnTestCompletingRawMaterial.TabIndex = 19;
            this.BtnTestCompletingRawMaterial.Text = "TestCompletingRawMaterial";
            this.BtnTestCompletingRawMaterial.UseVisualStyleBackColor = true;
            this.BtnTestCompletingRawMaterial.Click += new System.EventHandler(this.BtnTestCompletingRawMaterial_Click);
            // 
            // cbExecuteCleaningCompletingRawMaterial
            // 
            this.cbExecuteCleaningCompletingRawMaterial.AutoSize = true;
            this.cbExecuteCleaningCompletingRawMaterial.Location = new System.Drawing.Point(26, 64);
            this.cbExecuteCleaningCompletingRawMaterial.Name = "cbExecuteCleaningCompletingRawMaterial";
            this.cbExecuteCleaningCompletingRawMaterial.Size = new System.Drawing.Size(142, 17);
            this.cbExecuteCleaningCompletingRawMaterial.TabIndex = 31;
            this.cbExecuteCleaningCompletingRawMaterial.Text = "ExecuteCleaningScalper";
            this.cbExecuteCleaningCompletingRawMaterial.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.BtnInventory);
            this.tabPage3.Controls.Add(this.BtnPing);
            this.tabPage3.Controls.Add(this.BtnWeighingOfVehicles);
            this.tabPage3.Controls.Add(this.BtnExecutionTaskShipmentRaw_KZ);
            this.tabPage3.Controls.Add(this.BtnExecutionTaskShipmentRaw_Hose);
            this.tabPage3.Controls.Add(this.BtnExecutionTaskShipmentRaw_ExternalConsumers);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(693, 193);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Other";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // BtnInventory
            // 
            this.BtnInventory.Location = new System.Drawing.Point(337, 54);
            this.BtnInventory.Margin = new System.Windows.Forms.Padding(2);
            this.BtnInventory.Name = "BtnInventory";
            this.BtnInventory.Size = new System.Drawing.Size(274, 26);
            this.BtnInventory.TabIndex = 5;
            this.BtnInventory.Text = "Inventory";
            this.BtnInventory.UseVisualStyleBackColor = true;
            this.BtnInventory.Click += new System.EventHandler(this.BtnInventory_Click);
            // 
            // BtnPing
            // 
            this.BtnPing.Location = new System.Drawing.Point(337, 11);
            this.BtnPing.Margin = new System.Windows.Forms.Padding(2);
            this.BtnPing.Name = "BtnPing";
            this.BtnPing.Size = new System.Drawing.Size(274, 26);
            this.BtnPing.TabIndex = 4;
            this.BtnPing.Text = "Ping";
            this.BtnPing.UseVisualStyleBackColor = true;
            this.BtnPing.Click += new System.EventHandler(this.BtnPing_Click);
            // 
            // BtnWeighingOfVehicles
            // 
            this.BtnWeighingOfVehicles.Location = new System.Drawing.Point(14, 141);
            this.BtnWeighingOfVehicles.Margin = new System.Windows.Forms.Padding(2);
            this.BtnWeighingOfVehicles.Name = "BtnWeighingOfVehicles";
            this.BtnWeighingOfVehicles.Size = new System.Drawing.Size(274, 26);
            this.BtnWeighingOfVehicles.TabIndex = 3;
            this.BtnWeighingOfVehicles.Text = "WeighingOfVehicles";
            this.BtnWeighingOfVehicles.UseVisualStyleBackColor = true;
            this.BtnWeighingOfVehicles.Click += new System.EventHandler(this.BtnWeighingOfVehicles_Click);
            // 
            // BtnExecutionTaskShipmentRaw_KZ
            // 
            this.BtnExecutionTaskShipmentRaw_KZ.Location = new System.Drawing.Point(14, 98);
            this.BtnExecutionTaskShipmentRaw_KZ.Margin = new System.Windows.Forms.Padding(2);
            this.BtnExecutionTaskShipmentRaw_KZ.Name = "BtnExecutionTaskShipmentRaw_KZ";
            this.BtnExecutionTaskShipmentRaw_KZ.Size = new System.Drawing.Size(274, 26);
            this.BtnExecutionTaskShipmentRaw_KZ.TabIndex = 2;
            this.BtnExecutionTaskShipmentRaw_KZ.Text = "ExecutionTaskShipmentRaw_KZ";
            this.BtnExecutionTaskShipmentRaw_KZ.UseVisualStyleBackColor = true;
            this.BtnExecutionTaskShipmentRaw_KZ.Click += new System.EventHandler(this.BtnExecutionTaskShipmentRaw_KZ_Click);
            // 
            // BtnExecutionTaskShipmentRaw_Hose
            // 
            this.BtnExecutionTaskShipmentRaw_Hose.Location = new System.Drawing.Point(14, 54);
            this.BtnExecutionTaskShipmentRaw_Hose.Margin = new System.Windows.Forms.Padding(2);
            this.BtnExecutionTaskShipmentRaw_Hose.Name = "BtnExecutionTaskShipmentRaw_Hose";
            this.BtnExecutionTaskShipmentRaw_Hose.Size = new System.Drawing.Size(274, 26);
            this.BtnExecutionTaskShipmentRaw_Hose.TabIndex = 1;
            this.BtnExecutionTaskShipmentRaw_Hose.Text = "ExecutionTaskShipmentRaw_Hose";
            this.BtnExecutionTaskShipmentRaw_Hose.UseVisualStyleBackColor = true;
            this.BtnExecutionTaskShipmentRaw_Hose.Click += new System.EventHandler(this.BtnExecutionTaskShipmentRaw_Hose_Click);
            // 
            // BtnExecutionTaskShipmentRaw_ExternalConsumers
            // 
            this.BtnExecutionTaskShipmentRaw_ExternalConsumers.Location = new System.Drawing.Point(14, 11);
            this.BtnExecutionTaskShipmentRaw_ExternalConsumers.Margin = new System.Windows.Forms.Padding(2);
            this.BtnExecutionTaskShipmentRaw_ExternalConsumers.Name = "BtnExecutionTaskShipmentRaw_ExternalConsumers";
            this.BtnExecutionTaskShipmentRaw_ExternalConsumers.Size = new System.Drawing.Size(274, 26);
            this.BtnExecutionTaskShipmentRaw_ExternalConsumers.TabIndex = 0;
            this.BtnExecutionTaskShipmentRaw_ExternalConsumers.Text = "ExecutionTaskShipmentRaw_ExternalConsumers";
            this.BtnExecutionTaskShipmentRaw_ExternalConsumers.UseVisualStyleBackColor = true;
            this.BtnExecutionTaskShipmentRaw_ExternalConsumers.Click += new System.EventHandler(this.BtnExecutionTaskShipmentRaw_ExternalConsumers_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.txtUserName);
            this.tabPage4.Controls.Add(this.txtPassword);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(693, 193);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "User";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(108, 12);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(245, 20);
            this.txtUserName.TabIndex = 38;
            this.txtUserName.Text = "ASU_elevator";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(108, 36);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(245, 20);
            this.txtPassword.TabIndex = 37;
            this.txtPassword.Text = "ASU_elevator1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 40);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "Password";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 18);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "UserName";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(384, 310);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "NumberVehicles";
            // 
            // txtNumberVehicles
            // 
            this.txtNumberVehicles.Location = new System.Drawing.Point(492, 307);
            this.txtNumberVehicles.Name = "txtNumberVehicles";
            this.txtNumberVehicles.Size = new System.Drawing.Size(205, 20);
            this.txtNumberVehicles.TabIndex = 24;
            // 
            // txtResponsibleCode
            // 
            this.txtResponsibleCode.Location = new System.Drawing.Point(112, 277);
            this.txtResponsibleCode.Name = "txtResponsibleCode";
            this.txtResponsibleCode.Size = new System.Drawing.Size(244, 20);
            this.txtResponsibleCode.TabIndex = 16;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TxtResult);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 353);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(701, 208);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Отклик";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 279);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "ResponsibleCode";
            // 
            // txtRaw_Code
            // 
            this.txtRaw_Code.Location = new System.Drawing.Point(111, 252);
            this.txtRaw_Code.Name = "txtRaw_Code";
            this.txtRaw_Code.Size = new System.Drawing.Size(245, 20);
            this.txtRaw_Code.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 256);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Raw_Code";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 234);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "GUID";
            // 
            // txtGUID
            // 
            this.txtGUID.Location = new System.Drawing.Point(111, 228);
            this.txtGUID.Name = "txtGUID";
            this.txtGUID.Size = new System.Drawing.Size(245, 20);
            this.txtGUID.TabIndex = 34;
            // 
            // txtWarehouse
            // 
            this.txtWarehouse.Location = new System.Drawing.Point(492, 225);
            this.txtWarehouse.Name = "txtWarehouse";
            this.txtWarehouse.Size = new System.Drawing.Size(207, 20);
            this.txtWarehouse.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(384, 227);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Warehouse";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(492, 279);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(46, 20);
            this.txtQuantity.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(384, 284);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "Quantity";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(492, 252);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(163, 20);
            this.dtpDate.TabIndex = 39;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(384, 257);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 40;
            this.label7.Text = "Дата";
            // 
            // txtPartner
            // 
            this.txtPartner.Location = new System.Drawing.Point(112, 303);
            this.txtPartner.Name = "txtPartner";
            this.txtPartner.Size = new System.Drawing.Size(245, 20);
            this.txtPartner.TabIndex = 42;
            // 
            // txtOrganization
            // 
            this.txtOrganization.Location = new System.Drawing.Point(113, 328);
            this.txtOrganization.Name = "txtOrganization";
            this.txtOrganization.Size = new System.Drawing.Size(244, 20);
            this.txtOrganization.TabIndex = 43;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 307);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 41;
            this.label11.Text = "Partner";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 330);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 13);
            this.label13.TabIndex = 44;
            this.label13.Text = "Organization";
            // 
            // FormTest_ASU_elevator_1c
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 561);
            this.Controls.Add(this.txtPartner);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtOrganization);
            this.Controls.Add(this.txtNumberVehicles);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.txtQuantity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtWarehouse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtGUID);
            this.Controls.Add(this.txtRaw_Code);
            this.Controls.Add(this.txtResponsibleCode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TabControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormTest_ASU_elevator_1c";
            this.Text = "Test_ASU_elevator_1c";
            this.TabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusSenderWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusReceiptWarehouse)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnTestCompletingMove;
        private System.Windows.Forms.TextBox TxtResult;
        private System.Windows.Forms.TabControl TabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.NumericUpDown txtStatusReceiptWarehouse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtReceiptWarehouse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtResponsibleCode;
        private System.Windows.Forms.CheckBox cbExecuteCleaningSeparator;
        private System.Windows.Forms.CheckBox cbExecuteCleaningScalper;
        private System.Windows.Forms.CheckBox cbExecuteDrying;
        private System.Windows.Forms.NumericUpDown txtStatusSenderWarehouse;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSenderWarehouse;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtNumberVehicles;
        private System.Windows.Forms.CheckBox cbExecuteDryingCompletingRawMaterial;
        private System.Windows.Forms.CheckBox cbExecuteCleaningSeparatorCompletingRawMaterial;
        private System.Windows.Forms.Button BtnTestCompletingRawMaterial;
        private System.Windows.Forms.CheckBox cbExecuteCleaningCompletingRawMaterial;
        private System.Windows.Forms.CheckBox cbQualityControl;
        private System.Windows.Forms.TextBox txtRaw_Code;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtGUID;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button BtnExecutionTaskShipmentRaw_KZ;
        private System.Windows.Forms.Button BtnExecutionTaskShipmentRaw_Hose;
        private System.Windows.Forms.Button BtnExecutionTaskShipmentRaw_ExternalConsumers;
        private System.Windows.Forms.Button BtnWeighingOfVehicles;
        private System.Windows.Forms.Button BtnInventory;
        private System.Windows.Forms.Button BtnPing;
        private System.Windows.Forms.TextBox txtWarehouse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txtQuantity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPartner;
        private System.Windows.Forms.TextBox txtOrganization;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
    }
}

