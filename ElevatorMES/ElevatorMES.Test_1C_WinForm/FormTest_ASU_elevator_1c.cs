﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElevatorMES.Test_1C_WinForm
{
    public partial class FormTest_ASU_elevator_1c : Form
    {
        public FormTest_ASU_elevator_1c()
        {
            InitializeComponent();
        }

        private ServiceReference1.WebService_ASU_elevatorPortTypeClient GetClient()
        {
            #region Для HTTP Basic
            //var client = new ServiceReference1.WebService_ASU_elevatorPortTypeClient("WebService_ASU_elevatorSoap");

            //client.ClientCredentials.UserName.UserName = "ASU_elevator";  // Authorization: Basic QVNVX2VsZXZhdG9yOkFTVV9lbGV2YXRvcjE=
            //client.ClientCredentials.UserName.Password = "ASU_elevator1";
            #endregion

            #region Для Https NTML
            var _client = new ServiceReference1.WebService_ASU_elevatorPortTypeClient();

            if (_client.ClientCredentials != null)
            {
                //_client.ClientCredentials.Windows.ClientCredential = new NetworkCredential(txtUserName.Text, txtPassword.Text/*"TimoshevskiyKO", "iQnZkJ2P7V2E9FF"*/);
                _client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
                _client.ClientCredentials.Windows.ClientCredential = CredentialCache.DefaultNetworkCredentials;
            }
            #endregion

            _client.Open();
            return _client;
        }


        private void BtnTestCompletingMove_Click(object sender, EventArgs e)
        {
            txtGUID.Text = String.IsNullOrEmpty(txtGUID.Text) ? Guid.NewGuid().ToString() : txtGUID.Text;

            var client = GetClient();
            var tbl = new ServiceReference1.CompletingMove()
            {
                GUID = txtGUID.Text,
                Raw_Code = txtRaw_Code.Text.ToString(),
                ReceiptWarehouse = txtReceiptWarehouse.Text.ToString(),
                    StatusReceiptWarehouse = (float)txtStatusReceiptWarehouse.Value,
                SenderWarehouse = txtSenderWarehouse.Text.ToString(),
                    StatusSenderWarehouse = (float)txtStatusSenderWarehouse.Value,
                ExecuteDrying = cbExecuteDrying.Checked,
                ExecuteCleaningScalper = cbExecuteCleaningScalper.Checked,
                ExecuteCleaningSeparator = cbExecuteCleaningSeparator.Checked,
                ResponsibleCode = txtResponsibleCode.Text,
            };


            try
            {
                TxtResult.Text = client.CompletingMove(tbl).ToString();
            } 
            catch(Exception ex)
            {
                TxtResult.Text = ex.ToString();
            }
        }

        private void BtnTestCompletingRawMaterial_Click(object sender, EventArgs e)
        {
            txtGUID.Text = String.IsNullOrEmpty(txtGUID.Text) ? Guid.NewGuid().ToString() : txtGUID.Text;

            var client = GetClient();
            var tbl = new ServiceReference1.CompletingRawMaterial()
            {
                GUID = txtGUID.Text,
                NumberVehicles = txtNumberVehicles.Text,
                Raw_Code = txtRaw_Code.Text.ToString(),
                QualityControl = cbQualityControl.Checked,
                ExecuteDrying = cbExecuteDrying.Checked,
                ExecuteCleaningScalper = cbExecuteCleaningScalper.Checked,
                ExecuteCleaningSeparator = cbExecuteCleaningSeparator.Checked,
                ResponsibleCode = txtResponsibleCode.Text,
            };


            try
            {
                TxtResult.Text = client.CompletingRawMaterial(tbl).ToString();
            }
            catch (Exception ex)
            {
                TxtResult.Text = ex.ToString();
            }
        }

        private void BtnExecutionTaskShipmentRaw_ExternalConsumers_Click(object sender, EventArgs e)
        {
            txtGUID.Text = String.IsNullOrEmpty(txtGUID.Text) ? Guid.NewGuid().ToString() : txtGUID.Text;

            var client = GetClient();
            var tbl = new ServiceReference1.ExecutionTaskShipmentRaw_ExternalConsumers()
            {
                GUID = txtGUID.Text,
                NumberVehicles = txtNumberVehicles.Text,
                Quantity = (float)txtQuantity.Value,
                SenderWarehouse = txtSenderWarehouse.Text.ToString(),
                Raw_Code = txtRaw_Code.Text.ToString(),
                Partner = txtPartner.Text,
                Organization = txtOrganization.Text,
                DateExecute = dtpDate.Value,
                ResponsibleCode = txtResponsibleCode.Text,
            };


            try
            {
                TxtResult.Text = client.ExecutionTaskShipmentRaw_ExternalConsumers(tbl).ToString();
            }
            catch (Exception ex)
            {
                TxtResult.Text = ex.ToString();
            }
        }

        private void BtnExecutionTaskShipmentRaw_Hose_Click(object sender, EventArgs e)
        {
            txtGUID.Text = String.IsNullOrEmpty(txtGUID.Text) ? Guid.NewGuid().ToString() : txtGUID.Text;

            var client = GetClient();
            var tbl = new ServiceReference1.ExecutionTaskShipmentRaw_Hose()
            {
                GUID = txtGUID.Text,
                NumberVehicles = txtNumberVehicles.Text,
                Raw_Code = txtRaw_Code.Text.ToString(),
                ReceiptWarehouse = txtReceiptWarehouse.Text.ToString(),
                SenderWarehouse = txtSenderWarehouse.Text.ToString(),

                Quantity = 1,
                DateExecute = DateTime.Now,
                ResponsibleCode = txtResponsibleCode.Text,
            };


            try
            {
                TxtResult.Text = client.ExecutionTaskShipmentRaw_Hose(tbl).ToString();
            }
            catch (Exception ex)
            {
                TxtResult.Text = ex.ToString();
            }
        }

        private void BtnExecutionTaskShipmentRaw_KZ_Click(object sender, EventArgs e)
        {
            txtGUID.Text = String.IsNullOrEmpty(txtGUID.Text) ? Guid.NewGuid().ToString() : txtGUID.Text;

            var client = GetClient();
            var tbl = new ServiceReference1.ExecutionTaskShipmentRaw_KZ()
            {
                
                GUID = txtGUID.Text,
                
                Raw_Code = txtRaw_Code.Text.ToString(),
                Quantity = 1,
                DateExecute = DateTime.Now,
                ResponsibleCode = txtResponsibleCode.Text,
            };


            try
            {
                TxtResult.Text = client.ExecutionTaskShipmentRaw_KZ(tbl).ToString();
            }
            catch (Exception ex)
            {
                TxtResult.Text = ex.ToString();
            }
        }

        private void BtnWeighingOfVehicles_Click(object sender, EventArgs e)
        {
            txtGUID.Text = String.IsNullOrEmpty(txtGUID.Text) ? Guid.NewGuid().ToString() : txtGUID.Text;

            var client = GetClient();
            var tbl = new ServiceReference1.WeighingOfVehicles()
            {
                GUID = txtGUID.Text,
                NumberVehicles = txtNumberVehicles.Text,
                GrossWeight =1,
                DateGrossWeighing = DateTime.Now,
                NettoWeight = 1,
                DateTareWeighing = DateTime.Now,
                TareWeight = 1
            };


            try
            {
                TxtResult.Text = client.WeighingCars(tbl).ToString();
            }
            catch (Exception ex)
            {
                TxtResult.Text = ex.ToString();
            }
        }

        private void BtnPing_Click(object sender, EventArgs e)
        {
            var client = GetClient();
            try
            {
                TxtResult.Text = client.Ping().ToString();
            }
            catch (Exception ex)
            {
                TxtResult.Text = ex.ToString();
            }
        }

        private void BtnInventory_Click(object sender, EventArgs e)
        {
            txtGUID.Text = String.IsNullOrEmpty(txtGUID.Text) ? Guid.NewGuid().ToString() : txtGUID.Text;

            var client = GetClient();
            var tbl = new ServiceReference1.Inventory()
            {
                GUID = txtGUID.Text,
                Raw_Code = txtRaw_Code.Text,
                ResponsibleCode = txtResponsibleCode.Text,
                Warehouse = txtWarehouse.Text,
                Quantity = (float)txtQuantity.Value,
                Date = dtpDate.Value
            };


            try
            {
                TxtResult.Text = client.Inventory(tbl).ToString();
            }
            catch (Exception ex)
            {
                TxtResult.Text = ex.ToString();
            }
        }
    }
}
