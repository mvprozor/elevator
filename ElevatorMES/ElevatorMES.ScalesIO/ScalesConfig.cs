﻿using System;
using System.Collections.Generic;
using ElevatorMES.Domain.Scales;

namespace ElevatorMES.ScalesIO
{
    public class ScalesConfig : IScalesConfig
    {
        public string Hostname { get; set; }

        public int Port { get; set; }

        public string Type { get; set; }

        public string Name
        {
            get => String.IsNullOrWhiteSpace(name) ? Hostname : name;
            set
            {
                name = value;
            }
        }

        private string name;

        public override string ToString()
        {
            return $"{Name} ({Type}, {Hostname}:{Port})";
        }

        public IScales Create(out List<string> validationErrors)
        {
            IScales scales = null;
            validationErrors = new List<string>(5);
            if (String.IsNullOrWhiteSpace(Hostname))
                validationErrors.Add("Не указано сетевое имя или IP");
            if (Port<1 || Port>65535)
                validationErrors.Add($"Некорретный номер порта (должен быть 1-65535): {Port}");
            if (String.IsNullOrWhiteSpace(Type))
                validationErrors.Add("Тип весов не указан");
            else
            {
                switch (Type?.ToLower()?.Trim())
                {
                    case "cas":
                        scales = new CASScales();
                        break;

                    case "keli":
                        scales = new KelliScales();
                        break;

                    case "dummy":
                        scales = new DummyScales();
                        break;

                    default:
                        validationErrors.Add($"Неизвестный тип весов: {Type}. ");
                        break;
                }
            }
            return scales;
        }
    }
}
