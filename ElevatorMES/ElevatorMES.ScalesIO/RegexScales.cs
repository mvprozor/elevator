﻿using ElevatorMES.Domain.Scales;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ElevatorMES.ScalesIO
{
    /// <summary>
    /// Вес типа CAS
    /// </summary>
    public abstract class RegexScales : Scales
    {
        private static Encoding encoding = Encoding.ASCII;

        protected override TimeSpan ReconnectInterval => TimeSpan.FromSeconds(30);

        protected override int ResponseBufferSize => 255;

        protected override TimeSpan ResponseTimeout => TimeSpan.FromSeconds(10);

        protected abstract Regex ResponsePattern { get; }

        protected abstract decimal ToTonnConv { get; }

        protected virtual bool IsReverseDigits => false;

        protected override ScalesData[] ParseData(byte[] buffer, int byteCount)
        {
            var str = encoding.GetString(buffer, 0, byteCount);
            var matches = ResponsePattern.Matches(str);
            if (matches.Count == 0)
                throw new ArgumentException($"Некорректная поcылка ({str?.Length}): {str}");
            var res = new ScalesData[matches.Count];
            for (var i = 0; i < matches.Count; i++)
            {
                var match = matches[i];
                string weightStr;
                bool? isStable = null;
                if (match.Groups.Count < 3)
                    weightStr = match.Groups[1].Value;
                else
                {
                    isStable = match.Groups[1].Success;
                    weightStr = match.Groups[2].Value;
                }
                weightStr = weightStr.Trim().Replace(" ", String.Empty);
                if (IsReverseDigits)
                {
                    var weightArr = weightStr.ToCharArray();
                    Array.Reverse(weightArr);
                    weightStr = new string(weightArr);
                }
                var weight = Decimal.Parse(weightStr, CultureInfo.InvariantCulture) * ToTonnConv;
                res[i] = new ScalesData(weight, isStable);
            }
            return res;
        }
    }
}
