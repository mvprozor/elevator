﻿using ElevatorMES.Domain.Scales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElevatorMES.ScalesIO
{
    public abstract class Scales : IScales
    {
        #region Открытые свойства и методы

        public IScalesConfig DefaultConfig { get; set; }

        public IScalesSession Activate( 
            ScalesEventData.Action onData, 
            ScalesEventError.Action onError,
            ScalesEvent.Action onDeactivated = null,
            IScalesConfig scalesConfig = null,
            object userData = null)
        {
            scalesConfig = scalesConfig ?? DefaultConfig;
            if (scalesConfig == null)
                throw new InvalidOperationException("Не задана конфигурация");
            var deactivator = new CancellationTokenSource();
            var sessionContext = new ScalesSession(scalesConfig,
                deactivator, SynchronizationContext.Current, 
                onData, onError, onDeactivated, userData);
            Task.Factory.StartNew(Connect, sessionContext, deactivator.Token);
            return sessionContext;
        }

        #endregion


        #region Переопределяемые в наследуемых классах свойства и методы

        /// <summary>
        /// Время переподключения после сбоев
        /// </summary>
        protected abstract TimeSpan ReconnectInterval { get; }

        /// <summary>
        /// Размер буфера ответа
        /// </summary>
        protected abstract int ResponseBufferSize { get; }

        /// <summary>
        /// Время ожидания ответа от весов
        /// </summary>
        protected abstract TimeSpan ResponseTimeout { get; }

        /// <summary>
        /// Количество попыток повторной отправка запроса перед переподключением
        /// </summary>
        protected virtual int RetryRequestCount => 3;

        /// <summary>
        /// Функция, возвращающая текущий запрос для весов
        /// </summary>
        /// <returns>Содержимое запроса в виде массива байт</returns>
        protected abstract byte[] GetRequest();

        /// <summary>
        /// Функция разбора полученных от весов наборов данных
        /// </summary>
        /// <param name="buffer">Буфер</param>
        /// <param name="byteCount">Число прочитанных байт</param>
        /// <returns>Один или несколько разобранных наборов данных от весов</returns>
        protected abstract ScalesData[] ParseData(byte[] buffer, int byteCount);

        #endregion


        #region Закрытые поля и методы

        private const int WriteTimeoutMillesecond = 10000;

        private void Connect(object state)
        {
            var sessionContext = (ScalesSession)state;
            TcpClient client = null;
            NetworkStream stream = null;
            while (!sessionContext.IsDeactivationRequest)
            {
                try
                {
                    client = new TcpClient(sessionContext.Config.Hostname, 
                        sessionContext.Config.Port);
                    client.ReceiveTimeout = Convert.ToInt32(ResponseTimeout.TotalMilliseconds);
                    client.SendTimeout = WriteTimeoutMillesecond;
                    stream = client.GetStream();
                    Read(sessionContext, stream);
                }
                catch (OperationCanceledException)
                {
                    if (sessionContext.IsDeactivationRequest)
                        break;
                    else
                        sessionContext.OnError("Операция обмена была прервана");
                }
                catch (Exception exception)
                {
                    sessionContext.OnError(exception);
                }
                finally
                {
                    stream?.Close();
                    client?.Close();
                }
                if (!sessionContext.IsDeactivationRequest)
                    sessionContext.DeactivationToken.WaitHandle.WaitOne(ReconnectInterval);
            };
            sessionContext.ODeactivated();
        }

        private void Read(ScalesSession sessionContext, NetworkStream stream)
        {
            byte[] buffer = new byte[ResponseBufferSize];
            var responseTimeout = Convert.ToInt32(ResponseTimeout.TotalMilliseconds);
            var requestNo = 0;
            while (!sessionContext.IsDeactivationRequest)
            {
                var request = GetRequest();
                var hasRequest = request?.Length > 0;
                if (hasRequest)
                {
                    var taskWrite = stream.WriteAsync(request, 0, request.Length, sessionContext.DeactivationToken);
                    if (!taskWrite.Wait(WriteTimeoutMillesecond, sessionContext.DeactivationToken))
                    {
                        sessionContext.OnError("Таймаут отправки запроса");
                        break;
                    }
                }
                var taskRead = stream.ReadAsync(buffer, 0, buffer.Length, sessionContext.DeactivationToken);
                if (taskRead.Wait(responseTimeout, sessionContext.DeactivationToken))
                {
                    var byteCount = taskRead.Result;
                    if (byteCount <= 0)
                    {
                        sessionContext.OnError("Весы остановили обмен");
                        break;
                    }
                    var scalesDatas = ParseData(buffer, byteCount);
                    if (scalesDatas?.Length > 0)
                        sessionContext.OnData(scalesDatas);
                    requestNo = 0;
                }
                else if (!hasRequest || ++requestNo > RetryRequestCount)
                {
                    sessionContext.OnError("Таймаут ожидания ответа");
                    break;
                }
            }
        }

        #endregion
    }
}
