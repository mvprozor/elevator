﻿using ElevatorMES.Domain.Scales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElevatorMES.ScalesIO
{
    public class DummyScales : IScales
    {
        public IScalesConfig DefaultConfig { get; set; }

        public IScalesSession Activate(ScalesEventData.Action onData, ScalesEventError.Action onError, 
            ScalesEvent.Action onDeactivated = null, IScalesConfig scalesConfig = null, object userData = null)
        {
            var deactivator = new CancellationTokenSource();
            var sessionContext = new ScalesSession(scalesConfig,
                deactivator, SynchronizationContext.Current,
                onData, onError, onDeactivated, userData);
            Task.Factory.StartNew(Generate, sessionContext, deactivator.Token);
            return sessionContext;
        }

        private void Generate(object state)
        {
            var sessionContext = (ScalesSession)state;
            var rnd = new Random();
            try
            {
                while (!sessionContext.IsDeactivationRequest)
                {
                    Task.Delay(TimeSpan.FromSeconds(1), sessionContext.DeactivationToken)
                        .Wait(sessionContext.DeactivationToken);
                    decimal weight = Convert.ToDecimal(rnd.NextDouble() * 20.0);
                    var isStable = rnd.Next(4) != 3;
                    sessionContext.OnData(new ScalesData[] { new ScalesData(weight, isStable) });
                }
            }
            catch(OperationCanceledException)
            {

            }
        }
    }
}
