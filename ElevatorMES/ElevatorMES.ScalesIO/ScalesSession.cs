﻿using ElevatorMES.Domain.Scales;
using System;
using System.Threading;

namespace ElevatorMES.ScalesIO
{
    public class ScalesSession : IScalesSession
    {
        public IScalesConfig Config { get; }
        public CancellationToken DeactivationToken => _deactivator.Token;
        public bool IsDeactivationRequest => DeactivationToken.IsCancellationRequested;
        public bool IsDeactivated => _isDeactivated == 1;
        public object UserData { get; }
        private SynchronizationContext _notifyContext { get; }
        private ScalesEventData.Action _onData;
        private ScalesEventError.Action _onError;
        private ScalesEvent.Action _onDeactivated;
        private CancellationTokenSource _deactivator;
        private int _isDeactivated;

        public ScalesSession(IScalesConfig config,
            CancellationTokenSource deactivator, 
            SynchronizationContext notifyContext,
            ScalesEventData.Action onData,
            ScalesEventError.Action onError,
            ScalesEvent.Action onDeactivated,
            object userData = null)
        {
            Config = config;
            _deactivator = deactivator;
            _notifyContext = notifyContext;
            _onData = onData;
            _onError = onError;
            _onDeactivated = onDeactivated;
            UserData = userData;
        }

        public void Deactivate()
        {
            _deactivator.Cancel();
        }

        internal void OnData(ScalesData[] scalesDatas)
        {
            if (_onData != null)
            {
                var scaleEvent = new ScalesEventData(this, scalesDatas);
                if (_notifyContext == null)
                    _onData(scaleEvent);
                else
                    _notifyContext.Post(OnDataInContext, scaleEvent);
            }
        }

        private void OnDataInContext(object state)
        {
            _onData((ScalesEventData)state);
        }

        internal void OnError(string error)
        {
            if (_onError != null)
            {
                var scaleEvent = new ScalesEventError(this, error);
                if (_notifyContext == null)
                    _onError(scaleEvent);
                else
                    _notifyContext.Post(OnErrorContext, scaleEvent);
            }
        }

        internal void OnError(Exception exception)
        {
            OnError((exception.InnerException ?? exception).Message);
        }

        private void OnErrorContext(object state)
        {
            _onError((ScalesEventError)state);
        }

        internal void ODeactivated()
        {
            if (_onDeactivated != null)
            {
                var scaleEvent = new ScalesEvent(this);
                if (_notifyContext == null)
                    _onDeactivated(scaleEvent);
                else
                    _notifyContext.Post(OnDeactivatedContext, scaleEvent);
            }
        }

        private void OnDeactivatedContext(object state)
        {
            Interlocked.Exchange(ref _isDeactivated, 1);
            _onDeactivated((ScalesEvent)state);
        }

    }
}
