﻿using ElevatorMES.Domain.Scales;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ElevatorMES.ScalesIO
{
    public class KelliScales : RegexScales
    {
        private static Regex rxResponse = new Regex(@"\.(\d{6})", RegexOptions.Compiled);

        protected override Regex ResponsePattern => rxResponse;

        protected override decimal ToTonnConv => 0.001m;

        protected override bool IsReverseDigits => true;

        protected override byte[] GetRequest()
        {
            return null;
        }
    }
}
