﻿using ElevatorMES.Domain.Scales;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ElevatorMES.ScalesIO
{
    /// <summary>
    /// Вес типа CAS
    /// </summary>
    public class CASScales : RegexScales
    {
        private static Regex rxResponse = new Regex(@"(ST)?\S+,(?:(\-?\s*\d+(?:.\d+)?)(?:\s*kg))", RegexOptions.Compiled);

        protected override decimal ToTonnConv => 0.001m;

        protected override Regex ResponsePattern => rxResponse;

        protected override byte[] GetRequest()
        {
            return new byte[] { 1 };
        }
    }
}
