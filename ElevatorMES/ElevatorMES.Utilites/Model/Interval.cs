﻿using System;
using System.ComponentModel;
using ElevatorMES.Utilites.Enum;

namespace ElevatorMES.Utilites.Model
{
    public class Interval : IEquatable<Interval>
    {
        [Description("Начало")]
        public DateTime? Begin { get; set; }
        [Description("Окончание")]
        public DateTime? End { get; set; }

        public bool Equals(Interval other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Begin.Equals(other.Begin) && End.Equals(other.End);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Interval) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Begin.GetHashCode() * 397) ^ End.GetHashCode();
            }
        }

        private void Init(DateTime? begin, DateTime? end)
        {
            if (begin <= end)
            {
                Begin = begin;
                End = end;
            }
            else
            {
                End = begin;
                Begin = end;
            }
        }
        public Interval(DateTime? begin, DateTime? end)
        {
           Init(begin, end);
        }

        public Interval(IntervalType intervalType, Interval interval = null )
        {
            if (interval != null)
            {
                Init(interval.Begin, interval.End);
            }
            switch (intervalType)
            {
                case IntervalType.Today:
                {
                    Begin = DateTime.Today;
                    End = DateTime.Today.AddDays(+1);
                    return;
                }
                case IntervalType.Yesterday:
                {
                    Begin = DateTime.Today.AddDays(-1);
                    End = DateTime.Today;
                    return;
                }
                case IntervalType.Tommorow:
                {
                    Begin = DateTime.Today.AddDays(+1);
                    End = DateTime.Today.AddDays(+2);
                    return;
                }
                case IntervalType.BeginMonth:
                {
                    Begin = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    End = DateTime.Now;
                    return;
                }
                case IntervalType.BeginYear:
                    {
                        Begin = new DateTime(DateTime.Now.Year, 1, 1);
                        End = DateTime.Now;
                        return;
                    }
            }
        }
    }
}
