﻿using ElevatorMES.Utilites.Exceptions.Base;

namespace ElevatorMES.Utilites.Exceptions
{
    public class LogicException : BaseException
    {
        public LogicException(string message) 
            : base(message)
        {
            
        }
    }
}
