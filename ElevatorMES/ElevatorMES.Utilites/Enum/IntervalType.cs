﻿using System.ComponentModel;

namespace ElevatorMES.Utilites.Enum
{
    public enum IntervalType
    {
        [Description("Сегодня")]
        Today,
        [Description("Завтра")]
        Tommorow,
        [Description("Вчера")]
        Yesterday,
        [Description("С начала месяца")]
        BeginMonth,
        [Description("С начала года")]
        BeginYear,
        [Description("Заданный период")]
        CurrentPeriod
    }
}
