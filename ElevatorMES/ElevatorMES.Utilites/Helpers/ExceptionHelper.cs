﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Utilites.Helpers
{
    public static class ExceptionHelper 
    {
        public static string ExtractMessage(this Exception exception)
        {
            return (exception.InnerException ?? exception).Message;
        }
    }
}
