﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace ElevatorMES.Utilites.Helpers
{
    public static class EnumExtension
    {
        public static T GetAttribute<T>(this System.Enum value) where T : Attribute
        {
            var field = value.GetType().GetField(value.ToString());
            return field.GetCustomAttribute<T>();
        }

        public static string GetDescription(this System.Enum value)
        {
            var attribute = value.GetAttribute<DescriptionAttribute>();
            return attribute == null ? value.ToString() : attribute.Description;
        }

        public static IEnumerable<T> ToValues<T>() where T : struct
        {
            return System.Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static IList<KeyValuePair<T,string>> ToListKeyDesc<T>() where T : System.Enum
        {
            return System.Enum.GetValues(typeof(T)).Cast<T>().Select(
                m => new KeyValuePair<T, string>(m, m.GetDescription())).ToArray();
        }

        public static Dictionary<T, string> ToDictionary<T>() where T : struct
        {
            return ToValues<T>().ToDictionary(n => n, n => ((System.Enum)(object)n).GetDescription());
        }

    }
}
