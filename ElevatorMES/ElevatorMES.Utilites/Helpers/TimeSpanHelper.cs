﻿using System;
using System.Globalization;

namespace ElevatorMES.Utilites.Helpers
{
    public static class TimeSpanHelper
    {
        public static bool TryParseTimeSpan(this string source, out TimeSpan result)
        {
            if (TimeSpan.TryParse(source, CultureInfo.CurrentCulture, out result))
                return true;
            else
                return TimeSpan.TryParse(source, CultureInfo.InvariantCulture, out result);
        }

        public static string ToStringFriendly(this TimeSpan timeSpan)
        {
            if (timeSpan.TotalSeconds < 1.0)
                return "-";
            else if (timeSpan.TotalSeconds < 60)
                return $"{timeSpan.TotalSeconds:0.} сек.";
            else
            {
                timeSpan += TimeSpan.FromSeconds(30);
                if (timeSpan.TotalDays < 1.0)
                {
                    if (timeSpan.TotalHours < 1.0)
                        return $"{timeSpan.Minutes} мин.";
                    else
                        return $"{timeSpan.TotalHours:0.0} час.";
                }
                else
                    return $"{timeSpan.TotalDays:0.0} сут.";
            }
        }
    }
}
