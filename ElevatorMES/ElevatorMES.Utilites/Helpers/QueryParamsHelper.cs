﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Linq;
using System.Text;

namespace ElevatorMES.Utilites.Helpers
{
    public static class QueryParamsHelper
    {
        private abstract class ParamBase<TItem>
        {
            protected readonly string _name;
            protected readonly string _format;


            public ParamBase(string name, string format)
            {
                _name = name;
                _format = format;
            }

            protected abstract string GetStringValue(TItem item);

            public string ToString(TItem item)
            {
                var stringValue = GetStringValue(item);
                return String.IsNullOrWhiteSpace(stringValue) ? String.Empty :
                    $"{_name}={Uri.EscapeDataString(stringValue)}";
            }
        }

        private class QueryParam<TItem, TValue> : ParamBase<TItem>
        {
            private readonly Func<TItem, TValue> _getter;
            public QueryParam(string name, string format, Func<TItem, TValue> getter) 
                : base(name, format) { _getter = getter; }
            protected override string GetStringValue(TItem item) 
            {
                var value = _getter(item);
                if (value == null)
                    return null;
                var formattable = value as IFormattable;
                return formattable == null ? value.ToString() 
                    : formattable.ToString(_format, CultureInfo.InvariantCulture);
            }
        }

        private class QueryParamNullable<TItem, TValue> : ParamBase<TItem> where TValue: struct
        {
            private readonly Func<TItem, TValue?> _getter;
            public QueryParamNullable(string name, string format, Func<TItem, TValue?> getter) 
                : base(name, format) { _getter = getter; }
            protected override string GetStringValue(TItem item) 
            {
                var value = _getter(item);
                if (value == null)
                    return null;
                var formattable = value as IFormattable;
                return formattable == null ? value.ToString()
                    : formattable.ToString(_format, CultureInfo.InvariantCulture);
            }
        }



        private static Dictionary<Type, object> paramsByTypes = new Dictionary<Type, object>(16);

        public static string ToUriString<TItem>(TItem item)
        {
            if (item == null)
                return String.Empty;
            var dict = item as IDictionary<string, object>;
            if (dict != null)
                return ToUriString(dict);
            Type itemType = typeof(TItem);
            ParamBase<TItem>[] itemParams = null;
            if (paramsByTypes.TryGetValue(itemType, out var _paramsByType))
            {
                itemParams = (ParamBase<TItem>[])_paramsByType;
            }
            else
            {
                PropertyInfo[] properties = itemType.GetProperties(BindingFlags.Public |
                    BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
                var itemParamsList = new List<ParamBase<TItem>>(properties.Length);
                for (int i = 0; i < properties.Length; i++)
                {
                    PropertyInfo property = properties[i];
                    MethodInfo getMethod = property.GetGetMethod(true);
                    if (getMethod != null)
                    {
                        Type origType = property.PropertyType;
                        Type valueType;
                        var name = property.Name;
                        var isNullable = false;
                        if (origType.IsGenericType && origType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            // nullable type
                            valueType = Nullable.GetUnderlyingType(origType);
                            isNullable = true;
                        }
                        else
                            valueType = origType;

                        string format = null;
                        if (valueType == typeof(DateTime))
                            format = "s";
                        ParamBase<TItem> queryParam;
                        if (isNullable)
                        {
                            var queryParamType = typeof(QueryParamNullable<,>).MakeGenericType(typeof(TItem), valueType);
                            var getterType = typeof(Func<,>).MakeGenericType(typeof(TItem), origType);
                            queryParam = (ParamBase<TItem>)Activator.CreateInstance(queryParamType,
                                name, format, getMethod.CreateDelegate(getterType));
                        }
                        else
                        {
                            var queryParamType = typeof(QueryParam<,>).MakeGenericType(typeof(TItem), valueType);
                            var getterType = typeof(Func<,>).MakeGenericType(typeof(TItem), valueType);
                            queryParam = (ParamBase<TItem>)Activator.CreateInstance(queryParamType,
                                name, format, getMethod.CreateDelegate(getterType));
                        }
                        itemParamsList.Add(queryParam);
                    }
                }
                itemParams = itemParamsList.ToArray();
                paramsByTypes.Add(itemType, itemParams);
            }
            return String.Join("&", itemParams.Select(m => m.ToString(item)).Where(n => !String.IsNullOrEmpty(n)));
        }

        public static string ToUriString(IDictionary<string, object> paramValues)
        {
            StringBuilder sb = new StringBuilder(paramValues.Count*32);
            foreach(var paramValue in paramValues)
            {
                if (paramValue.Value != null)
                {
                    var valueType = paramValue.Value.GetType();
                    if (valueType==typeof(DateTime))
                    {
                        var dateTime = (DateTime)paramValue.Value;
                        var dataString = Uri.EscapeDataString(dateTime.ToString("s"));
                        sb.Append($"{paramValue.Key}={dataString}&");
                    } else if (valueType == typeof(DateTime?))
                    {
                        var dateTime = (DateTime?)paramValue.Value;
                        if (dateTime.HasValue)
                        {
                            var dataString = Uri.EscapeDataString(dateTime.Value.ToString("s"));
                            sb.Append($"{paramValue.Key}={dataString}&");
                        }
                    }
                    else
                    {
                        var list = paramValue.Value as IList;
                        if (list==null)
                        {
                            var stringValue = String.Format(CultureInfo.InvariantCulture, "{0}", paramValue.Value);
                            if (!String.IsNullOrWhiteSpace(stringValue))
                            {
                                var dataString = Uri.EscapeDataString(stringValue);
                                sb.Append($"{paramValue.Key}={dataString}&");
                            }
                        }
                        else
                        {
                            StringBuilder stringValue = new StringBuilder(64);
                            var div = String.Empty;
                            foreach (var item in list)
                            {
                                var stringItemValue = String.Format(CultureInfo.InvariantCulture, "{0}", item);
                                var dataString = Uri.EscapeDataString(stringItemValue);
                                stringValue.Append(div);
                                stringValue.Append(dataString);
                                div = ",";
                            }
                            sb.Append($"{paramValue.Key}={stringValue}&");
                        }
                    }
                }
            }
            if (sb.Length > 0 && sb[sb.Length - 1] == '&')
                sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }
    }
}
