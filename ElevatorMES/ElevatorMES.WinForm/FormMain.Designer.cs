﻿using System.Windows.Forms;

namespace ElevatorMES.WinForm
{
    public partial class FormMain
    {
        private ToolStrip tsMain;
        private ToolStripComboBox tscbStatus;
        private ToolStripComboBox tscbDateFilter;
        private ToolStripButton btnRefresh;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripButton btnTaskStart;
        private ToolStripButton btnTaskSuspend;
        private ToolStripButton btnTaskStop;
        private DataGridView grdMain;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tscbStatus = new System.Windows.Forms.ToolStripComboBox();
            this.tscbDateFilter = new System.Windows.Forms.ToolStripComboBox();
            this.tbTextFilter = new System.Windows.Forms.ToolStripTextBox();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnTaskStart = new System.Windows.Forms.ToolStripButton();
            this.btnTaskSuspend = new System.Windows.Forms.ToolStripButton();
            this.btnTaskStop = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAdd = new System.Windows.Forms.ToolStripSplitButton();
            this.btnAddTaskAcceptance = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAddTaskShipment = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnUndoTask = new System.Windows.Forms.ToolStripButton();
            this.grdMain = new System.Windows.Forms.DataGridView();
            this.ColId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabelCountAutoRegistered = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.StatusLabelTaskCountSelected = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMain)).BeginInit();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsMain
            // 
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tscbStatus,
            this.tscbDateFilter,
            this.tbTextFilter,
            this.btnRefresh,
            this.toolStripSeparator1,
            this.btnTaskStart,
            this.btnTaskSuspend,
            this.btnTaskStop,
            this.toolStripSeparator2,
            this.btnAdd,
            this.toolStripSeparator3,
            this.btnUndoTask});
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(965, 26);
            this.tsMain.TabIndex = 4;
            this.tsMain.Text = "toolStrip1";
            // 
            // tscbStatus
            // 
            this.tscbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tscbStatus.Name = "tscbStatus";
            this.tscbStatus.Size = new System.Drawing.Size(121, 26);
            // 
            // tscbDateFilter
            // 
            this.tscbDateFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tscbDateFilter.Items.AddRange(new object[] {
            "Сегодня",
            "Завтра",
            "Вчера",
            "С начала месяца",
            "С начала года",
            "Заданный период"});
            this.tscbDateFilter.Name = "tscbDateFilter";
            this.tscbDateFilter.Size = new System.Drawing.Size(121, 26);
            // 
            // tbTextFilter
            // 
            this.tbTextFilter.Name = "tbTextFilter";
            this.tbTextFilter.Size = new System.Drawing.Size(100, 26);
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = global::ElevatorMES.WinForm.Properties.Resources.Refresh16;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(0, 3, 0, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(23, 21);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 26);
            // 
            // btnTaskStart
            // 
            this.btnTaskStart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTaskStart.Image = global::ElevatorMES.WinForm.Properties.Resources.start_icon;
            this.btnTaskStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTaskStart.Margin = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.btnTaskStart.Name = "btnTaskStart";
            this.btnTaskStart.Size = new System.Drawing.Size(23, 20);
            this.btnTaskStart.Text = "Старт";
            this.btnTaskStart.Click += new System.EventHandler(this.btnTaskStart_Click);
            // 
            // btnTaskSuspend
            // 
            this.btnTaskSuspend.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTaskSuspend.Image = global::ElevatorMES.WinForm.Properties.Resources.Suspend16;
            this.btnTaskSuspend.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTaskSuspend.Margin = new System.Windows.Forms.Padding(0, 3, 0, 2);
            this.btnTaskSuspend.Name = "btnTaskSuspend";
            this.btnTaskSuspend.Size = new System.Drawing.Size(23, 21);
            this.btnTaskSuspend.Text = "Приостановить";
            this.btnTaskSuspend.Click += new System.EventHandler(this.btnTaskSuspend_Click);
            // 
            // btnTaskStop
            // 
            this.btnTaskStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTaskStop.Image = global::ElevatorMES.WinForm.Properties.Resources.Stop16;
            this.btnTaskStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTaskStop.Margin = new System.Windows.Forms.Padding(0, 3, 0, 2);
            this.btnTaskStop.Name = "btnTaskStop";
            this.btnTaskStop.Size = new System.Drawing.Size(23, 21);
            this.btnTaskStop.Text = "Стоп";
            this.btnTaskStop.Click += new System.EventHandler(this.btnTaskStop_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 26);
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddTaskAcceptance,
            this.btnAddTaskShipment});
            this.btnAdd.Image = global::ElevatorMES.WinForm.Properties.Resources.Add16;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Margin = new System.Windows.Forms.Padding(0, 3, 0, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(32, 21);
            this.btnAdd.Text = "Создание задания";
            this.btnAdd.ButtonClick += new System.EventHandler(this.btnAdd_ButtonClick);
            // 
            // btnAddTaskAcceptance
            // 
            this.btnAddTaskAcceptance.Name = "btnAddTaskAcceptance";
            this.btnAddTaskAcceptance.Size = new System.Drawing.Size(124, 22);
            this.btnAddTaskAcceptance.Text = "Приемка";
            this.btnAddTaskAcceptance.Click += new System.EventHandler(this.btnAddTaskAcceptance_Click);
            // 
            // btnAddTaskShipment
            // 
            this.btnAddTaskShipment.Enabled = false;
            this.btnAddTaskShipment.Name = "btnAddTaskShipment";
            this.btnAddTaskShipment.Size = new System.Drawing.Size(124, 22);
            this.btnAddTaskShipment.Text = "Отгрузка";
            this.btnAddTaskShipment.Click += new System.EventHandler(this.btnAddShipment_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 26);
            // 
            // btnUndoTask
            // 
            this.btnUndoTask.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndoTask.Image = global::ElevatorMES.WinForm.Properties.Resources.Undo16;
            this.btnUndoTask.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndoTask.Margin = new System.Windows.Forms.Padding(0, 3, 0, 2);
            this.btnUndoTask.Name = "btnUndoTask";
            this.btnUndoTask.Size = new System.Drawing.Size(23, 21);
            this.btnUndoTask.Text = "Отмена";
            // 
            // grdMain
            // 
            this.grdMain.AllowUserToAddRows = false;
            this.grdMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColId,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.ColStatus,
            this.Column7,
            this.Column8});
            this.grdMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdMain.Location = new System.Drawing.Point(0, 26);
            this.grdMain.MultiSelect = false;
            this.grdMain.Name = "grdMain";
            this.grdMain.RowHeadersVisible = false;
            this.grdMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdMain.Size = new System.Drawing.Size(965, 499);
            this.grdMain.TabIndex = 5;
            this.grdMain.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdMain_CellFormatting);
            this.grdMain.SelectionChanged += new System.EventHandler(this.grdMain_SelectionChanged);
            // 
            // ColId
            // 
            this.ColId.DataPropertyName = "Id";
            this.ColId.HeaderText = "Id";
            this.ColId.Name = "ColId";
            this.ColId.ReadOnly = true;
            this.ColId.Visible = false;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "TypeDescr";
            this.Column1.HeaderText = "Тип";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "NomenclatureDisplayName";
            this.Column2.HeaderText = "Номенклатура";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Source";
            this.Column3.HeaderText = "Откуда";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Destination";
            this.Column4.HeaderText = "Куда";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Operations";
            this.Column5.HeaderText = "Операции";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "EndAt";
            this.Column6.HeaderText = "Выполнить";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // ColStatus
            // 
            this.ColStatus.DataPropertyName = "StatusDescr";
            this.ColStatus.HeaderText = "Статус";
            this.ColStatus.Name = "ColStatus";
            this.ColStatus.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "PlanFact";
            this.Column7.HeaderText = "План/Факт";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column8.DataPropertyName = "Comment";
            this.Column8.HeaderText = "Примечание";
            this.Column8.MinimumWidth = 100;
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabelCountAutoRegistered,
            this.toolStripSeparator4,
            this.StatusLabelTaskCountSelected});
            this.StatusStrip.Location = new System.Drawing.Point(0, 502);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(965, 23);
            this.StatusStrip.TabIndex = 7;
            this.StatusStrip.Text = "statusStrip1";
            this.StatusStrip.Visible = false;
            // 
            // statusLabelCountAutoRegistered
            // 
            this.statusLabelCountAutoRegistered.Name = "statusLabelCountAutoRegistered";
            this.statusLabelCountAutoRegistered.Size = new System.Drawing.Size(260, 18);
            this.statusLabelCountAutoRegistered.Text = "Зарегистировано 24 единицы автотранспорта";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 23);
            // 
            // StatusLabelTaskCountSelected
            // 
            this.StatusLabelTaskCountSelected.Name = "StatusLabelTaskCountSelected";
            this.StatusLabelTaskCountSelected.Size = new System.Drawing.Size(260, 18);
            this.StatusLabelTaskCountSelected.Text = "Выбрано 3 задания, из них незавершенных - 3";
            // 
            // FormMain
            // 
            this.ClientSize = new System.Drawing.Size(965, 525);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.grdMain);
            this.Controls.Add(this.tsMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "Задания";
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMain)).EndInit();
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private ToolStripTextBox tbTextFilter;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripSplitButton btnAdd;
        private ToolStripMenuItem btnAddTaskAcceptance;
        private ToolStripButton btnUndoTask;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripMenuItem btnAddTaskShipment;
        private StatusStrip StatusStrip;
        private ToolStripStatusLabel statusLabelCountAutoRegistered;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripStatusLabel StatusLabelTaskCountSelected;
        private DataGridViewTextBoxColumn ColId;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column4;
        private DataGridViewTextBoxColumn Column5;
        private DataGridViewTextBoxColumn Column6;
        private DataGridViewTextBoxColumn ColStatus;
        private DataGridViewTextBoxColumn Column7;
        private DataGridViewTextBoxColumn Column8;
    }
}