﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ElevatorMES.Data;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Enum;
using ElevatorMES.Utilites.Helpers;
using ElevatorMES.Utilites.Model;
using ElevatorMES.WinForm.Dialogs;
using ElevatorMES.WinForm.Helpers;

namespace ElevatorMES.WinForm
{
    
    public partial class FormMain : Form
    {
        private IEnumerable<TaskUIItem> TaskItems { get; set; }
        private static UnitOfWork db => new UnitOfWork(new DatabaseContext());
        private TaskManager TaskManager => new TaskManager(db);
        private DictManager DictManager => new DictManager(db);
        private GetTaskStatusEnum SelectedStatus => tscbStatus.ComboBox.GetComboboxItem<GetTaskStatusEnum>();
        private IntervalType SelectedIntervalType => tscbDateFilter.ComboBox.GetComboboxItem<IntervalType>();
        private TaskUIItem SelectedTask => grdMain.SelectedRows.Count < 1  ? null : (TaskUIItem) grdMain.SelectedRows[0].DataBoundItem;
        public FormMain()
        {
            InitializeComponent();
            FillFilters();
            UpdateButtonsEnability();
        }

        private void FillFilters()
        {
            tscbStatus.ComboBox.SetComboBoxSourceFromEnum<GetTaskStatusEnum>();
            tscbDateFilter.ComboBox.SetComboBoxSourceFromEnum<IntervalType>();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void FillGrid()
        {
            var selectedTaskId = SelectedTask?.Id;

            var interval = new Interval(SelectedIntervalType);

            var arg = new GetTaskArg()
            {
                TaskStatus = SelectedStatus,
                Begin = SelectedStatus == GetTaskStatusEnum.Active ? null : interval.Begin,
                End = SelectedStatus == GetTaskStatusEnum.Active ? null : interval.End,
                TextFilter = tbTextFilter.Text
            };
            var taskUI = ExecuteResult(() => TaskManager.GetTaskUI(arg));
            TaskItems = taskUI;
            grdMain.DataSource = TaskItems;

            SelectGridItemById(selectedTaskId);
            UpdateButtonsEnability();
        }

        private void SelectGridItemById(int? selectedTaskId)
        {
            if(grdMain.SelectedRows.Count < 1) return;
            //grdMain.SelectedRows.Clear();
            if(selectedTaskId == null) return;
            foreach (DataGridViewRow row in grdMain.Rows)
            {
                if ((row.DataBoundItem as TaskUIItem)?.Id == selectedTaskId)
                    row.Selected = true;
            }
        }

        private void grdMain_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e
        )
        {
            if (grdMain.Columns[e.ColumnIndex].Name == ColStatus.Name)
            {
                var item = GetItem(e.RowIndex);
                if(item == null ) return;
                var defColorAttr = item.Status.GetAttribute<DefaultValueAttribute>()?.Value;
                if (defColorAttr != null)
                {
                    e.CellStyle.BackColor = (Color)defColorAttr;
                }
            }
        }

        private TaskUIItem GetItem(int rowIndex)
        {
            var result = ExecuteResult(() =>
                {
                    var id = (grdMain?.Rows[rowIndex].DataBoundItem as TaskUIItem)?.Id;
                    return id != null ? TaskItems.SingleOrDefault(r => r.Id == int.Parse(id.ToString())) : null;
                });
            return result;
        }

        private void btnAddShipment_Click(object sender, EventArgs e)
        {

        }

        private void btnTaskStart_Click(object sender, EventArgs e)
        {
            ExecuteAction(()=>TaskManager.Start(new TaskStartArg() { TaskId = SelectedTask.Id }));
            FillGrid();
        }

        private void grdMain_SelectionChanged(object sender, EventArgs e)
        {
            UpdateButtonsEnability();
        }

        private void UpdateButtonsEnability()
        {
            var isHideAll = SelectedTask == null;
            var t = SelectedTask;
            btnTaskStart.Enabled = !isHideAll && new[] { TaskStatus.Queued, TaskStatus.Suspended }.Contains(t.Status);
            btnTaskSuspend.Enabled = !isHideAll && TaskStatus.Running == t.Status;
            btnTaskStop.Enabled = !isHideAll && new[] { TaskStatus.Running, TaskStatus.Suspended }.Contains(t.Status);
            btnUndoTask.Enabled = !isHideAll;
        }

        private void btnTaskSuspend_Click(object sender, EventArgs e)
        {
            var dlg = new TaskSuspendDialog()
            {
                Task = SelectedTask
            };
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                var result = dlg.Result;
                ExecuteAction(()=>
                        TaskManager.Stop(new TaskStopArg()
                        {
                            Comment = result.Comment,
                            IsCloseTask = false,
                            IsFailed = result.IsFailed,
                            TaskId = SelectedTask.Id,
                            Weight = result.WeightKilogram
                        })
                    );
                FillGrid();
            }
        }

        private void btnTaskStop_Click(object sender, EventArgs e)
        {
            var dlg = new TaskEndDialog()
            {
                Task = SelectedTask
            };
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                var result = dlg.Result;
                ExecuteAction(() =>
                    TaskManager.Stop(new TaskStopArg()
                    {
                        Comment = result.Comment,
                        IsCloseTask = true,
                        IsFailed = result.IsFailed,
                        TaskId = SelectedTask.Id,
                        Weight = result.WeightKilogram
                    }));

                FillGrid();
            }
        }

        private void btnAddTaskAcceptance_Click(object sender, EventArgs e)
        {
            var dlg = new TaskAddDialog()
            {
                NomenclatureList = DictManager.GetNomenclatures(),
                OrganizationList = DictManager.GetOrganizations(),
                SilosList = DictManager.GetSilos()
            };
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                var result = dlg.Result;
                ExecuteAction(() =>
                    TaskManager.RowAccept(new RowAcceptArg()
                    {
                        VehicleRegNum = result.VehicleRegNum,
                        NomenclatureCode1C = result.NomenclatureCode1C,
                        OrganizationCode1C = result.OrganizationCode1C,
                        DestUnitCode1C = result.SilosCode1C,
                        IsDrying = result.IsDrying,
                        IsQualityControl = result.IsQualityControl,
                        IsScalping = result.IsSorting,
                        IsSeparating = result.IsSorting,
                        Comment = result.Comment,
                        //LotNo = DateTime.Now.ToString("ddMMyy HH:mm:ss"),
                        User = Environment.UserName
                    }));

            FillGrid();
            }
        }

        private void btnAdd_ButtonClick(object sender, EventArgs e)
        {
            btnAddTaskAcceptance_Click(sender, e);
        }

        private T ExecuteResult<T>(Func<T> func)
        {
            try
            {
                return func();
            }
            catch (Exception ex)
            {
                ShowException(ex);
                return default(T);
            }
        }

        private void ShowException(Exception ex)
        {
            MessageBox.Show(ex.ToString(), @"Ошибка");
        }

        private void ExecuteAction(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                ShowException(ex);
            }
        }

    }
}