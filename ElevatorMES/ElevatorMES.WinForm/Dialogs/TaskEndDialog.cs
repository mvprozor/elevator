﻿using System.Windows.Forms;
using ElevatorMES.Domain.Model.UI;

namespace ElevatorMES.WinForm.Dialogs
{
    public partial class TaskEndDialog : Form
    {
        public TaskUIItem Task { get; set; }
        public TaskEndDialogResult Result { get; set; }
        public TaskEndDialog()
        {
            InitializeComponent();
        }

        private void TaskEndDialog_Load(object sender, System.EventArgs e)
        {
            label1.Text = $@"""{Task.StatusDescr}"" на {Task.ExecuteAt:dd.MM.yy} для номенклатуры:";
            label2.Text = $@"""{Task.NomenclatureDisplayName}"" от ""{Task.SuppCons}""";
        }

        private void btnOK_Click(object sender, System.EventArgs e)
        {
            Result = new TaskEndDialogResult()
            {
                WeightKilogram = tbWeight.Value,
                IsFailed = cbFailed.Checked,
                Comment = tbComment.Text
            };
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Result = null;
            Close();
        }
    }
}
