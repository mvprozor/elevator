﻿using System.Collections.Generic;
using System.Windows.Forms;
using ElevatorMES.Domain.Data;
using ElevatorMES.WinForm.Helpers;

namespace ElevatorMES.WinForm.Dialogs
{
    public partial class TaskAddDialog : Form
    {
        public TaskAddDialog()
        {
            InitializeComponent();
        }

        private void FillComboBoxes()
        {
            cbNomenclature.SetComboBoxSourceFromEntity(NomenclatureList);
            cbOrganization.SetComboBoxSourceFromEntity(OrganizationList);
            cbSilos.SetComboBoxSourceFromEntity(SilosList);
        }

        public IEnumerable<Nomenclature> NomenclatureList { get; set; }
        public IEnumerable<Organization> OrganizationList { get; set; }
        public IEnumerable<Unit> SilosList { get; set; }
        public TaskAddDialogResult Result { get; set; }

        private void TaskAddDialog_Load(object sender, System.EventArgs e)
        {
            FillComboBoxes();
        }

        private void btnOK_Click(object sender, System.EventArgs e)
        {
            Result = new TaskAddDialogResult()
            {
                IsSorting = cbSorting.Checked,
                IsDrying = cbDrying.Checked,
                NomenclatureCode1C = cbNomenclature.SelectedValue == null ? cbNomenclature.Text : cbNomenclature.SelectedValue.ToString(),
                VehicleRegNum = tbRegNum.Text,
                Comment = tbComment.Text,
                SilosCode1C = cbSilos.SelectedValue.ToString(),
                OrganizationCode1C = cbOrganization.SelectedValue == null ? cbOrganization.Text : cbOrganization.SelectedValue.ToString(),
            };
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Result = null;
            this.Close();
        }
    }

    public class TaskAddDialogResult
    {
        public string VehicleRegNum { get; set; }
        public string NomenclatureCode1C { get; set; }
        public string OrganizationCode1C { get; set; }
        public string SilosCode1C { get; set; }
        public bool IsQualityControl { get; set; }
        public bool IsDrying { get; set; }
        public bool IsSorting { get; set; }
        public string Comment { get; set; }
    }
}
