﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.WinForm.Helpers
{
    public static class ComboboxHelper
    {
        public static void SetComboBoxSourceFromEnum<T>(this ComboBox cb) where T : struct
        {
            var items = EnumExtension.ToDictionary<T>().ToList();
            cb.ValueMember = "Key";
            cb.DisplayMember = "Value";
            cb.DataSource = items;
        }

        public static void SetComboBoxSourceFromEntity(this ComboBox cb, IEnumerable<BaseSyncEntity> itemList)
        {
            var items = itemList.ToList();
            cb.ValueMember = "Code1C";
            cb.DisplayMember = "DisplayName";
            cb.DataSource = items;
        }

        public static T GetComboboxItem<T>(this ComboBox cb)
        {
            var dictItem = (KeyValuePair<T, string>)cb.SelectedItem;
            return dictItem.Key;
        }
    }
}
