﻿using ElevatorMES.Domain.Scales;
using ElevatorMES.ScalesIO;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            ScalesConfig[] scalesConfigs;
            using (var sr = new StreamReader("ScalesConfig.json"))
            using (var reader = new JsonTextReader(sr))
            {
                var serializer = new JsonSerializer();
                scalesConfigs = serializer.Deserialize<ScalesConfig[]>(reader);
            }
            Console.WriteLine($"Choose scales (1-{scalesConfigs.Length})");
            for (var i = 0; i < scalesConfigs.Length; i++)
                Console.WriteLine($"{i + 1}) {scalesConfigs[i]}");
            var key = Console.ReadKey();
            if (!char.IsDigit(key.KeyChar) || key.KeyChar=='0')
                return;
            var index = key.KeyChar - '1';
            if (index >= scalesConfigs.Length)
                return;
            var config = scalesConfigs[index];
            IScales scales = config.Create(out var errors);
            if (scales==null)
            {
                errors.ForEach(m => Console.WriteLine(m));
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Activating session...");
            var session = scales.Activate(OnData, OnError, OnFinished, config);
            Console.ReadKey();
            Console.WriteLine("Finishing...");
            session.Deactivate();
            Console.ReadKey();
        }

        private static void OnData(ScalesEventData scalesEvent)
        {
            foreach(var scalesData in scalesEvent.Data)
            {
                var stable = scalesData.IsStable.HasValue ?
                    (scalesData.IsStable.Value ? "Stable" : "Unstable")
                    : "W/O Stability";
                Console.WriteLine($"{stable} {scalesData.Weight}");
            }
        }

        private static void OnError(ScalesEventError scalesEvent)
        {
            Console.WriteLine(scalesEvent.Error);
        }

        private static void OnFinished(ScalesEvent scalesEvent)
        {
            Console.WriteLine($"Session finished {scalesEvent.Session.Config}");
        }
    }
}
