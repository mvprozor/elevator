﻿using ElevatorMES.Domain.Enums;

namespace ElevatorMES.Domain.Model.Args
{
    public class UpdateDictArg
    {
        public DictionaryType Type { get; set; }

        public int? Id { get; set; }

        public string DisplayName { get; set; }

        public string Code1C { get; set; }

        public string Type1C { get; set; }

        public string CodeParent1C { get; set; }
    }
}
