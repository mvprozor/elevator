﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Поступление задания на приемку сырья от 1С
    /// </summary>
    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "http://ElevatorMES.ru/", IsNullable = false)]
    public class RowAcceptArg
    {
        /// <summary>
        /// идентификатор документа
        /// </summary>
        [Required(ErrorMessage = "Укажите идентификатор документа")]
        [MaxLength(36)]
        public string DocId { get; set; }

        /// <summary>
        /// номер ТС
        /// </summary>
        [Required(ErrorMessage = "Укажите номер ТС")]
        [MaxLength(36)]
        public string VehicleRegNum { get; set; }

        /// <summary>
        /// значение «Code1C» из справочника «Номенклатура»
        /// </summary>
        [MaxLength(36)]
        [Required(ErrorMessage = "Укажите номенклатуру")]
        public string NomenclatureCode1C { get; set; }

        /// <summary>
        /// значение «Code1C» из справочника «Организация»
        /// </summary>
        [MaxLength(36)]
        public string OrganizationCode1C { get; set; }

        /// <summary>
        /// точка прибытия. «Code1C» из справочника «Узлы»
        /// </summary>
        [MaxLength(36)]
        [Required(ErrorMessage = "Укажите точку прибытия")]
        public string DestUnitCode1C { get; set; }

        /// <summary>
        /// признак контроля качества Bool True-положительный, False - отрицательный
        /// </summary>
        public bool IsGood { get; set; }

        /// <summary>
        /// номер партии
        /// </summary>
        [MaxLength(64)]
        public string LotNo { get; set; }

        // Качественные характеристики ?
        public bool IsQualityControl { get; set; }
        /// <summary>
        /// выполнить сушку - True-выполнить, False – не выполнять
        /// </summary>
        public bool IsDrying { get; set; }

        ///// <summary>
        ///// выполнить сортировку - True-выполнить, False – не выполнять
        ///// </summary>
        //public bool IsSorting { get; set; }

        /// <summary>
        /// Выполнить очистку скальператором
        /// </summary>
        public bool IsScalping { get; set; }

        /// <summary>
        /// Выполнить очистку сепаратором
        /// </summary>
        public bool IsSeparating { get; set; }

        /// <summary>
        /// ответственный
        /// </summary>
        [MaxLength(100)]
        public string User { get; set; }

        [MaxLength(3000)]
        public string Comment { get; set; }
    }
}
