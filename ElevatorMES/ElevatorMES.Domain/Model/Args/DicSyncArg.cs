﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Model.Args
{
    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "http://ElevatorMES.ru/", IsNullable = false)]
    public class SyncItem1C
    {
        /// <summary>
        /// Код 1С
        /// </summary>
        [MaxLength(64)]
        [Required]
        public string Code1C { get; set; }

        /// <summary>
        /// Тип в 1С
        /// </summary>
        [MaxLength(36)]
        public string Type1C { get; set; }

        /// <summary>
        /// Отображаемое имя
        /// </summary>
        [MaxLength(64)]
        [Required]
        public string DisplayName { get; set; }

        /// <summary>
        /// Тип в 1С
        /// </summary>
        [MaxLength(64)]
        public string CodeParent1C { get; set; }

        /// <summary>
        /// Признак удаления
        /// </summary>
        [Required]
        public bool Deleted { get; set; }
    }

    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "http://ElevatorMES.ru/", IsNullable = false)]
    public class DictSyncArg
    {
        [MaxLength(16)]
        public string TableName { get; set; }
        public List<SyncItem1C> Items { get; set; }
    }

    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "http://ElevatorMES.ru/", IsNullable = false)]
    public class PriorityArg
    {
        /// <summary>
        /// идентификатор документа
        /// </summary>
        [Required(ErrorMessage = "Укажите идентификатор документа")]
        [MaxLength(36)]
        public string DocId { get; set; }

        /// <summary>
        /// значение «Code1C» из справочника «Номенклатура»
        /// </summary>
        [MaxLength(36)]
        [Required(ErrorMessage = "Укажите сырьё")]
        public string NomenclatureCode1C { get; set; }

        /// <summary>
        /// Приоритет
        /// </summary>
        [Required(ErrorMessage = "Укажите приоритет")]
        [Range(0, 5)]
        public int Priority { get; set; }

        /// <summary>
        /// ответственный
        /// </summary>
        [MaxLength(100)]
        public string User { get; set; }

    }
}
