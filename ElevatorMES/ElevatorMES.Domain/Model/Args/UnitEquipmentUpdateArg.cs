﻿using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class UnitEquipmentUpdateArg : IEquatable<UnitEquipmentUpdateArg>
    {
        public int UnitId { get; set; }
        public int EquipmentId { get; set; }
        public bool IsContainer { get; set; }
        public bool IsRouteSource { get; set; }
        public bool IsRouteDestination { get; set; }

        public UnitEquipmentLinkType LinkType =>
            (IsContainer? UnitEquipmentLinkType.Container : UnitEquipmentLinkType.None)
            | (IsRouteSource? UnitEquipmentLinkType.RouteSource : UnitEquipmentLinkType.None)
            | (IsRouteDestination? UnitEquipmentLinkType.RouteDestination : UnitEquipmentLinkType.None);

        public bool Equals(UnitEquipmentUpdateArg other)
        {
            return UnitId == other.UnitId && EquipmentId == other.EquipmentId
                && IsContainer == other.IsContainer && IsRouteSource == other.IsRouteSource
                && IsRouteDestination == other.IsRouteDestination;
        }
    }
}
