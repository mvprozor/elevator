﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class GetTransmitLogArg
    {
        /// <summary>
        /// Дата начала
        /// </summary>
        public DateTime? Begin { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime? End { get; set; }

        /// <summary>
        /// Фильтр по всем текстовым полям
        /// </summary>
        public string TextFilter 
        {
            get => textFilter;
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    textFilter = null;
                else
                    textFilter = value.Trim();
            }
        }

        private string textFilter;
    }
}
