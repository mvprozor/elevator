﻿using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class StorageInventoryArg
    {
        /// <summary>
        /// Идентификатор склада
        /// </summary>
        public int UnitId { get; set; }

        /// <summary>
        /// Время верхнего слоя (для контроля изменений)
        /// </summary>
        public DateTime? TopLayerCreatedAt { get; set; }

        /// <summary>
        /// Количество в верхнем слое (для контроля изменений)
        /// </summary>
        public decimal TopLayerQuantity { get; set; }

        /// <summary>
        /// Время нижнего слоя (для контроля изменений)
        /// </summary>
        public DateTime? BottomLayerCreatedAt { get; set; }

        /// <summary>
        /// Количество в нижнем слое (для контроля изменений)
        /// </summary>
        public decimal BottomLayerQuantity { get; set; }

        /// <summary>
        /// Новое количество
        /// </summary>
        public decimal NewQuantity { get; set; }

        /// <summary>
        /// Режим коррекции
        /// </summary>
        public InventoryMode Mode { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }

        public bool Equals(StorageInventoryArg other)
        {
            return UnitId == other.UnitId 
                && TopLayerCreatedAt == other.TopLayerCreatedAt && TopLayerQuantity==other.TopLayerQuantity
                && BottomLayerCreatedAt == other.BottomLayerCreatedAt && BottomLayerQuantity == other.BottomLayerQuantity
                && NewQuantity == other.NewQuantity && Mode == other.Mode;
        }
    }
}
