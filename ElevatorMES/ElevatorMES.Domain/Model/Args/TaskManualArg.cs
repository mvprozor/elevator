﻿using ElevatorMES.Domain.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Ручное создание задания на приемку сырья
    /// </summary>
    public class TaskManualArg
    {
        public TaskType TaskType { get; set; }

        public string VehicleRegNum { get; set; }
        
        public int NomenclatureId { get; set; }

        public int OrganizationId { get; set; }

        public int ContragentId { get; set; }

        public int UnitSourceId { get; set; }

        public int UnitDestId { get; set; }

        public int UnitExternalId { get; set; }

        public bool IsGood { get; set; }

        public bool IsDrying { get; set; }

        public bool IsScalping { get; set; }

        public bool IsSeparating { get; set; }

        public string User { get; set; }

        public decimal Quantity { get; set; }

        public string Comment { get; set; }
    }
}
