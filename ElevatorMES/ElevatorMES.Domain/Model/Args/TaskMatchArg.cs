﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class TaskMatchArg
    {
        public int TaskManualId { get; set; }
        public int TaskERPId { get; set; }
    }
}
