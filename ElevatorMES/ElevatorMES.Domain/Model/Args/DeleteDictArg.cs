﻿using ElevatorMES.Domain.Enums;

namespace ElevatorMES.Domain.Model.Args
{
    public class DeleteDictArg
    {
        public DictionaryType Type { get; set; }

        public int Id { get; set; }
    }
}
