﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Задание перемещения между силосами
    /// </summary>
    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "http://ElevatorMES.ru/", IsNullable = false)]
    public class SilosMovingArg
    {
        /// <summary>
        /// идентификатор документа
        /// </summary>
        public string DocId { get; set; }

        /// <summary>
        /// значение «Code1C» из справочника «Номенклатура»
        /// </summary>
        [MaxLength(9)]
        [Required(ErrorMessage = "Укажите номенклатуру")]
        public string NomenclatureCode1C { get; set; }

        /// <summary>
        /// источник. «Code1C» из справочника «Узлы»
        /// </summary>
        [MaxLength(9)]
        [Required(ErrorMessage = "Укажите источник сырья")]
        public string SourceUnitCode1C { get; set; }

        /// <summary>
        /// источник. «Code1C» из справочника «Узлы»
        /// </summary>
        [MaxLength(9)]
        [Required(ErrorMessage = "Укажите приёмник сырья")]
        public string DestUnitCode1C { get; set; }

        /// <summary>
        /// значение количества отгружаемого сырья
        /// </summary>
        [Required(ErrorMessage = "Укажите количество")]
        public decimal Quantity { get; set; }

        /// <summary>
        /// дата исполнения
        /// </summary>
        [Required(ErrorMessage = "Укажите дату исполнения")]
        public DateTime ExecuteAt { get; set; }

        /// <summary>
        /// ответственный
        /// </summary>
        [MaxLength(100)]
        public string User { get; set; }

        /// <summary>
        /// комментарий
        /// </summary>
        [MaxLength(3000)]
        public string Comment { get; set; }
    }
}
