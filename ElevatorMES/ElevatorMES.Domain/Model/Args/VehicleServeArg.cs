﻿using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class VehicleServeArg
    {
        public int VehicleId { get; set; }
        public VehicleAction Action { get; set; }
        public int? NomenclatureId { get; set; }
        public int? UnitId { get; set; }
        public decimal? Weight { get; set; }
        public string User { get; set; }
        public DateTime ActionAt { get; set; }
        public string Comment { get; set; }
    }
}
