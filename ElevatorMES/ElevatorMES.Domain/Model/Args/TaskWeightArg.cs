﻿namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Аргумееиы фмксации веса по заданию
    /// </summary>
    public class TaskWeightArg : TaskArgBase
    {
        /// <summary>
        /// Какой вес удалось протащить по заданию
        /// </summary>
        public decimal Weight { get; set; }
    }
}
