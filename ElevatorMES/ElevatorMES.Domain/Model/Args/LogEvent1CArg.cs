﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ElevatorMES.Domain.Model.Args
{
    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "http://ElevatorMES.ru/", IsNullable = false)]
    public class LogEvent1CArg
    {
        /// <summary>
        /// идентификатор документа
        /// </summary>
        [Required(ErrorMessage = "Укажите идентификатор документа")]
        [MaxLength(36)]
        public string DocId { get; set; }

        /// <summary>
        /// Тип события
        /// </summary>
        [MaxLength(16)]
        [Required(ErrorMessage = "Укажите тип события")]
        public string EventType { get; set; }

        /// <summary>
        /// Приоритет
        /// </summary>
        [Required(ErrorMessage = "Укажите текст ошибки")]
        public string Message { get; set; }

        /// <summary>
        /// Дата ошибки
        /// </summary>
        [Required(ErrorMessage = "Укажите дату ошибки")]
        public DateTime DateMessage { get; set; }

    }
}
