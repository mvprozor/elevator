﻿using ElevatorMES.Domain.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Запрос регистрации автотранспорта
    /// </summary>
    public class VehicleRegisterArg
    {
        public int VehicleTypeId { get; set; }
        public string VehicleRegNum { get; set; }
        public int? NomenclatureId { get; set; }
        public decimal? Weight { get; set; }
        public string User { get; set; }
        public DateTime RegisteredAt { get; set; }
        public string Comment { get; set; }
    }
}