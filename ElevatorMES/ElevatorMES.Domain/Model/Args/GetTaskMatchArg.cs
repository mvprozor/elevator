﻿using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class GetTaskMatchArg
    {
        public int? TaskId { get; set; }
        public string DocId { get; set; }
        public bool? IsFromERP { get; set; }
        public GetTaskStatusEnum TaskGenStatus { get; set; }
        public TaskType? TaskType { get; set; }
        public int? NomenclatureId { get; set; }
        public string TextFilter { get; set; }
        public int? TaskCount { get; set; }
    }
}
