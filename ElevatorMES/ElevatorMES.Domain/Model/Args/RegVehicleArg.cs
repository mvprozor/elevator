﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Задание на отгрузку сырья на КЗ
    /// </summary>
    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "http://ElevatorMES.ru/", IsNullable = false)]
    public class RegVehicleArg
    {
        /// <summary>
        /// идентификатор документа
        /// </summary>
        [Required(ErrorMessage = "Укажите идентификатор документа")]
        [MaxLength(36)]
        public string DocId { get; set; }

        /// <summary>
        /// номер ТС
        /// </summary>
        [Required(ErrorMessage = "Укажите номер ТС")]
        [MaxLength(36)]
        public string VehicleRegNum { get; set; }

        /// <summary>
        /// значение «Code1C» из справочника «Тип Т/С»
        /// </summary>
        [Required(ErrorMessage = "Укажите код 1С из справочника «Тип Т/С»")]
        [MaxLength(36)]
        public string VehicleTypeCode1C { get; set; }

        /// <summary>
        /// значение «Code1C» из справочника «Номенклатура»
        /// </summary>
        [MaxLength(36)]
        public string NomenclatureCode1C { get; set; }

        /// <summary>
        /// ответственный
        /// </summary>
        [MaxLength(100)]
        public string User { get; set; }

    }
}
