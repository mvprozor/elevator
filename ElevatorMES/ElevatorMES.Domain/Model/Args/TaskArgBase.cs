﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class TaskArgBase
    {
        /// <summary>
        /// Id задания из Tasks
        /// </summary>
        public int TaskId { get; set; }

        /// <summary>
        /// Пользователь, выполняющий действия с заданием
        /// </summary>
        public string User { get; set; }
    }
}
