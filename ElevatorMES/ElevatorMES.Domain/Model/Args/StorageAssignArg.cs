﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class StorageAssignArg : IEquatable<StorageAssignArg>
    {
        /// <summary>
        /// Идентификатор склада
        /// </summary>
        public int UnitId { get; set; }

        /// <summary>
        /// Номенклатура склада
        /// </summary>
        /// <remarks>
        /// null - склад не используется
        /// </remarks>
        public int? NomenclatureId { get; set; }

        public bool Equals(StorageAssignArg other)
        {
            return NomenclatureId == other.NomenclatureId;
        }
    }
}
