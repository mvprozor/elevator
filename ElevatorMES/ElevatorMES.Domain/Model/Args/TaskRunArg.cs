﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Аргументы перевода задания в состояние "Запущено"
    /// </summary>
    public class TaskRunArg : TaskArgBase
    {
    }
}
