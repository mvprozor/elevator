﻿namespace ElevatorMES.Domain.Model.Args
{
    public class GetDictArg
    {
        public bool? IsDeleted { get; set; }

        public string SearchText { get; set; }

        public static readonly GetDictArg Default = new GetDictArg()
        {
            IsDeleted = false
        };
    }
}
