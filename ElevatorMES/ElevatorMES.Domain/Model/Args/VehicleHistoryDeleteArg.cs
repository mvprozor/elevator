﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class VehicleHistoryDeleteArg
    {
        public int VehicleId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
