﻿using ElevatorMES.Domain.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Создание задания по данным от ПЛК
    /// </summary>
    public class TaskPLCArg
    {
        public string Nomenclature { get; set; }

        public decimal Quantity { get; set; }

        public string User{ get; set; }
    }
}
