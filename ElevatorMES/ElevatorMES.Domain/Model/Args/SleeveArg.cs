﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Поступление задания на отгрузку в рукава от 1С
    /// </summary>
    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "http://ElevatorMES.ru/", IsNullable = false)]
    public class SleeveArg
    {
        /// <summary>
        /// идентификатор документа
        /// </summary>
        [Required(ErrorMessage = "Укажите идентификатор документа")]
        [MaxLength(36)]
        public string DocId { get; set; }

        /// <summary>
        /// значение «Code1C» из справочника «Номенклатура»
        /// </summary>
        [MaxLength(36)]
        [Required(ErrorMessage = "Укажите номенклатуру")]
        public string NomenclatureCode1C { get; set; }

        /// <summary>
        /// cклад отгрузки. «Code1C» из справочника «Узлы»
        /// </summary>
        [MaxLength(36)]
        [Required(ErrorMessage = "Укажите склад отгрузки")]
        public string SourceUnitCode1C { get; set; }

        /// <summary>
        /// cклад приемки. «Code1C» из справочника «Узлы»
        /// </summary>
        [MaxLength(36)]
        [Required(ErrorMessage = "Укажите cклад приемки")]
        public string DestUnitCode1C { get; set; }

        /// <summary>
        /// номер ТС
        /// </summary>
        [Required(ErrorMessage = "Укажите номер ТС")]
        [MaxLength(36)]
        public string VehicleRegNum { get; set; }

        /// <summary>
        /// значение количества отгружаемого сырья
        /// </summary>
        [Required(ErrorMessage = "Укажите количество")]
        public decimal Quantity { get; set; }

        /// <summary>
        /// дата исполнения
        /// </summary>
        [Required(ErrorMessage = "Укажите дату исполнения")]
        public DateTime ExecuteAt { get; set; }

        /// <summary>
        /// ответственный
        /// </summary>
        [MaxLength(100)]
        public string User { get; set; }

        /// <summary>
        /// комментарий
        /// </summary>
        [MaxLength(3000)]
        public string Comment { get; set; }
    }
}
