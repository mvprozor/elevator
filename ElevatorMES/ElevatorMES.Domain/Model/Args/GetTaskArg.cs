﻿using System;
using System.ComponentModel;

namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Обобщенные статусы заданий для запроса
    /// </summary>
    public enum GetTaskStatusEnum
    {
        /// <summary>
        /// Все
        /// </summary>
        [Description("Все")]
        All = 0,

        /// <summary>
        /// Незавершенные
        /// </summary>
        [Description("В очереди")]
        Queued = 1,

        /// <summary>
        /// Активные
        /// </summary>
        [Description("Активные")]
        Active = 2,

        /// <summary>
        /// История
        /// </summary>
        [Description("История")]
        History = 3
    }


    /// <summary>
    /// Фильтры получения списка заданий
    /// </summary>
    public class GetTaskArg
    {
        /// <summary>
        /// Идентификатор задания
        /// </summary>
        public int? TaskId { get; set; }
        
        /// <summary>
        /// Только незавершенные/все/удаленные - по дефолту все
        /// </summary>
        public GetTaskStatusEnum TaskStatus { get; set; }
        
        /// <summary>
        /// Дата начала
        /// </summary>
        public DateTime? Begin { get; set; }
        
        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime? End { get; set; }

        /// <summary>
        /// Фильтр по всем текстовым полям
        /// </summary>
        public string TextFilter { get; set; }
    }
}
