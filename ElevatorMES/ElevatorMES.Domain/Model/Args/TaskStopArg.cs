﻿namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Фильтры завершения (приостановки) задания
    /// </summary>
    public class TaskStopArg : TaskWeightArg
    {
        /// <summary>
        /// признак закрытия всего задания, false - возврат или приостанов задания
        /// </summary>
        public bool IsCloseTask { get; set; }

        /// <summary>
        /// признак возврата задания в очередь
        /// </summary>
        public bool IsReturn { get; set; }

        /// <summary>
        /// признак сбоя при выполнении задания
        /// </summary>
        public bool IsFailed { get; set; }

        /// <summary>
        /// комментарий (особенно нужен при сбое - что, собственно, случилось)
        /// </summary>
        public string Comment { get; set; }
    }
}
