﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class GetVehicleArg
    {
        /// <summary>
        /// Идентификатор транспорта
        /// </summary>
        public int? VehicleId { get; set; }

        /// <summary>
        /// Включая уехавших
        /// </summary>
        public bool IncludingLeft { get; set; }

        /// <summary>
        /// Фильтр по номеру
        /// </summary>
        public string RegNum
        {
            get => regNum;
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    regNum = null;
                else
                    regNum = value.Trim();
            }
        }

        private string regNum;
    }
}
