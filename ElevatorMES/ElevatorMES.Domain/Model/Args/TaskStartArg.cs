﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    /// <summary>
    /// Аргумены задания постановки на выполнение
    /// </summary>
    public class TaskStartArg : TaskArgBase, IEquatable<TaskStartArg>
    {
        /// <summary>
        /// Откуда
        /// </summary>
        public int? SourceUnitId { get; set; }

        /// <summary>
        /// Куда
        /// </summary>
        public int? DestUnitId { get; set; }

        public bool Equals(TaskStartArg other)
        {
            return TaskId == other.TaskId && SourceUnitId == other.SourceUnitId && DestUnitId == other.DestUnitId;
        }
    }
}
