﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.Args
{
    public class GetVehicleHistoryArg
    {
        /// <summary>
        /// Идентификатор транспортной единицы
        /// </summary>
        public int VehicleId { get; set; }
    }
}

