﻿using System;
using System.ComponentModel;
using System.Text;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Domain.Model.UI
{
    public class TaskMatchUIItem : INotifyPropertyChanged
    {
        public int Id { get; set; }
        public string DocId { get; set; }
        public TaskType Type { get; set; }
        public string TypeDescr => Type.GetDescription();
        public DateTime CreatedAt { get; set; }
        public int NomenclatureId { get; set; }
        public string NomenclatureDisplayName { get; set; }
        public int? SourceLotId { get; set; }
        public int? SourceUnitId { get; set; }
        public string SourceUnitName { get; set; }
        public int? DestUnitId { get; set; }
        public string DestUnitName { get; set; }
        public string SourceLotNo { get; set; }
        public int? VehicleId { get; set; }
        public string SuppCons { get; set; }
        public bool IsDrying { get; set; }
        public bool IsScalping { get; set; }
        public bool IsSeparating { get; set; }
        public DateTime ExecuteAt { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public string UserDisplayName { get; set; }
        public DateTime? StartedAt { get; set; }
        public DateTime? EndAt { get; set; }
        public TimeSpan? Duration => (EndAt ?? DateTime.Now) - StartedAt;

        public string ExecPeriod => StartedAt.HasValue ?
            $"{StartedAt.Value:dd MMM HH:mm} {Duration.Value.ToStringFriendly()}" : String.Empty;

        public string Operations => GetOperations();

        private string GetOperations()
        {
            StringBuilder sb = new StringBuilder(32);
            sb.Append(IsDrying ? "сушка" : String.Empty);
            sb.Append(IsSeparating ? (sb.Length > 0 ? "+" : String.Empty) + "сепарация" : String.Empty);
            sb.Append(IsScalping ? (sb.Length > 0 ? "+" : String.Empty) + "скалп." : String.Empty);
            return sb.ToString();
        }
        public decimal QuantityPlanned { get; set; }
        public decimal QuantityActual { get; set; }
        public TaskStatus Status { get; set; }
        public string StatusDescr => Status.GetDescription();

        public string ShortInfo
        {
            get => $"{Id}.{TypeDescr} {NomenclatureDisplayName} от {ExecuteAt:dd.MM}";
        }

        public override string ToString()
        {
            return ShortInfo;
        }


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
