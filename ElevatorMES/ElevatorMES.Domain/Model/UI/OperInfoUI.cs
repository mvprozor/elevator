﻿using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.UI
{
    public class OperInfoUI
    {
        public int QueuedTasks { get; set; }

        public int FinishedTasks { get; set; }
        
        public int CarsInside { get; set; }

        public bool Is1CAvailable { get; set; }

        public UserRole UserRole { get; set; }

        public string UserName { get; set; }

        public string UserRoleDesc => UserRole.GetDescription();

    }
}
