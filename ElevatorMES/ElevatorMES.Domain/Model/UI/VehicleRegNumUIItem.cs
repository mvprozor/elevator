﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.UI
{
    public class VehicleRegNumUIItem : IEquatable<VehicleRegNumUIItem>
    {
        public string RegNum { get; set; }
        public VehicleType Type { get; set; }

        public bool Equals(VehicleRegNumUIItem other)
        {
            return String.Equals(RegNum, other?.RegNum, StringComparison.OrdinalIgnoreCase)
                && Type?.Id == other.Type?.Id;
        }
    }
}
