﻿using System;
using System.ComponentModel;
using System.Text;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Domain.Model.UI
{
    public class TaskUIItem : INotifyPropertyChanged
    {
        [System.ComponentModel.Browsable(false)]
        public int Id { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string DocId { get; set; }
        [System.ComponentModel.Browsable(false)]
        public TaskType Type { get; set; }

        /// <summary>
        /// текстовая рашифровка типа (приемка и т.п.)
        /// </summary>
        
        public string TypeDescr => Type.GetDescription();
        [System.ComponentModel.Browsable(false)]
        public int NomenclatureId { get; set; }
        public string NomenclatureDisplayName { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string NomenclatureCode1C { get; set; }
        [System.ComponentModel.Browsable(false)]
        public int? NomenclaturePriority { get; set; }
        [System.ComponentModel.Browsable(false)]
        public int? SourceLotId { get; set; }
        [System.ComponentModel.Browsable(false)]
        public int? SourceUnitId { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string SourceUnitName { get; set; }
        [System.ComponentModel.Browsable(false)]
        public int? DestUnitId { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string DestUnitName { get; set; }
        [System.ComponentModel.Browsable(false)]
		public int? ExternalUnitId { get; set; }
		[System.ComponentModel.Browsable(false)]
		public string ExternalUnitName { get; set; }
		[System.ComponentModel.Browsable(false)]
		public string SourceLotNo { get; set; }
        [System.ComponentModel.Browsable(false)]
        public int? VehicleId { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string SuppCons { get; set; }
        [System.ComponentModel.Browsable(false)]
        public bool IsDrying { get; set; }
        [System.ComponentModel.Browsable(false)]
        public bool IsScalping { get; set; }
        [System.ComponentModel.Browsable(false)]
        public bool IsSeparating { get; set; }
        [System.ComponentModel.Browsable(false)]
        public bool IsQualityControl { get; set; }
        [System.ComponentModel.Browsable(false)]
        public DateTime ExecuteAt { get; set; }
        public string Comment { get; set; }
        [System.ComponentModel.Browsable(false)]
        public int UserId { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string UserDisplayName { get; set; }
        [System.ComponentModel.Browsable(false)]
        public int? LastUserId { get; set; }
        public string LastUserName { get; set; }

        [System.ComponentModel.Browsable(false)]
        public DateTime CreatedAt { get; set; }
        public DateTime? StartedAt { get; set; }
        public DateTime? EndAt { get; set; }
        public TimeSpan? Duration => (EndAt ?? DateTime.Now) - StartedAt;
        public string Equipments { get; set; }
        public double? Power { get; set; }

        public string ExecPeriod => StartedAt.HasValue ?
            $"{StartedAt.Value:dd MMM HH:mm} {Duration.Value.ToStringFriendly()}" : String.Empty;

        public string Operations => GetOperations();

        private string GetOperations()
        {
            StringBuilder sb = new StringBuilder(32);
            sb.Append(IsDrying ? "сушка" : String.Empty);
            sb.Append(IsSeparating ? (sb.Length>0 ? "+" : String.Empty)+"сепарация" : String.Empty);
            sb.Append(IsScalping ? (sb.Length > 0 ? "+" : String.Empty) + "скалп." : String.Empty);
            return sb.ToString();
        }
        [System.ComponentModel.Browsable(false)]
        public decimal QuantityPlanned { get; set; }

        [System.ComponentModel.Browsable(false)]
        public decimal QuantityActual 
        {
            get => quantityActual;
            set
            {
                if (quantityActual!=value)
                {
                    quantityActual = value;
                    OnPropertyChanged(nameof(QuantityActual));
                    OnPropertyChanged(nameof(QuantityCurrent));
                }
            }
        }
        private decimal quantityActual;

        [System.ComponentModel.Browsable(false)]
        public TaskStatus Status
        {
            get => status;
            set
            {
                if (status != value)
                {
                    status = value;
                    OnPropertyChanged(nameof(Status));
                    OnPropertyChanged(nameof(StatusDescr));
                    UpdateFlags();
                }
            }
        }
        private TaskStatus status;

        public string StatusDescr => Status.GetDescription();

        public string PlanFact => $@"{QuantityPlanned:N2} / {QuantityActual:N2}";

        public string RegNum { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string SourceUnitCode1C { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string DestUnitCode1C { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string ContragentCode1C { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string OrganizationCode1C { get; set; }

        [System.ComponentModel.Browsable(false)]
        public EquipmentType? SourceEquipmentType { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string SourceEquipmentName { get; set; }
        [System.ComponentModel.Browsable(false)]
        public int? SourceEquipmentId { get; set; }

        [System.ComponentModel.Browsable(false)]
        public EquipmentType? DestinationEquipmentType { get; set; }
        [System.ComponentModel.Browsable(false)]
        public string DestinationEquipmentName { get; set; }
        [System.ComponentModel.Browsable(false)]
        public int? DestinationEquipmentId { get; set; }

        [System.ComponentModel.Browsable(false)]
        public bool IsProcessing 
        {
            get => isProcessing;
            set
            { 
                if (isProcessing!=value)
                {
                    isProcessing = value;
                    OnPropertyChanged(nameof(IsProcessing));
                    UpdateFlags();
                }
            }
        }
        private bool isProcessing;


        public bool HasQuantityActual
        {
            get => QuantityActual > decimal.Zero;
        }


        public bool CanStart
        {
            get => canStart;
            set
            {
                if (canStart != value)
                {
                    canStart = value;
                    OnPropertyChanged(nameof(CanStart));
                }
            }
        }
        private bool canStart;


        public bool CanResume
        {
            get => canResume;
            set
            {
                if (canResume != value)
                {
                    canResume = value;
                    OnPropertyChanged(nameof(CanResume));
                }
            }
        }
        private bool canResume;
        

        public bool CanDelete
        {
            get => canDelete;
            set
            {
                if (canDelete != value)
                {
                    canDelete = value;
                    OnPropertyChanged(nameof(CanDelete));
                }
            }
        }
        private bool canDelete;


        public bool CanSuspend
        {
            get => canSuspend;
            set
            {
                if (canSuspend != value)
                {
                    canSuspend = value;
                    OnPropertyChanged(nameof(CanSuspend));
                }
            }
        }
        private bool canSuspend;


        public bool CanComplete
        {
            get => canComplete;
            set
            {
                if (canComplete != value)
                {
                    canComplete = value;
                    OnPropertyChanged(nameof(CanComplete));
                }
            }
        }
        private bool canComplete;


        public bool CanRevert
        {
            get => canRevert;
            set
            {
                if (canRevert != value)
                {
                    canRevert = value;
                    OnPropertyChanged(nameof(CanRevert));
                }
            }
        }
        private bool canRevert;


        public string Alert
        {

            get
            {
                StringBuilder sb = new StringBuilder(512);
                string div = String.Empty;
                if (Type == TaskType.Receiving && !IsQualityControl)
                {
                    sb.Append("Недопустимое качество сырья");
                    div = Environment.NewLine;
                }
                if ((Type == TaskType.Receiving || Type == TaskType.SilosMoving) && !DestinationEquipmentId.HasValue)
                {
                    sb.Append(div);
                    sb.Append("Нет связанного исходного оборудования для маршрута");
                }
                if ((Type == TaskType.ShippingExt || Type == TaskType.SilosMoving || Type == TaskType.PlantMoving) && !SourceEquipmentId.HasValue)
                {
                    sb.Append(div);
                    sb.Append("Нет связанного конечного оборудования для маршрута");
                }
                return sb.ToString();
            }
        }

        public string ShortInfo
        {
            get => $"{Id}.{TypeDescr} {NomenclatureDisplayName} от {ExecuteAt:dd.MM}";
        }

        public override string ToString()
        {
            return ShortInfo;
        }

        #region Обновление признаков 

        private void UpdateFlags()
        {
            if (IsProcessing)
            {
                CanStart = CanResume = CanSuspend = CanRevert = CanDelete = CanComplete = false;
            }
            else
            {
                CanStart = (Status == TaskStatus.Queued || Status == TaskStatus.Suspended) && (Type != TaskType.Receiving || IsQualityControl);
                CanResume = Status == TaskStatus.Suspended && !IsProcessingPLC && StatusPLC==PLCTaskStatus.RouteStopped;
                CanSuspend = Status == TaskStatus.Running && !IsProcessingPLC && StatusPLC == PLCTaskStatus.RouteRunning;
                CanDelete = Status == TaskStatus.Queued && QuantityActual == 0;
                CanRevert = Status == TaskStatus.Failed || Status == TaskStatus.Deleted || Status == TaskStatus.Completed;
                CanComplete = Status == TaskStatus.Suspended || Status == TaskStatus.Running || Status == TaskStatus.Pending;
            }
        }

        public void AssignActiveState(TaskUIItem source)
        {
            Status = source.Status;
            QuantityActual = source.QuantityActual;
        }

        #endregion


        #region Данные ПЛК

        public decimal QuantityPLC
        {
            get => quantityPLC;
            set
            {
                if (value != quantityPLC)
                {
                    quantityPLC = value;
                    OnPropertyChanged(nameof(QuantityPLC));
                    OnPropertyChanged(nameof(QuantityCurrent));
                }
            }
        }
        private decimal quantityPLC;

        public decimal QuantityCurrent
        {
            get => QuantityActual + QuantityPLC;
               //((StatusPLC==PLCTaskStatus.RouteStopped || StatusPLC==PLCTaskStatus.RouteCompleted
                //|| StatusPLC==PLCTaskStatus.EquipmentFailure) ? decimal.Zero : QuantityPLC);
        }

        public PLCTaskStatus StatusPLC
        {
            get => statusPLC;
            set
            {
                if (statusPLC != value)
                {
                    statusPLC = value;
                    OnPropertyChanged(nameof(StatusPLC));
                    OnPropertyChanged(nameof(StatusDescrPLC));
                    UpdateFlags();
                }
            }
        }
        private PLCTaskStatus statusPLC;

        public string StatusDescrPLC
        {
            get
            {
                var descr = StatusPLC.GetDescription();
                if (String.IsNullOrWhiteSpace(descr))
                    descr = StatusPLC.ToString();
                return descr;
            }
        }


        public bool IsProcessingPLC
        {
            get => isProcessingPLC;
            set
            {
                if (isProcessingPLC != value)
                {
                    isProcessingPLC = value;
                    OnPropertyChanged(nameof(IsProcessingPLC));
                    UpdateFlags();
                }
            }
        }
        private bool isProcessingPLC;

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
