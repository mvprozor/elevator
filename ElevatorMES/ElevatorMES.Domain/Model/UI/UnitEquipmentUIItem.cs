﻿using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.UI
{
    public class UnitEquipmentUIItem : INotifyPropertyChanged
    {
        #region Открытые поля

        public int UnitId { get; set; }

        public string UnitDisplayName { get; set; }

        public int EquipmentId { get; set; }

        public string EquipmentDisplayName { get; set; }

        public bool IsContainer
        {
            get => isContainer;
            set
            {
                if (isContainer!=value)
                {
                    StoreOld();
                    isContainer = value;
                    OnPropertyChanged(nameof(IsContainer));
                    OnStartProcessing();
                }
            }
        }

        public bool IsRouteSource
        {
            get => isRouteSource;
            set
            {
                if (isRouteSource != value)
                {
                    StoreOld();
                    isRouteSource = value;
                    OnPropertyChanged(nameof(IsRouteSource));
                    OnStartProcessing();
                }
            }
        }

        public bool IsRouteDestination
        {
            get => isRouteDestination;
            set
            {
                if (isRouteDestination != value)
                {
                    StoreOld();
                    isRouteDestination = value;
                    OnPropertyChanged(nameof(IsRouteDestination));
                    OnStartProcessing();
                }
            }
        }

        public bool IsProcessing
        {
            get => isProcessing;
            private set
            {
                if (isProcessing != value)
                {
                    isProcessing = value;
                    OnPropertyChanged(nameof(IsProcessing));
                }
            }
        }

        public event EventHandler Processing;

        #endregion


        #region Закрытые поля

        private bool isContainer;

        private bool isContainerOld;

        private bool isRouteSource;

        private bool isRouteSourceOld;

        private bool isRouteDestination;

        private bool isRouteDestinationOld;

        private bool isProcessing;

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion


        #region Сохранение и откат значений

        private void StoreOld()
        {
            isContainerOld = isContainer;
            isRouteSourceOld = isRouteSource;
            isRouteDestinationOld = IsRouteDestination;
        }

        private void OnStartProcessing()
        {
            var handler = Processing;
            if (handler!=null)
            {
                IsProcessing = true;
                handler(this, EventArgs.Empty);
            }
        }

        public void CommitChanges()
        {
            IsProcessing = false;
        }

        public void RollbackChanges()
        {
            if (IsProcessing)
            {
                isContainer = isContainerOld;
                isRouteSource = isRouteSourceOld;
                isRouteDestination = isRouteDestinationOld;
                IsProcessing = false;
            }
        }

        #endregion


        #region Базовые сервисные методы

        public override string ToString()
        {
            return $"{UnitDisplayName} - {EquipmentDisplayName}";
        }

        #endregion

    }
}
