﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Domain.Model.UI
{
    public class VehicleUIItem : INotifyPropertyChanged
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }

        public string DocId { get; set; }

        public string RegNum { get; set; }

        public int VehicleTypeId { get; set; }

        public string VehicleTypeName { get; set; }

        public VehicleClass VehicleClass { get; set; }

        public int? RawId { get; set; }

        public string RawName { get; set; }

        public int? RawPriority { get; set; }

        public DateTime? RegisteredAt { get; set; }

        public DateTime? UnloadedAt { get; set; }

        /// <summary>
        /// Вес нетто сырья по документам (введенный вручную)
        /// </summary>
        public decimal? RawWeightNet { get; set; }

        /// <summary>
        /// Вес брутто с сырьем по весам
        /// </summary>
        public decimal? RawWeightBrutto { get; set; }

        /// <summary>
        /// Время брутто с сырьем по весам
        /// </summary>
        public DateTime? RawWeightBruttoAt { get; set; }

        /// <summary>
        /// Вес нетто сырья
        /// </summary>
        public decimal? RawWeight => (RawWeightBrutto - WeightTare) ?? RawWeightNet;

        public int? ProdId { get; set; }

        public string ProdName { get; set; }

        public DateTime? LoadedAt { get; set; }

        /// <summary>
        /// Вес нетто отгружаемый по другим источникам (например, с конвейерных весов)
        /// </summary>
        public decimal? ProdWeightNet { get; set; }

        /// <summary>
        /// Вес брутто отгружаемый по весам
        /// </summary>
        public decimal? ProdWeightBrutto { get; set; }

        /// <summary>
        /// Время взвешивания брутто отгрузки
        /// </summary>
        public DateTime? ProdWeightBruttoAt { get; set; }

        /// <summary>
        /// Вес нетто отгружаемый
        /// </summary>
        public decimal? ProdWeight => (ProdWeightBrutto - WeightTare) ?? ProdWeightNet;

        /// <summary>
        /// Вес тары
        /// </summary>
        public decimal? WeightTare { get; set; }

        /// <summary>
        /// Время взвешивания тары
        /// </summary>
        public DateTime? WeightTareAt { get; set; }

        /// <summary>
        /// Дата и время выезда с територии
        /// </summary>
        public DateTime? LeftAt { get; set; }

        public bool CanDelete
        {
            get => String.IsNullOrWhiteSpace(DocId);
        }

        public bool CanServe
        {
            get => !LeftAt.HasValue;
        }

        public List<KeyValuePair<VehicleAction, string>> GetAvailableActions()
        {
            var actions = new List<KeyValuePair<VehicleAction, string>>(4);
            actions.Add(new KeyValuePair<VehicleAction, string>(
                VehicleAction.Weighting, VehicleAction.Weighting.GetDescription()));
            if (!UnloadedAt.HasValue)
                actions.Add(new KeyValuePair<VehicleAction, string>(
                    VehicleAction.Unloading, VehicleAction.Unloading.GetDescription()));
            if (!LoadedAt.HasValue)
                actions.Add(new KeyValuePair<VehicleAction, string>(
                    VehicleAction.Loading, VehicleAction.Loading.GetDescription()));
            actions.Add(new KeyValuePair<VehicleAction, string>(
                VehicleAction.Leaving, VehicleAction.Leaving.GetDescription()));
            return actions;
        }

        public VehicleQueue Queue
        {
            get
            {
                if (LeftAt.HasValue)
                    return VehicleQueue.Leaved;
                if (RawId.HasValue)
                {
                    if (UnloadedAt.HasValue)
                        return WeightTare.HasValue ? VehicleQueue.ToLeave : VehicleQueue.ToTare;
                    else
                        return RawWeightBrutto.HasValue ? VehicleQueue.ToUnload : VehicleQueue.ToBrutto;
                }
                else
                {
                    if (LoadedAt.HasValue)
                        return ProdWeightBrutto.HasValue ? VehicleQueue.ToLeave : VehicleQueue.ToBrutto;
                    else
                        return WeightTare.HasValue ? VehicleQueue.ToLoad : VehicleQueue.ToTare;
                }
            }
        }

        public VehicleAction NextAction
        {
            get
            {
                switch(Queue)
                {
                    case VehicleQueue.ToUnload:
                        return VehicleAction.Unloading;
                    case VehicleQueue.ToLoad:
                        return VehicleAction.Loading;
                    default:
                        return VehicleAction.Weighting;
                }
            }
        }

        public override string ToString()
        {
            return $"{Id}. {VehicleTypeName} {RegNum} от {RegisteredAt:dd.MMM HH:mm}";
        }

        

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
