﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.UI
{
    public class EquipmentStatUIItem
    {
        public int EquipmentId { get; set; }
        public string EquipmentName { get; set; }
        public DateTime RunFrom { get; set; }
        public DateTime RunTo { get; set; }
        public int RunSeconds { get; set; }
        public decimal PowerConsumption { get; set; } 
        public int? TaskType { get; set; }
        public bool? IsDrying { get; set; }
        public bool? IsScalping { get; set; }
        public bool? IsSeparating { get; set; }
        public string NomenclatureName { get; set; }
        public decimal? QuantityActual { get; set; }
    }
}
