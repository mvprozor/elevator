﻿using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.UI
{
    public class NomenclaturePriorityUIItem : INotifyPropertyChanged
    {
        #region Открытые поля

        public int NomenclatureId { get; set; }

        public string NomenclatureDisplayName { get; set; }

        public int? Priority
        {
            get => priority;
            set
            {
                if (priority != value)
                {
                    StoreOld();
                    priority = value;
                    OnPropertyChanged(nameof(Priority));
                    OnStartProcessing();
                }
            }
        }

        public bool IsProcessing
        {
            get => isProcessing;
            private set
            {
                if (isProcessing != value)
                {
                    isProcessing = value;
                    OnPropertyChanged(nameof(IsProcessing));
                }
            }
        }

        public event EventHandler Processing;

        #endregion


        #region Закрытые поля

        private int? priority;

        private int? priorityOld;

        private bool isProcessing;

        #endregion


        #region Сохранение и откат значений

        private void StoreOld()
        {
            priorityOld = priority;
        }

        private void OnStartProcessing()
        {
            var handler = Processing;
            if (handler != null)
            {
                IsProcessing = true;
                handler(this, EventArgs.Empty);
            }
        }

        public void CommitChanges()
        {
            IsProcessing = false;
        }

        public void RollbackChanges()
        {
            if (IsProcessing)
            {
                priority = priorityOld;
                IsProcessing = false;
            }
        }

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion


        public override string ToString()
        {
            return $"{NomenclatureDisplayName}: {Priority}";
        }
    }
}