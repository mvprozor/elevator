﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.UI
{
    public class LayerUIItem
    {
        public DateTime CreatedAt { get; set; }

        public int LotId { get; set; }

        public decimal Quantity { get; set; }

		public string LotNo { get; set; }

        public int NomenclatureId { get; set; }

        public string NomenclatureName { get; set; }

        public int OrganizationId { get; set; }

        public string OrganizationName { get; set; }
    }
}
