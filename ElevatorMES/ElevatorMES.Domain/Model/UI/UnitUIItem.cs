﻿using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.UI
{
    public class UnitUIItem : INotifyPropertyChanged
    {
        public int UnitId { get; set; }

        public string UnitDisplayName { get; set; }

        public UnitType UnitType { get; set; }

        public EquipmentType? SourceEquipmentType { get; set; }

        public string SourceEquipmentName { get; set; }

        public int? SourceEquipmentId { get; set; }

        public EquipmentType? DestinationEquipmentType { get; set; }

        public string DestinationEquipmentName { get; set; }

        public int? DestinationEquipmentId { get; set; }

        public bool IsProcessing
        {
            get => isProcessing;
            set
            {
                if (isProcessing != value)
                {
                    isProcessing = value;
                    OnPropertyChanged(nameof(IsProcessing));
                }
            }
        }
        private bool isProcessing;


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public override string ToString()
        {
            return UnitDisplayName;
        }
    }
}
