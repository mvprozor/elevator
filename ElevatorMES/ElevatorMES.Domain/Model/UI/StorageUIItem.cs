﻿using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.UI
{
    public class StorageUIItem : UnitUIItem
    {
        public decimal Capacity { get; set; }

        public int? NomenclatureId { get; set; }

        public string NomenclatureDisplayName { get; set; }
        
        public decimal? Quantity { get; set; }

        public decimal FillFraction => Capacity > decimal.Zero ? (Quantity ?? decimal.Zero) / Capacity : decimal.Zero;

        public decimal FillPercent => FillFraction * 100m;

        public bool CanInventory => NomenclatureId.HasValue;

        public string FullInfo
        { 
            get
            {
                if (NomenclatureId.HasValue)
                {
                    var quantityString = Quantity.HasValue ? FillFraction.ToString("0.00%") : "пустой";
                    return $"{UnitDisplayName} ({NomenclatureDisplayName}, {Quantity:0.0 т.}, {quantityString})";
                }
                return UnitType == UnitType.SleeveStorage ? UnitDisplayName : $"{UnitDisplayName} (не используется)";
            }
        }

        public override string ToString()
        {
            return FullInfo;
        }

        public bool CanAdd(decimal quantityAdd)
        {
            return (Quantity ?? decimal.Zero) + quantityAdd <= Capacity;
        }

        public bool CanRemove(decimal quantityRemove)
        {
            return (Quantity ?? decimal.Zero) - quantityRemove >= 0;
        }
    }
}
