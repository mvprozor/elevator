﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Model.UI
{
    public class MoveUIItem
    {
		public int Id { get; set; }
		public DateTime MovedAt { get; set; }
		public decimal Quantity { get; set; }
		public bool IsIncome { get; set; }
		public int LotId { get; set; }
		public string LotNo { get; set; }
		public int NomenclatureId { get; set; }
		public string NomenclatureName { get; set; }
		public int LotOrganizationId { get; set; }
		public string LotOrganizationName { get; set; }
		public string EntityName { get; set; }
	}
}
