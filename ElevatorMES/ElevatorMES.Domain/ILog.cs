﻿namespace ElevatorMES.Domain
{
    public interface ILog
    {
        void Trace(string text);
        void Info(string text);
        void Error(string text);
    }
}
