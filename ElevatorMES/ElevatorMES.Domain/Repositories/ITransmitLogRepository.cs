﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Repositories.Base;
using System.Collections.Generic;

namespace ElevatorMES.Domain.Repositories
{
    public interface ITransmitLogRepository : IBaseRepository<TransmitLog>
    {
     
    }

}
