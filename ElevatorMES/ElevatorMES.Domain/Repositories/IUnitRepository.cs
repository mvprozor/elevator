﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories.Base;

namespace ElevatorMES.Domain.Repositories
{
    public interface IUnitRepository : IBaseSyncRepository<Unit>
    {
        Unit GetPlant();
    }
}
