﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories.Base;

namespace ElevatorMES.Domain.Repositories
{
    public interface ILogEvent1CRepository : IBaseRepository<LogEvent1C>
    {
    }

}
