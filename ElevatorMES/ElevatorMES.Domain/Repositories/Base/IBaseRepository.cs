﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Repositories.Base
{
    public interface IBaseRepository<T> : ICommRepository<T> where T : IBaseEntity
    {
        T GetById(int id, bool isThrowNotFound = false);

        IQueryable<T> GetMany(IEnumerable<int> ids);

        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        T LastCreated(Expression<Func<T, bool>> predicate);
        T FirstCreated(Expression<Func<T, bool>> predicate);

        void TryDelete(int id);
        void Restore(int id);
    }
}
