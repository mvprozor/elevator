﻿using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Repositories.Base
{
    public interface IBaseSyncRepository<T> : IBaseRepository<T> where T : BaseSyncEntity
    {
        T GetOrCreateByCode1C(string code1C);
        T GetByCode1C(string code1C);
    }
}