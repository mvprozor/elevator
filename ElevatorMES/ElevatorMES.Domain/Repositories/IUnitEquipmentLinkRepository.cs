﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories.Base;

namespace ElevatorMES.Domain.Repositories
{
    public interface IUnitEquipmentLinkRepository : ICommRepository<UnitEquipmentLink>
    {
        UnitEquipmentLink GetByIds(int unitId, int equipmentId, bool isThrowNotFound = false);
    }
}
