﻿using System;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories.Base;

namespace ElevatorMES.Domain.Repositories
{
    public interface IUnitInventoryRepository : IBaseRepository<UnitInventory>
    {
        UnitInventory GetByIds(int unitId, DateTime createdAt, bool isThrowNotFound = false);
    }
}
