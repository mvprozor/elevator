﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories.Base;

namespace ElevatorMES.Domain.Repositories
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User GetOrCreateByDisplayName(string displayName);
    }
}
