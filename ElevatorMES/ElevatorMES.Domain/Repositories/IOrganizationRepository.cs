﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Repositories.Base;

namespace ElevatorMES.Domain.Repositories
{
    public interface IOrganizationRepository : IBaseSyncRepository<Organization>
    {
    }

    public interface IContragentsRepository : IBaseSyncRepository<Contragent>
    {
    }
}
