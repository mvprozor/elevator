﻿using System.Collections.Generic;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Repositories.Base;

namespace ElevatorMES.Domain.Repositories
{
    public interface IStorageRepository
    {
        List<StorageUIItem> GetList<StorageUIItem>();
    }
}
