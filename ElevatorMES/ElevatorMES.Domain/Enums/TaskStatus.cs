﻿using System.ComponentModel;
using System.Drawing;

namespace ElevatorMES.Domain.Enums
{
    public enum TaskStatus
    {
        /// <summary>
        /// поставлено в очередь
        /// </summary>
        [Description("В очереди")]
        [DefaultValue(typeof(Color), "Yellow")]
        Queued = 1,
        /// <summary>
        /// удалено
        /// </summary>
        [Description("Удалено")]
        [DefaultValue(typeof(Color), "Red")]
        Deleted = 2,
        /// <summary>
        /// выполняется
        /// </summary>
        [Description("Выполняется")]
        [DefaultValue(typeof(Color), "LightGreen")]
        Running = 3,
        /// <summary>
        /// завершено
        /// </summary>
        [Description("Завершено")]
        [DefaultValue(typeof(Color), "Green")]
        Completed = 4,
        /// <summary>
        /// неудачно
        /// </summary>
        [Description("Сбой")]
        [DefaultValue(typeof(Color), "Pink")]
        Failed = 5,
        /// <summary>
        /// отменено
        /// </summary>
        [Description("Ожидание")]
        [DefaultValue(typeof(Color), "LightSkyBlue")]
        Pending = 6,
        /// <summary>
        /// приостановлено
        /// </summary>
        [Description("Пауза")]
        [DefaultValue(typeof(Color), "Blue")]
        Suspended = 7


    }
}