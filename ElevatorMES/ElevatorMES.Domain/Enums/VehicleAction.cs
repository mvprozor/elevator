﻿using System;
using System.ComponentModel;

namespace ElevatorMES.Domain.Enums
{
    public enum VehicleAction
    {

        [Description("Регистрация")]
        Registering = 1,

        [Description("Взвешивание")]
        Weighting = 2,

        [Description("Выгрузка")]
        Unloading = 3,

        [Description("Погрузка")]
        Loading = 4,

        [Description("Выезд")]
        Leaving = 5

    }
}