﻿using System;
using System.ComponentModel;

namespace ElevatorMES.Domain.Enums
{
    public enum VehicleQueue
    {
        [Description("Все")]
        All = 0,

        [Description("На брутто")]
        ToBrutto,

        [Description("На выгрузку")]
        ToUnload,

        [Description("На тару")]
        ToTare,

        [Description("На погрузку")]
        ToLoad,
            
        [Description("На выезд")]
        ToLeave,

        [Description("Выехали")]
        Leaved

    }
}
