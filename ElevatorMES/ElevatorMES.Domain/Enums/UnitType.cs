﻿using System;

namespace ElevatorMES.Domain.Enums
{
    [Flags]
    public enum UnitType
    {
        Storage = 1, // базовый тип склада

        Silos = Storage | (1 << 1),
        Bunker = Storage | (2 << 1),
        Dryer = Storage | (3 << 1),

        Points = 1 << 4,
        LoadingPoint = Points | (1 << 5),
        UnloadingPoint = Points | (2 << 5),

		CompoundFeedPlant = 1 << 7,

        SleeveStorage = Storage | (1 << 8)
	}
}