﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ElevatorMES.Domain.Enums
{
    public enum PLCTaskStatus
    {
        [Description("Неопределенное состояние")]
        Undetermined = 0,

        [Description("Маршрут остановлен")]
        RouteStopped = 1,

        [Description("Маршрут запущен")]
        RouteRunning = 2,

        [Description("Маршрут останавливается")]
        RouteStopping = 3,

        [Description("Маршрут запускается")]
        RouteStarting = 4,

        [Description("Останов по весу")]
        RouteCompleted = 5,

        [Description("Неизвестный ID задания")]
        UnknownTaskId = -1,

        [Description("Некорректная конфигурация маршрута")]
        ConfigurationError = -2,

        [Description("Нет готовности маршрута")]
        RouteNotReady = -3,

        [Description("Авария маршрута")]
        EquipmentFailure = -4
    }
}
