﻿using System.ComponentModel;

namespace ElevatorMES.Domain.Enums
{
    /// <summary>
    /// Режим инвентаризации
    /// </summary>
    public enum InventoryMode
    {
        [Description("Изменение нижнего слоя")]
        SetBottomLayer = 0,

        [Description("Изменение верхнего слоя")]
        SetTopLayer = 1,
    }
}
