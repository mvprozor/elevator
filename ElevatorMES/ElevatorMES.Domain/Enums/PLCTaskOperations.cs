﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Enums
{
    [Flags]
    public enum PLCTaskOperations
    {
        [Description("Без операций")]
        None = 0,

        [Description("Сушка")]
        Drying = 1,

        [Description("Сепарация")]
        Separating = 2,

        [Description("Очистка скальператором")]
        Scalping = 4
    }
}
