﻿namespace ElevatorMES.Domain.Enums
{
    public enum VehicleClass
    {
        Truck = 1,
        RailwayCar = 2
    }
}