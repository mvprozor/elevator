﻿using System.ComponentModel;

namespace ElevatorMES.Domain.Enums
{
    public enum DictionaryType
    {
        [Description("Номенклатура")]
        Nomenclature,

        [Description("Организации")]
        Organization,

        [Description("Контрагенты")]
        Contragent
    }
}
