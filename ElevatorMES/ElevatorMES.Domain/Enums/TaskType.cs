﻿using System.ComponentModel;

namespace ElevatorMES.Domain.Enums
{
    public enum TaskType
    {
        /// <summary>
        /// приемка
        /// </summary>
        [Description("Приемка")]
        Receiving = 1,

        /// <summary>
        /// перемещение между силосами
        /// </summary>
        [Description("Перемещение")]
        SilosMoving = 2,

        /// <summary>
        /// перемещение на комб. завод
        /// </summary>
        [Description("На ККЗ")]
        PlantMoving = 3,

        /// <summary>
        /// отгрузка внешнему потребителю
        /// </summary>
        [Description("Отгрузка")]
        ShippingExt = 4,

        /// <summary>
        /// отгрузка в рукава
        /// </summary>
        [Description("В рукава")]
        Sleeve = 5
    }
}