﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Enums
{
    [Flags]
    public enum UnitEquipmentLinkType
    {
        [Description("Нет связи")]
        None = 0,

        [Description("Контейнер")]
        Container = 1,

        [Description("Начальная точка маршрута")]
        RouteSource = 2,

        [Description("Конечная точка маршрута")]
        RouteDestination = 4
    }
}

