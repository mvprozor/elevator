﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Enums
{
    public enum EquipmentType
    {
        Common = 1,
        Noria = 2,
        Conveyor = 3,
        Fan = 4,
        Airlock = 5,
        VibratingPlate = 6,
        Scalpel = 7,
        Separator = 8,
        CarUnloader = 9,
        Screw = 10,
        HopperScales = 11,
        GrainDryer = 12,
        Siren = 13,
        GateValve = 14,
        TwoWayValve = 15,
        ThreeWayValve = 16,
        Bunker = 17
    }
}
