﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Enums
{
    public enum UserRole
    {
        [Description("Администратор")]
        Administrator = 0,

        [Description("Оператор")]
        Operator = 1,

        [Description("Пользователь")]
        Reader = 2
    }
}
