﻿using System.ComponentModel;

namespace ElevatorMES.Domain.Enums
{
    //[Description("Типы событий передачи данных в 1C")]
    public enum MessageType
    {
        /// <summary>
        /// Выполнение задания по отгрузке вмешнему потребителю
        /// </summary>
        [Description("Выполнение задания по отгрузке вмешнему потребителю")]        
        ExecutionTaskShipmentRaw_ExternalConsumers = 1,

        /// <summary>
        /// Выполнение задания по отгрузке в рукава
        /// </summary>
        [Description("Выполнение задания по отгрузке в рукава")]
        ExecutionTaskShipmentRaw_Hose,

        /// <summary>
        /// Выполнение задания по отгрузке на КЗ
        /// </summary>
        [Description("Выполнение задания по отгрузке на КЗ")]
        ExecutionTaskShipmentRaw_KZ,


        /// <summary>
        /// Взвешивание транспорта
        /// </summary>
        [Description("Взвешивание транспорта")]
        WeightingOfVehicles,

        /// <summary>
        /// Состояние склада
        /// </summary>
        [Description("Состояние склада")]
        Inventory,

        /// <summary>
        /// Завершение перемещения
        /// </summary>
        [Description("Завершение перемещения")]
        CompletingMove,

        /// <summary>
        /// Завершение приёмки сырья
        /// </summary>
        [Description("Завершение приёмки сырья")]
        CompletingRawMaterial,
    }
}
