﻿using System.ComponentModel;

namespace ElevatorMES.Domain.Enums
{
    [Description("Типы ошибок при передаче")]
    public enum ErrorType
    {
        [Description("Логическая ошибка")]
        Logical,

        [Description("Ошибка сети")]
        Network,
    }
}
