﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Scales
{
    public struct ScalesData
    {
        public readonly decimal Weight;
        public readonly bool? IsStable;

        public ScalesData(decimal weight, bool? isStable = null)
        {
            Weight = weight;
            IsStable = isStable;
        }
    }
}
