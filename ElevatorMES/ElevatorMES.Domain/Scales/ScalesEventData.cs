﻿using System;

namespace ElevatorMES.Domain.Scales
{
    public class ScalesEventData : ScalesEvent
    {
        public ScalesData[] Data { get; }

        public ScalesEventData(IScalesSession session, ScalesData[] data) : base(session)
        {
            if ((data?.Length ?? 0) == 0)
                throw new ArgumentNullException(nameof(data));
            Data = data;
        }

        public new delegate void Action(ScalesEventData scalesEvent);
    }
}
