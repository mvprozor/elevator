﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Scales
{
    public interface IScalesSession
    {
        IScalesConfig Config { get; }
        void Deactivate();
        bool IsDeactivationRequest { get; }
        bool IsDeactivated { get; }
    }
}
