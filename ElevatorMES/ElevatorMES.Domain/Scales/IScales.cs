﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Scales
{
    public interface IScales
    {
        IScalesConfig DefaultConfig { get; set; }

        IScalesSession Activate(
            ScalesEventData.Action onData, 
            ScalesEventError.Action onError,
            ScalesEvent.Action onDeactivated = null,
            IScalesConfig scalesConfig = null,
            object userData = null);
    }
}
