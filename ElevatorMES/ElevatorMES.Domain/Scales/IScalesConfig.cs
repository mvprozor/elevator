﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Scales
{
    public interface IScalesConfig
    {
        /// <summary>
        /// Адрес
        /// </summary>
        string Hostname { get; }

        /// <summary>
        /// Порт
        /// </summary>
        int Port { get; }
    }
}
