﻿using System;

namespace ElevatorMES.Domain.Scales
{
    public class ScalesEvent
    {
        public IScalesSession Session { get; }

        public ScalesEvent(IScalesSession session)
        {
            Session = session ?? throw new ArgumentException(nameof(session));
        }

        public delegate void Action(ScalesEvent scalesEvent);
    }
}
