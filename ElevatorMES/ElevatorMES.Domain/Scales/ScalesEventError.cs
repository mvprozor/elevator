﻿using System;

namespace ElevatorMES.Domain.Scales
{
    public class ScalesEventError : ScalesEvent
    {
        public string Error { get; }

        public ScalesEventError(IScalesSession session, string error) : base(session)
        {
            if (String.IsNullOrWhiteSpace(error))
                throw new ArgumentNullException(nameof(error));
            Error = error;
        }

        public new delegate void Action(ScalesEventError scalesEvent);
    }
}

