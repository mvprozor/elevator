﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Net;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Repositories;

namespace ElevatorMES.Domain
{
    public interface IUnitOfWork
    {
        ILotRepository LotRepository { get; }
        INomenclatureRepository NomenclatureRepository { get; }
        IOrganizationRepository OrganizationRepository { get; }
        IContragentsRepository ContragentsRepository { get; }
        ITaskRepository TaskRepository { get; }
        ITaskExecRepository TaskExecRepository { get; }
        IUnitRepository UnitRepository { get; }
        IUnitEquipmentLinkRepository UnitEquipmentLinkRepository { get; }
        ITaskExecMoveRepository TaskExecMoveRepository { get; }
        IUnitLayerRepository UnitLayerRepository { get; }
        IUnitSettingRepository UnitSettingRepository { get; }
        IEquipmentRepository EquipmentRepository { get; }
        IUserRepository UserRepository { get; }
        IVehicleRepository VehicleRepository { get; }
        IVehicleActivityRepository VehicleActivityRepository { get; }
        IVehicleTypeRepository VehicleTypeRepository { get; }
        INomenclaturePriorityRepository NomenclaturePriorityRepository { get; }
        ILogEvent1CRepository LogEvent1CRepository { get; }
        ITransmitLogRepository TransmitLogRepository { get; }
        IUnitInventoryRepository UnitInventoryRepository { get; }
        void ExecuteNonQuery(string query, params object[] parameters);
        List<T> ExecuteQuery<T>(string query, params object[] parameters);
        IEnumerable<string> GetValidationErrors();
        DbContextTransaction BeginTransaction();
        void SaveChanges();
    }
}
