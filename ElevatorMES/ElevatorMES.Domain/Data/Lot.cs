﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.RegularExpressions;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// Отражает партию как сущность - некоторое количество определенной продукции,
    /// полученное от одного поставщика в одно время на одной или нескольких транспортных единицах.
    /// Может содержать результаты анализов (влажность и.т.п.). Обладает уникальным номером партии.
    /// </summary>
    public class Lot : BaseEntity
    {
        /// <summary>
        /// Уникальный номер партии
        /// </summary>
        [Required]
        [MaxLength(64)]
        [Index(IsUnique = true)]
        public string LotNo { get; set; }
        public int NomenclatureId { get; set; }
        /// <summary>
        /// Сырьё
        /// </summary>
        public Nomenclature Nomenclature { get; set; }

        public int? OrganizationId { get; set; }
        /// <summary>
        /// Поставщик
        /// </summary>
        public Organization Organization { get; set; }

        private static Regex namePattern = new Regex(@"\b(?!(?:ООО|ЧОО)\b)\w+", RegexOptions.Compiled);

        public void GenerateLotNo()
        {
            if (Organization == null)
                throw new InvalidOperationException("Не задана организация");
            if (Nomenclature == null)
                throw new InvalidOperationException("Не задана номенклатура");

            var div = String.Empty;
            var lotBuilder = new StringBuilder(64);

            var orgNames = namePattern.Matches(Organization.DisplayName);
            for (int i=0; i<orgNames.Count && i<4; i++)
            {
                lotBuilder.Append(div);
                lotBuilder.Append(orgNames[i].Value.Substring(0, Math.Min(orgNames[i].Length, 4)).ToUpper());
                div = "_";
            }

            var nomNames = namePattern.Matches(Nomenclature.DisplayName);
            for (int i = 0; i < nomNames.Count && i < 4; i++)
            {
                lotBuilder.Append(div);
                lotBuilder.Append(nomNames[i].Value.Substring(0, Math.Min(nomNames[i].Length, 4)).ToUpper());
                div = "_";
            }

            lotBuilder.Append(div);
            lotBuilder.Append(CreatedAt.ToString("yyMMdd_HHmmss"));

            LotNo = lotBuilder.ToString();
        }
    }
}
