﻿using System.Collections.Generic;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Data
{
    public class Organization : BaseSyncEntity
    {
        public ICollection<Lot> Lots { get; set; }
        public static IEnumerable<Organization> Seed()
        {
            return new List<Organization>
            {
                new Organization {Id = 1, Code1C = "1", DisplayName = "Поставщик 1"},
                new Organization {Id = 2, Code1C = "2", DisplayName = "Поставщик 2"},
                new Organization {Id = 3, Code1C = "3", DisplayName = "Поставщик 3"},
            };
        }
    }
}