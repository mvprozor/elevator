﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// Журнал регистрации транспорта Vehicles
    /// В эту таблицу пишем транспорт, поступивший на предприятие под погрузку/отгрузку.
    /// </summary>
    public class Vehicle : BaseEntity
    {
        public int VehicleTypeId { get; set; }
        /// <summary>
        /// тип транспортного средства
        /// </summary>
        public VehicleType VehicleType { get; set; }
        /// <summary>
        /// номер транспортного средства
        /// </summary>
        [MaxLength(36)]
        [Required]
        public string RegNum { get; set; }

        /// <summary>
        /// дата ухода с территории
        /// </summary>
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        public DateTime? LeftAt { get; set; }

        /// <summary>
        /// идентификатор документа (1C)
        /// </summary>
        [MaxLength(36)]
        public string DocId { get; set; }

        public int? UserId { get; set; }
        /// <summary>
        /// автор (диспетчер или пользователь в 1С)
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// дата передачи/приема в 1С
        /// </summary>
        public DateTime? Sync1CAt { get; set; }

        public override string ToString()
        {
            return $"{Id}. {DocId}. {RegNum}, {VehicleType}";
        }
    }
}
