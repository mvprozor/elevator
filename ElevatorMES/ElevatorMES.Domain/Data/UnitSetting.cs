﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// В эту таблицу пишем, какая номенклатура лежит в каком силосе и другие долговременные настройки.
    /// Нужно для контроля.
    /// 1. Когда вычищаем силос для нового продукта, в NomenclatureId пишем NULL.
    /// 2. Не можем вычистить, если там есть слои.
    /// </summary>
    public class UnitSetting : BaseEntity, IUpdateable
    {
        /// <summary>
        /// идентификатор силоса, он же PK (в одном силосе не могу лежать разные продукты)
        /// </summary>
        [Index("IX_InitId_NomenclatureId", 1, IsUnique = true)]
        public int UnitId { get; set; }
        public Unit Unit { get; set; }

        [Index("IX_InitId_NomenclatureId", 2, IsUnique = true)]
        public int? NomenclatureId { get; set; }
        public Nomenclature Nomenclature { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
