﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.Domain.Data
{
    [Description("Связка подразделений и оборудования")]
    public class UnitEquipmentLink
    {
        [Key, Column(Order = 0)]
        [ForeignKey("Unit")]
        public int UnitId { get; set; }
        public Unit Unit { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Equipment")]
        public int EquipmentId { get; set; }
        public Equipment Equipment { get; set; }

        public UnitEquipmentLinkType LinkType { get; set; }

        public DateTime CreatedAt { get; private set; }

        public UnitEquipmentLink()
        {
            CreatedAt = DateTime.Now;
        }
    }
}
