﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Linq;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Domain.Data
{
    [Description("Передача данных в 1С")]
    public class TransmitLog : BaseEntity
    {
        public String xMessage { get; set; }
        [NotMapped]
        public XElement xMessageWrapper
        {
            get { return XElement.Parse(xMessage); }
            set { xMessage = value.ToString(); }
        }
        public MessageType MessageType { get; set; }
        public string MessageTypeRus { get; set; }
        public string Result { get; set; }
        public bool? IsSuccess { get; set; }
        public int? CountError { get; set; }
        public DateTime? DateTransmit { get; set; }
        public DateTime? LastTryDate { get; set; }
        public DateTime? NextTryDate { get; set; }

        public ErrorType? ErrorType { get; set; }
        public TransmitLog() { }

        public TransmitLog(XElement xMessage, MessageType messageType)
        {
            this.xMessageWrapper = xMessage;
            this.MessageType = messageType;
            this.MessageTypeRus = messageType.GetDescription();
            this.LastTryDate = null;
            this.NextTryDate = DateTime.Now;
        }
    }
}
