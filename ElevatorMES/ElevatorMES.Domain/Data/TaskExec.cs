﻿using System;
using System.ComponentModel.DataAnnotations;
using ElevatorMES.Domain.Data.Base;
using TaskStatus = ElevatorMES.Domain.Enums.TaskStatus;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// Журнал фактического выполнения заданий
    /// </summary>
    public class TaskExec : BaseEntity  
    {
        public int TaskId { get; set; }
        public Task Task { get; set; }

        /// <summary>
        /// фактическая дата начала
        /// </summary>
        public DateTime Start { get; set; }

        /// <summary>
        /// фактическая дата завершения
        /// </summary>
        public DateTime? End { get; set; }

        /// <summary>
        /// статус выполнения (enum: Running, Completed, Failed)
        /// </summary>
        public TaskStatus Status { get; set; }
        [MaxLength(2000)]
        public string Comment { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }

    }
}
