﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// Типы транспортных средста (VehicleTypes)
    /// </summary>
    public class VehicleType : BaseSyncEntity
    {
        [Required]
        public VehicleClass Class { get; set; }

        public static IEnumerable<VehicleType> Seed()
        {
            return new List<VehicleType>()
            {
                new VehicleType(){Id = 1, Class = VehicleClass.Truck, Code1C = "1", DisplayName = "Бортовой зерновоз"}
            };
        }

        public override string ToString()
        {
            return $"{Id}. {Code1C}. {DisplayName}";
        }
    }
}
