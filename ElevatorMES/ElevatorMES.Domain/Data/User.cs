﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// Пользователи
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// признак удаления
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// отображаемое имя
        /// </summary>
        [MaxLength(255)]
        [Required]
        public string DisplayName { get; set; }

        public static IEnumerable<User> Seed()
        {
            return new List<User>
            {
                new User {Id = 1, DisplayName = "1C"},
                new User {Id = 10, DisplayName = "Михаил"},
                new User {Id = 11, DisplayName = "Сергей"},
            };
        }
    }
}
