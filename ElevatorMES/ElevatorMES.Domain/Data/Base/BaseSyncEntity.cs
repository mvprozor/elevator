﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElevatorMES.Domain.Data.Base
{
    public class BaseSyncEntity : BaseEntity, IDeletable
    {
        [Required]
        public bool Deleted { get; set; }

        [MaxLength(255)]
        [Required]
        public string DisplayName { get; set; }

        [MaxLength(36)]
        [Index(IsUnique = true)]
        [Required]
        public string Code1C { get; set; }

        /// <summary>
        /// Тип в 1С
        /// </summary>
        [MaxLength(36)]
        public string Type1C { get; set; }

        /// <summary>
        /// Тип в 1С
        /// </summary>
        [MaxLength(36)]
        public string CodeParent1C { get; set; }

    }
}
