﻿using System;

namespace ElevatorMES.Domain.Data.Base
{
    public interface IBaseEntity : IEntity<int>
    {
        DateTime CreatedAt { get; }

    }
}
