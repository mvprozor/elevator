﻿using System;

namespace ElevatorMES.Domain.Data.Base
{
    public interface IUpdateable
    {
        DateTime UpdatedAt { get; set; }
    }
}
