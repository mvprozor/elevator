﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.Data.Base
{
    public interface IDeletable
    {
        bool Deleted { get; set; }
    }
}
