﻿using System;

namespace ElevatorMES.Domain.Data.Base
{
    public interface IEntity<T> where T : IEquatable<T>
    {
        T Id { get; set; }
    }
}
