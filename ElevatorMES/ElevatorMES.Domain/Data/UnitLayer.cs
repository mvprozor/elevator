﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// В эту таблицу пишем послойное состояние силоса.
    /// По датам слоя понимаем в каком они порядке.
    /// C точки зрения подачи в силос и выдачи из него слои организованы в виде FIFO (подается сверху, выдается снизу).
    /// В слое храним определенное количество из одной партии.
    /// Партии в слоях могут повторяться (взяли чуть-чуть снизу, прогнали через сушку и навалили наверх).
    ///
    /// Это конечная расчетная таблица - на нее ссылаться никто не должен. Но зато она часто будет модифицироваться. Соответственно удобнее для поиска и выстраивания FIFO сделать составной PK - UnitsId + CreatedAt, не нужно генерить постоянно ID при добавлении слоев.
    /// Когда кладем, проверяем верхний слой(с макс. датой CreatedAt) - если он такой же, как новая партия, которую кладем - объединяем вес, если нет - создаем новый слой.Также нужно проверять, что в силосе лежит та же номенклатура и ругаться, если другая.Когда забираем, удаляем нижние слои (по возрастанию даты CreatedAt) до тех пор, пока не наберем нужный вес.Какой-то слой будет частично забран, его оставляем, модифицируем у него только вес.
    /// </summary>
    public class UnitLayer: IBaseEntity
    {
        [NotMapped]
        public int Id { get; set; }

        [Key, Column(Order = 0)]
        public DateTime CreatedAt { get; set; }
        /// <summary>
        /// идентификатор силоса, он же PK (в одном силосе не могу лежать разные продукты)
        /// </summary>
        [Key, Column(Order = 1)]
        public int UnitId { get; set; }
        public Unit Unit { get; set; }

        public int LotId { get; set; }
        public Lot Lot { get; set; }

        public decimal Quantity { get; set; }

        public UnitLayer()
        {
            CreatedAt = DateTime.Now;
        }
        public UnitLayer(int silosId, int lotId, decimal weight): this()
        {
            this.UnitId = silosId;
            this.LotId = lotId;
            this.Quantity = weight;
        }
    }
}
