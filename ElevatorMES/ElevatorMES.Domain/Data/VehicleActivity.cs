﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// Журнал активности транспорта на территории VehiclesActivities
    /// Это конечная расчетная таблица - на нее ссылаться никто не должен.
    /// Для поиска записей лучше сделать составной PK (VehicleId, CreatedAt).
    /// </summary>
    [Description("Журнал активности транспорта")]
    public class VehicleActivity : IBaseEntity
    {
        [NotMapped]
        public int Id { get; set; }

        [Key, Column(Order = 0)]
        public int VehicleId { get; set; }
        public Vehicle Vehicle { get; set; }

        [Key, Column(Order = 1)]
        public DateTime CreatedAt { get; set; }

        [Required]
        public VehicleAction Action { get; set; }
        public int? NomenclatureId { get; set; }
        /// <summary>
        ///  что в транспоре в зависимоcти от действия
        /// </summary>
        public Nomenclature Nomenclature { get; set; }

        public int? UnitId { get; set; }
        public Unit Unit { get; set; }
        public decimal? Weight { get; set; }
        [MaxLength(255)]
        public string Comment { get; set; }

        public VehicleActivity()
        {
            CreatedAt = DateTime.Now;
        }

        public override string ToString()
        {
            return $"{Action.GetDescription()} в {CreatedAt:g}";
        }
    }
}
