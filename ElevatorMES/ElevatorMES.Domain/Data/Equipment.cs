﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.Domain.Data
{
    [Description("Оборудование")]
    public class Equipment : BaseEntity
    {
        [Required]
        public EquipmentType EquipmentType { get; set; }

        [Required]
        [MaxLength(128)]
        public string SysName { get; set; }

        [Required]
        [MaxLength(128)]
        public string DisplayName { get; set; }

        public static IEnumerable<Equipment> Seed()
        {
            return new Equipment[]
            {
                new Equipment() { Id = 1, EquipmentType=EquipmentType.Noria, SysName="Nor_2_1_1", DisplayName="нория 2.1.1" }
                ,new Equipment() { Id = 2, EquipmentType=EquipmentType.Noria, SysName="Nor_2_1_2", DisplayName="нория 2.1.2" }
                ,new Equipment() { Id = 3, EquipmentType=EquipmentType.Noria, SysName="Nor_2_1_3", DisplayName="нория 2.1.3" }
                ,new Equipment() { Id = 4, EquipmentType=EquipmentType.Noria, SysName="Nor_2_1_4", DisplayName="нория 2.1.4" }
                ,new Equipment() { Id = 5, EquipmentType=EquipmentType.Noria, SysName="Nor_2_2_1", DisplayName="нория 2.2.1" }
                ,new Equipment() { Id = 6, EquipmentType=EquipmentType.Noria, SysName="Nor_2_2_2", DisplayName="нория 2.2.2" }
                ,new Equipment() { Id = 7, EquipmentType=EquipmentType.Noria, SysName="Nor_2_2_3", DisplayName="нория 2.2.3" }
                ,new Equipment() { Id = 8, EquipmentType=EquipmentType.Noria, SysName="Nor_2_2_4", DisplayName="нория 2.2.4" }
                ,new Equipment() { Id = 9, EquipmentType=EquipmentType.Noria, SysName="Nor_5_1_1", DisplayName="нория 5.1.1" }
                ,new Equipment() { Id = 10, EquipmentType=EquipmentType.Noria, SysName="Nor_5_1_2", DisplayName="нория 5.1.2" }
                ,new Equipment() { Id = 11, EquipmentType=EquipmentType.Noria, SysName="Nor_5_2_1", DisplayName="нория 5.2.1" }
                ,new Equipment() { Id = 12, EquipmentType=EquipmentType.Noria, SysName="Nor_5_2_2", DisplayName="нория 5.2.2" }
                ,new Equipment() { Id = 13, EquipmentType=EquipmentType.Noria, SysName="Nor_9_1_1", DisplayName="нория 9.1.1" }
                ,new Equipment() { Id = 14, EquipmentType=EquipmentType.Noria, SysName="Nor_9_1_2", DisplayName="нория 9.1.2" }
                ,new Equipment() { Id = 15, EquipmentType=EquipmentType.Noria, SysName="Nor_10_1_1", DisplayName="нория 10.1.1" }
                ,new Equipment() { Id = 16, EquipmentType=EquipmentType.Noria, SysName="Nor_10_1_2", DisplayName="нория 10.1.2" }
                ,new Equipment() { Id = 17, EquipmentType=EquipmentType.Noria, SysName="Nor_11_1_1", DisplayName="нория 11.1.1" }
                ,new Equipment() { Id = 18, EquipmentType=EquipmentType.Noria, SysName="Nor_11_1_2", DisplayName="нория 11.1.2" }
                ,new Equipment() { Id = 19, EquipmentType=EquipmentType.Noria, SysName="Nor_2_3_1", DisplayName="нория 2.3.1" }
                ,new Equipment() { Id = 20, EquipmentType=EquipmentType.Noria, SysName="Nor_2_3_2", DisplayName="нория 2.3.2" }
                ,new Equipment() { Id = 21, EquipmentType=EquipmentType.Noria, SysName="Nor_2_3_3", DisplayName="нория 2.3.3" }
                ,new Equipment() { Id = 22, EquipmentType=EquipmentType.Noria, SysName="Nor_2_3_4", DisplayName="нория 2.3.4" }
                ,new Equipment() { Id = 23, EquipmentType=EquipmentType.Noria, SysName="Nor_5_3_1", DisplayName="нория 5.3.1" }
                ,new Equipment() { Id = 24, EquipmentType=EquipmentType.Noria, SysName="Nor_5_3_2", DisplayName="нория 5.3.2" }
                ,new Equipment() { Id = 25, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_1_1_4", DisplayName="конвейер 1.1.4" }
                ,new Equipment() { Id = 26, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_1_1_4_a", DisplayName="конвейер 1.1.4.a" }
                ,new Equipment() { Id = 27, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_1_1_5", DisplayName="конвейер 1.1.5" }
                ,new Equipment() { Id = 28, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_1_1_5_a", DisplayName="конвейер 1.1.5.a" }
                ,new Equipment() { Id = 29, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_3_1_6", DisplayName="конвейер 3.1.6" }
                ,new Equipment() { Id = 30, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_3_1_8", DisplayName="конвейер 3.1.8" }
                ,new Equipment() { Id = 31, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_4_1_4", DisplayName="конвейер 4.1.4" }
                ,new Equipment() { Id = 32, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_4_1_6", DisplayName="конвейер 4.1.6" }
                ,new Equipment() { Id = 33, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_3_3_5", DisplayName="конвейер 3.3.5" }
                ,new Equipment() { Id = 34, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_3_3_7", DisplayName="конвейер 3.3.7" }
                ,new Equipment() { Id = 35, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_3_3_8", DisplayName="конвейер 3.3.8" }
                ,new Equipment() { Id = 36, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_4_2_4", DisplayName="конвейер 4.2.4" }
                ,new Equipment() { Id = 37, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_4_2_6", DisplayName="конвейер 4.2.6" }
                ,new Equipment() { Id = 38, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_5_1_4", DisplayName="конвейер 5.1.4" }
                ,new Equipment() { Id = 39, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_6_1_4", DisplayName="конвейер 6.1.4" }
                ,new Equipment() { Id = 40, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_6_2_4", DisplayName="конвейер 6.2.4" }
                ,new Equipment() { Id = 41, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_7_1_1", DisplayName="конвейер 7.1.1" }
                ,new Equipment() { Id = 42, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_7_1_2", DisplayName="конвейер 7.1.2" }
                ,new Equipment() { Id = 43, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_8_1_1", DisplayName="конвейер 8.1.1" }
                ,new Equipment() { Id = 44, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_13_1_4", DisplayName="конвейер 13.1.4" }
                ,new Equipment() { Id = 45, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_16_1_1", DisplayName="конвейер 16.1.1" }
                ,new Equipment() { Id = 46, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_13_2", DisplayName="конвейер 14.13.2" }
                ,new Equipment() { Id = 47, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_13_4", DisplayName="конвейер 14.13.4" }
                ,new Equipment() { Id = 48, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_16_14", DisplayName="конвейер 14.16.14" }
                ,new Equipment() { Id = 49, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_16_27", DisplayName="конвейер 14.16.27" }
                ,new Equipment() { Id = 50, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_16_40", DisplayName="конвейер 14.16.40" }
                ,new Equipment() { Id = 51, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_16_1_2", DisplayName="конвейер 16.1.2" }
                ,new Equipment() { Id = 52, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_16_1_4", DisplayName="конвейер 16.1.4" }
                ,new Equipment() { Id = 53, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_8_1_3", DisplayName="конвейер 8.1.3" }
                ,new Equipment() { Id = 54, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_13_1", DisplayName="конвейер 14.13.1" }
                ,new Equipment() { Id = 55, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_13_1_7", DisplayName="конвейер 13.1.7" }
                ,new Equipment() { Id = 56, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_16_1_3", DisplayName="конвейер 16.1.3" }
                ,new Equipment() { Id = 57, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_12_1_1", DisplayName="конвейер 12.1.1" }
                ,new Equipment() { Id = 58, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_14_1", DisplayName="конвейер 14.14.1" }
                ,new Equipment() { Id = 59, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_14_2", DisplayName="конвейер 14.14.2" }
                ,new Equipment() { Id = 60, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_14_4", DisplayName="конвейер 14.14.4" }
                ,new Equipment() { Id = 61, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_17_14", DisplayName="конвейер 14.17.14" }
                ,new Equipment() { Id = 62, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_17_27", DisplayName="конвейер 14.17.27" }
                ,new Equipment() { Id = 63, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_17_40", DisplayName="конвейер 14.17.40" }
                ,new Equipment() { Id = 64, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_2_20_1", DisplayName="конвейер 2.20.1" }
                ,new Equipment() { Id = 65, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_2_20_2", DisplayName="конвейер 2.20.2" }
                ,new Equipment() { Id = 66, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_1_1_2", DisplayName="конвейер 1.1.2" }
                ,new Equipment() { Id = 67, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_1_1_3", DisplayName="конвейер 1.1.3" }
                ,new Equipment() { Id = 68, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_12_1_2", DisplayName="конвейер 12.1.2" }
                ,new Equipment() { Id = 69, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_12_1_3", DisplayName="конвейер 12.1.3" }
                ,new Equipment() { Id = 70, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_13_1_1", DisplayName="конвейер 13.1.1" }
                ,new Equipment() { Id = 71, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_13_6", DisplayName="конвейер 14.13.6" }
                ,new Equipment() { Id = 72, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_14_6", DisplayName="конвейер 14.14.6" }
                ,new Equipment() { Id = 73, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_15_1", DisplayName="конвейер 14.15.1" }
                ,new Equipment() { Id = 74, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_15_2", DisplayName="конвейер 14.15.2" }
                ,new Equipment() { Id = 75, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_15_6", DisplayName="конвейер 14.15.6" }
                ,new Equipment() { Id = 76, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_16_1", DisplayName="конвейер 14.16.1" }
                ,new Equipment() { Id = 77, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_17_1", DisplayName="конвейер 14.17.1" }
                ,new Equipment() { Id = 78, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_18_1", DisplayName="конвейер 14.18.1" }
                ,new Equipment() { Id = 79, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_18_14", DisplayName="конвейер 14.18.14" }
                ,new Equipment() { Id = 80, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_18_27", DisplayName="конвейер 14.18.27" }
                ,new Equipment() { Id = 81, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_18_40", DisplayName="конвейер 14.18.40" }
                ,new Equipment() { Id = 82, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_2_20_3", DisplayName="конвейер 2.20.3" }
                ,new Equipment() { Id = 83, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_3_5_5", DisplayName="конвейер 3.5.5" }
                ,new Equipment() { Id = 84, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_3_5_6", DisplayName="конвейер 3.5.6" }
                ,new Equipment() { Id = 85, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_3_5_8", DisplayName="конвейер 3.5.8" }
                ,new Equipment() { Id = 86, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_3_5_9", DisplayName="конвейер 3.5.9" }
                ,new Equipment() { Id = 87, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_4_3_4", DisplayName="конвейер 4.3.4" }
                ,new Equipment() { Id = 88, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_4_3_6", DisplayName="конвейер 4.3.6" }
                ,new Equipment() { Id = 89, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_4_3_7", DisplayName="конвейер 4.3.7" }
                ,new Equipment() { Id = 90, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_5_2_4", DisplayName="конвейер 5.2.4" }
                ,new Equipment() { Id = 91, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_6_3_4", DisplayName="конвейер 6.3.4" }
                ,new Equipment() { Id = 92, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_7_1_3", DisplayName="конвейер 7.1.3" }
                ,new Equipment() { Id = 93, EquipmentType=EquipmentType.Fan, SysName="Fan_3_1_4_M1", DisplayName="вентилятор 3.1.4.M1" }
                ,new Equipment() { Id = 94, EquipmentType=EquipmentType.Fan, SysName="Fan_3_1_4_M2", DisplayName="вентилятор 3.1.4.M2" }
                ,new Equipment() { Id = 95, EquipmentType=EquipmentType.Fan, SysName="Fan_3_2_4_M1", DisplayName="вентилятор 3.2.4.M1" }
                ,new Equipment() { Id = 96, EquipmentType=EquipmentType.Fan, SysName="Fan_3_2_4_M2", DisplayName="вентилятор 3.2.4.M2" }
                ,new Equipment() { Id = 97, EquipmentType=EquipmentType.Fan, SysName="Fan_3_3_4_M1", DisplayName="вентилятор 3.3.4.M1" }
                ,new Equipment() { Id = 98, EquipmentType=EquipmentType.Fan, SysName="Fan_3_3_4_M2", DisplayName="вентилятор 3.3.4.M2" }
                ,new Equipment() { Id = 99, EquipmentType=EquipmentType.Fan, SysName="Fan_3_4_4_M1", DisplayName="вентилятор 3.4.4.M1" }
                ,new Equipment() { Id = 100, EquipmentType=EquipmentType.Fan, SysName="Fan_3_4_4_M2", DisplayName="вентилятор 3.4.4.M2" }
                ,new Equipment() { Id = 101, EquipmentType=EquipmentType.Fan, SysName="Fan_14_1_3", DisplayName="вентилятор 14.1.3" }
                ,new Equipment() { Id = 102, EquipmentType=EquipmentType.Fan, SysName="Fan_14_1_4", DisplayName="вентилятор 14.1.4" }
                ,new Equipment() { Id = 103, EquipmentType=EquipmentType.Fan, SysName="Fan_14_1_5", DisplayName="вентилятор 14.1.5" }
                ,new Equipment() { Id = 104, EquipmentType=EquipmentType.Fan, SysName="Fan_14_1_6", DisplayName="вентилятор 14.1.6" }
                ,new Equipment() { Id = 105, EquipmentType=EquipmentType.Fan, SysName="Fan_14_2_3", DisplayName="вентилятор 14.2.3" }
                ,new Equipment() { Id = 106, EquipmentType=EquipmentType.Fan, SysName="Fan_14_2_4", DisplayName="вентилятор 14.2.4" }
                ,new Equipment() { Id = 107, EquipmentType=EquipmentType.Fan, SysName="Fan_14_2_5", DisplayName="вентилятор 14.2.5" }
                ,new Equipment() { Id = 108, EquipmentType=EquipmentType.Fan, SysName="Fan_14_2_6", DisplayName="вентилятор 14.2.6" }
                ,new Equipment() { Id = 109, EquipmentType=EquipmentType.Fan, SysName="Fan_14_3_3", DisplayName="вентилятор 14.3.3" }
                ,new Equipment() { Id = 110, EquipmentType=EquipmentType.Fan, SysName="Fan_14_3_4", DisplayName="вентилятор 14.3.4" }
                ,new Equipment() { Id = 111, EquipmentType=EquipmentType.Fan, SysName="Fan_14_3_5", DisplayName="вентилятор 14.3.5" }
                ,new Equipment() { Id = 112, EquipmentType=EquipmentType.Fan, SysName="Fan_14_3_6", DisplayName="вентилятор 14.3.6" }
                ,new Equipment() { Id = 113, EquipmentType=EquipmentType.Fan, SysName="Fan_14_5_3", DisplayName="вентилятор 14.5.3" }
                ,new Equipment() { Id = 114, EquipmentType=EquipmentType.Fan, SysName="Fan_14_5_4", DisplayName="вентилятор 14.5.4" }
                ,new Equipment() { Id = 115, EquipmentType=EquipmentType.Fan, SysName="Fan_14_5_5", DisplayName="вентилятор 14.5.5" }
                ,new Equipment() { Id = 116, EquipmentType=EquipmentType.Fan, SysName="Fan_14_5_6", DisplayName="вентилятор 14.5.6" }
                ,new Equipment() { Id = 117, EquipmentType=EquipmentType.Fan, SysName="Fan_14_6_3", DisplayName="вентилятор 14.6.3" }
                ,new Equipment() { Id = 118, EquipmentType=EquipmentType.Fan, SysName="Fan_14_6_4", DisplayName="вентилятор 14.6.4" }
                ,new Equipment() { Id = 119, EquipmentType=EquipmentType.Fan, SysName="Fan_14_6_5", DisplayName="вентилятор 14.6.5" }
                ,new Equipment() { Id = 120, EquipmentType=EquipmentType.Fan, SysName="Fan_14_6_6", DisplayName="вентилятор 14.6.6" }
                ,new Equipment() { Id = 121, EquipmentType=EquipmentType.Fan, SysName="Fan_14_7_3", DisplayName="вентилятор 14.7.3" }
                ,new Equipment() { Id = 122, EquipmentType=EquipmentType.Fan, SysName="Fan_14_7_4", DisplayName="вентилятор 14.7.4" }
                ,new Equipment() { Id = 123, EquipmentType=EquipmentType.Fan, SysName="Fan_14_7_5", DisplayName="вентилятор 14.7.5" }
                ,new Equipment() { Id = 124, EquipmentType=EquipmentType.Fan, SysName="Fan_14_7_6", DisplayName="вентилятор 14.7.6" }
                ,new Equipment() { Id = 125, EquipmentType=EquipmentType.Fan, SysName="Fan_1_3_1", DisplayName="вентилятор 1.3.1" }
                ,new Equipment() { Id = 126, EquipmentType=EquipmentType.Fan, SysName="Fan_1_3_2", DisplayName="вентилятор 1.3.2" }
                ,new Equipment() { Id = 127, EquipmentType=EquipmentType.Fan, SysName="Fan_1_3_3", DisplayName="вентилятор 1.3.3" }
                ,new Equipment() { Id = 128, EquipmentType=EquipmentType.Fan, SysName="Fan_1_3_4", DisplayName="вентилятор 1.3.4" }
                ,new Equipment() { Id = 129, EquipmentType=EquipmentType.Fan, SysName="Fan_1_3_5", DisplayName="вентилятор 1.3.5" }
                ,new Equipment() { Id = 130, EquipmentType=EquipmentType.Fan, SysName="Fan_1_4_1", DisplayName="вентилятор 1.4.1" }
                ,new Equipment() { Id = 131, EquipmentType=EquipmentType.Fan, SysName="Fan_1_4_2", DisplayName="вентилятор 1.4.2" }
                ,new Equipment() { Id = 132, EquipmentType=EquipmentType.Fan, SysName="Fan_1_4_3", DisplayName="вентилятор 1.4.3" }
                ,new Equipment() { Id = 133, EquipmentType=EquipmentType.Fan, SysName="Fan_1_4_4", DisplayName="вентилятор 1.4.4" }
                ,new Equipment() { Id = 134, EquipmentType=EquipmentType.Fan, SysName="Fan_1_4_5", DisplayName="вентилятор 1.4.5" }
                ,new Equipment() { Id = 135, EquipmentType=EquipmentType.Fan, SysName="Fan_2_10_5", DisplayName="вентилятор 2.10.5" }
                ,new Equipment() { Id = 136, EquipmentType=EquipmentType.Fan, SysName="Fan_2_7_7", DisplayName="вентилятор 2.7.7" }
                ,new Equipment() { Id = 137, EquipmentType=EquipmentType.Fan, SysName="Fan_2_13_6", DisplayName="вентилятор 2.13.6" }
                ,new Equipment() { Id = 138, EquipmentType=EquipmentType.Fan, SysName="Fan_2_16_1", DisplayName="вентилятор 2.16.1" }
                ,new Equipment() { Id = 139, EquipmentType=EquipmentType.Fan, SysName="Fan_2_11_5", DisplayName="вентилятор 2.11.5" }
                ,new Equipment() { Id = 140, EquipmentType=EquipmentType.Fan, SysName="Fan_2_8_7", DisplayName="вентилятор 2.8.7" }
                ,new Equipment() { Id = 141, EquipmentType=EquipmentType.Fan, SysName="Fan_2_17_1", DisplayName="вентилятор 2.17.1" }
                ,new Equipment() { Id = 142, EquipmentType=EquipmentType.Fan, SysName="Fan_1_2_1", DisplayName="вентилятор 1.2.1" }
                ,new Equipment() { Id = 143, EquipmentType=EquipmentType.Fan, SysName="Fan_1_2_2", DisplayName="вентилятор 1.2.2" }
                ,new Equipment() { Id = 144, EquipmentType=EquipmentType.Fan, SysName="Fan_1_2_3", DisplayName="вентилятор 1.2.3" }
                ,new Equipment() { Id = 145, EquipmentType=EquipmentType.Fan, SysName="Fan_1_2_4", DisplayName="вентилятор 1.2.4" }
                ,new Equipment() { Id = 146, EquipmentType=EquipmentType.Fan, SysName="Fan_1_2_5", DisplayName="вентилятор 1.2.5" }
                ,new Equipment() { Id = 147, EquipmentType=EquipmentType.Fan, SysName="Fan_14_1_1_1", DisplayName="вентилятор 14.1.1.1" }
                ,new Equipment() { Id = 148, EquipmentType=EquipmentType.Fan, SysName="Fan_14_1_1_2", DisplayName="вентилятор 14.1.1.2" }
                ,new Equipment() { Id = 149, EquipmentType=EquipmentType.Fan, SysName="Fan_14_1_1_3", DisplayName="вентилятор 14.1.1.3" }
                ,new Equipment() { Id = 150, EquipmentType=EquipmentType.Fan, SysName="Fan_14_1_1_4", DisplayName="вентилятор 14.1.1.4" }
                ,new Equipment() { Id = 151, EquipmentType=EquipmentType.Fan, SysName="Fan_14_10_1_1", DisplayName="вентилятор 14.10.1.1" }
                ,new Equipment() { Id = 152, EquipmentType=EquipmentType.Fan, SysName="Fan_14_10_1_2", DisplayName="вентилятор 14.10.1.2" }
                ,new Equipment() { Id = 153, EquipmentType=EquipmentType.Fan, SysName="Fan_14_10_1_3", DisplayName="вентилятор 14.10.1.3" }
                ,new Equipment() { Id = 154, EquipmentType=EquipmentType.Fan, SysName="Fan_14_10_1_4", DisplayName="вентилятор 14.10.1.4" }
                ,new Equipment() { Id = 155, EquipmentType=EquipmentType.Fan, SysName="Fan_14_10_3", DisplayName="вентилятор 14.10.3" }
                ,new Equipment() { Id = 156, EquipmentType=EquipmentType.Fan, SysName="Fan_14_10_4", DisplayName="вентилятор 14.10.4" }
                ,new Equipment() { Id = 157, EquipmentType=EquipmentType.Fan, SysName="Fan_14_10_5", DisplayName="вентилятор 14.10.5" }
                ,new Equipment() { Id = 158, EquipmentType=EquipmentType.Fan, SysName="Fan_14_10_6", DisplayName="вентилятор 14.10.6" }
                ,new Equipment() { Id = 159, EquipmentType=EquipmentType.Fan, SysName="Fan_14_11_1_1", DisplayName="вентилятор 14.11.1.1" }
                ,new Equipment() { Id = 160, EquipmentType=EquipmentType.Fan, SysName="Fan_14_11_1_2", DisplayName="вентилятор 14.11.1.2" }
                ,new Equipment() { Id = 161, EquipmentType=EquipmentType.Fan, SysName="Fan_14_11_1_3", DisplayName="вентилятор 14.11.1.3" }
                ,new Equipment() { Id = 162, EquipmentType=EquipmentType.Fan, SysName="Fan_14_11_1_4", DisplayName="вентилятор 14.11.1.4" }
                ,new Equipment() { Id = 163, EquipmentType=EquipmentType.Fan, SysName="Fan_14_11_3", DisplayName="вентилятор 14.11.3" }
                ,new Equipment() { Id = 164, EquipmentType=EquipmentType.Fan, SysName="Fan_14_11_4", DisplayName="вентилятор 14.11.4" }
                ,new Equipment() { Id = 165, EquipmentType=EquipmentType.Fan, SysName="Fan_14_11_5", DisplayName="вентилятор 14.11.5" }
                ,new Equipment() { Id = 166, EquipmentType=EquipmentType.Fan, SysName="Fan_14_11_6", DisplayName="вентилятор 14.11.6" }
                ,new Equipment() { Id = 167, EquipmentType=EquipmentType.Fan, SysName="Fan_14_12_1_1", DisplayName="вентилятор 14.12.1.1" }
                ,new Equipment() { Id = 168, EquipmentType=EquipmentType.Fan, SysName="Fan_14_12_1_2", DisplayName="вентилятор 14.12.1.2" }
                ,new Equipment() { Id = 169, EquipmentType=EquipmentType.Fan, SysName="Fan_14_12_1_3", DisplayName="вентилятор 14.12.1.3" }
                ,new Equipment() { Id = 170, EquipmentType=EquipmentType.Fan, SysName="Fan_14_12_1_4", DisplayName="вентилятор 14.12.1.4" }
                ,new Equipment() { Id = 171, EquipmentType=EquipmentType.Fan, SysName="Fan_14_12_3", DisplayName="вентилятор 14.12.3" }
                ,new Equipment() { Id = 172, EquipmentType=EquipmentType.Fan, SysName="Fan_14_12_4", DisplayName="вентилятор 14.12.4" }
                ,new Equipment() { Id = 173, EquipmentType=EquipmentType.Fan, SysName="Fan_14_12_5", DisplayName="вентилятор 14.12.5" }
                ,new Equipment() { Id = 174, EquipmentType=EquipmentType.Fan, SysName="Fan_14_12_6", DisplayName="вентилятор 14.12.6" }
                ,new Equipment() { Id = 175, EquipmentType=EquipmentType.Fan, SysName="Fan_14_2_1_1", DisplayName="вентилятор 14.2.1.1" }
                ,new Equipment() { Id = 176, EquipmentType=EquipmentType.Fan, SysName="Fan_14_2_1_2", DisplayName="вентилятор 14.2.1.2" }
                ,new Equipment() { Id = 177, EquipmentType=EquipmentType.Fan, SysName="Fan_14_2_1_3", DisplayName="вентилятор 14.2.1.3" }
                ,new Equipment() { Id = 178, EquipmentType=EquipmentType.Fan, SysName="Fan_14_2_1_4", DisplayName="вентилятор 14.2.1.4" }
                ,new Equipment() { Id = 179, EquipmentType=EquipmentType.Fan, SysName="Fan_14_3_1_1", DisplayName="вентилятор 14.3.1.1" }
                ,new Equipment() { Id = 180, EquipmentType=EquipmentType.Fan, SysName="Fan_14_3_1_2", DisplayName="вентилятор 14.3.1.2" }
                ,new Equipment() { Id = 181, EquipmentType=EquipmentType.Fan, SysName="Fan_14_3_1_3", DisplayName="вентилятор 14.3.1.3" }
                ,new Equipment() { Id = 182, EquipmentType=EquipmentType.Fan, SysName="Fan_14_3_1_4", DisplayName="вентилятор 14.3.1.4" }
                ,new Equipment() { Id = 183, EquipmentType=EquipmentType.Fan, SysName="Fan_14_4_1_1", DisplayName="вентилятор 14.4.1.1" }
                ,new Equipment() { Id = 184, EquipmentType=EquipmentType.Fan, SysName="Fan_14_4_1_2", DisplayName="вентилятор 14.4.1.2" }
                ,new Equipment() { Id = 185, EquipmentType=EquipmentType.Fan, SysName="Fan_14_4_1_3", DisplayName="вентилятор 14.4.1.3" }
                ,new Equipment() { Id = 186, EquipmentType=EquipmentType.Fan, SysName="Fan_14_4_1_4", DisplayName="вентилятор 14.4.1.4" }
                ,new Equipment() { Id = 187, EquipmentType=EquipmentType.Fan, SysName="Fan_14_4_3", DisplayName="вентилятор 14.4.3" }
                ,new Equipment() { Id = 188, EquipmentType=EquipmentType.Fan, SysName="Fan_14_4_4", DisplayName="вентилятор 14.4.4" }
                ,new Equipment() { Id = 189, EquipmentType=EquipmentType.Fan, SysName="Fan_14_4_5", DisplayName="вентилятор 14.4.5" }
                ,new Equipment() { Id = 190, EquipmentType=EquipmentType.Fan, SysName="Fan_14_4_6", DisplayName="вентилятор 14.4.6" }
                ,new Equipment() { Id = 191, EquipmentType=EquipmentType.Fan, SysName="Fan_14_5_1_1", DisplayName="вентилятор 14.5.1.1" }
                ,new Equipment() { Id = 192, EquipmentType=EquipmentType.Fan, SysName="Fan_14_5_1_2", DisplayName="вентилятор 14.5.1.2" }
                ,new Equipment() { Id = 193, EquipmentType=EquipmentType.Fan, SysName="Fan_14_5_1_3", DisplayName="вентилятор 14.5.1.3" }
                ,new Equipment() { Id = 194, EquipmentType=EquipmentType.Fan, SysName="Fan_14_5_1_4", DisplayName="вентилятор 14.5.1.4" }
                ,new Equipment() { Id = 195, EquipmentType=EquipmentType.Fan, SysName="Fan_14_6_1_1", DisplayName="вентилятор 14.6.1.1" }
                ,new Equipment() { Id = 196, EquipmentType=EquipmentType.Fan, SysName="Fan_14_6_1_2", DisplayName="вентилятор 14.6.1.2" }
                ,new Equipment() { Id = 197, EquipmentType=EquipmentType.Fan, SysName="Fan_14_6_1_3", DisplayName="вентилятор 14.6.1.3" }
                ,new Equipment() { Id = 198, EquipmentType=EquipmentType.Fan, SysName="Fan_14_6_1_4", DisplayName="вентилятор 14.6.1.4" }
                ,new Equipment() { Id = 199, EquipmentType=EquipmentType.Fan, SysName="Fan_14_7_1_1", DisplayName="вентилятор 14.7.1.1" }
                ,new Equipment() { Id = 200, EquipmentType=EquipmentType.Fan, SysName="Fan_14_7_1_2", DisplayName="вентилятор 14.7.1.2" }
                ,new Equipment() { Id = 201, EquipmentType=EquipmentType.Fan, SysName="Fan_14_7_1_3", DisplayName="вентилятор 14.7.1.3" }
                ,new Equipment() { Id = 202, EquipmentType=EquipmentType.Fan, SysName="Fan_14_7_1_4", DisplayName="вентилятор 14.7.1.4" }
                ,new Equipment() { Id = 203, EquipmentType=EquipmentType.Fan, SysName="Fan_14_8_1_1", DisplayName="вентилятор 14.8.1.1" }
                ,new Equipment() { Id = 204, EquipmentType=EquipmentType.Fan, SysName="Fan_14_8_1_2", DisplayName="вентилятор 14.8.1.2" }
                ,new Equipment() { Id = 205, EquipmentType=EquipmentType.Fan, SysName="Fan_14_8_1_3", DisplayName="вентилятор 14.8.1.3" }
                ,new Equipment() { Id = 206, EquipmentType=EquipmentType.Fan, SysName="Fan_14_8_1_4", DisplayName="вентилятор 14.8.1.4" }
                ,new Equipment() { Id = 207, EquipmentType=EquipmentType.Fan, SysName="Fan_14_8_3", DisplayName="вентилятор 14.8.3" }
                ,new Equipment() { Id = 208, EquipmentType=EquipmentType.Fan, SysName="Fan_14_8_4", DisplayName="вентилятор 14.8.4" }
                ,new Equipment() { Id = 209, EquipmentType=EquipmentType.Fan, SysName="Fan_14_8_5", DisplayName="вентилятор 14.8.5" }
                ,new Equipment() { Id = 210, EquipmentType=EquipmentType.Fan, SysName="Fan_14_8_6", DisplayName="вентилятор 14.8.6" }
                ,new Equipment() { Id = 211, EquipmentType=EquipmentType.Fan, SysName="Fan_14_9_1_1", DisplayName="вентилятор 14.9.1.1" }
                ,new Equipment() { Id = 212, EquipmentType=EquipmentType.Fan, SysName="Fan_14_9_1_2", DisplayName="вентилятор 14.9.1.2" }
                ,new Equipment() { Id = 213, EquipmentType=EquipmentType.Fan, SysName="Fan_14_9_1_3", DisplayName="вентилятор 14.9.1.3" }
                ,new Equipment() { Id = 214, EquipmentType=EquipmentType.Fan, SysName="Fan_14_9_1_4", DisplayName="вентилятор 14.9.1.4" }
                ,new Equipment() { Id = 215, EquipmentType=EquipmentType.Fan, SysName="Fan_14_9_3", DisplayName="вентилятор 14.9.3" }
                ,new Equipment() { Id = 216, EquipmentType=EquipmentType.Fan, SysName="Fan_14_9_4", DisplayName="вентилятор 14.9.4" }
                ,new Equipment() { Id = 217, EquipmentType=EquipmentType.Fan, SysName="Fan_14_9_5", DisplayName="вентилятор 14.9.5" }
                ,new Equipment() { Id = 218, EquipmentType=EquipmentType.Fan, SysName="Fan_14_9_6", DisplayName="вентилятор 14.9.6" }
                ,new Equipment() { Id = 219, EquipmentType=EquipmentType.Fan, SysName="Fan_2_12_5", DisplayName="вентилятор 2.12.5" }
                ,new Equipment() { Id = 220, EquipmentType=EquipmentType.Fan, SysName="Fan_2_18_1", DisplayName="вентилятор 2.18.1" }
                ,new Equipment() { Id = 221, EquipmentType=EquipmentType.Fan, SysName="Fan_2_9_7", DisplayName="вентилятор 2.9.7" }
                ,new Equipment() { Id = 222, EquipmentType=EquipmentType.Fan, SysName="Fan_3_5_4", DisplayName="вентилятор 3.5.4" }
                ,new Equipment() { Id = 223, EquipmentType=EquipmentType.Fan, SysName="Fan_3_6_4", DisplayName="вентилятор 3.6.4" }
                ,new Equipment() { Id = 224, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_3_8", DisplayName="шлюзовой затвор 1.3.8" }
                ,new Equipment() { Id = 225, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_3_9", DisplayName="шлюзовой затвор 1.3.9" }
                ,new Equipment() { Id = 226, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_3_10", DisplayName="шлюзовой затвор 1.3.10" }
                ,new Equipment() { Id = 227, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_3_11", DisplayName="шлюзовой затвор 1.3.11" }
                ,new Equipment() { Id = 228, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_3_12", DisplayName="шлюзовой затвор 1.3.12" }
                ,new Equipment() { Id = 229, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_4_8", DisplayName="шлюзовой затвор 1.4.8" }
                ,new Equipment() { Id = 230, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_4_9", DisplayName="шлюзовой затвор 1.4.9" }
                ,new Equipment() { Id = 231, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_4_10", DisplayName="шлюзовой затвор 1.4.10" }
                ,new Equipment() { Id = 232, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_4_11", DisplayName="шлюзовой затвор 1.4.11" }
                ,new Equipment() { Id = 233, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_4_12", DisplayName="шлюзовой затвор 1.4.12" }
                ,new Equipment() { Id = 234, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_7_8", DisplayName="шлюзовой затвор 2.7.8" }
                ,new Equipment() { Id = 235, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_7_9", DisplayName="шлюзовой затвор 2.7.9" }
                ,new Equipment() { Id = 236, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_7_10", DisplayName="шлюзовой затвор 2.7.10" }
                ,new Equipment() { Id = 237, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_10_6", DisplayName="шлюзовой затвор 2.10.6" }
                ,new Equipment() { Id = 238, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_10_7", DisplayName="шлюзовой затвор 2.10.7" }
                ,new Equipment() { Id = 239, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_10_8", DisplayName="шлюзовой затвор 2.10.8" }
                ,new Equipment() { Id = 240, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_13_7", DisplayName="шлюзовой затвор 2.13.7" }
                ,new Equipment() { Id = 241, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_16_2", DisplayName="шлюзовой затвор 2.16.2" }
                ,new Equipment() { Id = 242, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_8_8", DisplayName="шлюзовой затвор 2.8.8" }
                ,new Equipment() { Id = 243, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_8_9", DisplayName="шлюзовой затвор 2.8.9" }
                ,new Equipment() { Id = 244, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_8_10", DisplayName="шлюзовой затвор 2.8.10" }
                ,new Equipment() { Id = 245, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_11_6", DisplayName="шлюзовой затвор 2.11.6" }
                ,new Equipment() { Id = 246, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_11_7", DisplayName="шлюзовой затвор 2.11.7" }
                ,new Equipment() { Id = 247, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_11_8", DisplayName="шлюзовой затвор 2.11.8" }
                ,new Equipment() { Id = 248, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_17_2", DisplayName="шлюзовой затвор 2.17.2" }
                ,new Equipment() { Id = 249, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_2_10", DisplayName="шлюзовой затвор 1.2.10" }
                ,new Equipment() { Id = 250, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_2_11", DisplayName="шлюзовой затвор 1.2.11" }
                ,new Equipment() { Id = 251, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_2_12", DisplayName="шлюзовой затвор 1.2.12" }
                ,new Equipment() { Id = 252, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_2_8", DisplayName="шлюзовой затвор 1.2.8" }
                ,new Equipment() { Id = 253, EquipmentType=EquipmentType.Airlock, SysName="Shl_1_2_9", DisplayName="шлюзовой затвор 1.2.9" }
                ,new Equipment() { Id = 254, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_12_6", DisplayName="шлюзовой затвор 2.12.6" }
                ,new Equipment() { Id = 255, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_12_7", DisplayName="шлюзовой затвор 2.12.7" }
                ,new Equipment() { Id = 256, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_12_8", DisplayName="шлюзовой затвор 2.12.8" }
                ,new Equipment() { Id = 257, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_18_2", DisplayName="шлюзовой затвор 2.18.2" }
                ,new Equipment() { Id = 258, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_9_10", DisplayName="шлюзовой затвор 2.9.10" }
                ,new Equipment() { Id = 259, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_9_8", DisplayName="шлюзовой затвор 2.9.8" }
                ,new Equipment() { Id = 260, EquipmentType=EquipmentType.Airlock, SysName="Shl_2_9_9", DisplayName="шлюзовой затвор 2.9.9" }
                ,new Equipment() { Id = 261, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_1_5_9_a", DisplayName="виброплита 1.5.9.a" }
                ,new Equipment() { Id = 262, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_1_5_10_a", DisplayName="виброплита 1.5.10.a" }
                ,new Equipment() { Id = 263, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_1_5_11", DisplayName="виброплита 1.5.11" }
                ,new Equipment() { Id = 264, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_1_5_12", DisplayName="виброплита 1.5.12" }
                ,new Equipment() { Id = 265, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_2_19_19", DisplayName="виброплита 2.19.19" }
                ,new Equipment() { Id = 266, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_2_19_20", DisplayName="виброплита 2.19.20" }
                ,new Equipment() { Id = 267, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_2_19_21", DisplayName="виброплита 2.19.21" }
                ,new Equipment() { Id = 268, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_2_19_22", DisplayName="виброплита 2.19.22" }
                ,new Equipment() { Id = 269, EquipmentType=EquipmentType.Scalpel, SysName="Dev_2_7_6", DisplayName="скальператор 2.7.6" }
                ,new Equipment() { Id = 270, EquipmentType=EquipmentType.Scalpel, SysName="Dev_2_8_6", DisplayName="скальператор 2.8.6" }
                ,new Equipment() { Id = 271, EquipmentType=EquipmentType.Separator, SysName="Dev_2_10_4_M1", DisplayName="сепаратор 2.10.4.M1" }
                ,new Equipment() { Id = 272, EquipmentType=EquipmentType.Separator, SysName="Dev_2_10_4_M2", DisplayName="сепаратор 2.10.4.M2" }
                ,new Equipment() { Id = 273, EquipmentType=EquipmentType.Separator, SysName="Dev_2_11_4_M1", DisplayName="сепаратор 2.11.4.M1" }
                ,new Equipment() { Id = 274, EquipmentType=EquipmentType.Separator, SysName="Dev_2_11_4_M2", DisplayName="сепаратор 2.11.4.M2" }
                ,new Equipment() { Id = 275, EquipmentType=EquipmentType.CarUnloader, SysName="Dev_1_1_1_a", DisplayName="разгрузчик автомобилей 1.1.1.a" }
                ,new Equipment() { Id = 276, EquipmentType=EquipmentType.CarUnloader, SysName="Dev_1_1_1_b", DisplayName="разгрузчик автомобилей 1.1.1.b" }
                ,new Equipment() { Id = 277, EquipmentType=EquipmentType.Screw, SysName="Shn_14_1_2", DisplayName="шнек 14.1.2" }
                ,new Equipment() { Id = 278, EquipmentType=EquipmentType.Screw, SysName="Shn_14_2_2", DisplayName="шнек 14.2.2" }
                ,new Equipment() { Id = 279, EquipmentType=EquipmentType.Screw, SysName="Shn_14_3_2", DisplayName="шнек 14.3.2" }
                ,new Equipment() { Id = 280, EquipmentType=EquipmentType.Screw, SysName="Shn_14_5_2", DisplayName="шнек 14.5.2" }
                ,new Equipment() { Id = 281, EquipmentType=EquipmentType.Screw, SysName="Shn_14_6_2", DisplayName="шнек 14.6.2" }
                ,new Equipment() { Id = 282, EquipmentType=EquipmentType.Screw, SysName="Shn_14_7_2", DisplayName="шнек 14.7.2" }
                ,new Equipment() { Id = 283, EquipmentType=EquipmentType.CarUnloader, SysName="Dev_1_1_1", DisplayName="разгрузчик автомобилей 1.1.1" }
                ,new Equipment() { Id = 284, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_1_5_9", DisplayName="виброплита 1.5.9" }
                ,new Equipment() { Id = 285, EquipmentType=EquipmentType.Screw, SysName="Shn_14_10_2", DisplayName="шнек 14.10.2" }
                ,new Equipment() { Id = 286, EquipmentType=EquipmentType.Screw, SysName="Shn_14_11_2", DisplayName="шнек 14.11.2" }
                ,new Equipment() { Id = 287, EquipmentType=EquipmentType.Screw, SysName="Shn_14_12_2", DisplayName="шнек 14.12.2" }
                ,new Equipment() { Id = 288, EquipmentType=EquipmentType.Screw, SysName="Shn_14_4_2", DisplayName="шнек 14.4.2" }
                ,new Equipment() { Id = 289, EquipmentType=EquipmentType.Screw, SysName="Shn_14_8_2", DisplayName="шнек 14.8.2" }
                ,new Equipment() { Id = 290, EquipmentType=EquipmentType.Screw, SysName="Shn_14_9_2", DisplayName="шнек 14.9.2" }
                ,new Equipment() { Id = 291, EquipmentType=EquipmentType.Separator, SysName="Dev_2_12_4_M1", DisplayName="сепаратор 2.12.4.M1" }
                ,new Equipment() { Id = 292, EquipmentType=EquipmentType.Separator, SysName="Dev_2_12_4_M2", DisplayName="сепаратор 2.12.4.M2" }
                ,new Equipment() { Id = 293, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_2_19_23", DisplayName="виброплита 2.19.23" }
                ,new Equipment() { Id = 294, EquipmentType=EquipmentType.VibratingPlate, SysName="Dev_2_19_24", DisplayName="виброплита 2.19.24" }
                ,new Equipment() { Id = 295, EquipmentType=EquipmentType.Scalpel, SysName="Dev_2_9_6", DisplayName="скальператор 2.9.6" }
                ,new Equipment() { Id = 296, EquipmentType=EquipmentType.Conveyor, SysName="Cnv_14_15_4", DisplayName="конвейер 14.15.4" }
                ,new Equipment() { Id = 320, EquipmentType=EquipmentType.HopperScales, SysName="Dev_2_13_2", DisplayName="весы бункерные 2.13.2" }
                ,new Equipment() { Id = 321, EquipmentType=EquipmentType.HopperScales, SysName="Dev_2_14_2", DisplayName="весы бункерные 2.14.2" }
                ,new Equipment() { Id = 322, EquipmentType=EquipmentType.HopperScales, SysName="Dev_2_15_2", DisplayName="весы бункерные 2.15.2" }
                ,new Equipment() { Id = 323, EquipmentType=EquipmentType.GrainDryer, SysName="Dev_6_1_1", DisplayName="зерносушилка 6.1.1" }
                ,new Equipment() { Id = 324, EquipmentType=EquipmentType.GrainDryer, SysName="Dev_6_2_1", DisplayName="зерносушилка 6.2.1" }
                ,new Equipment() { Id = 325, EquipmentType=EquipmentType.GrainDryer, SysName="Dev_6_3_1", DisplayName="зерносушилка 6.3.1" }
                ,new Equipment() { Id = 329, EquipmentType=EquipmentType.Siren, SysName="ALR", DisplayName="сирена" }
                ,new Equipment() { Id = 330, EquipmentType=EquipmentType.GateValve, SysName="Vlv_1_5_5_a", DisplayName="задвижка 1.5.5.a" }
                ,new Equipment() { Id = 331, EquipmentType=EquipmentType.GateValve, SysName="Vlv_1_5_6_a", DisplayName="задвижка 1.5.6.a" }
                ,new Equipment() { Id = 332, EquipmentType=EquipmentType.GateValve, SysName="Vlv_1_5_7", DisplayName="задвижка 1.5.7" }
                ,new Equipment() { Id = 333, EquipmentType=EquipmentType.GateValve, SysName="Vlv_1_5_8", DisplayName="задвижка 1.5.8" }
                ,new Equipment() { Id = 334, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_7_3", DisplayName="задвижка 2.7.3" }
                ,new Equipment() { Id = 335, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_7", DisplayName="задвижка 2.19.7" }
                ,new Equipment() { Id = 336, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_8", DisplayName="задвижка 2.19.8" }
                ,new Equipment() { Id = 337, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_9", DisplayName="задвижка 2.19.9" }
                ,new Equipment() { Id = 338, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_10", DisplayName="задвижка 2.19.10" }
                ,new Equipment() { Id = 339, EquipmentType=EquipmentType.GateValve, SysName="Vlv_3_1_2", DisplayName="задвижка 3.1.2" }
                ,new Equipment() { Id = 340, EquipmentType=EquipmentType.GateValve, SysName="Vlv_3_2_2", DisplayName="задвижка 3.2.2" }
                ,new Equipment() { Id = 341, EquipmentType=EquipmentType.GateValve, SysName="Vlv_3_1_7", DisplayName="задвижка 3.1.7" }
                ,new Equipment() { Id = 342, EquipmentType=EquipmentType.GateValve, SysName="Vlv_4_1_2", DisplayName="задвижка 4.1.2" }
                ,new Equipment() { Id = 343, EquipmentType=EquipmentType.GateValve, SysName="Vlv_4_1_5", DisplayName="задвижка 4.1.5" }
                ,new Equipment() { Id = 344, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_8_3", DisplayName="задвижка 2.8.3" }
                ,new Equipment() { Id = 345, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_11", DisplayName="задвижка 2.19.11" }
                ,new Equipment() { Id = 346, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_12", DisplayName="задвижка 2.19.12" }
                ,new Equipment() { Id = 347, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_13", DisplayName="задвижка 2.19.13" }
                ,new Equipment() { Id = 348, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_14", DisplayName="задвижка 2.19.14" }
                ,new Equipment() { Id = 349, EquipmentType=EquipmentType.GateValve, SysName="Vlv_3_3_2", DisplayName="задвижка 3.3.2" }
                ,new Equipment() { Id = 350, EquipmentType=EquipmentType.GateValve, SysName="Vlv_3_4_2", DisplayName="задвижка 3.4.2" }
                ,new Equipment() { Id = 351, EquipmentType=EquipmentType.GateValve, SysName="Vlv_3_3_6", DisplayName="задвижка 3.3.6" }
                ,new Equipment() { Id = 352, EquipmentType=EquipmentType.GateValve, SysName="Vlv_4_2_2", DisplayName="задвижка 4.2.2" }
                ,new Equipment() { Id = 353, EquipmentType=EquipmentType.GateValve, SysName="Vlv_4_2_5", DisplayName="задвижка 4.2.5" }
                ,new Equipment() { Id = 354, EquipmentType=EquipmentType.GateValve, SysName="Vlv_6_1_2", DisplayName="задвижка 6.1.2" }
                ,new Equipment() { Id = 355, EquipmentType=EquipmentType.GateValve, SysName="Vlv_6_2_2", DisplayName="задвижка 6.2.2" }
                ,new Equipment() { Id = 356, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_13_3", DisplayName="задвижка 14.13.3" }
                ,new Equipment() { Id = 357, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_13_5", DisplayName="задвижка 14.13.5" }
                ,new Equipment() { Id = 358, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_16_15", DisplayName="задвижка 14.16.15" }
                ,new Equipment() { Id = 359, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_16_28", DisplayName="задвижка 14.16.28" }
                ,new Equipment() { Id = 360, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_16_41", DisplayName="задвижка 14.16.41" }
                ,new Equipment() { Id = 361, EquipmentType=EquipmentType.GateValve, SysName="Vlv_15_1_2", DisplayName="задвижка 15.1.2" }
                ,new Equipment() { Id = 362, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_14_3", DisplayName="задвижка 14.14.3" }
                ,new Equipment() { Id = 363, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_17_15", DisplayName="задвижка 14.17.15" }
                ,new Equipment() { Id = 364, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_17_28", DisplayName="задвижка 14.17.28" }
                ,new Equipment() { Id = 365, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_17_41", DisplayName="задвижка 14.17.41" }
                ,new Equipment() { Id = 367, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_10_3", DisplayName="задвижка 2.10.3" }
                ,new Equipment() { Id = 368, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_11_3", DisplayName="задвижка 2.11.3" }
                ,new Equipment() { Id = 369, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_14_5", DisplayName="задвижка 14.14.5" }
                ,new Equipment() { Id = 371, EquipmentType=EquipmentType.GateValve, SysName="Vlv_1_5_5", DisplayName="задвижка 1.5.5" }
                ,new Equipment() { Id = 372, EquipmentType=EquipmentType.GateValve, SysName="Vlv_1_5_6", DisplayName="задвижка 1.5.6" }
                ,new Equipment() { Id = 373, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_13_7", DisplayName="задвижка 14.13.7" }
                ,new Equipment() { Id = 374, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_14_7", DisplayName="задвижка 14.14.7" }
                ,new Equipment() { Id = 375, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_15_3", DisplayName="задвижка 14.15.3" }
                ,new Equipment() { Id = 376, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_15_5", DisplayName="задвижка 14.15.5" }
                ,new Equipment() { Id = 377, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_15_7", DisplayName="задвижка 14.15.7" }
                ,new Equipment() { Id = 378, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_16_2", DisplayName="задвижка 14.16.2" }
                ,new Equipment() { Id = 379, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_17_2", DisplayName="задвижка 14.17.2" }
                ,new Equipment() { Id = 380, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_18_15", DisplayName="задвижка 14.18.15" }
                ,new Equipment() { Id = 381, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_18_2", DisplayName="задвижка 14.18.2" }
                ,new Equipment() { Id = 382, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_18_28", DisplayName="задвижка 14.18.28" }
                ,new Equipment() { Id = 383, EquipmentType=EquipmentType.GateValve, SysName="Vlv_14_18_41", DisplayName="задвижка 14.18.41" }
                ,new Equipment() { Id = 384, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_15_3", DisplayName="задвижка 2.15.3" }
                ,new Equipment() { Id = 385, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_15", DisplayName="задвижка 2.19.15" }
                ,new Equipment() { Id = 386, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_16", DisplayName="задвижка 2.19.16" }
                ,new Equipment() { Id = 387, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_17", DisplayName="задвижка 2.19.17" }
                ,new Equipment() { Id = 388, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_19_18", DisplayName="задвижка 2.19.18" }
                ,new Equipment() { Id = 389, EquipmentType=EquipmentType.GateValve, SysName="Vlv_2_9_3", DisplayName="задвижка 2.9.3" }
                ,new Equipment() { Id = 390, EquipmentType=EquipmentType.GateValve, SysName="Vlv_3_5_2", DisplayName="задвижка 3.5.2" }
                ,new Equipment() { Id = 391, EquipmentType=EquipmentType.GateValve, SysName="Vlv_3_5_7", DisplayName="задвижка 3.5.7" }
                ,new Equipment() { Id = 392, EquipmentType=EquipmentType.GateValve, SysName="Vlv_3_6_2", DisplayName="задвижка 3.6.2" }
                ,new Equipment() { Id = 393, EquipmentType=EquipmentType.GateValve, SysName="Vlv_4_3_2", DisplayName="задвижка 4.3.2" }
                ,new Equipment() { Id = 394, EquipmentType=EquipmentType.GateValve, SysName="Vlv_4_3_5", DisplayName="задвижка 4.3.5" }
                ,new Equipment() { Id = 395, EquipmentType=EquipmentType.GateValve, SysName="Vlv_6_3_2", DisplayName="задвижка 6.3.2" }
                ,new Equipment() { Id = 396, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_1_10", DisplayName="клапан двухходовой 2.1.10" }
                ,new Equipment() { Id = 398, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_10_1", DisplayName="клапан двухходовой 2.10.1" }
                ,new Equipment() { Id = 399, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_13_1", DisplayName="клапан двухходовой 2.13.1" }
                ,new Equipment() { Id = 400, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_13_4", DisplayName="клапан двухходовой 2.13.4" }
                ,new Equipment() { Id = 401, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_13_5", DisplayName="клапан двухходовой 2.13.5" }
                ,new Equipment() { Id = 402, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_10", DisplayName="клапан двухходовой 2.2.10" }
                ,new Equipment() { Id = 403, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_11_1", DisplayName="клапан двухходовой 2.11.1" }
                ,new Equipment() { Id = 404, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_14_1", DisplayName="клапан двухходовой 2.14.1" }
                ,new Equipment() { Id = 405, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_14_4", DisplayName="клапан двухходовой 2.14.4" }
                ,new Equipment() { Id = 406, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_14_5", DisplayName="клапан двухходовой 2.14.5" }
                ,new Equipment() { Id = 407, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_11", DisplayName="клапан двухходовой 2.2.11" }
                ,new Equipment() { Id = 408, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_12", DisplayName="клапан двухходовой 2.2.12" }
                ,new Equipment() { Id = 409, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_5_1_3", DisplayName="клапан двухходовой 5.1.3" }
                ,new Equipment() { Id = 410, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_6_1_5", DisplayName="клапан двухходовой 6.1.5" }
                ,new Equipment() { Id = 411, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_6_2_5", DisplayName="клапан двухходовой 6.2.5" }
                ,new Equipment() { Id = 412, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_7_1_4", DisplayName="клапан двухходовой 7.1.4" }
                ,new Equipment() { Id = 413, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_9_1_4", DisplayName="клапан двухходовой 9.1.4" }
                ,new Equipment() { Id = 414, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_9_1_5", DisplayName="клапан двухходовой 9.1.5" }
                ,new Equipment() { Id = 415, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_9_1_6", DisplayName="клапан двухходовой 9.1.6" }
                ,new Equipment() { Id = 416, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_13_1_6", DisplayName="клапан двухходовой 13.1.6" }
                ,new Equipment() { Id = 417, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_14_16_53", DisplayName="клапан двухходовой 14.16.53" }
                ,new Equipment() { Id = 418, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_14_17_53", DisplayName="клапан двухходовой 14.17.53" }
                ,new Equipment() { Id = 419, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_13_1_2", DisplayName="клапан двухходовой 13.1.2" }
                ,new Equipment() { Id = 420, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_13_1_3", DisplayName="клапан двухходовой 13.1.3" }
                ,new Equipment() { Id = 421, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_14_18_53", DisplayName="клапан двухходовой 14.18.53" }
                ,new Equipment() { Id = 422, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_12_1", DisplayName="клапан двухходовой 2.12.1" }
                ,new Equipment() { Id = 423, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_15_1", DisplayName="клапан двухходовой 2.15.1" }
                ,new Equipment() { Id = 424, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_15_4", DisplayName="клапан двухходовой 2.15.4" }
                ,new Equipment() { Id = 425, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_15_5", DisplayName="клапан двухходовой 2.15.5" }
                ,new Equipment() { Id = 426, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_3_10", DisplayName="клапан двухходовой 2.3.10" }
                ,new Equipment() { Id = 427, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_5_1_5", DisplayName="клапан двухходовой 5.1.5" }
                ,new Equipment() { Id = 428, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_5_2_3", DisplayName="клапан двухходовой 5.2.3" }
                ,new Equipment() { Id = 429, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_6_3_5", DisplayName="клапан двухходовой 6.3.5" }
                ,new Equipment() { Id = 430, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_3_6_a", DisplayName="клапан двухходовой 2.3.6.a" }
                ,new Equipment() { Id = 431, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_3_6_b", DisplayName="клапан двухходовой 2.3.6.b" }
                ,new Equipment() { Id = 432, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_3_7_a", DisplayName="клапан двухходовой 2.3.7.a" }
                ,new Equipment() { Id = 433, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_3_7_b", DisplayName="клапан двухходовой 2.3.7.b" }
                ,new Equipment() { Id = 434, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_3_8_a", DisplayName="клапан двухходовой 2.3.8.a" }
                ,new Equipment() { Id = 435, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_3_8_b", DisplayName="клапан двухходовой 2.3.8.b" }
                ,new Equipment() { Id = 436, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_3_9_a", DisplayName="клапан двухходовой 2.3.9.a" }
                ,new Equipment() { Id = 437, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_3_9_b", DisplayName="клапан двухходовой 2.3.9.b" }
                ,new Equipment() { Id = 438, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_1_6_a", DisplayName="клапан двухходовой 2.1.6.a" }
                ,new Equipment() { Id = 439, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_1_6_b", DisplayName="клапан двухходовой 2.1.6.b" }
                ,new Equipment() { Id = 440, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_1_7_a", DisplayName="клапан двухходовой 2.1.7.a" }
                ,new Equipment() { Id = 441, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_1_7_b", DisplayName="клапан двухходовой 2.1.7.b" }
                ,new Equipment() { Id = 442, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_1_8_a", DisplayName="клапан двухходовой 2.1.8.a" }
                ,new Equipment() { Id = 443, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_1_8_b", DisplayName="клапан двухходовой 2.1.8.b" }
                ,new Equipment() { Id = 444, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_1_9_a", DisplayName="клапан двухходовой 2.1.9.a" }
                ,new Equipment() { Id = 445, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_1_9_b", DisplayName="клапан двухходовой 2.1.9.b" }
                ,new Equipment() { Id = 446, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_6_a", DisplayName="клапан двухходовой 2.2.6.a" }
                ,new Equipment() { Id = 447, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_6_b", DisplayName="клапан двухходовой 2.2.6.b" }
                ,new Equipment() { Id = 448, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_7_a", DisplayName="клапан двухходовой 2.2.7.a" }
                ,new Equipment() { Id = 449, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_7_b", DisplayName="клапан двухходовой 2.2.7.b" }
                ,new Equipment() { Id = 450, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_8_a", DisplayName="клапан двухходовой 2.2.8.a" }
                ,new Equipment() { Id = 451, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_8_b", DisplayName="клапан двухходовой 2.2.8.b" }
                ,new Equipment() { Id = 452, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_9_a", DisplayName="клапан двухходовой 2.2.9.a" }
                ,new Equipment() { Id = 453, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_2_2_9_b", DisplayName="клапан двухходовой 2.2.9.b" }
                ,new Equipment() { Id = 454, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_8_1_2_a", DisplayName="клапан двухходовой 8.1.2.a" }
                ,new Equipment() { Id = 455, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_8_1_2_b", DisplayName="клапан двухходовой 8.1.2.b" }
                ,new Equipment() { Id = 456, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_8_1_4_a", DisplayName="клапан двухходовой 8.1.4.a" }
                ,new Equipment() { Id = 457, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_8_1_4_b", DisplayName="клапан двухходовой 8.1.4.b" }
                ,new Equipment() { Id = 458, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_9_1_3_a", DisplayName="клапан двухходовой 9.1.3.a" }
                ,new Equipment() { Id = 459, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_9_1_3_b", DisplayName="клапан двухходовой 9.1.3.b" }
                ,new Equipment() { Id = 460, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_13_1_5_a", DisplayName="клапан двухходовой 13.1.5.a" }
                ,new Equipment() { Id = 461, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_13_1_5_b", DisplayName="клапан двухходовой 13.1.5.b" }
                ,new Equipment() { Id = 462, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_10_1_3_a", DisplayName="клапан двухходовой 10.1.3.a" }
                ,new Equipment() { Id = 463, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_10_1_3_b", DisplayName="клапан двухходовой 10.1.3.b" }
                ,new Equipment() { Id = 464, EquipmentType=EquipmentType.GateValve, SysName="Vlv_15_1_2_a", DisplayName="задвижка 15.1.2.a" }
                ,new Equipment() { Id = 465, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_15_1_4", DisplayName="клапан двухходовой 15.1.4" }
                ,new Equipment() { Id = 466, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_15_1_5", DisplayName="клапан двухходовой 15.1.5" }
                ,new Equipment() { Id = 467, EquipmentType=EquipmentType.TwoWayValve, SysName="Clp_15_1_6", DisplayName="клапан двухходовой 15.1.6" }
                ,new Equipment() { Id = 480, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_1_6", DisplayName="клапан трехходовой 2.1.6" }
                ,new Equipment() { Id = 481, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_1_7", DisplayName="клапан трехходовой 2.1.7" }
                ,new Equipment() { Id = 482, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_1_8", DisplayName="клапан трехходовой 2.1.8" }
                ,new Equipment() { Id = 483, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_1_9", DisplayName="клапан трехходовой 2.1.9" }
                ,new Equipment() { Id = 484, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_2_6", DisplayName="клапан трехходовой 2.2.6" }
                ,new Equipment() { Id = 485, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_2_7", DisplayName="клапан трехходовой 2.2.7" }
                ,new Equipment() { Id = 486, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_2_8", DisplayName="клапан трехходовой 2.2.8" }
                ,new Equipment() { Id = 487, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_2_9", DisplayName="клапан трехходовой 2.2.9" }
                ,new Equipment() { Id = 488, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_8_1_2", DisplayName="клапан трехходовой 8.1.2" }
                ,new Equipment() { Id = 489, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_8_1_4", DisplayName="клапан трехходовой 8.1.4" }
                ,new Equipment() { Id = 490, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_9_1_3", DisplayName="клапан трехходовой 9.1.3" }
                ,new Equipment() { Id = 491, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_13_1_5", DisplayName="клапан трехходовой 13.1.5" }
                ,new Equipment() { Id = 492, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_10_1_3", DisplayName="клапан трехходовой 10.1.3" }
                ,new Equipment() { Id = 493, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_3_6", DisplayName="клапан трехходовой 2.3.6" }
                ,new Equipment() { Id = 494, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_3_7", DisplayName="клапан трехходовой 2.3.7" }
                ,new Equipment() { Id = 495, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_3_8", DisplayName="клапан трехходовой 2.3.8" }
                ,new Equipment() { Id = 496, EquipmentType=EquipmentType.ThreeWayValve, SysName="Clp_4PS_2_3_9", DisplayName="клапан трехходовой 2.3.9" }
                ,new Equipment() { Id = 500, EquipmentType=EquipmentType.Bunker, SysName="Bnk_1_5_1_a", DisplayName="бункер 1.5.1.a" }
                ,new Equipment() { Id = 501, EquipmentType=EquipmentType.Bunker, SysName="Bnk_1_5_2_a", DisplayName="бункер 1.5.2.a" }
                ,new Equipment() { Id = 502, EquipmentType=EquipmentType.Bunker, SysName="Bnk_1_5_3", DisplayName="бункер 1.5.3" }
                ,new Equipment() { Id = 503, EquipmentType=EquipmentType.Bunker, SysName="Bnk_1_5_4", DisplayName="бункер 1.5.4" }
                ,new Equipment() { Id = 504, EquipmentType=EquipmentType.Bunker, SysName="Bnk_1_5_1", DisplayName="бункер 1.5.1" }
                ,new Equipment() { Id = 505, EquipmentType=EquipmentType.Bunker, SysName="Bnk_1_5_2", DisplayName="бункер 1.5.2" }
                ,new Equipment() { Id = 506, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_7_2", DisplayName="бункер 2.7.2" }
                ,new Equipment() { Id = 507, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_8_2", DisplayName="бункер 2.8.2" }
                ,new Equipment() { Id = 508, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_9_2", DisplayName="бункер 2.9.2" }
                ,new Equipment() { Id = 509, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_10_2", DisplayName="бункер 2.10.2" }
                ,new Equipment() { Id = 510, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_11_2", DisplayName="бункер 2.11.2" }
                ,new Equipment() { Id = 511, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_12_2", DisplayName="бункер 2.12.2" }
                ,new Equipment() { Id = 512, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_19_1", DisplayName="бункер 2.19.1" }
                ,new Equipment() { Id = 513, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_19_2", DisplayName="бункер 2.19.2" }
                ,new Equipment() { Id = 514, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_19_3", DisplayName="бункер 2.19.3" }
                ,new Equipment() { Id = 515, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_19_4", DisplayName="бункер 2.19.4" }
                ,new Equipment() { Id = 516, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_19_5", DisplayName="бункер 2.19.5" }
                ,new Equipment() { Id = 517, EquipmentType=EquipmentType.Bunker, SysName="Bnk_2_19_6", DisplayName="бункер 2.19.6" }
                ,new Equipment() { Id = 518, EquipmentType=EquipmentType.Bunker, SysName="Bnk_3_1_1", DisplayName="бункер 3.1.1" }
                ,new Equipment() { Id = 519, EquipmentType=EquipmentType.Bunker, SysName="Bnk_3_2_1", DisplayName="бункер 3.2.1" }
                ,new Equipment() { Id = 520, EquipmentType=EquipmentType.Bunker, SysName="Bnk_3_3_1", DisplayName="бункер 3.3.1" }
                ,new Equipment() { Id = 521, EquipmentType=EquipmentType.Bunker, SysName="Bnk_3_4_1", DisplayName="бункер 3.4.1" }
                ,new Equipment() { Id = 522, EquipmentType=EquipmentType.Bunker, SysName="Bnk_3_5_1", DisplayName="бункер 3.5.1" }
                ,new Equipment() { Id = 523, EquipmentType=EquipmentType.Bunker, SysName="Bnk_3_6_1", DisplayName="бункер 3.6.1" }
                ,new Equipment() { Id = 524, EquipmentType=EquipmentType.Bunker, SysName="Bnk_4_1_1", DisplayName="бункер 4.1.1" }
                ,new Equipment() { Id = 525, EquipmentType=EquipmentType.Bunker, SysName="Bnk_4_2_1", DisplayName="бункер 4.2.1" }
                ,new Equipment() { Id = 526, EquipmentType=EquipmentType.Bunker, SysName="Bnk_4_3_1", DisplayName="бункер 4.3.1" }
                ,new Equipment() { Id = 527, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_1_1", DisplayName="бункер 14.1.1" }
                ,new Equipment() { Id = 528, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_2_1", DisplayName="бункер 14.2.1" }
                ,new Equipment() { Id = 529, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_3_1", DisplayName="бункер 14.3.1" }
                ,new Equipment() { Id = 530, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_4_1", DisplayName="бункер 14.4.1" }
                ,new Equipment() { Id = 531, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_5_1", DisplayName="бункер 14.5.1" }
                ,new Equipment() { Id = 532, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_6_1", DisplayName="бункер 14.6.1" }
                ,new Equipment() { Id = 533, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_7_1", DisplayName="бункер 14.7.1" }
                ,new Equipment() { Id = 534, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_8_1", DisplayName="бункер 14.8.1" }
                ,new Equipment() { Id = 535, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_9_1", DisplayName="бункер 14.9.1" }
                ,new Equipment() { Id = 536, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_10_1", DisplayName="бункер 14.10.1" }
                ,new Equipment() { Id = 537, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_11_1", DisplayName="бункер 14.11.1" }
                ,new Equipment() { Id = 538, EquipmentType=EquipmentType.Bunker, SysName="Bnk_14_12_1", DisplayName="бункер 14.12.1" }
                ,new Equipment() { Id = 539, EquipmentType=EquipmentType.Bunker, SysName="Bnk_15_1_1", DisplayName="бункер 15.1.1" }
                ,new Equipment() { Id = 540, EquipmentType=EquipmentType.Bunker, SysName="Bnk_15_1_1_a", DisplayName="бункер 15.1.1.a" }
                ,new Equipment() { Id = 541, EquipmentType=EquipmentType.Bunker, SysName="Bnk_KKZ", DisplayName="на ККЗ" }
            };
        }
    }
}
