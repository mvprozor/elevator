﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Data
{
    [Description("Журнал замеров")]
    public class UnitInventory : IBaseEntity
    {
        [NotMapped]
        public int Id { get; set; }

        [Key, Column(Order = 0)]
        public int UnitId { get; set; }
        public Unit Unit
        {
            get; set;
        }

        [Key, Column(Order = 1)]
        public DateTime CreatedAt { get; set; }
        
        public int NomenclatureId { get; set; }
        public Nomenclature Nomenclature { get; set; }

        public decimal Quantity { get; set; }

        [MaxLength(2000)]
        public string Comment { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }
    }
}
