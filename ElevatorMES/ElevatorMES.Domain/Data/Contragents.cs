﻿using ElevatorMES.Domain.Data.Base;
using System.Collections.Generic;

namespace ElevatorMES.Domain.Data
{
    public class Contragent : BaseSyncEntity
    {
        public static IEnumerable<Contragent> Seed()
        {
            return new List<Contragent>
            {
                new Contragent {Id = 1, Code1C = "1", DisplayName = "Контрагент 1"},
                new Contragent {Id = 2, Code1C = "2", DisplayName = "Контрагент 2"},
                new Contragent {Id = 3, Code1C = "3", DisplayName = "Контрагент 3"},
            };
        }
    }
}