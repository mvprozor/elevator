﻿using System.Collections.Generic;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// Номенклатура сырья и продукции
    /// </summary>
    public class Nomenclature : BaseSyncEntity
    {
        public ICollection<Lot> Lots { get; set; }
        public static IEnumerable<Nomenclature> Seed()
        {
            return new List<Nomenclature>()
            {
                new Nomenclature {Id = 1, Code1C = "11", DisplayName = "Пшеница 2019 1 сорт"},
                new Nomenclature {Id = 2, Code1C = "12", DisplayName = "Пшеница 2019 2 сорт"},
                new Nomenclature {Id = 3, Code1C = "20", DisplayName = "Ячмень 2019"},
                new Nomenclature {Id = 4, Code1C = "14", DisplayName = "Подсолнечник 2018"},
                new Nomenclature {Id = 5, Code1C = "15", DisplayName = "Подсолнечник 2019"},
            };

        }
    }
}