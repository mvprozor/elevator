﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Data
{
    [Description("приоритет приёмки сырья")]
    public class NomenclaturePriority : BaseEntity
    {
        /// <summary>
        /// идентификатор документа (1C)
        /// </summary>
        [MaxLength(36)]
        public string DocId { get; set; }
        public int NomenclatureId { get; set; }
        public Nomenclature Nomenclature { get; set; }
        /// <summary>
        /// Приоритет приёмки от 0 до 5
        /// </summary>
        public int Priority { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
    }
}
