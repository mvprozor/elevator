﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// Описывает силосы, точки выгрузки, времененые склады и.т.п.
    /// </summary>
    public class Unit : BaseSyncEntity
    {
        [Required]
        public UnitType Type { get; set; }

        [Required]
        public decimal Capacity { get; set; }

        public static IEnumerable<Unit> Seed()
        {
            return new List<Unit>
            {
                new Unit {Id = 1, Type = UnitType.Silos, DisplayName = "Силос №1.1", Code1C="111", Capacity = 1000m},
                new Unit {Id = 2, Type = UnitType.Silos, DisplayName = "Силос №1.2", Code1C="112", Capacity = 1000m},
                new Unit {Id = 3, Type = UnitType.Silos, DisplayName = "Силос №1.3", Code1C="113", Capacity = 1000m},
                new Unit {Id = 4, Type = UnitType.Silos, DisplayName = "Силос №1.4", Code1C="114", Capacity = 1000m},
                new Unit {Id = 5, Type = UnitType.Silos, DisplayName = "Силос №2.1", Code1C="121", Capacity = 1000m},
                new Unit {Id = 6, Type = UnitType.Silos, DisplayName = "Силос №2.2", Code1C="122", Capacity = 1000m},
                new Unit {Id = 7, Type = UnitType.Silos, DisplayName = "Силос №2.3", Code1C="123", Capacity = 1000m},
                new Unit {Id = 8, Type = UnitType.Silos, DisplayName = "Силос №2.4", Code1C="124", Capacity = 1000m},
                new Unit {Id = 9, Type = UnitType.Silos, DisplayName = "Силос №3.1", Code1C="131", Capacity = 1000m},
                new Unit {Id = 10, Type = UnitType.Silos, DisplayName = "Силос №3.2", Code1C="132", Capacity = 1000m},
                new Unit {Id = 11, Type = UnitType.Silos, DisplayName = "Силос №3.3", Code1C="133", Capacity = 1000m},
                new Unit {Id = 12, Type = UnitType.Silos, DisplayName = "Силос №3.4", Code1C="134", Capacity = 1000m},

                new Unit {Id = 13, Type = UnitType.UnloadingPoint, DisplayName = "Выгрузка №1", Code1C="201", Capacity = 0m},
                new Unit {Id = 14, Type = UnitType.UnloadingPoint, DisplayName = "Выгрузка №2", Code1C="202", Capacity = 0m},
                new Unit {Id = 15, Type = UnitType.UnloadingPoint, DisplayName = "Выгрузка №3", Code1C="203", Capacity = 0m},

                new Unit {Id = 16, Type = UnitType.LoadingPoint, DisplayName = "Бункер 15.1.1", Code1C="301", Capacity = 0m},
                new Unit {Id = 17, Type = UnitType.CompoundFeedPlant, DisplayName = "Комбикормовый завод", Code1C="302", Capacity = 0m},
                new Unit {Id = 18, Type = UnitType.LoadingPoint, DisplayName = "Бункер 15.1.1.а", Code1C="303", Capacity = 0m},

                new Unit {Id = 19, Type = UnitType.Bunker, DisplayName = "Бункер 4.1.1", Capacity = 100m},
                new Unit {Id = 20, Type = UnitType.Bunker, DisplayName = "Бункер 4.2.1", Capacity = 100m},
                new Unit {Id = 21, Type = UnitType.Bunker, DisplayName = "Бункер 4.3.1", Capacity = 100m},
                new Unit {Id = 22, Type = UnitType.Bunker, DisplayName = "Бункер 3.1.1", Capacity = 100m},
                new Unit {Id = 23, Type = UnitType.Bunker, DisplayName = "Бункер 3.2.1", Capacity = 100m},
                new Unit {Id = 24, Type = UnitType.Bunker, DisplayName = "Бункер 3.3.1", Capacity = 100m},
                new Unit {Id = 25, Type = UnitType.Bunker, DisplayName = "Бункер 3.4.1", Capacity = 100m},
                new Unit {Id = 26, Type = UnitType.Bunker, DisplayName = "Бункер 3.5.1", Capacity = 100m},
                new Unit {Id = 27, Type = UnitType.Bunker, DisplayName = "Бункер 3.6.1", Capacity = 100m},

                new Unit {Id = 28, Type = UnitType.Dryer, DisplayName = "Сушилка 6.1.1", Capacity = 100m},
                new Unit {Id = 29, Type = UnitType.Dryer, DisplayName = "Сушилка 6.2.1", Capacity = 100m},
                new Unit {Id = 30, Type = UnitType.Dryer, DisplayName = "Сушилка 6.3.1", Capacity = 100m}
            };
        }
    }
}
