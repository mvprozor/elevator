﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Data
{
    /// <summary>
    /// Перемещения при выполнении задания
    /// </summary>
    public class TaskExecMove : BaseEntity
    {
        public int TaskExecId { get; set; }
        public TaskExec TaskExec { get; set; }

        public int? LotId { get; set; }
        public Lot Lot { get; set; }

        [ForeignKey("SourceUnit")]
        public int? SourceUnitId { get; set; }
        public Unit SourceUnit { get; set; }

        [ForeignKey("SourceVehicle")]
        public int? SourceVehicleId { get; set; }
        public Vehicle SourceVehicle { get; set; }

        [ForeignKey("DestUnit")]
        public int? DestUnitId { get; set; }
        public Unit DestUnit { get; set; }

        [ForeignKey("DestVehicle")]
        public int? DestVehicleId { get; set; }
        public Vehicle DestVehicle { get; set; }

        /// <summary>
        /// Дата окончания перемещения
        /// </summary>
        public DateTime MovedAt { get; set; }

        public decimal Quantity { get; set; }
    }
}
