﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.Domain.Data
{
    [Description("Логи от 1С")]
    public class LogEvent1C : BaseEntity
    {
        /// <summary>
        /// идентификатор документа (1C)
        /// </summary>
        [MaxLength(36)]
        public string DocId { get; set; }
        public string EventType { get; set; }
        public string Message { get; set; }
        public DateTime DateMessage { get; set; }
    }
}
