﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.Domain.Data
{

    [Description("задание")]
    public class Task : BaseEntity
    {
        /// <summary>
        /// идентификатор документа (1C)
        /// </summary>
        [MaxLength(36)]
        public string DocId { get; set; }
        /// <summary>
        ///  тип задания
        /// </summary>
        public TaskType Type { get; set; }

#warning Должно быть NOT NULL - вызывает ошибку создания бд: "Introducing FOREIGN KEY constraint 'FK_dbo.TaskExecMoves_dbo.TaskExecs_TaskExecId' on table 'TaskExecMoves' may cause cycles or multiple cascade paths. Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints.Could not create constraint or index.See previous errors."
        public int? NomenclatureId { get; set; }
        public Nomenclature Nomenclature { get; set; }

        [ForeignKey("SourceLot")]
        public int? SourceLotId { get; set; }
        public Lot SourceLot { get; set; }

        [ForeignKey("SourceUnit")]
        public int? SourceUnitId { get; set; }
        public Unit SourceUnit { get; set; }

        [ForeignKey("DestLot")]
        public int? DestLotId { get; set; }
        public Lot DestLot { get; set; }

        [ForeignKey("DestUnit")]
        public int? DestUnitId { get; set; }
        public Unit DestUnit { get; set; }

        [ForeignKey("Organization")]
        public int? OrganizationId { get; set; }
        /// <summary>
        ///  откуда или кому (NULL для комбикормового завода и рукавов, перемещений)
        /// </summary>
        public Organization Organization { get; set; }

        [ForeignKey("Contragent")]
        public int? ContragentId { get; set; }
        public Contragent Contragent { get; set; }

        [ForeignKey("Vehicle")]
        public int? VehicleId { get; set; }
        public Vehicle Vehicle { get; set; }

        /// <summary>
        /// Сушка
        /// </summary>
        public bool IsDrying { get; set; } = false;
        /// <summary>
        /// Контроль качества
        /// </summary>
        public bool IsQualityControl { get; set; } = false;


        /// <summary>
        /// Сортировка
        /// </summary>
        // public bool IsSorting { get; set; } = false;

        /// <summary>
        /// Выполнить очистку скальператором
        /// </summary>
        public bool IsScalping { get; set; }

        /// <summary>
        /// Выполнить очистку сепаратором
        /// </summary>
        public bool IsSeparating { get; set; }

        /// <summary>
        /// требуемая дата исполнения
        /// </summary>
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        public DateTime? ExecuteAt { get; set; }
        public decimal Quantity { get; set; }
        public TaskStatus Status { get; set; } = TaskStatus.Queued;
        [MaxLength(2000)]
        public string Comment { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        /// <summary>
        /// дата передачи/приема в 1С, NULL - необходима синхронизации.
        /// Нужен условный индекс по условию IS NULL для ускорения определения какие надо синхронизировать.
        /// </summary>
        public DateTime? Sync1CAt { get; set; }

        /// <summary>
        /// Внешний склад
        /// </summary>
		[ForeignKey("ExternalUnit")]
		public int? ExternalUnitId { get; set; }
		public Unit ExternalUnit { get; set; }

		public override string ToString()
        {
            return $"{Id}. {DocId}. {Type.GetDescription()}, {Nomenclature}";
        }
    }
}
