﻿using System;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.Domain.PLC
{
    /// <summary>
    /// Аргументы события оповещения о получении статуса задания ПЛК
    /// </summary>
    public class PLCTaskStatusEventArgs : EventArgs
    {
        /// <summary>
        /// Идентификатор задания
        /// </summary>
        public int TaskId { get; }

        /// <summary>
        /// Статус задания в PLC
        /// </summary>
        public PLCTaskStatus Status { get; }

        /// <summary>
        /// Фактический вес по заданию
        /// </summary>
        public decimal Weight { get; }

        public PLCTaskStatusEventArgs(int taskId, PLCTaskStatus status, decimal weight)
        {
            TaskId = taskId;
            Status = status;
            Weight = weight;
        }

        public override string ToString()
        {
            return $"{TaskId}: {Status}, {Weight:0.0}т.";
        }
    }
}
