﻿using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.Domain.PLC
{
    /// <summary>
    /// Интерфейс обмена заданиями с ПЛК
    /// </summary>
    public interface IPLCTasks
    {
        /// <summary>
        /// Запуск задания
        /// </summary>
        /// <param name="taskId">Идентификатор задания в PLC</param>
        /// <param name="source">Источник в PLC</param>
        /// <param name="destination">Приеменик в PLC</param>
        /// <param name="operations">Операции в PLC</param>
        /// <param name="weight">Плановый вес</param>
        void StartTask(int taskId, int source, int destination, PLCTaskOperations operations, decimal weight);

        /// <summary>
        /// Останов задания
        /// </summary>
        /// <param name="taskId">Идентификатор задания в PLC</param>
        void StopTask(int taskId);

        /// <summary>
        /// Приостанов задания
        /// </summary>
        /// <param name="taskId">Идентификатор задания в PLC</param>
        void SuspendTask(int taskId);

        /// <summary>
        /// Продолжение задания
        /// </summary>
        /// <param name="taskId">Идентификатор задания в PLC</param>
        void ResumeTask(int taskId);

        /// <summary>
        /// Запросить статус задания
        /// </summary>
        /// <param name="taskId">Идентификатор задания в PLC</param>
        /// <remarks>
        /// После получения статуса будет вызвано событие <see cref="TaskStatusReceived"/> 
        /// </remarks>
        void GetTaskStatus(int taskId);

        /// <summary>
        /// Статус задания получен
        /// </summary>
        event EventHandler<PLCTaskStatusEventArgs> TaskStatusReceived;

        /// <summary>
        /// Задание от ПЛК
        /// </summary>
        event EventHandler<PLCNewTaskEventArgs> NewTaskReceived;

    }
}
