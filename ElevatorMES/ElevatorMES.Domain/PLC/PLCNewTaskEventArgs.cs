﻿using System;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.Domain.PLC
{
    /// <summary>
    /// Аргументы события оповещения о получении статуса задания ПЛК
    /// </summary>
    public class PLCNewTaskEventArgs : EventArgs
    {
        /// <summary>
        /// Идентификатор номенклатуры
        /// </summary>
        public string Nomenclature { get; }

        /// <summary>
        /// Плановый вес по заданию
        /// </summary>
        public decimal Quantity { get; }

        public PLCNewTaskEventArgs(string nomenclature, decimal weight)
        {
            Nomenclature = nomenclature;
            Quantity = weight;
        }

        public override string ToString()
        {
            return $"{Nomenclature}, {Quantity:0.0}т.";
        }
    }
}
