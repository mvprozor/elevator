﻿using ElevatorMES.WPF.Models;
using ElevatorMES.Domain.Data.Base;
using NLog;
using System;
using System.Windows;
using System.Windows.Controls;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Interaction logic for DictWindow.xaml
    /// </summary>
    public partial class DictWindow : Window
    {

        #region Закрытые поля

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private IDictionaryViewModel DictViewModel => (IDictionaryViewModel)DataContext;

        #endregion


        #region Конструкторы и иницилизация

        public DictWindow()
        {
            InitializeComponent();
            Loaded += WindowLoaded;
            Unloaded += WindowUnloaded;
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug($"Загружена форма редактирования элемента справочника \"{DictViewModel.DictName}\"");
        }

        private void WindowUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug($"Выгружена форма редактирования элемента справочника \"{DictViewModel.DictName}\"");
        }

        #endregion


        #region Обрыботка событий

        private void ButtonSaveClick(object sender, RoutedEventArgs e)
        {
            DictViewModel.UpdateItem();
        }

        private void ButtonCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion
    }
}
