﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.WPF.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Interaction logic for VehicleRegistrationWindow.xaml
    /// </summary>
    public partial class VehicleRegistrationWindow : Window
    {
        #region Открытые поля

        public VehicleViewModel VehicleView
        {
            get => DataContext as VehicleViewModel;
            set => DataContext = value;
        }

        public MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    VehicleView.IsDataRequestEnabled = IsLoaded && value != null;
                }
            }
        }

        public bool IsPerformed { get; private set; }

        #endregion


        #region Закрытые поля

        private MESRepository repository;

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        #region Конструкторы и иницилизация

        public VehicleRegistrationWindow()
        {
            VehicleView = new VehicleViewModel();
            VehicleView.VehicleTypes.GetDataTask = GetVehicleTypesTask;
            VehicleView.Nomenclatures.GetDataTask = GetNomenclaturesTask;
            VehicleView.VehicleRegNums.GetDataTask = GetVehicleRegNumsTask;

            VehicleView.GetDataTask = RegisterVehicle;
            VehicleView.Processed += VehicleProcessed;
            InitializeComponent();
            Loaded += WindowLoaded;
            Unloaded += WindowUnloaded;

        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Загружена форма регистрации транспорта");
            IsPerformed = false;
            if (Repository == null)
                logger.Info("Ожидание иницилизации репозитория MES API для формы регистрации транспорта...");
            else
                VehicleView.IsDataRequestEnabled = true;
        }

        private void WindowUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Выгружена форма регистрации транспорта");
        }

        #endregion


        #region Обновление данных

        private Task<IList<VehicleType>> GetVehicleTypesTask(EmptyRequest request)
        {
            return repository.GetVehicleTypes(request.AbandonToken);
        }

        private Task<IList<Nomenclature>> GetNomenclaturesTask(EmptyRequest request)
        {
            return repository.GetNomenclature(GetDictArg.Default, request.AbandonToken);
        }

        private Task<IList<VehicleRegNumUIItem>> GetVehicleRegNumsTask(EmptyRequest request)
        {
            return repository.GetVehicleRegNums(request.AbandonToken);
        }

        private Task<VehicleUIItem> RegisterVehicle(VehicleRegisterRequest request)
        {
            var currentIdentity = WindowsIdentity.GetCurrent();
            var user = currentIdentity?.Name ?? Environment.UserName ?? Environment.MachineName;
            return repository.RegisterVehicle(new VehicleRegisterArg()
            {
                VehicleTypeId = request.VehicleTypeId,
                VehicleRegNum = request.VehicleRegNum,
                NomenclatureId = request.NomenclatureId,
                RegisteredAt = DateTime.Now, //request.RegisteredAt,
                Weight = request.Weight,
                User = user,
                Comment = request.Comment
            }, request.AbandonToken);
        }

        #endregion


        #region Обрыботка событий

        private void ButtonSaveClick(object sender, RoutedEventArgs e)
        {
            if (!VehicleView.IsForLoadingOnly && Validation.GetHasError(tbWeight))
            {
                VehicleView.Weight.ValidationError = "Задайте корректный вес";
            }
            else
            {
                VehicleView.NewRequest();
            }
        }

        private void ButtonCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void VehicleProcessed(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(VehicleView.ActualError))
            {
                IsPerformed = true;
                Close();
            }
        }

        #endregion
    }
}
