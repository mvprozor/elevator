﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.WPF.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Interaction logic for TaskWindow.xaml
    /// </summary>
    [Browsable(false)]
    public partial class TaskWindow : Window
    {
        #region Открытые поля

        public TaskViewModel TaskView
        {
            get => DataContext as TaskViewModel;
            set => DataContext = value;
        }

        public MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    TaskView.IsDataRequestEnabled = IsLoaded && value != null;
                }
            }
        }

        public bool IsPerformed { get; private set; }

        #endregion


        #region Закрытые поля

        private MESRepository repository;

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        #region Конструкторы и иницилизация
        
        public TaskWindow()
        {
            TaskView = new TaskViewModel();
            TaskView.Nomenclatures.GetDataTask = GetNomenclaturesTask;
            TaskView.Organizations.GetDataTask = GetOrganizationsTask;
            TaskView.Contragents.GetDataTask = GetContragentsTask;
            TaskView.Storages.GetDataTask = GetStoragesTask;
            TaskView.Points.GetDataTask = GetPointsTask;
            TaskView.Vehicles.GetDataTask = GetVehicles;
            TaskView.GetDataTask = CreateTask;
            TaskView.Processed += TaskProcessed;
            InitializeComponent();
            Loaded += TaskWindowLoaded;
            Unloaded += TaskWindowUnloaded;

        }

        private void TaskWindowLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Загружена форма заданий");
            IsPerformed = false;
            if (Repository == null)
                logger.Info("Ожидание иницилизации репозитория MES API для формы заданий...");
            else
                TaskView.IsDataRequestEnabled = true;
        }

        private void TaskWindowUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Выгружена форма заданий");
        }

        #endregion


        #region Обновление данных

        private Task<IList<Nomenclature>> GetNomenclaturesTask(EmptyRequest request)
        {
            return repository.GetNomenclature(GetDictArg.Default, request.AbandonToken);
        }

        private Task<IList<Organization>> GetOrganizationsTask(EmptyRequest request)
        {
            return repository.GetOrganizations(GetDictArg.Default, request.AbandonToken);
        }

        private Task<IList<Contragent>> GetContragentsTask(EmptyRequest request)
        {
            return repository.GetContragents(GetDictArg.Default, request.AbandonToken);
        }

        private Task<IList<StorageUIItem>> GetStoragesTask(EmptyRequest request)
        {
            return repository.GetStorages(request.AbandonToken);
        }

        private Task<IList<UnitUIItem>> GetPointsTask(EmptyRequest request)
        {
            return repository.GetPoints(request.AbandonToken);
        }

        private Task<IList<VehicleUIItem>> GetVehicles(VehiclesRequest request)
        {
            return repository.GetVehicles(
                new GetVehicleArg()
                {
                    RegNum = request.RegNum,
                    IncludingLeft = request.IncludingLeft
                }, request.AbandonToken);
        }

        private Task<TaskUIItem> CreateTask(TaskCreateRequest request)
        {
            var currentIdentity = WindowsIdentity.GetCurrent();
            var user = currentIdentity?.Name ?? Environment.UserName ?? Environment.MachineName;
            return repository.CreateTask(new TaskManualArg()
            {
                TaskType = request.TaskType,
                NomenclatureId = request.NomenclatureId,
                OrganizationId = request.OrganizationId,
                ContragentId = request.ContragentId,
                UnitSourceId = request.UnitSourceId,
                UnitDestId = request.UnitDestId,
                UnitExternalId = request.UnitExtId,
                VehicleRegNum = request.VehicleRegNum,
                IsSeparating = request.IsSeparating,
                IsScalping = request.IsScalping,
                IsDrying = request.IsDrying,
                IsGood = true,
                Quantity = request.Quantity,
                Comment = request.Comment,
                User = user
            }, request.AbandonToken);
        }

        #endregion


        #region Обрыботка событий

        private void ButtonSaveClick(object sender, RoutedEventArgs e)
        {
            TaskView.NewRequest();
        }

        private void ButtonCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TaskProcessed(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(TaskView.ActualError))
            {
                IsPerformed = true;
                Close();
            }
        }

        #endregion
    }
}
