﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.WPF.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Interaction logic for StorageAssignWindow.xaml
    /// </summary>
    [Browsable(false)]
    public partial class StorageAssignWindow : Window
    {

        #region Открытые поля

        public StorageAssignViewModel StorageAssignView
        {
            get => DataContext as StorageAssignViewModel;
            set => DataContext = value;
        }

        public StorageUIItem StorageItem
        {
            get => StorageAssignView.StorageItem;
            set
            {
                if (value != StorageAssignView.StorageItem)
                {
                    StorageAssignView.StorageItem = value;
                }
            }
        }

        public MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    StorageAssignView.IsDataRequestEnabled = IsLoaded && value != null;
                }
            }
        }

        public bool IsPerformed { get; private set; }

        #endregion


        #region Закрытые поля

        private MESRepository repository;

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        #region Конструкторы и иницилизация

        public StorageAssignWindow()
        {
            StorageAssignView = new StorageAssignViewModel();
            StorageAssignView.ProcessDataTask = StorageAssignTask;
            StorageAssignView.Processed += StorageAssignProcessed;
            StorageAssignView.Nomenclatures.GetDataTask = GetNomenclaturesTasks;
            InitializeComponent();
            Loaded += StartWindowLoaded;
            Unloaded += StartWindowUnloaded;
        }

        private void StartWindowLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Загружена форма назначения склада");
            IsPerformed = false;
            if (Repository == null)
                logger.Info("Ожидание инициализации репозитория MES API для формы назначения склада...");
            else
                StorageAssignView.IsDataRequestEnabled = true;
        }

        private void StartWindowUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Выгружена форма назначения склада");
        }

        #endregion


        #region Обновление и сохранение данных

        private Task<IList<Nomenclature>> GetNomenclaturesTasks(EmptyRequest request)
        {
            return repository.GetNomenclature(GetDictArg.Default, request.AbandonToken);
        }

        private System.Threading.Tasks.Task StorageAssignTask(StorageAssignRequest request)
        {
            return Repository.StorageAssign(request.Arg, request.AbandonToken);
        }

        #endregion


        #region Обрыботка событий

        private void ButtonSaveClick(object sender, RoutedEventArgs e)
        {
            StorageAssignView.StorageItem.IsProcessing = true;
            StorageAssignView.NewRequest();
        }

        private void ButtonCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void StorageAssignProcessed(object sender, EventArgs e)
        {
            StorageAssignView.StorageItem.IsProcessing = false;
            if (String.IsNullOrWhiteSpace(StorageAssignView.ActualError))
            {
                IsPerformed = true;
                Close();
            }
        }

        #endregion
    }
}
