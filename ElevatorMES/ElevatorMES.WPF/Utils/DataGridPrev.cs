﻿using ElevatorMES.WPF.Models;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace ElevatorMES.WPF.Utils
{
    public static class DataGridPrev
    {
        private const string asc = "ASC";
        private const string desc = "DESC";

        public static void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = (DataGrid)sender;
            if (e.AddedItems.Count == 0)
            {
                var prevSelection = dataGrid.DataContext as IPrevSelection;
                var prevItem = prevSelection?.GetPrevSelection();
                if (prevItem != null)
                {
                    dataGrid.ScrollIntoView(prevItem);
                    var row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromItem(prevItem);
                    if (row != null)
                    {
                        row.IsSelected = true;
                        row.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    }
                }
            }
        }

        public static void OnSorting(object sender, DataGridSortingEventArgs e)
        {
            var dataGrid = (DataGrid)sender;
            var prevSorting = dataGrid.DataContext as IPrevSorting;
            if (prevSorting != null)
            {
                dataGrid.Dispatcher.BeginInvoke((Action)delegate ()
                {
                    prevSorting.PrevSorting = String.Join(",",
                        dataGrid.Items.SortDescriptions.Select(m => $"{m.PropertyName} { (m.Direction == ListSortDirection.Ascending ? asc : desc)}"));
                });
            }
        }
    }
}
