﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Converters
{
    public class IdToValueConverter : IValueConverter
    {
        public object Value0 { get; set; }

        public object Value1 { get; set; }

        public object Value2 { get; set; }

        public object Value3 { get; set; }

        public object Value4 { get; set; }

        public object Value5 { get; set; }

        public object Value6 { get; set; }

        public object Value7 { get; set; }

        public object Value8 { get; set; }

        public object Value9 { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var id = (int)value;
            switch (id % 10)
            {
                case 0:
                    return Value0;
                case 1:
                    return Value1;
                case 2:
                    return Value2;
                case 3:
                    return Value3;
                case 4:
                    return Value4;
                case 5:
                    return Value5;
                case 6:
                    return Value6;
                case 7:
                    return Value7;
                case 8:
                    return Value8;
                case 9:
                    return Value9;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}