﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Converters
{
    public class TaskStatusConverter : IValueConverter
    {
        public object QueuedValue { get; set; }

        public object DeletedValue { get; set; }

        public object RunningValue { get; set; }

        public object CompletedValue { get; set; }

        public object FailedValue { get; set; }

        public object PendingValue { get; set; }

        public object SuspendedValue { get; set; }


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = (TaskStatus)value;
            switch (status)
            {
                case TaskStatus.Queued:
                    return QueuedValue;
                case TaskStatus.Deleted:
                    return QueuedValue;
                case TaskStatus.Running:
                    return RunningValue;
                case TaskStatus.Completed:
                    return CompletedValue;
                case TaskStatus.Failed:
                    return FailedValue;
                case TaskStatus.Pending:
                    return PendingValue;
                case TaskStatus.Suspended:
                    return SuspendedValue;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
