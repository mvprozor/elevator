﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Converters
{
    public class DictionaryTypeConverter : IValueConverter
    {
        public object NomenclatureValue { get; set; }

        public object ContragentValue { get; set; }

        public object OrganizationValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var type = (DictionaryType)value;
            switch (type)
            {
                case DictionaryType.Nomenclature:
                    return NomenclatureValue;
                case DictionaryType.Contragent:
                    return ContragentValue;
                case DictionaryType.Organization:
                    return OrganizationValue;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
