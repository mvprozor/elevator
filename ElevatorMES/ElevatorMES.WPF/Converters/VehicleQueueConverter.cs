﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Converters
{
    public class VehicleQueueConverter : IValueConverter
    {
        public object AllValue { get; set; }

        public object ToBruttoValue { get; set; }

        public object ToUnloadValue { get; set; }

        public object ToTareValue { get; set; }

        public object ToLoadValue { get; set; }

        public object ToLeaveValue { get; set; }
        
        public object LeavedValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var queue = (VehicleQueue)value;
            switch (queue)
            {
                case VehicleQueue.All:
                    return AllValue;
                case VehicleQueue.ToBrutto:
                    return ToBruttoValue;
                case VehicleQueue.ToUnload:
                    return ToUnloadValue;
                case VehicleQueue.ToTare:
                    return ToTareValue;
                case VehicleQueue.ToLoad:
                    return ToLoadValue;
                case VehicleQueue.ToLeave:
                    return ToLeaveValue;
                case VehicleQueue.Leaved:
                    return LeavedValue;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
