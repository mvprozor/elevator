﻿using System;
using System.Globalization;
using System.Windows.Data;
using ElevatorMES.WPF.Enums;

namespace ElevatorMES.WPF.Converters
{
    public class ScalesInputStateConverter : IValueConverter
    {
        public object Initializing { get; set; }

        public object Receiving { get; set; }

        public object Failed { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var state = (ScalesInputStateEnum)value;
            switch (state)
            {
                case ScalesInputStateEnum.Initializing:
                    return Initializing;
                case ScalesInputStateEnum.Receiving:
                    return Receiving;
                case ScalesInputStateEnum.Failed:
                    return Failed;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
