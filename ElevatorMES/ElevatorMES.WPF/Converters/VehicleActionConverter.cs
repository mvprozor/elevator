﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Converters
{
    public class VehicleActionConverter : IValueConverter
    {
        public object RegisteringValue { get; set; }

        public object WeightingValue { get; set; }

        public object UnloadingValue { get; set; }

        public object LoadingValue { get; set; }

        public object LeavingValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = (VehicleAction)value;
            switch (status)
            {
                case VehicleAction.Registering:
                    return RegisteringValue;
                case VehicleAction.Weighting:
                    return WeightingValue;
                case VehicleAction.Unloading:
                    return UnloadingValue;
                case VehicleAction.Loading:
                    return LoadingValue;
                case VehicleAction.Leaving:
                    return LeavingValue;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
