﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.WPF.Converters
{
    public class ExceptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var exception = value as Exception;
            return exception == null ? String.Empty : exception.ExtractMessage();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
