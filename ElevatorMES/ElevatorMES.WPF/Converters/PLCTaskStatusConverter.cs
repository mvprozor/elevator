﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Converters
{
    public class PLCTaskStatusConverter : IValueConverter
    {
        public object UndeterminedValue { get; set; }

        public object RouteStoppedValue { get; set; }

        public object RouteRunningValue { get; set; }

        public object RouteStoppingValue { get; set; }

        public object RouteStartingValue { get; set; }

        public object RouteCompletedValue { get; set; }

        public object UnknownTaskIdValue { get; set; }

        public object ConfigurationErrorValue { get; set; }
        
        public object RouteNotReadyValue { get; set; }

        public object EquipmentFailureValue { get; set; }


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = (PLCTaskStatus)value;
            switch (status)
            {
                case PLCTaskStatus.Undetermined:
                    return UndeterminedValue;
                case PLCTaskStatus.RouteStopped:
                    return RouteStoppedValue;
                case PLCTaskStatus.RouteRunning:
                    return RouteRunningValue;
                case PLCTaskStatus.RouteStopping:
                    return RouteStoppingValue;
                case PLCTaskStatus.RouteStarting:
                    return RouteStartingValue;
                case PLCTaskStatus.RouteCompleted:
                    return RouteCompletedValue;
                case PLCTaskStatus.UnknownTaskId:
                    return UnknownTaskIdValue;
                case PLCTaskStatus.ConfigurationError:
                    return ConfigurationErrorValue;
                case PLCTaskStatus.RouteNotReady:
                    return RouteNotReadyValue;
                case PLCTaskStatus.EquipmentFailure:
                    return EquipmentFailureValue;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
