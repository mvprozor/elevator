﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.PLC;
using ElevatorMES.WPF.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Логика взаимодействия для TaskStopWindow.xaml
    /// </summary>
    [Browsable(false)]
    public partial class TaskStopWindow : Window, INotifyPropertyChanged
    {
        #region Открытые поля

        public TaskStopViewModel TaskStopView
        {
            get => DataContext as TaskStopViewModel;
            set => DataContext = value;
        }

        public TaskUIItem TaskItem
        {
            get => TaskStopView.TaskItem;
            set
            {
                if (value != TaskStopView.TaskItem)
                {
                    TaskStopView.TaskItem = value;
                    CheckEnabled();
                }
            }
        }

        public MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    CheckEnabled();
                }
            }
        }

        public IPLCTasks PLCTasks { get; set; }

        public bool IsPerformed { get; private set; }

        #endregion


        #region Закрытые поля

        private MESRepository repository;

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        
        #region Конструкторы и иницилизация
        
        public TaskStopWindow(bool isFinished)
        {
            TaskStopView = new TaskStopViewModel();
            TaskStopView.IsReturn.IsVisible = isFinished;
            TaskStopView.IsFailed.IsVisible = isFinished;
            TaskStopView.GetDataTask = StopTask;
            TaskStopView.Processed += TaskStopProcessed;
            InitializeComponent();
            Title = isFinished ? "Завершение задания" : "Приостанов задания";
            Loaded += TaskWindowLoaded;
            Unloaded += TaskWindowUnloaded;

        }

        private void TaskWindowLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Загружена форма останова заданий");
            IsPerformed = false;
            CheckEnabled();
        }

        private void TaskWindowUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Выгружена форма останова заданий");
        }

        private void CheckEnabled()
        {
            bool isEnabled = IsLoaded && Repository != null && TaskItem != null
                && (TaskStopView.IsReturn.IsVisible ? TaskItem.CanComplete : TaskItem.CanSuspend);
            TaskStopView.IsEnabled = isEnabled;
            if (isEnabled && TaskItem.VehicleId.HasValue)
            {
                GetVehicleWeight();
            }

        }

        #endregion

        #region Чтение доп. данных

        private async void GetVehicleWeight()
        {
            if (TaskItem == null || !TaskItem.VehicleId.HasValue)
            {
                TaskStopView.VehicleQuantity = null;
                TaskStopView.VehicleInfo = "нет";
            }
            else
            {
				TaskStopView.VehicleInfo = "загрузка...";
				var vehicles = await Repository.GetVehicles(new GetVehicleArg() { VehicleId = TaskItem.VehicleId },
                    CancellationToken.None);
                var vehicle = vehicles.FirstOrDefault();
                if (vehicle == null)
                {
					TaskStopView.VehicleQuantity = null;
					TaskStopView.VehicleInfo = "н/д";
				}
                else
                {
                    TaskStopView.VehicleQuantity = TaskItem.Type == TaskType.Receiving ?
                        vehicle.RawWeight : vehicle.ProdWeight;
                    TaskStopView.VehicleInfo = vehicle.RegNum;
				}    
            }
        }

        #endregion


        #region Сохранение данных

        private Task<TaskUIItem> StopTask(TaskStopRequest request)
        {
            return Repository.StopTask(request.Arg, request.AbandonToken);
        }
        
        #endregion

        
        #region Обрыботка событий
        
        private async void ButtonSaveClick(object sender, RoutedEventArgs e)
        {
            if (Validation.GetHasError(tbWeight))
            {
                TaskStopView.Weight.ValidationError = "Задайте корректный вес";
            }
            else
            {
                var arg = TaskStopView.GetArgs();
                if (arg == null)
                    return;
                TaskStopView.TaskItem.IsProcessing = true;
                TaskStopView.ActualResult = null;
                try
                {

                    logger.Info($"Завершение задания ID={TaskStopView.TaskItem.Id}, текущий вес {TaskStopView.Weight.Selected}...");
                    var task = await Repository.StopTask(arg, CancellationToken.None);
                    logger.Info($"Задание ID={TaskStopView.TaskItem.Id} завершено");
                    TaskStopView.ActualResult = task;
                }
                catch(Exception ex)
                {
                    logger.Error(ex, "Ошибка при останове задания");
                    TaskStopView.ActualError = (ex.InnerException ?? ex).Message;
                }
                TaskStopView.TaskItem.IsProcessing = false;
                if (TaskStopView.ActualResult!=null)
                {
                    PLCTasks.StopTask(TaskStopView.TaskItem.Id);
                    IsPerformed = true;
                    Close();
                }
                //TaskStopView.NewRequest();
            }
        }

        private void ButtonCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TaskStopProcessed(object sender, EventArgs e)
        {
            var task = TaskStopView.TaskItem;
            task.IsProcessing = false;
            if (String.IsNullOrWhiteSpace(TaskStopView.ActualError))
            {
                PLCTasks.StopTask(task.Id);
                IsPerformed = true;
                Close();
            }
        }

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
