﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Helpers;
using ElevatorMES.WPF.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Interaction logic for SetupControl.xaml
    /// </summary>
    [Browsable(false)]
    public partial class SetupControl : UserControl
    {

        #region Открытые поля

        public MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    SetupView.IsDataRequestEnabled = IsLoaded && value != null;
                }
            }
        }

        /// <summary>
        /// Настройки
        /// </summary>
        public SetupViewModel SetupView
        {
            get => DataContext as SetupViewModel;
            set => DataContext = value;
        }

        #endregion


        #region Закрытые поля

        private MESRepository repository;

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        #region Конструкторы и иницилизация

        public SetupControl()
        {
            SetupView = new SetupViewModel();
            SetupView.UnitEquipment.GetDataTask = GetUnitEquipments;
            SetupView.UpdateLink = UpdateLink;
            SetupView.NomenclaturePriority.GetDataTask = GetNomenclaturePriority;
            SetupView.UpdateNomenclaturePriority = UpdateNomenclaturePriority;
            SetupView.Nomenclature.GetDataTask = GetNomenclature;
            SetupView.Nomenclature.UpdateDataTask = UpdateDictionary;
            SetupView.Nomenclature.DeleteAction = DeleteDictionary;
            SetupView.Nomenclature.RestoreAction = RestoreDictionary;
            SetupView.Organization.GetDataTask = GetOrganization;
            SetupView.Organization.UpdateDataTask = UpdateDictionary;
            SetupView.Organization.DeleteAction = DeleteDictionary;
            SetupView.Organization.RestoreAction = RestoreDictionary;
            SetupView.Contragent.GetDataTask = GetContragent;
            SetupView.Contragent.UpdateDataTask = UpdateDictionary;
            SetupView.Contragent.DeleteAction = DeleteDictionary;
            SetupView.Contragent.RestoreAction = RestoreDictionary;

            InitializeComponent();
            Loaded += SetupControlLoaded;
            Unloaded += SetupControlUnloaded;
        }

        private void SetupControlLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Загружена форма настроек");
            if (Repository == null)
                logger.Info("Ожидание иницилизации подключения к MES API...");
            else
            {
                SetupView.IsDataRequestEnabled = true;
            }
        }

        private void SetupControlUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Выгружена форма настроек");
        }

        #endregion


        #region Обновление и сохранение данных

        private Task<List<UnitEquipmentUIItem>> GetUnitEquipments(UnitEquipmentsRequest request)
        {
            return Repository.GetUnitEquipments(new GetUnitEquipmentsArg
            {
                UnitId = request.UnitId
            }, request.AbandonToken);
        }

        private System.Threading.Tasks.Task UpdateLink(UnitEquipmentUIItem link, CancellationToken token)
        {
            return Repository.UpdateUnitEquipments(
                new UnitEquipmentUpdateArg()
                {
                    UnitId = link.UnitId,
                    EquipmentId = link.EquipmentId,
                    IsContainer = link.IsContainer,
                    IsRouteSource = link.IsRouteSource,
                    IsRouteDestination = link.IsRouteDestination
                }, token);
        }

        private Task<IList<NomenclaturePriorityUIItem>> GetNomenclaturePriority(EmptyRequest request)
        {
            return Repository.GetNomenclaturePriority(request.AbandonToken);
        }

        private System.Threading.Tasks.Task UpdateNomenclaturePriority(NomenclaturePriorityUIItem priority, CancellationToken token)
        {
            return Repository.UpdateNomenclaturePriority(
                new NomenclaturePriorityUpdateArg()
                {
                    NomenclatureId = priority.NomenclatureId,
                    Priority = priority.Priority
                }, token);
        }

        private Task<IList<Nomenclature>> GetNomenclature(DictionaryRequest request)
        {
            return Repository.GetNomenclature(request.ToArg(), request.AbandonToken);
        }

        private Task<IList<Organization>> GetOrganization(DictionaryRequest request)
        {
            return Repository.GetOrganizations(request.ToArg(), request.AbandonToken);
        }

        private Task<IList<Contragent>> GetContragent(DictionaryRequest request)
        {
            return Repository.GetContragents(request.ToArg(), request.AbandonToken);
        }

        private System.Threading.Tasks.Task UpdateDictionary(DictionaryUpdateRequest request)
        {
            return Repository.UpdateDictionary(request.ToArg(), request.AbandonToken);
        }

        private async void DeleteDictionary(DeleteDictArg arg, Func<bool> onSucess)
        {
            var itemInfo = $"ID={arg.Id} справочника «{arg.Type.GetDescription()}»";
            var result = MessageBox.Show($"Вы действительно хотите удалить элемент {itemInfo}?",
                "Потдверждение", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var isSuccess = false;
                try
                {
                    await Repository.DeleteDictionary(arg, CancellationToken.None);
                    isSuccess = true;
                }
                catch (Exception exception)
                {
                    logger.Error(exception, $"Ошибка при удалении элемента {itemInfo}");
                }
                if (isSuccess)
                    onSucess();
            }
        }

        private async void RestoreDictionary(DeleteDictArg arg, Func<bool> onSucess)
        {
            var isSuccess = false;
            try
            {
                await Repository.RestoreDictionary(arg, CancellationToken.None);
                isSuccess = true;
            }
            catch (Exception exception)
            {
                var itemInfo = $"ID={arg.Id} справочника «{arg.Type.GetDescription()}»";
                logger.Error(exception, $"Ошибка при восстановлении элемента {itemInfo}");
            }
            if (isSuccess)
                onSucess();
        }

        #endregion


        #region События от элементов

        private void NewLinkClick(object sender, RoutedEventArgs e)
        {
            var unitEquipmentWindow = new UnitEquipmentWindow()
            {
                Repository = Repository,
                Owner = Window.GetWindow(this)
            };
            unitEquipmentWindow.Closed += (o, ee) =>
            {
                this.IsEnabled = true;
                var win = (UnitEquipmentWindow)o;
                if (win.IsPerformed)
                {
                    SetupView.UnitEquipment.NewRequest();
                }
            };
            this.IsEnabled = false;
            unitEquipmentWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            unitEquipmentWindow.Show();
        }

        #endregion
    }
}
