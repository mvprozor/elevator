﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace ElevatorMES.WPF.Logging
{
    public class LogLevelConverter : IValueConverter
    {
        public object Trace { get; set; }
        public object Debug { get; set; }
        public object Info { get; set; }
        public object Warn { get; set; }
        public object Error { get; set; }
        public object Fatal { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (System.Convert.ToString(value))
            {
                case "Trace":
                    return Trace;
                case "Debug":
                    return Debug;
                case "Info":
                    return Info;
                case "Warn":
                    return Warn;
                case "Error":
                    return Error;
                case "Fatal":
                    return Fatal;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
