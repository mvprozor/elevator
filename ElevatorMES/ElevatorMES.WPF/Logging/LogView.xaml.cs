﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using NLog;
using System.ComponentModel;

namespace ElevatorMES.WPF.Logging
{
    /// <summary>
    /// Логика взаимодействия для LogView.xaml
    /// </summary>
    [Browsable(false)]
    public partial class LogView : UserControl
    {
        public ObservableCollection<LogEventInfo> LogCollection { get; set; }

        public LogView()
        {
            LogCollection = new ObservableCollection<LogEventInfo>();

            InitializeComponent();

            var target = LogManager.Configuration?.AllTargets?.OfType<MemoryEventTarget>().FirstOrDefault();
            if (target!=null)
                target.EventReceived += EventReceived;
        }

        private void EventReceived(LogEventInfo message)
        {
            Dispatcher.Invoke(new Action(() => {
                if (LogCollection.Count >= 50) LogCollection.RemoveAt(0);
                LogCollection.Add(message);
            }));
        }
    }
}
