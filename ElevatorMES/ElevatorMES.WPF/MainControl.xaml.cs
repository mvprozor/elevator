﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Helpers;
using NLog;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using ElevatorMES.WPF.Logging;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.WPF.Models;
using Microsoft.Reporting.WinForms;
using System.Collections;
using ElevatorMES.Domain.PLC;
using System.Windows.Threading;
using ElevatorMES.Domain.Enums;
using ElevatorMES.WPF.Utils;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Markup;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Interaction logic for MainControl.xaml
    /// </summary>
    [Browsable(false)]
    public partial class MainControl : UserControl
    {
        #region Dependency properties

        /// <summary>
        /// Адрес MES Web API
        /// </summary>
        public static readonly DependencyProperty MESApiProperty =
            DependencyProperty.Register(nameof(MESApi), typeof(string), typeof(MainControl),
                new FrameworkPropertyMetadata(null,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, MESApiChanged));


        public string MESApi
        {
            get => (string)this.GetValue(MainControl.MESApiProperty);
            set => this.SetValue(MainControl.MESApiProperty, value);
        }

        private static void MESApiChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var mainControl = (MainControl)d;
            mainControl.OnMESApiChanged((string)e.NewValue);
        }


        /// <summary>
        /// Частота обновления данных из API
        /// </summary>
        public static readonly DependencyProperty RefreshSecondsProperty =
            DependencyProperty.Register(nameof(RefreshSeconds), typeof(int), typeof(MainControl),
                new FrameworkPropertyMetadata(60,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, RefreshSecondsChanged), ValidateRefreshSeconds);

        public int RefreshSeconds
        {
            get => (int)this.GetValue(MainControl.RefreshSecondsProperty);
            set => this.SetValue(MainControl.RefreshSecondsProperty, value);
        }

        private static bool ValidateRefreshSeconds(object value)
        {
            var intValue = (int)value;
            return intValue >= 10;
        }

        private static void RefreshSecondsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var mainControl = (MainControl)d;
            mainControl.OnRefreshSecondsChanged((int)e.NewValue);
        }

        #endregion


        #region Открытые поля

        public IPLCTasks PLCTasks
        {
            get => plcTasks;
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(PLCTasks));
                if (plcTasks != value)
                {
                    if (plcTasks != null)
                    {
                        plcTasks.TaskStatusReceived -= PLCTaskStatusReceived;
                        plcTasks.NewTaskReceived -= PLCTasks_NewTaskReceived;
                    }
                    plcTasks = value;
                    plcTasks.TaskStatusReceived += PLCTaskStatusReceived;
                    plcTasks.NewTaskReceived += PLCTasks_NewTaskReceived;
                }
            }
        }

        public MainViewModel MainView 
        {
            get => DataContext as MainViewModel;
            set => DataContext = value;
        }

        private MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    setupControl.Repository = value;
                    MainView.IsDataRequestEnabled = IsLoaded && value != null;
                }
            }
        }

        #endregion


        #region Закрытые поля

        private IPLCTasks plcTasks;

        private MESRepository repository;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private DispatcherTimer plcTimer;

        private int activeTaskIndex;

        private DispatcherTimer refreshTimer;

        #endregion


        #region Конструкторы и инициализация

        public MainControl()
        {
            InitLogging();
            PLCTasks = DefaultPLCTasks.Instance;
            MainView = new MainViewModel();
            MainView.OperInfo.GetDataTask = GetOperInfo;
            MainView.ActiveTasks.GetDataTask = GetActiveTasks;
            MainView.QueuedTasks.GetDataTask = GetQueuedTasks;
            MainView.HistoryTasks.GetDataTask = GetHistoryTasks;
            MainView.Storages.GetDataTask = GetStorages;
            MainView.Vehicles.GetDataTask = GetVehicles;
            MainView.Vehicles.History.GetDataTask = GetVehicleHistory;
            MainView.TransmitLog.GetDataTask = GetTransmitLog;
            InitializeComponent();
            MainView.Reports.Viewer = reportViewer;
            MainView.Reports.GetParamIntValues = GetReportParamIntValues;
            MainView.Reports.GetMainDataSet = GetReportMainDataSet;
            plcTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle)
            {
                Interval = TimeSpan.FromSeconds(5),
                IsEnabled = true
            };
            plcTimer.Tick += PLCTimerTick;
            refreshTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle)
            {
                Interval = TimeSpan.FromSeconds(RefreshSeconds),
                IsEnabled = true
            };
            refreshTimer.Tick += RefreshTimerTick;
            Loaded += MainControlLoaded;
            Unloaded += MainControlUnloaded;
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(
                XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
        }

        private void MainControlLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug($"Загружена основная форма. Версия сборки {Assembly.GetExecutingAssembly().GetName().Version}");
            if (Repository == null)
                logger.Info("Ожидание иницилизации подключения к MES API...");
            else
            {
                MainView.IsDataRequestEnabled = true;
            }
        }

        private void MainControlUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Выгружена основная форма");
        }

        private void InitLogging()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logfile = new NLog.Targets.FileTarget("logfile")
            {
                FileName = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "logmescl.txt")
            };
            var logview = new MemoryEventTarget();

            // Rules for mapping loggers to targets            
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logview);
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);

            // Apply config           
            NLog.LogManager.Configuration = config;
        }

        private void OnMESApiChanged(string newMESApi)
        {
            Repository?.Abandon();
            Repository = null;
            if (String.IsNullOrWhiteSpace(newMESApi))
            {
                logger.Warn("Подключение к MES API не задано");
            }
            else
            {
                try
                {
                    Repository = new MESRepository(newMESApi);
                    logger.Info($"Подключение к MES API инициализировано: {Repository.BaseAddress}");
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Ошибка иницилизации подключения к MES API");
                }
            }
        }

        private void OnRefreshSecondsChanged(int newRefreshSeconds)
        {
            refreshTimer.Interval = TimeSpan.FromSeconds(RefreshSeconds);
        }

        #endregion


        #region Обновление данных

        private void RefreshTimerTick(object sender, EventArgs e)
        {
            MainView.Refresh();
        }

        private Task<OperInfoUI> GetOperInfo(EmptyRequest request)
        {
            return Repository.GetOperInfo(request.AbandonToken);
        }

        private async Task<List<TaskUIItem>> GetActiveTasks(EmptyRequest request)
        {
            var activeTasks = await Repository.GetTasks(new GetTaskArg()
            {
                TaskStatus = GetTaskStatusEnum.Active
            }, request.AbandonToken);
            if (activeTasks?.Count > 0)
            {
                var prevActiveTasks = MainView.ActiveTasks.ActualResult;
                foreach (var activeTask in activeTasks)
                {
                    var prevActiveTask = prevActiveTasks?.FirstOrDefault(m => m.Id == activeTask.Id);
                    if (prevActiveTask!=null)
                    { 
                        logger.Debug($"Восстановление состояния от ПЛК при обновлении из БД: {prevActiveTask.StatusPLC}");
                        activeTask.StatusPLC = prevActiveTask.StatusPLC;
                        activeTask.QuantityPLC = prevActiveTask.QuantityPLC;
                    }
                }
            }
            return activeTasks;
        }

        private Task<List<TaskUIItem>> GetQueuedTasks(EmptyRequest request)
        {
            return Repository.GetTasks(new GetTaskArg()
            {
                TaskStatus = GetTaskStatusEnum.Queued
            }, request.AbandonToken);
        }

        private Task<List<TaskUIItem>> GetHistoryTasks(TasksHistoryRequest request)
        {
            return Repository.GetTasks(new GetTaskArg()
            {
                TaskStatus = GetTaskStatusEnum.History,
                Begin = request.DateFrom,
                End = request.DateTill,
                TextFilter = request.SearchText
            }, request.AbandonToken);
        }

        private Task<IList<StorageUIItem>> GetStorages(EmptyRequest request)
        {
            return Repository.GetStorages(request.AbandonToken);
        }

        private Task<IList<VehicleUIItem>> GetVehicles(VehiclesRequest request)
        {
            return Repository.GetVehicles(new GetVehicleArg()
            { 
                IncludingLeft = request.IncludingLeft,
                RegNum = request.RegNum
            }, request.AbandonToken);
        }

        private Task<IList<VehicleActivity>> GetVehicleHistory(VehicleHistoryRequest request)
        {
            if (request.VehicleId.HasValue)
                return Repository.GetVehicleHistory(request.VehicleId.Value, request.AbandonToken);
            else
                return System.Threading.Tasks.Task.FromResult((IList<VehicleActivity>)Array.Empty<VehicleActivity>());
        }

        private Task<List<TransmitLog>> GetTransmitLog(TransmitLogRequest request)
        {
            return Repository.GetTransmitLog(new GetTransmitLogArg()
            { 
                Begin = request.DateFrom,
                End = request.DateTill,
                TextFilter = request.SearchText
            }, request.AbandonToken);
        }

        private Task<ReportParamItemsModel<int>> GetReportParamIntValues(
            string reportName, string paramName, IDictionary<string, object> queryValues)
        {
            switch(paramName)
            {
                case "SiloId":
                    return Repository.GetStorages(CancellationToken.None).ContinueWith(
                        m => new ReportParamItemsModel<int>(
                            new ReportDataSource("DSetSilos", m.Result),
                            m.Result.Select(n => new ReportParamListItemModel<int>(n.UnitId, n.FullInfo)).OrderBy(p => p.Label).ToArray()));
                case "EquipmentId":
                    return Repository.GetEquipments(CancellationToken.None).ContinueWith(
                        m => new ReportParamItemsModel<int>(
                            new ReportDataSource("DSetEquipments", m.Result),
                            m.Result.Select(n => new ReportParamListItemModel<int>(n.Id, n.DisplayName)).OrderBy(p => p.Label).ToArray()));
                default:
                    return System.Threading.Tasks.Task.FromResult<ReportParamItemsModel<int>>(null);
            }
        }

        private Task<IList> GetReportMainDataSet(string reportName, string dataSetName, 
            IDictionary<string, object> queryValues)
        {
            var token = CancellationToken.None;
            switch (reportName)
            {
                case "Tasks":
                    return Repository.GetTasks(queryValues, token);

                case "SiloState":
                    return dataSetName == "DSetState" ?
                        Repository.GetSiloStateReport(queryValues, token) : Repository.GetSiloMoveReport(queryValues, token);

                case "EquipmentStats":
                    return Repository.GetEquipmentStatsReport(queryValues, token);

                default:
                    return System.Threading.Tasks.Task.FromResult<IList>(null);
            }
        }

        #endregion


        #region Работа с заданиями

        // создание нового задания
        private void NewTask()
        {
            var taskWindow = new TaskWindow()
            {
                Repository = Repository,
                Owner = Window.GetWindow(this)
            };
            taskWindow.Closed += (o, e) =>
            {
                this.IsEnabled = true;
                if (((TaskWindow)o).IsPerformed)
                    MainView.QueuedTasks.Refresh();
            };
            this.IsEnabled = false;
            taskWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            taskWindow.Show();
        }

        // удаление задания
        private async void DeleteTask(TaskUIItem task)
        {
            var result = MessageBox.Show($"Вы действительно хотите удалить задание «{task}»?",
                "Потдверждение", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var isSuccess = false;
                task.IsProcessing = true;
                try
                {
                    await Repository.DeleteTask(task.Id, CancellationToken.None);
                    isSuccess = true;
                }
                catch (Exception exception)
                {
                    logger.Error(exception, $"Ошибка при удалении задачи {task}");
                }
                task.IsProcessing = false;
                if (isSuccess)
                    MainView.QueuedTasks.Refresh();
            }
        }

        // постановка задания на выполнение
        private void StartTask(TaskUIItem task)
        {
            var startWindow = new TaskStartWindow()
            {
                Repository = Repository,
                PLCTasks = PLCTasks,
                TaskItem = task,
                Owner = Window.GetWindow(this)
            };
            startWindow.Closed += (o, e) =>
            {
                this.IsEnabled = true;
                if (((TaskStartWindow)o).IsPerformed)
                {
                    MainView.QueuedTasks.Refresh();
                    MainView.ActiveTasks.Refresh();
                }
            };
            this.IsEnabled = false;
            startWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            startWindow.Show();
        }

        // перевод задания в статус "Запущено"
        private async System.Threading.Tasks.Task RunTask(TaskUIItem task)
        {
            task.IsProcessing = true;
            try
            {
                var updatedTask = await Repository.RunTask(
                    new TaskRunArg() { TaskId = task.Id },
                    CancellationToken.None);
                task.AssignActiveState(updatedTask);
            }
            catch (Exception exception)
            {
                logger.Error(exception, $"Ошибка при запуске задачи {task.ShortInfo}");
            }
            task.IsProcessing = false;
        }

		// фиксация веса по заданию
		private async System.Threading.Tasks.Task RegPLCWeight(TaskUIItem task)
		{
			task.IsProcessing = true;
			try
			{
				var updatedTask = await Repository.RegWeightTask(new TaskWeightArg() { 
                    TaskId = task.Id, Weight = task.QuantityPLC }, CancellationToken.None);
				task.AssignActiveState(updatedTask);

			}
			catch (Exception exception)
			{
				logger.Error(exception, $"Ошибка при фиксации веса по заданию: {task.ShortInfo}");
			}
			task.IsProcessing = false;
		}

		// перевод задания в статус "Приостановлено"
		private async System.Threading.Tasks.Task SuspendTask(TaskUIItem task)
        {
            task.IsProcessing = true;
            try
            {
                //var weight = (task.StatusPLC == PLCTaskStatus.RouteStopped
                //    || task.StatusPLC == PLCTaskStatus.EquipmentFailure
                //    || task.StatusPLC == PLCTaskStatus.RouteCompleted) ? task.QuantityPLC : decimal.Zero;
                var updatedTask = await Repository.StopTask(
                    new TaskStopArg()
                    { 
                        TaskId = task.Id
                        //Weight = task.QuantityPLC
                    },
                    CancellationToken.None);
                task.AssignActiveState(updatedTask);
                /*
                if (weight>decimal.Zero && Object.ReferenceEquals(MainView.SelectedTab, MainView.Storages))
                {
                    MainView.Storages.Refresh();
                }*/
            }
            catch (Exception exception)
            {
                logger.Error(exception, $"Ошибка при приостановке задачи {task.ShortInfo}");
            }
            task.IsProcessing = false;
        }

        // снятие задачи с выполнения
        private void StopTask(TaskUIItem task)
        {
            var taskStopWindow = new TaskStopWindow(true)
            {
                Repository = Repository,
                PLCTasks = PLCTasks,
                TaskItem = task,
                Owner = Window.GetWindow(this)
            };
            taskStopWindow.Closed += (o, e) =>
            {
                this.IsEnabled = true;
                var win = (TaskStopWindow)o;
                if (win.IsPerformed)
                {
                    MainView.ActiveTasks.Refresh();
                    if (win.TaskStopView.ActualResult.Status == Domain.Enums.TaskStatus.Queued)
                    {
                        if (Object.ReferenceEquals(MainView.SelectedTab, MainView.QueuedTasks))
                            MainView.QueuedTasks.Refresh();
                    }
                    else
                    {
                        if (Object.ReferenceEquals(MainView.SelectedTab, MainView.HistoryTasks))
                            MainView.HistoryTasks.Refresh();
                    }
                    if (Object.ReferenceEquals(MainView.SelectedTab, MainView.Storages))
                    {
                        MainView.Storages.Refresh();
                    }
                }
            };
            this.IsEnabled = false;
            taskStopWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            taskStopWindow.Show();
        }

        private void MatchTask()
        {
            var taskMatchWindow = new TaskMatchWindow()
            {
                Repository = Repository,
                Owner = Window.GetWindow(this)
            };
            taskMatchWindow.Closed += (o, e) =>
            {
                this.IsEnabled = true;
                if (((TaskMatchWindow)o).IsPerformed)
                {
                    MainView.ActiveTasks.Refresh();
                    MainView.HistoryTasks.Refresh();
                }
            };
            this.IsEnabled = false;
            taskMatchWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            taskMatchWindow.Show();
        }

        // отмена выполнения задания
        private async void RevertTask(TaskUIItem task)
        {
            var result = MessageBox.Show($"Вы действительно хотите отменить выполнение задания «{task}»?",
                "Потдверждение", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var isSuccess = false;
                task.IsProcessing = true;
                try
                {
                    await Repository.RevertTask(task.Id, CancellationToken.None);
                    isSuccess = true;
                }
                catch (Exception exception)
                {
                    logger.Error(exception, $"Ошибка при отмене выполнения {task}");
                }
                task.IsProcessing = false;
                if (isSuccess)
                    MainView.HistoryTasks.Refresh();
            }
        }

        #endregion


        #region Работа со складами

        private void StorehouseAssign(StorageUIItem storage)
        {
            var assignWindow = new StorageAssignWindow()
            {
                Repository = Repository,
                StorageItem = storage,
                Owner = Window.GetWindow(this)
            };
            assignWindow.Closed += (o, e) =>
            {
                this.IsEnabled = true;
                var win = (StorageAssignWindow)o;
                if (win.IsPerformed)
                {
                    MainView.Storages.Refresh();
                }
            };
            this.IsEnabled = false;
            assignWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            assignWindow.Show();
        }

        private void StorehouseInventory(StorageUIItem storage)
        {
            var inventoryWindow = new StorageInventoryWindow(MainView)
            {
                Repository = Repository,
                StorageItem = storage,
                Owner = Window.GetWindow(this)
            };
            inventoryWindow.Closed += (o, e) =>
            {
                this.IsEnabled = true;
                var win = (StorageInventoryWindow)o;
                if (win.IsPerformed)
                {
                    MainView.Storages.Refresh();
                }
            };
            this.IsEnabled = false;
            inventoryWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            inventoryWindow.Show();
        }

        #endregion


        #region Работа с автотранспортом
        
        // регистрация нового транспорта
        private void RegisterVehicle()
        {
            var vehicleWindow = new VehicleRegistrationWindow()
            {
                Repository = Repository,
                Owner = Window.GetWindow(this)
            };
            vehicleWindow.Closed += (o, e) =>
            {
                this.IsEnabled = true;
                if (((VehicleRegistrationWindow)o).IsPerformed)
                    MainView.Vehicles.Refresh();
            };
            this.IsEnabled = false;
            vehicleWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            vehicleWindow.Show();
        }

        private void ServeVehicle(VehicleUIItem vehicle)
        {
            var vehicleServeWindow = new VehicleServeWindow(vehicle)
            {
                Repository = Repository,
                Owner = Window.GetWindow(this),
            };
            vehicleServeWindow.Closed += (o, e) =>
            {
                this.IsEnabled = true;
                if (((VehicleServeWindow)o).IsPerformed)
                    MainView.Vehicles.Refresh();
            };
            this.IsEnabled = false;
            vehicleServeWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            vehicleServeWindow.Show();
        }

        private async void DeleteVehicle(VehicleUIItem vehicle)
        {
            var result = MessageBox.Show($"Вы действительно хотите удалить автортранспорт «{vehicle}»?",
                "Потдверждение", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var isSuccess = false;
                try
                {
                    await Repository.DeleteVehicle(vehicle.Id, CancellationToken.None);
                    isSuccess = true;
                }
                catch (Exception exception)
                {
                    logger.Error(exception, $"Ошибка при удалении автотранспорта {vehicle}");
                }
                if (isSuccess)
                    MainView.Vehicles.Refresh();
            }
        }

        private async void DeleteVehicleActivity(VehicleActivity activity)
        {
            var result = MessageBox.Show($"Вы действительно хотите удалить действие «{activity}»?",
                "Потдверждение", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var isSuccess = false;
                try
                {
                    await Repository.DeleteVehicleHistory(new VehicleHistoryDeleteArg()
                    {
                        VehicleId = activity.VehicleId,
                        CreatedAt = activity.CreatedAt

                    }, CancellationToken.None);
                    isSuccess = true;
                }
                catch (Exception exception)
                {
                    logger.Error(exception, $"Ошибка при удалении действие {activity}");
                }
                if (isSuccess)
                    MainView.Vehicles.Refresh();
            }
        }

        private void ClearVehicleRegNumClick(object sender, RoutedEventArgs e)
        {
            MainView.Vehicles.RegNum = null;
        }

        #endregion


        #region События от элементов интерфейса

        private void NewTaskClick(object sender, RoutedEventArgs e)
        {
            NewTask();
        }

        private void DeleteTaskClick(object sender, RoutedEventArgs e)
        {
            var task = MainView.QueuedTasks.SelectedTask;
            DeleteTask(task);
        }

        private void StartTaskClick(object sender, RoutedEventArgs e)
        {
            StartTask(MainView.QueuedTasks.SelectedTask);
        }

        private void SuspendTaskClick(object sender, RoutedEventArgs e)
        {
            var task = (TaskUIItem)((FrameworkElement)sender).DataContext;
            task.IsProcessingPLC = true;
            PLCTasks.SuspendTask(task.Id);
        }

        private void ResumeTaskClick(object sender, RoutedEventArgs e)
        {
            var task = (TaskUIItem)((FrameworkElement)sender).DataContext;
            task.IsProcessingPLC = true;
            PLCTasks.ResumeTask(task.Id);
        }

        private void CompleteTaskClick(object sender, RoutedEventArgs e)
        {
            var task = (TaskUIItem)((FrameworkElement)sender).DataContext;
            StopTask(task);
        }

        private void RevertTaskClick(object sender, RoutedEventArgs e) 
        {
            var task = MainView.HistoryTasks.SelectedTask;
            RevertTask(task);
        }

        private void MatchTaskClick(object sender, RoutedEventArgs e)
        {
            MatchTask();
        }

        private void StorehouseAssignClick(object sender, RoutedEventArgs e)
        {
            var storehouse = MainView.Storages.SelectedStore;
            StorehouseAssign(storehouse);
        }

        private void StorehouseCorrectionClick(object sender, RoutedEventArgs e)
        {
            var storehouse = MainView.Storages.SelectedStore;
            StorehouseInventory(storehouse);
        }

        private void RegisterVehicleClick(object sender, RoutedEventArgs e)
        {
            RegisterVehicle();
        }

        private void ServeVehicleClick(object sender, RoutedEventArgs e)
        {
            var vehicle = MainView.Vehicles.SelectedVehicle;
            ServeVehicle(vehicle);
        }

        private void DeleteVehicleClick(object sender, RoutedEventArgs e)
        {
            var vehicle = MainView.Vehicles.SelectedVehicle;
            DeleteVehicle(vehicle);
        }

        private void DeleteVehicleHistoryClick(object sender, RoutedEventArgs e)
        {
            var activity = MainView.Vehicles.History.SelectedActivity;
            DeleteVehicleActivity(activity);
        }

        public void RefreshReportClick(object sender, RoutedEventArgs e)
        {
            MainView.Reports.RefreshReport();
        }

        private void DataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridPrev.OnSelectionChanged(sender, e);
        }

        private void DataGridSorting(object sender, DataGridSortingEventArgs e)
        {
            DataGridPrev.OnSorting(sender, e);
        }

        #endregion


        #region Обмен с ПЛК

        private void PLCTimerTick(object sender, EventArgs e)
        {
            var activeTasks = MainView.ActiveTasks.ActualResult;
            if (activeTasks?.Count > 0)
            {
                activeTaskIndex %= activeTasks.Count;
                plcTimer.Interval = TimeSpan.FromSeconds(30); // таймаут операции
                plcTasks.GetTaskStatus(MainView.ActiveTasks.ActualResult[activeTaskIndex].Id);
                activeTaskIndex++;
            }
        }

        private async void PLCTaskStatusReceived(object sender, PLCTaskStatusEventArgs e)
        {
            plcTimer.Stop();
            var task = MainView.ActiveTasks.ActualResult?.FirstOrDefault(m => m.Id == e.TaskId);
            if (task == null)
            {
                logger.Warn($"Получен статус по заданию {e.TaskId}, но в списке активных его нет");
            }
            else
            {
                bool isSameStatusPLC = task.StatusPLC == e.Status;
                task.StatusPLC = e.Status;
                if (e.Weight<task.QuantityPLC)
                {
					logger.Info($"Обнаружен сброс счетчика, текущее значение: {task.QuantityPLC}, новое: {e.Weight}. Сохранение накопленного веса.");
                    await RegPLCWeight(task);
				}
                task.QuantityPLC = e.Weight;
                task.IsProcessingPLC = false;

                if (e.Status != PLCTaskStatus.Undetermined)
                {
                    if (e.Status == PLCTaskStatus.RouteRunning)
                    {
                        if (task.Status != ElevatorMES.Domain.Enums.TaskStatus.Running)
                        {
                            // запускаем задание
                            await RunTask(task);
                        }
                    }
                    else
                    {
                        // маршрут по данным ПЛК не запущен
                        if (task.Status != Domain.Enums.TaskStatus.Suspended || !isSameStatusPLC)
                        {
                            // останавливаем маршрут
                            await SuspendTask(task);
                        }
                    }
                }
            }
            plcTimer.Interval = TimeSpan.FromSeconds(5);
            plcTimer.Start();
        }

        private async void PLCTasks_NewTaskReceived(object sender, PLCNewTaskEventArgs e)
        {
            try
            {
                var task = await Repository.CreateTaskPLC(new TaskPLCArg()
                {
                    Nomenclature = e.Nomenclature,
                    Quantity = e.Quantity
                }, CancellationToken.None);
            }
            catch(Exception ex)
            {
                logger.Error(ex, "Ошибка при создании задания от ПЛК");
            }
            MainView.QueuedTasks.Refresh();
        }

        #endregion

    }
}
