﻿using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System.Collections.Generic;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Data.Base;


namespace ElevatorMES.WPF.Models
{
    [Description("Справочник")]
    public class DictionaryViewModel<TItem> : MESDataNoResult<DictionaryUpdateRequest>, IDictionaryViewModel where TItem : BaseSyncEntity
    {
        #region Открытые поля

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => isDataRequestEnabled;
            set
            {
                if (isDataRequestEnabled != value)
                {
                    isDataRequestEnabled = value;
                    IsEnabled = value;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        public int? Id { get; set; }

        public InputItem<string> DisplayName { get; }

        public InputItem<string> Code1C { get; }
        
        public InputItem<string> Type1C { get; }

        public InputItem<string> CodeParent1C { get; }

        public TItem DictItem
        {
            set
            {
                if (value == null)
                {
                    Id = null;
                    DisplayName.Selected = null;
                    Code1C.Selected = null;
                    Type1C.Selected = null;
                    CodeParent1C.Selected = null;
                }
                else
                {
                    Id = value.Id;
                    DisplayName.Selected = value.DisplayName;
                    Code1C.Selected = value.Code1C;
                    Type1C.Selected = value.Type1C;
                    CodeParent1C.Selected = value.CodeParent1C;
                }
            }
        }

        public DictionaryType Type { get;  }

        #endregion


        #region Закрытые поля

        private bool isDataRequestEnabled;

        #endregion


        #region Конструкторы и инициализация

        public DictionaryViewModel(DictionaryType type)
        {
            Type = type;
            DisplayName = new InputItem<string>(StringComparer.OrdinalIgnoreCase);
            Code1C = new InputItem<string>(StringComparer.OrdinalIgnoreCase);
            Type1C = new InputItem<string>(StringComparer.OrdinalIgnoreCase);
            CodeParent1C = new InputItem<string>(StringComparer.OrdinalIgnoreCase);
        }

        #endregion


        #region Проверка данных

        private bool ValidateDisplayName()
        {
            if (DisplayName.Selected?.Length > 4)
            {
                DisplayName.ValidationError = null;
                return true;
            }
            else
            {
                DisplayName.ValidationError = "Наименование должно быть заполнено";
                return false;
            }
        }

        private bool ValidateMultiple(params Func<bool>[] validators)
        {
            bool isValidTotal = true;
            foreach (var validator in validators)
            {
                var isValid = validator();
                isValidTotal = isValidTotal && isValid;
            }
            return isValidTotal;
        }

        private void CleanValidationError()
        {
            DisplayName.ValidationError = null;
        }

        #endregion


        #region MESDataResult<TaskCreateRequest, TaskUIItem>

        protected override DictionaryUpdateRequest GetRequest()
        {
            DictionaryUpdateRequest dictRequest = null;
            if (ValidateMultiple(ValidateDisplayName))
            {
                dictRequest = new DictionaryUpdateRequest(
                    Type, Id, DisplayName.Selected, Code1C.Selected,
                    Type1C.Selected, CodeParent1C.Selected);
            }
            return dictRequest;
        }

        #endregion


        #region IDictionaryViewModel

        public string DictName { get; }

        public bool UpdateItem()
        {
            return NewRequest();
        }

        #endregion
    }
}

