﻿using System;
using System.ComponentModel;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using System.Text;
using NLog.Targets.Wrappers;
using System.Linq;

namespace ElevatorMES.WPF.Models
{
    [Description("Запуск задания")]
    public class TaskStartViewModel : MESDataResult<TaskStartRequest, TaskUIItem>
    {
        #region Открытые поля

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => isDataRequestEnabled;
            set
            {
                if (isDataRequestEnabled != value)
                {
                    isDataRequestEnabled = value;
                    Storages.IsEnabled = value;
                    Points.IsEnabled = value;
                    IsEnabled = value && taskItem != null;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        public StoragesResult Storages { get; }

        public PointsResult Points { get; }

        public InputItems<int, UnitUIItem> LoadingPoint { get; }

        public InputItems<int, UnitUIItem> UnloadingPoint { get; }

        public InputItems<int, StorageUIItem> StorageSource { get; }

        public InputItems<int, StorageUIItem> StorageDest { get; }

        public TaskUIItem TaskItem
        {
            get => taskItem;
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(TaskItem));
                IsEnabled = isDataRequestEnabled;
                if (taskItem != value)
                {
                    taskItem = value;
                    switch (taskItem.Type)
                    {
                        case TaskType.Receiving:
                            UnloadingPoint.IsVisible = true;
                            UnloadingPoint.Selected = taskItem.SourceUnitId ?? 0;
                            StorageDest.IsVisible = true;
                            StorageDest.Selected = taskItem.DestUnitId ?? 0;
                            LoadingPoint.IsVisible = false;
                            StorageSource.IsVisible = false;
                            break;
                        case TaskType.ShippingExt:
						case TaskType.Sleeve:
							LoadingPoint.IsVisible = true;
                            LoadingPoint.Selected = taskItem.DestUnitId ?? 0;
                            UnloadingPoint.IsVisible = false;
                            StorageDest.IsVisible = false;
                            StorageSource.IsVisible = true;
                            StorageSource.Selected = taskItem.SourceUnitId ?? 0;
                            break;
                        case TaskType.SilosMoving:
                            LoadingPoint.IsVisible = false;
                            UnloadingPoint.IsVisible = false;
                            StorageSource.IsVisible = true;
                            StorageSource.Selected = taskItem.SourceUnitId ?? 0;
                            StorageDest.IsVisible = true;
                            StorageDest.Selected = taskItem.DestUnitId ?? 0;
                            break;
                        case TaskType.PlantMoving:
                            LoadingPoint.IsVisible = false;
                            UnloadingPoint.IsVisible = false;
                            StorageDest.IsVisible = false;
                            StorageSource.IsVisible = true;
                            StorageSource.Selected = taskItem.SourceUnitId ?? 0;
                            break;
                        default:
                            LoadingPoint.IsVisible = false;
                            UnloadingPoint.IsVisible = false;
                            StorageDest.IsVisible = false;
                            StorageSource.IsVisible = false;
                            break;
                    }
                    OnPropertyChanged(nameof(TaskItem));
                }
            }
        }

        #endregion


        #region Закрытые поля

        private bool isDataRequestEnabled;

        private TaskUIItem taskItem;

        private UnitUIItem plant;

        #endregion


        #region Конструкторы и инициализация

        public TaskStartViewModel() : base(false)
        {
            Storages = new StoragesResult();
            Storages.Processed += StoragesProcessed;
            Points = new PointsResult();
            Points.Processed += PointsProcessed;
            StorageDest = new InputItems<int, StorageUIItem>();
            StorageSource = new InputItems<int, StorageUIItem>();
            LoadingPoint = new InputItems<int, UnitUIItem>();
            UnloadingPoint = new InputItems<int, UnitUIItem>();
        }

        #endregion


        #region Обрыботка событий

        private void StoragesProcessed(object sender, EventArgs e)
        {
            FillStorages();
        }

        private void PointsProcessed(object sender, EventArgs e)
        {
            FillPoints();
        }

        #endregion


        #region Заполнение элементов ввода списками

        private void FillStorages()
        {
            StorageSource.FillItems(Storages);
            StorageDest.FillItems(Storages);
        }

        private void FillPoints()
        {
            UnloadingPoint.FillItems(Points, m => m.UnitType == UnitType.UnloadingPoint);
            LoadingPoint.FillItems(Points, m => m.UnitType == UnitType.LoadingPoint);
            if (LoadingPoint.Items.Count == 1)
                LoadingPoint.SelectedItem = LoadingPoint.Items[0];
            plant = Points.ActualResult.FirstOrDefault(m => m.UnitType == UnitType.CompoundFeedPlant);
        }

        #endregion


        #region Проверка данных и сохранение

        private bool ValidateStorageSource()
        {
            if (StorageSource.ValidateItem("Не указан склад-источник"))
            {
                var validationError = new StringBuilder();
                if (StorageSource.SelectedItem.NomenclatureId.HasValue)
                {
                    if (StorageSource.SelectedItem.NomenclatureId != TaskItem.NomenclatureId)
                        validationError.Append("Номенклатура в задании не совпадает с номенклатурой на складе. ");
                }
                else
                    validationError.Append("Склад-источник не используется (не назначена номенклатура). ");

                if (!StorageSource.SelectedItem.CanRemove(taskItem.QuantityPlanned))
                    validationError.Append("Недостаточно сырья на склад-источнике. ");

                if (!StorageSource.SelectedItem.SourceEquipmentId.HasValue)
                    validationError.Append("Не связки с начальным оборудованием маршрута. ");

                if (validationError.Length > 0)
                {
                    StorageSource.ValidationError = validationError.ToString();
                    return false;
                }
                return true;
            }
            return false;
        }

        private bool ValidateStorageDest()
        {
            if (StorageDest.ValidateItem("Не указан склад-приемник"))
            {
                var validationError = new StringBuilder();
                if (StorageDest.SelectedItem.NomenclatureId.HasValue)
                {
                    if (StorageDest.SelectedItem.NomenclatureId != TaskItem.NomenclatureId)
                        validationError.Append("Номенклатура в задании не совпадает с номенклатурой на складе. ");
                }
                else
                    validationError.Append("Склад-приёмник не используется (не назначена номенклатура). ");

                if (!StorageDest.SelectedItem.CanAdd(taskItem.QuantityPlanned))
                    validationError.Append("Слишком много сырья на складе-приемнике. ");

                if (!StorageDest.SelectedItem.DestinationEquipmentId.HasValue)
                    validationError.Append("Не связки с конечным оборудованием маршрута. ");

                if (validationError.Length > 0)
                {
                    StorageDest.ValidationError = validationError.ToString();
                    return false;
                }
                return true;
            }
            return false;
        }

        private bool ValidateUnloadingPoint()
        {
            if (UnloadingPoint.ValidateItem("Не указана точка выгрузки"))
            {
                if (!UnloadingPoint.SelectedItem.SourceEquipmentId.HasValue)
                {
                    UnloadingPoint.ValidationError = "Не связки с начальным оборудованием маршрута. ";
                    return false;
                }
                return true;
            }
            return false;
        }

        private bool ValidateLoadingPoint()
        {
            if (LoadingPoint.ValidateItem("Не указана точка отгрузки"))
            {
                if (!LoadingPoint.SelectedItem.DestinationEquipmentId.HasValue)
                {
                    LoadingPoint.ValidationError = "Не связки с конечным оборудованием маршрута. ";
                    return false;
                }
                return true;
            }
            return false;
        }

        private bool ValidatePlant()
        {
            if (plant == null)
            {
				StorageSource.ValidationError = "Не найден завод в справочнике";
				return false;
			}
            return true;
		}

        private bool ValidateMultiple(params Func<bool>[] validators)
        {
            bool isValidTotal = true;
            foreach (var validator in validators)
            {
                var isValid = validator();
                isValidTotal = isValidTotal && isValid;
            }
            return isValidTotal;
        }

        protected override TaskStartRequest GetRequest()
        {
            TaskStartArg arg = null;
            switch (taskItem.Type)
            {
                case TaskType.Receiving:
                    if (ValidateMultiple(ValidateUnloadingPoint, ValidateStorageDest))
                    {
                        arg = new TaskStartArg()
                        {
                            TaskId = TaskItem.Id,
                            SourceUnitId = UnloadingPoint.Selected,
                            DestUnitId = StorageDest.Selected
                        };
                    }
                    break;

                case TaskType.ShippingExt:
				case TaskType.Sleeve:
					if (ValidateMultiple(ValidateLoadingPoint, ValidateStorageSource))
                    {
                        arg = new TaskStartArg()
                        {
                            TaskId = TaskItem.Id,
                            SourceUnitId = StorageSource.Selected,
                            DestUnitId = LoadingPoint.Selected
                        };
                    }
                    break;

                case TaskType.SilosMoving:
                    if (ValidateMultiple(ValidateStorageSource, ValidateStorageDest))
                    {
                        arg = new TaskStartArg()
                        {
                            TaskId = TaskItem.Id,
                            SourceUnitId = StorageSource.Selected,
                            DestUnitId = StorageDest.Selected
                        };
                    }
                    break;

                case TaskType.PlantMoving:
                    if (ValidateMultiple(ValidatePlant, ValidateStorageSource))
                    {
                        arg = new TaskStartArg()
                        {
                            TaskId = TaskItem.Id,
                            SourceUnitId = StorageSource.Selected,
                            DestUnitId = plant?.UnitId ?? 0
                        };
                    }
                    break;

                default:
                    arg = new TaskStartArg()
                    {
                        TaskId = TaskItem.Id,
                        SourceUnitId = taskItem.SourceUnitId,
                        DestUnitId = taskItem.DestUnitId
                    };
                    break;
            }
            return arg == null ? null : new TaskStartRequest(arg);
        }

        #endregion
    }
}