﻿using ElevatorMES.Utilites.Helpers;
using NLog;
using System;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;

namespace ElevatorMES.WPF.Models
{
    public abstract class MESDataResult<TRequest, TResult> 
        : MESDataProcess<TRequest>, IRefreshable, IMESDataResult<TResult>
        where TRequest : IEquatable<TRequest>, IAbandonable
    {
        #region Закрытые поля

        // текущие актуальные данные
        private TResult actualData;

        #endregion


        #region Открытые свойства и события
        
        public Func<TRequest, Task<TResult>> GetDataTask { get; set; }

        public TResult ActualResult
        {
            get => actualData;
            set
            {
                actualData = PrepareResult(value);
                OnPropertyChanged(nameof(ActualResult));
            }
        }

        public bool RefreshOnEnable { get; }

        #endregion


        #region Конструкторы и иницилизация

        public MESDataResult():this(true)
        {

            
        }

        public MESDataResult(bool refreshOnEnable)
        {
            RefreshOnEnable = refreshOnEnable;
        }

        #endregion


        #region Обрыботка запросов

        protected override void OnDefaultData()
        {
            ActualResult = default;
        }

        protected async override void ProcessRequests()
        {
            while (currentRequest != null)
            {
                IsLoading = true;
                var request = currentRequest;
                string error = null;
                TResult result = default;
                var timeFrom = DateTime.Now;
                try
                {
                    var resultTask = GetDataTask?.Invoke(request);
                    if (resultTask == null)
                        logger.Debug($"Нет задачи для заполнения «{dataDesc}»");
                    else
                    {
                        result = await resultTask;
                        logger.Debug($"Запрос для «{dataDesc}» выполнен за {(DateTime.Now - timeFrom).TotalMilliseconds:0.0} мсек");
                    }
                }
                catch (Exception exception)
                {
                    if (!request.IsAbandoned)
                    {
                        var message = $"Ошибка получения данных для «{dataDesc}»";
                        logger.Error(exception, message);
                        error = exception.ExtractMessage();
                    }
                }
                IsLoading = false;
                if (!request.IsAbandoned && queuedRequest == null)
                {
                    ActualResult = result;
                    ActualError = error;
                }
                else
                {
                    ActualResult = default;
                    ActualError = null;
                }
                currentRequest = queuedRequest;
                queuedRequest = default;
            }
            OnProcessed();
        }

        protected override void OnEnableChanged()
        {
            base.OnEnableChanged();
            if (RefreshOnEnable)
                NewRequest();
        }

        protected virtual TResult PrepareResult(TResult source)
        {
            return source;
        }

        #endregion


        #region IRefreshable

        public void Refresh()
        {
            if (IsEnabled)
                NewRequest();
        }

        #endregion

    }
}
