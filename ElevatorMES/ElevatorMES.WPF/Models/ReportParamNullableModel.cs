﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class ReportParamNullableModel<TValue> : ReportParamModel where TValue : struct, IEquatable<TValue>
    {
        public TValue? SelectedValue
        {
            get => selectedValue;
            set
            {
                if (!(value.HasValue && selectedValue.HasValue &&
                    value.Value.Equals(selectedValue.Value) ||
                    !value.HasValue && !selectedValue.HasValue))
                {
                    var isPrevValid = IsValidValue;
                    selectedValue = value;
                    OnPropertyChanged(nameof(SelectedValue));
                    if (isPrevValid != IsValidValue)
                        OnPropertyChanged(nameof(IsValidValue));
                    OnValueChanged();
                }
            }
        }

        public override object Value => SelectedValue;

        public override bool IsValidValue
        {
            get => AllowNull ? true : selectedValue.HasValue;
        }

        public override string[] ViewerValues => new string[] { selectedValue.HasValue ? selectedValue.Value.ToString() : null};

        private TValue? selectedValue;

        public ReportParamNullableModel(string name, string title, bool allowNull) :
            base(name, title, allowNull, false)
        {

        }
    }
}
