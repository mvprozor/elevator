﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class ReportParamListModel<TValue> : ReportParamNullableModel<TValue> where TValue : struct, IEquatable<TValue>
    {
        public IList<ReportParamListItemModel<TValue>> Values 
        {
            get => values;
            set
            {
                if (values != null)
                {
                    foreach (var val in values)
                        val.SelectedChanged -= ItemSelectedChanged;
                }
                if (values != value)
                {
                    values = value;
                    foreach (var val in values)
                        val.SelectedChanged += ItemSelectedChanged;
                    OnPropertyChanged(nameof(Values));
                }
            }
        }

        protected virtual void ItemSelectedChanged(object sender, EventArgs e)
        {

        }

        private IList<ReportParamListItemModel<TValue>> values;

        public ReportParamListModel(string name, string title, bool allowNull) :
            base(name, title, allowNull)
        {
        }

    }
}
