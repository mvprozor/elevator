﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public interface IMESDataResult<TResult>
    {
        TResult ActualResult { get; }

        string ActualError { get; }
    }
}
