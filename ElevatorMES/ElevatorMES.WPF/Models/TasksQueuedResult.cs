﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using System.ComponentModel;
using ElevatorMES.Domain.Model.UI;

namespace ElevatorMES.WPF.Models
{
    [Description("Задания в очереди")]
    public class TasksQueuedResult : MESDataResult<EmptyRequest, List<TaskUIItem>>, ITabViewModel, IPrevSelection, IPrevSorting
    {
        public TaskUIItem SelectedTask
        {
            get => selectedTask;
            set
            {
                if (selectedTask != value)
                {
                    prevSelectedTaskId = selectedTask?.Id;
                    selectedTask = value;
                    OnPropertyChanged(nameof(SelectedTask));
                }
            }
        }

        private int? prevSelectedTaskId;

        public string PrevSorting { get; set; }

        private TaskUIItem selectedTask;

        protected override EmptyRequest GetRequest()
        {
            return new EmptyRequest();
        }

        public object GetPrevSelection()
        {
            return ActualResult?.Find( m => m.Id == prevSelectedTaskId);
        }

        protected override List<TaskUIItem> PrepareResult(List<TaskUIItem> source)
        {
            return String.IsNullOrWhiteSpace(PrevSorting) ?
                source : source.AsQueryable<TaskUIItem>().OrderBy(PrevSorting).ToList();
        }
    }
}
