﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Типы транспорта")]
    public class VehicleTypesResult : MESDataResult<EmptyRequest, IList<VehicleType>>
    {
        protected override EmptyRequest GetRequest()
        {
            return new EmptyRequest();
        }
    }
}
