﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class InputItems<TSelected, TItem> : InputItem<TSelected>
    {
        public TItem SelectedItem
        {
            get => selectedItem;
            set
            {
                if (!EqualityComparer<TItem>.Default.Equals(selectedItem, value))
                {
                    selectedItem = value;
                    OnPropertyChanged(nameof(SelectedItem));
                }
            }
        }

        public IList<TItem> Items
        {
            get => items;
            set
            {
                if (items != value)
                {
                    items = value;
                    OnPropertyChanged(nameof(Items));
                }
            }
        }

        private IList<TItem> items;

        private TItem selectedItem;

        public void FillItems(IMESDataResult<IList<TItem>> dataResult, Func<TItem, bool> predicate = null)
        {
            var resultItems = dataResult.ActualResult;
            if (resultItems != null && predicate!=null)
            {
                resultItems = resultItems.Where(predicate).ToArray();
            }
            Items = resultItems;
            ValidationError = dataResult.ActualError;
        }

        public bool ValidateItem(string error)
        {
            if (SelectedItem == null)
            {
                ValidationError = error;
                return false;
            }
            else
            {
                ValidationError = null;
                return true;
            }
        }

        public bool SelectNextItem()
        {
            var index = items.IndexOf(selectedItem);
            index++;
            if (index < items.Count)
            {
                SelectedItem = items[index];
                return true;
            }
            return false;
        }
    }
}
