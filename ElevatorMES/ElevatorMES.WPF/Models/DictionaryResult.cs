﻿using System;
using ElevatorMES.Domain.Data.Base;
using ElevatorMES.Domain.Enums;
using System.Collections.Generic;
using System.ComponentModel;
using ElevatorMES.Domain.Model.Args;

namespace ElevatorMES.WPF.Models
{
    [Description("Справочники")]
    public class DictionaryResult<TItem> : MESDataResult<DictionaryRequest, IList<TItem>>, ITabViewModel, IDictionaryView where TItem : BaseSyncEntity
    {
        public DictionaryType Type
        {
            get;
        }

        public TItem SelectedItem
        {
            get => selectedItem;
            set
            {
                if (selectedItem?.Id != value?.Id)
                {
                    selectedItem = value;
                    OnPropertyChanged(nameof(SelectedItem));
                }
            }
        }
        private TItem selectedItem;

        public bool? IsDeleted
        {
            get => isDeleted;
            set
            {
                if (isDeleted != value)
                {
                    isDeleted = value;
                    NewRequest();
                    OnPropertyChanged(nameof(IsDeleted));
                }
            }
        }
        private bool? isDeleted = false;

        public string SearchText
        {
            get => searchText;
            set
            {
                if (String.Compare(searchText, value, true) != 0)
                {
                    searchText = value;
                    NewRequest();
                    OnPropertyChanged(nameof(SearchText));
                }
            }
        }
        private string searchText;

        protected override DictionaryRequest GetRequest()
        {
            return new DictionaryRequest(isDeleted, SearchText);
        }

        public Func<DictionaryUpdateRequest, System.Threading.Tasks.Task> UpdateDataTask { get; set; }

        public Action<DeleteDictArg, Func<bool>> DeleteAction { get; set; }

        public Action<DeleteDictArg, Func<bool>> RestoreAction { get; set; }

        public IDictionaryViewModel GetViewModel(bool isNew, Action onSuccess)
        {
            var dictView = new DictionaryViewModel<TItem>(Type)
            {
                ProcessDataTask = UpdateDataTask,
                DictItem = isNew ? null : SelectedItem,
                IsDataRequestEnabled = true,
            };
            dictView.Processed += (s, e) =>
            {
                var dv = (DictionaryViewModel<TItem>)s;
                if (String.IsNullOrWhiteSpace(dv.ActualError))
                    onSuccess();
            };
            return dictView;
        }

        public void Delete()
        {
            if (SelectedItem != null)
                DeleteAction(new DeleteDictArg()
                {
                    Id = SelectedItem.Id,
                    Type = Type
                }, NewRequest);
        }

        public void Restore()
        {
            if (SelectedItem != null)
                RestoreAction(new DeleteDictArg()
                {
                    Id = SelectedItem.Id,
                    Type = Type
                }, NewRequest);
        }

        public void ClearSearch()
        {
            SearchText = null;
            NewRequest();
        }

        public DictionaryResult(DictionaryType type)
        {
            Type = type;
        }
       
    }
}
