﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using ElevatorMES.Utilites.Helpers;
using ElevatorMES.Domain.Scales;

namespace ElevatorMES.WPF.Models
{
    public class InputScalesGroup : INotifyPropertyChanged
    {
        #region Открытые поля

        public bool IsVisible
        {
            get => isVisible;
            set
            {
                if (isVisible != value)
                {
                    isVisible = value;
                    Array.ForEach(Scales, m => m.IsActive = value);
                    OnPropertyChanged(nameof(IsVisible));
                }
            }
        }

        public InputScales[] Scales { get; }

        #endregion


        #region Закрытые поля

        private bool isVisible;

        #endregion


        #region Конструкторы и инициализация

        public InputScalesGroup(InputScales[] scales)
        {
            Scales = scales ?? throw new ArgumentNullException(nameof(scales));
        }

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
