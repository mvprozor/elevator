﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class ReportParamDateModel : ReportParamNullableModel<DateTime>
    {
        public ReportParamDateModel(string name, string title, bool allowNull)
            : base(name, title, allowNull)
        {
        }
    }
}
