﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using ElevatorMES.Domain.Data;
using System.Linq.Dynamic.Core;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Helpers;
using ElevatorMES.WPF.Enums;
using System.Linq;

namespace ElevatorMES.WPF.Models
{
    [Description("Обмен с ERP")]
    public class TransmitLogResult : MESDataResult<TransmitLogRequest, List<TransmitLog>>, ITabViewModel, IPrevSelection, IPrevSorting
    {
        public IList<KeyValuePair<ViewPeriodEnum, string>> ViewPeriods { get; } =
            EnumExtension.ToListKeyDesc<ViewPeriodEnum>();

        public ViewPeriodEnum SelectedViewPeriod
        {
            get => selectedViewPeriod;
            set
            {
                if (value != selectedViewPeriod)
                {
                    selectedViewPeriod = value;
                    NewRequest();
                    OnPropertyChanged(nameof(SelectedViewPeriod));
                }
            }
        }

        public DateTime? DateFrom
        {
            get => dateFrom;
            set
            {
                if (dateFrom != value)
                {
                    dateFrom = value;
                    NewRequest();
                    OnPropertyChanged(nameof(DateFrom));
                }
            }
        }

        public DateTime? DateTill
        {
            get => dateTill;
            set
            {
                if (dateTill != value)
                {
                    dateTill = value;
                    NewRequest();
                    OnPropertyChanged(nameof(DateTill));
                }

            }
        }

        public string SearchText
        {
            get => searchText;
            set
            {
                if (String.Compare(searchText, value, true) != 0)
                {
                    searchText = value;
                    NewRequest();
                    OnPropertyChanged(nameof(SearchText));
                }
            }
        }

        public TransmitLog SelectedTransmit
        {
            get => selectedTransmit;
            set
            {
                if (selectedTransmit != value)
                {
                    prevSelectedCreatedAt = selectedTransmit?.CreatedAt;
                    selectedTransmit = value;
                    OnPropertyChanged(nameof(SelectedTransmit));
                }
            }
        }

        private ViewPeriodEnum selectedViewPeriod;
        private DateTime? dateFrom;
        private DateTime? dateTill;
        private string searchText;
        private TransmitLog selectedTransmit;
        private DateTime? prevSelectedCreatedAt;
        public string PrevSorting { get; set; }

        public TransmitLogResult()
        {
            dateFrom = DateTime.Today;
            dateTill = DateTime.Today.AddDays(1);
        }

        protected override TransmitLogRequest GetRequest()
        {
            SelectedViewPeriod.ToPeriod(DateTill, DateFrom, out var begin, out var end);
            return new TransmitLogRequest(begin, end, SearchText);
        }

        public object GetPrevSelection()
        {
            return ActualResult?.Find(m => m.CreatedAt == prevSelectedCreatedAt);
        }

        protected override List<TransmitLog> PrepareResult(List<TransmitLog> source)
        {
            return String.IsNullOrWhiteSpace(PrevSorting) ?
                source : source.AsQueryable<TransmitLog>().OrderBy(PrevSorting).ToList();
        }

        #region ITabViewModel
        #endregion
    }
}
