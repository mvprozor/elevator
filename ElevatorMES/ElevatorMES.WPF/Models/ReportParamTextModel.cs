﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class ReportParamTextModel : ReportParamModel
    {
        public string Text { get; set; }

        public override object Value => Text;
        
        public override bool IsValidValue
        {
            get => AllowNull ? AllowEmpty || Text==null :
                Text != null && (AllowEmpty || !String.IsNullOrWhiteSpace(Text));
        }

        public override string[] ViewerValues => new string[] { Text };

        public ReportParamTextModel(string name, string title, bool allowNull, bool allowEmpty) 
            : base(name, title, allowNull, allowEmpty)
        {

        }
    }
}
