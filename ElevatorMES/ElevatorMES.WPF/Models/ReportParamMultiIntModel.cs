﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class ReportParamMultiIntModel : ReportParamMultiValueModel<int>
    {
        public ReportParamMultiIntModel(string name, string title, bool allowNull) :
            base(name, title, allowNull)
        {

        }
    }
}
