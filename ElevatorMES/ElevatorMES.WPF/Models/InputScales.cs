﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using ElevatorMES.Utilites.Helpers;
using ElevatorMES.Domain.Scales;
using ElevatorMES.WPF.Enums;

namespace ElevatorMES.WPF.Models
{
    public class InputScales : INotifyPropertyChanged
    {
        #region Открытые поля

        public string Name { get; }

        public decimal? Weight 
        {
            get => weight;
            set
            {
                if (weight!=value)
                {
                    weight = value;
                    Error = null;
                    if (value.HasValue && IsActive)
                        State = ScalesInputStateEnum.Receiving;
                    OnPropertyChanged(nameof(Weight));
                }
            }
        }

        public bool? IsStable
        {
            get => isStable;
            set
            {
                if (isStable != value)
                {
                    isStable = value;
                    OnPropertyChanged(nameof(IsStable));
                }
            }
        }

        public string Error
        {
            get => error;
            set
            {
                if (!String.Equals(error, value, StringComparison.OrdinalIgnoreCase))
                {
                    error = value;
                    OnPropertyChanged(nameof(Error));
                    if (!String.IsNullOrWhiteSpace(value) && IsActive)
                        State = ScalesInputStateEnum.Failed;
                }
            }
        }
        
        public ScalesInputStateEnum State
        {
            get => state;
            set
            {
                if (state!=value)
                {
                    state = value;
                    OnPropertyChanged(nameof(State));
                }
            }
        }

        public bool IsActive
        {
            get => scalesSession != null;
            set
            {
                var isActive = IsActive;
                if (value)
                    scalesSession = scalesSession ?? _scales.Activate(OnData, OnError);
                else
                {
                    scalesSession?.Deactivate();
                    scalesSession = null;
                    Error = null;
                    Weight = null;
                    IsStable = null;
                    State = ScalesInputStateEnum.Initializing;
                }
                if (isActive != IsActive)
                    OnPropertyChanged(nameof(IsActive));
            }
        }

        #endregion


        #region Закрытые поля

        private bool? isStable;
        private decimal? weight;
        private string error;
        private ScalesInputStateEnum state;
        private IScalesSession scalesSession;
        private IScales _scales { get; }

        #endregion


        #region Конструкторы и инициализация

        public InputScales(string name, IScales scales)
        {
            Name = name;
            _scales = scales;
        }

        #endregion


        #region События от весов

        private void OnData(ScalesEventData eventData)
        {
            var lastData = eventData.Data[eventData.Data.Length - 1];
            Weight = lastData.Weight;
            IsStable = lastData.IsStable;
        }

        private void OnError(ScalesEventError eventError)
        {
            Error = eventError.Error;
        }

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion


        #region Перегружаемые базовые методы

        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}
