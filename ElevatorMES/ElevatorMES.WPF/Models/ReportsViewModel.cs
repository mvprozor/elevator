﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.WPF.Enums;
using Microsoft.Reporting.Map.WebForms.BingMaps;
using Microsoft.Reporting.WinForms;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ElevatorMES.WPF.Models
{
    public class ReportsViewModel : ITabViewModel, INotifyPropertyChanged
    {
        public ObservableCollection<ReportToolModel> ReportParams { get; }

        public ReportViewer Viewer { get; set; }

        public delegate Task<ReportParamItemsModel<TValue>> GetParamValuesDelegate<TValue>(
            string reportName, string paramName, IDictionary<string, object> queryValues) where TValue : struct;

        public delegate Task<IList> GetMainDataSetDelegate(string reportName, string dataSetName, IDictionary<string, object> queryValues);

        public GetParamValuesDelegate<int> GetParamIntValues { get; set; }

        public GetMainDataSetDelegate GetMainDataSet { get; set; }

        private const string NullValue = "(NULL)";

        private readonly List<ReportParamListModel<int>> dynamicLists;

        private readonly List<ReportDataSource> paramDataSources;

        private readonly ReportsListModel reports;

        private readonly ReportRefreshModel refresh;

        public bool IsLoading
        {
            get => isLoading;
            set
            {
                if (value!=isLoading)
                {
                    isLoading = value;
                    OnPropertyChanged(nameof(IsLoading));
                }
            }
        }

        private bool isLoading;

        protected static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public ReportsViewModel()
        {
            ReportParams = new ObservableCollection<ReportToolModel>();
            dynamicLists = new List<ReportParamListModel<int>>(4);
            paramDataSources = new List<ReportDataSource>(4);
            reports = new ReportsListModel();
            reports.ReportChanged += ReportsReportChanged;
            ReportParams.Add(reports);
            refresh = new ReportRefreshModel();
            ReportParams.Add(refresh);
        }

        private void ReportsReportChanged(object sender, EventArgs e)
        {
            if (Viewer != null)
            {
                Viewer.Clear();
                if (reports.Selected != null)
                {
                    try
                    {
                        PrepareReport(Viewer, reports.Selected);
                    }
                    catch(Exception ex)
                    {
                        logger.Error(ex, "Ошибка при подготовке отчета");
                    }
                }
            }
        }


        #region Загрузка и заполнение параметров

        private void PrepareReport(ReportViewer viewer, ReportModel report)
        {
            logger.Info($"Загрузка отчета: {report.ReportResource}");
            viewer.LocalReport.ReportEmbeddedResource = $"{report.ReportResource}";
            var reportParameters = viewer.LocalReport.GetParameters();
            dynamicLists.Clear();
            paramDataSources.Clear();
            while (ReportParams.Count > 2)
                ReportParams.RemoveAt(ReportParams.Count - 2);

            foreach(var reportParameter in reportParameters)
            {
                if (reportParameter.PromptUser)
                {
                    var item = GetReportParam(reportParameter);
                    ReportParams.Insert(ReportParams.Count-1, item);
                    item.ValueChanged += ParamValueChanged;
                }
            }
            dynamicLists.ForEach(FillDynamic);
            viewer.LocalReport.DataSources.Clear();
            foreach (var dataSourceName in Viewer.LocalReport.GetDataSourceNames())
                viewer.LocalReport.DataSources.Add(new ReportDataSource(dataSourceName, (IEnumerable)null));
            refresh.IsEnabled = CanRefresh();
        }

        private ReportParamModel GetReportParam(ReportParameterInfo paramInfo)
        {
            ReportParamModel reportParam = null;
            switch (paramInfo.DataType)
            {
                case ParameterDataType.DateTime:
                    var reportDate = new ReportParamDateModel(paramInfo.Name, paramInfo.Prompt, paramInfo.Nullable);
                    if (paramInfo.Values?.Count > 0)
                    {
                        if (DateTime.TryParse(paramInfo.Values[0], out var selectedDate))
                            reportDate.SelectedValue = selectedDate;
                    }
                    reportParam = reportDate;
                    break;

                case ParameterDataType.Integer:
                    if (paramInfo.ValidValues==null)
                    {
                        if (paramInfo.Values?.Count>0)
                        {
                            var reportInt =
                                new ReportParamIntModel(paramInfo.Name, paramInfo.Prompt, paramInfo.Nullable);
                            if (int.TryParse(paramInfo.Values[0], out var intValue))
                                reportInt.SelectedValue = intValue;
                            reportParam = reportInt;
                        }
                        else
                        {
                            // динамическое заполнение
                            if (paramInfo.MultiValue)
                            {
                                var reportListMulti = new ReportParamMultiIntModel(paramInfo.Name, paramInfo.Prompt, paramInfo.Nullable);
                                dynamicLists.Add(reportListMulti);
                                reportParam = reportListMulti;
                            }
                            else
                            {
                                var reportList = new ReportParamListIntModel(paramInfo.Name, paramInfo.Prompt, paramInfo.Nullable);
                                dynamicLists.Add(reportList);
                                reportParam = reportList;
                            }
                        }
                    }
                    else
                    {
                        var reportList = new ReportParamListIntModel(paramInfo.Name, paramInfo.Prompt, paramInfo.Nullable);
                        reportList.Values =
                            paramInfo.ValidValues.Select(m => new ReportParamListItemModel<int>(
                                String.IsNullOrWhiteSpace(m.Value) || String.Compare(NullValue, m.Value, true) == 0 ? null : (int?)int.Parse(m.Value), m.Label)).ToArray();
                        if (paramInfo.Values?.Count > 0 && int.TryParse(paramInfo.Values[0], out var intValue))
                            reportList.SelectedValue = intValue;
                        reportParam = reportList;
                    }
                    break;
            }

            if (reportParam == null)
            {
                var reportText=
                    new ReportParamTextModel(paramInfo.Name, paramInfo.Prompt, paramInfo.Nullable, paramInfo.AllowBlank);
                var defaultText = paramInfo.Values?.Count > 0 ? paramInfo.Values[0] : null;
                if (String.Compare(NullValue, defaultText, true) == 0)
                    defaultText = null;
                reportText.Text = defaultText;
                reportParam = reportText;
            }
            if (paramInfo.Dependencies?.Count > 0)
            {
                var dependencyNames = paramInfo.Dependencies.Select(m => m.Name);
                reportParam.AddDependencies(ReportParams.OfType<ReportParamModel>().Where(m => dependencyNames.Contains(m.Name)));
            }
            return reportParam;
        }

        #endregion


        #region Обновление и запрос данных
        
        private void ParamValueChanged(object sender, EventArgs args)
        {
            var reportParam = (ReportParamModel)sender;
            foreach (var list in dynamicLists)
                if (list.Dependencies.Contains(reportParam))
                    FillDynamic(list);
            refresh.IsEnabled = CanRefresh();
        }

        private IDictionary<string, object> GetQueryValues(IEnumerable<ReportParamModel> reportParams)
        {
            return reportParams.ToDictionary(m => m.Name, m => m.Value);
        }

        private async void FillDynamic(ReportParamListModel<int> list)
        {
            if ((list.Dependencies.Count==0 || list.Dependencies.All(m => m.IsValidValue)) && GetParamIntValues != null)
            {
                var reportName = reports.Selected.Name;
                try
                {
                    var listItems = await GetParamIntValues(reportName, list.Name, GetQueryValues(list.Dependencies));
                    if (listItems == null)
                    {
                        logger.Error($"Не предоставлен набор данных для параметра {list.Name} отчета {reportName}");
                        return;
                    }
                    list.Values = listItems.Values;
                    if (listItems.DataSource != null)
                        paramDataSources.Add(listItems.DataSource);
                }
                catch(Exception exception)
                {
                    logger.Error(exception, $"Ошибка при заполнении данных для параметра {list.Name} отчета {reportName}");
                }
            }
        }

        private bool CanRefresh()
        {
            return GetMainDataSet != null && Viewer != null && !IsLoading
                && ReportParams.OfType<ReportParamModel>().All(m => m.IsValidValue);
        }

        public async void RefreshReport()
        {
            if (CanRefresh())
            {
                IsLoading = true;
                refresh.IsEnabled = false;
                reports.IsEnabled = false;
                var reportName = reports.Selected.Name;
                var dataSources = Viewer.LocalReport.DataSources;
                foreach (var paramDataSource in paramDataSources)
                {
                    var dataSource = dataSources[paramDataSource.Name];
                    if (dataSource != null)
                        dataSource.Value = paramDataSource.Value;
                    else
                        dataSources.Add(paramDataSource);
                };
                var reportParams = ReportParams.OfType<ReportParamModel>();
                var paramValues = GetQueryValues(reportParams);
                Viewer.LocalReport.SetParameters(reportParams.Select(m => new ReportParameter(m.Name, m.ViewerValues)));
                foreach (var dataSource in dataSources)
                {
                    try
                    {
                        if (!paramDataSources.Exists(m => m.Name==dataSource.Name))
                            dataSource.Value = await GetMainDataSet(reportName, dataSource.Name, paramValues);
                    }
                    catch (Exception exception)
                    {
                        logger.Error(exception, $"Ошибка заполнения набора данных {dataSource.Name} отчета {reportName}");
                    }
                }
                Viewer?.RefreshReport();
                IsLoading = false;
                refresh.IsEnabled = CanRefresh();
                reports.IsEnabled = true;
            }
        }

        #endregion


        #region ITabViewModel
        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
