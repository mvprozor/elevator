﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using System;
using System.ComponentModel;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос регистрация транспорта")]
    public class VehicleRegisterRequest : MESDataRequest, IEquatable<VehicleRegisterRequest>
    {
        public int VehicleTypeId { get; }
        public string VehicleRegNum { get; }
        public DateTime RegisteredAt { get; set; }
        public int? NomenclatureId { get; }
        public decimal? Weight { get; set; }
        public string Comment { get; set; }

        public VehicleRegisterRequest(int vehicleTypeId, string vehicleRegNum, DateTime registeredAt,
             string comment = null, int? nomenclatureId = null, decimal? weight = null)
        {
            VehicleTypeId = vehicleTypeId;
            VehicleRegNum = vehicleRegNum;
            RegisteredAt = registeredAt;
            Comment = comment;
            NomenclatureId = nomenclatureId;
            Weight = weight;
        }

        public bool Equals(VehicleRegisterRequest other)
        {
            return VehicleTypeId == other.VehicleTypeId && RegisteredAt == other.RegisteredAt
                && String.Compare(VehicleRegNum, other.VehicleRegNum, StringComparison.OrdinalIgnoreCase) == 0
                && NomenclatureId == other.NomenclatureId && Weight == other.Weight
                && String.Compare(Comment, other.Comment, StringComparison.OrdinalIgnoreCase) == 0;
        }

    }
}
