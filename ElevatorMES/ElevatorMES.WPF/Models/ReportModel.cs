﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class ReportModel
    {
        public string Name { get; }
        public string Title { get; }
        public string ReportResource { get; }

        public ReportModel(string name, string title, string reportResource)
        {
            Name = name;
            Title = title;
            ReportResource = reportResource;
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
