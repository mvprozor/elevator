﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.UI;
using NLog;
using System;
using System.ComponentModel;
using System.Threading;

namespace ElevatorMES.WPF.Models
{
    public class SetupViewModel : ITabViewModel, INotifyPropertyChanged
    {
        #region Открытые поля

        /// <summary>
        /// Текущая закладка
        /// </summary>
        public ITabViewModel SelectedTab
        {
            get => _selectedTab;
            set
            {
                if (_selectedTab != value)
                {
                    _selectedTab = value;
                    OnPropertyChanged(nameof(SelectedTab));
                    (_selectedTab as IRefreshable)?.Refresh();
                }
            }
        }

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => _isDataRequestEnabled;
            set
            {
                if (_isDataRequestEnabled != value)
                {
                    _isDataRequestEnabled = value;
                    UnitEquipment.IsEnabled = value;
                    NomenclaturePriority.IsEnabled = value;
                    Nomenclature.IsEnabled = value;
                    Organization.IsEnabled = value;
                    Contragent.IsEnabled = value;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        public UnitEquipmentsResult UnitEquipment { get; }

        public NomenclaturePriorityResult NomenclaturePriority { get; }

        public DictionaryResult<Nomenclature> Nomenclature { get; }

        public DictionaryResult<Organization> Organization { get; }

        public DictionaryResult<Contragent> Contragent { get; }

        public Func<UnitEquipmentUIItem, CancellationToken, System.Threading.Tasks.Task> UpdateLink { get; set; }

        public Func<NomenclaturePriorityUIItem, CancellationToken, System.Threading.Tasks.Task> UpdateNomenclaturePriority { get; set; }

        #endregion


        #region Закрытые поля

        private bool _isDataRequestEnabled;

        private ITabViewModel _selectedTab;

        // логирование
        protected static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        #region Конструкторы и иницилизация

        public SetupViewModel()
        {
            UnitEquipment = new UnitEquipmentsResult();
            UnitEquipment.Processed += UnitEquipmentProcessed;
            NomenclaturePriority = new NomenclaturePriorityResult();
            NomenclaturePriority.Processed += NomenclaturePriorityProcessed;
            Nomenclature = new DictionaryResult<Nomenclature>(DictionaryType.Nomenclature);
            Organization = new DictionaryResult<Organization>(DictionaryType.Organization);
            Contragent = new DictionaryResult<Contragent>(DictionaryType.Contragent);
        }

        #endregion


        #region События

        private void UnitEquipmentProcessed(object sender, EventArgs e)
        {
            if (UnitEquipment.ActualResult!=null)
            {
                foreach(var link in UnitEquipment.ActualResult)
                    link.Processing += LinkProcessing;
            }
        }

        private async void LinkProcessing(object sender, EventArgs e)
        {
            var link = (UnitEquipmentUIItem)sender;
            try
            {
                await UpdateLink(link, CancellationToken.None);
                link.CommitChanges();
            }
            catch(Exception exception)
            {
                logger.Error(exception, $"Ошибка обновления связки {link}");
                link.RollbackChanges();
            }
        }

        private void NomenclaturePriorityProcessed(object sender, EventArgs e)
        {
            if (NomenclaturePriority.ActualResult != null)
            {
                foreach (var priority in NomenclaturePriority.ActualResult)
                    priority.Processing += NomenclaturePriorityProcessing;
            }
        }

        private async void NomenclaturePriorityProcessing(object sender, EventArgs e)
        {
            var priority = (NomenclaturePriorityUIItem)sender;
            try
            {
                await UpdateNomenclaturePriority(priority, CancellationToken.None);
                priority.CommitChanges();
            }
            catch (Exception exception)
            {
                logger.Error(exception, $"Ошибка обновления приоритета {priority}");
                priority.RollbackChanges();
            }
        }

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
