﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Слои на складе")]
    public class UnitLayersResult : MESDataResult<UnitLayersRequest, IList<LayerUIItem>>
    {
        public int UnitId
        {
            get;
            set;
        }

        public double MaxWeight
        {
            get;
            set;
        }

        public LayerUIItem SelectedLayer
        {
            get => selectedLayer;
            set
            {
                if (selectedLayer != value)
                {
                    selectedLayer = value;
                    OnPropertyChanged(nameof(SelectedLayer));
                }
            }
        }

        private LayerUIItem selectedLayer;

        protected override UnitLayersRequest GetRequest()
        {
            return new UnitLayersRequest(UnitId);
        }

        protected override IList<LayerUIItem> PrepareResult(IList<LayerUIItem> source)
        {
            return source.OrderByDescending( m => m.CreatedAt).ToList();
        }
    }
}