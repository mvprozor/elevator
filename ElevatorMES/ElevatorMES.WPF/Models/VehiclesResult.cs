﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using System.Linq.Dynamic.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.WPF.Models
{
    [Description("Журнал автотранспорта")]
    public class VehiclesResult : MESDataResult<VehiclesRequest, IList<VehicleUIItem>>, ITabViewModel, IPrevSelection, IPrevSorting
    {
        public VehicleHistoryResult History { get; }

        public VehicleUIItem SelectedVehicle
        {
            get => selectedVehicle;
            set
            {
                if (selectedVehicle != value)
                {
                    prevSelectedId = selectedVehicle?.Id;
                    selectedVehicle = value;
                    OnPropertyChanged(nameof(SelectedVehicle));
                    History.VehicleId = selectedVehicle?.Id;
                }
            }
        }

        private VehicleUIItem selectedVehicle;

        private int? prevSelectedId;

        private string regNum;

        private bool includingLeft;

        public string PrevSorting { get; set; }

        public string RegNum
        {
            get => regNum;
            set
            {
                if (String.Compare(regNum, value, true) != 0)
                {
                    regNum = value;
                    NewRequest();
                    OnPropertyChanged(nameof(RegNum));
                }
            }
        }

        public bool IncludingLeft
        {
            get => includingLeft;
            set
            {
                if (includingLeft!=value)
                {
                    includingLeft = value;
                    NewRequest();
                    OnPropertyChanged(nameof(IncludingLeft));
                }
            }
        }

        public VehicleQueueViewModel[] Queues { get; }

        protected override VehiclesRequest GetRequest()
        {
            return new VehiclesRequest(IncludingLeft, RegNum);
        }

        public object GetPrevSelection()
        {
            return ActualResult?.FirstOrDefault(m => m.Id == prevSelectedId);
        }

        protected override IList<VehicleUIItem> PrepareResult(IList<VehicleUIItem> source)
        {
            if (Queues.All(m => m.Items != source))
            {
                var currQueues = source.GroupBy(m => m.Queue).ToDictionary(
                    m => m.Key, m => (IList<VehicleUIItem>)m.ToArray());
                currQueues.Add(VehicleQueue.All, source);
                foreach (var q in Queues)
                {
                    currQueues.TryGetValue(q.Queue, out var items);
                    q.Items = items;
                    if (q.IsSelected)
                        source = items;
                }
            }
            return String.IsNullOrWhiteSpace(PrevSorting) ?
                source : source.AsQueryable<VehicleUIItem>().OrderBy(PrevSorting).ToList();
        }

        #region ITabViewModel
        #endregion

        public VehiclesResult()
        {
            History = new VehicleHistoryResult();
            Queues = Enum.GetValues(typeof(VehicleQueue)).Cast<VehicleQueue>()
                .Select(m => new VehicleQueueViewModel(m, m == VehicleQueue.All)).ToArray();
            Array.ForEach(Queues, m => m.PropertyChanged += QueuePropertyChanged);
        }

        private void QueuePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var queue = (VehicleQueueViewModel)sender;
            if (e.PropertyName == nameof(VehicleQueueViewModel.IsSelected) && queue.IsSelected)
            {
                ActualResult = queue.Items;
            }
        }

        protected override void OnEnableChanged()
        {
            base.OnEnableChanged();
            History.IsEnabled = IsEnabled;
        }

    }
}
