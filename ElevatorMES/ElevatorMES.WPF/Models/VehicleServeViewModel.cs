﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.WPF.Models
{
    [Description("Действия с транспортом")]
    public class VehicleServeViewModel : MESDataResult<VehicleServeRequest, VehicleUIItem>
    {
        #region Открытые поля

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => isDataRequestEnabled;
            set
            {
                if (isDataRequestEnabled != value)
                {
                    isDataRequestEnabled = value;
                    Nomenclatures.IsEnabled = value;
                    Points.IsEnabled = value;
                    IsEnabled = value;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        public VehicleUIItem Vehicle { get; }

        public InputItems<VehicleAction, KeyValuePair<VehicleAction, string>> InputAction { get; }

        public NomenclaturesResult Nomenclatures { get; }

        public InputItems<int?, Nomenclature> Nomenclature { get; }

        public PointsResult Points { get; }

        public InputItems<int?, UnitUIItem> Point { get; }

        public InputItem<decimal> Weight { get; }

        public InputScalesGroup ScalesWeights { get; }

        public InputDayTime ActionAt { get; }

        public InputItem<string> Comment { get; }

        #endregion


        #region Закрытые поля

        private bool isDataRequestEnabled;

        #endregion


        #region Конструкторы и инициализация

        public VehicleServeViewModel(VehicleUIItem vehicle, InputScales[] scales) : base(false)
        {
            Vehicle = vehicle;
            Nomenclatures = new NomenclaturesResult();
            Nomenclatures.Processed += NomenclaturesProcessed;
            Nomenclature = new InputItems<int?, Nomenclature>();
            Points = new PointsResult();
            Points.Processed += PointsProcessed;
            Point = new InputItems<int?, UnitUIItem>();
            Weight = new InputItem<decimal>();
            ScalesWeights = new InputScalesGroup(scales);
            ActionAt = new InputDayTime()
            {
                IsVisible = true
            };
            Comment = new InputItem<string>()
            {
                IsVisible = true
            };
            InputAction = new InputItems<VehicleAction, KeyValuePair<VehicleAction, string>>()
            {
                IsVisible = true
            };
            InputAction.SelectedChanged += ActionChanged;
            InputAction.Items = vehicle.GetAvailableActions();
            InputAction.Selected = VehicleAction.Weighting;
        }

        #endregion


        #region Обработка событий

        private void NomenclaturesProcessed(object sender, EventArgs e)
        {
            FillNomenclatures();
        }

        private void PointsProcessed(object sender, EventArgs e)
        {
            FillPoints();
        }

        private void ActionChanged(object sender, EventArgs e)
        {
            switch(InputAction.Selected)
            {
                case VehicleAction.Weighting:
                    Weight.IsVisible = true;
                    ScalesWeights.IsVisible = true;
                    Nomenclature.IsVisible = false;
                    Point.IsVisible = false;
                    break;
                case VehicleAction.Unloading:
                    Weight.IsVisible = false;
                    ScalesWeights.IsVisible = false;
                    Nomenclature.IsVisible = false;
                    Point.IsVisible = true;
                    FillPoints();
                    break;
                case VehicleAction.Loading:
                    Weight.IsVisible = false;
                    ScalesWeights.IsVisible = false;
                    Nomenclature.IsVisible = true;
                    Point.IsVisible = true;
                    FillPoints();
                    break;
                case VehicleAction.Leaving:
                    Weight.IsVisible = false;
                    ScalesWeights.IsVisible = false;
                    Nomenclature.IsVisible = false;
                    Point.IsVisible = false;
                    break;
            }
            CleanValidationError();
        }

        #endregion


        #region Заполнение элементов ввода списками

        private void FillNomenclatures()
        {
            Nomenclature.FillItems(Nomenclatures);
        }

        private void FillPoints()
        {
            if (InputAction.Selected == VehicleAction.Unloading)
                Point.FillItems(Points, m => m.UnitType == UnitType.UnloadingPoint);
            else if (InputAction.Selected == VehicleAction.Loading)
                Point.FillItems(Points, m => m.UnitType == UnitType.LoadingPoint);
            else
                Point.Items = null;
        }

        #endregion


        #region Проверка данных

        private bool ValidateMultiple(params Func<bool>[] validators)
        {
            bool isValidTotal = true;
            foreach (var validator in validators)
            {
                var isValid = validator();
                isValidTotal = isValidTotal && isValid;
            }
            return isValidTotal;
        }

        private void CleanValidationError()
        {
            Nomenclature.ValidationError = null;
            Weight.ValidationError = null;
            ActionAt.ValidationError = null;
        }

        private bool ValidateNomenclature()
        {
            return !Nomenclature.IsVisible || Nomenclature.ValidateItem("Не указана номенклатура");
        }

        private bool ValidatePoint()
        {
            return !Point.IsVisible || Point.ValidateItem("Не указано место");
        }

        private bool ValidateWeight()
        {
            if (Weight.IsVisible && Weight.Selected < 0)
            {
                Weight.ValidationError = "Указан отрицательный вес";
                return false;
            }
            else
            {
                Weight.ValidationError = null;
                return true;
            }
        }

        private bool ValidateActionAt()
        {
            if (ActionAt.Selected <= Vehicle.RegisteredAt)
            {
                ActionAt.ValidationError = $"\"{InputAction.SelectedItem.Value}\" должно быть позже регистрации";
                return false;
            }
            return true;
        }
        #endregion


        #region MESDataResult<TaskCreateRequest, TaskUIItem>

        protected override VehicleServeRequest GetRequest()
        {
            VehicleServeRequest vehicleServeRequest = null;
            if (ValidateMultiple(ValidateNomenclature, ValidatePoint, ValidateWeight, ValidateActionAt))
            {
                vehicleServeRequest = new VehicleServeRequest(
                    Vehicle.Id, InputAction.Selected, ActionAt.Selected,
                    Comment.Selected, Nomenclature.Selected, Point.Selected,
                    Weight.Selected == 0 ? null : (decimal?)Weight.Selected);
            }
            return vehicleServeRequest;
        }

        #endregion
    }
}
