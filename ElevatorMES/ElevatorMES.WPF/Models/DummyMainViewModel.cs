﻿using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.UI;
using System;
using System.Collections.Generic;

namespace ElevatorMES.WPF.Models
{
    public class DummyMainViewModel : MainViewModel
    {
        public DummyMainViewModel()
        {
            OperInfo.ActualResult = new OperInfoUI()
            {
                QueuedTasks = 10,
                FinishedTasks = 20,
                CarsInside = 11,
                Is1CAvailable = true,
                UserRole = UserRole.Operator,
                UserName = "Логин пользователя"
            };

            var activeTasks = new List<TaskUIItem>(4);

            activeTasks.Add(new TaskUIItem()
            {
                Type = TaskType.Receiving,
                DestUnitName = "Силос 1.1",
                SuppCons = "Т/С № K323ОН161RUS, ООО Поставщик",
                IsDrying = true,
                NomenclatureDisplayName = "Пшеница 2020 сорт 1 (Некл.р-н)",
                QuantityPlanned = 10,
                QuantityActual = 0,
                StartedAt = DateTime.Now,
                Status = TaskStatus.Running,
                Comment = "Некоторый комментарий к задаче",
                StatusPLC = PLCTaskStatus.ConfigurationError
            });
            activeTasks.Add(new TaskUIItem()
            {
                Type = TaskType.SilosMoving,
                DestUnitName = "Силос 2.1",
                SuppCons = "№121212, ПОставщик 2",
                IsDrying = true,
                IsScalping = true,
                NomenclatureDisplayName = "Овес н/p",
                QuantityPlanned = 10,
                QuantityActual = 2,
                StartedAt = DateTime.Now,
                Status = TaskStatus.Suspended,
                StatusPLC = PLCTaskStatus.RouteStopped,
                QuantityPLC = 2
            });
            ActiveTasks.ActualResult = activeTasks;

            var queuedTasks = new List<TaskUIItem>(4);
            queuedTasks.Add(new TaskUIItem()
            {
                Type = TaskType.Receiving,
                ExecuteAt = DateTime.Now,
                DestUnitName = "Силос 3.1",
                SuppCons = "K3237Н161RUS, Посташик №3",
                IsDrying = true,
                NomenclatureDisplayName = "Пшеница 2020 сорт 1 (Некл.р-н)",
                QuantityPlanned = 10,
                Status = TaskStatus.Queued,
                Comment = "Некоторый комментарий к задаче"
            });
            queuedTasks.Add(new TaskUIItem()
            {
                Type = TaskType.Receiving,
                ExecuteAt = DateTime.Now,
                DestUnitName = "Силос 4.1",
                SuppCons = "K3235Н161RUS",
                IsDrying = true,
                NomenclatureDisplayName = "Пшеница 2020 сорт 1 (Некл.р-н)",
                QuantityPlanned = 10,
                Status = TaskStatus.Queued,
                Comment = "Некоторый комментарий к задаче"
            });
            queuedTasks.Add(new TaskUIItem()
            {
                Type = TaskType.ShippingExt,
                ExecuteAt = DateTime.Now,
                SuppCons = "K323ОН161RUS",
                SourceUnitName = "Силос 3.1",
                NomenclatureDisplayName = "Пшеница 2021 сорт 1 (Некл.р-н)",
                QuantityPlanned = 12,
                QuantityActual = 3,
                Status = TaskStatus.Queued,
                Comment = "Некоторый комментарий к задаче"
            });
            QueuedTasks.ActualResult = queuedTasks;

            var historyTasks = new List<TaskUIItem>(4);
            historyTasks.Add(new TaskUIItem()
            {
                CreatedAt = DateTime.Now.AddDays(-1),
                Type = TaskType.Receiving,
                ExecuteAt = DateTime.Now.AddHours(-4.9),
                DestUnitName = "Силос 1.1",
                SuppCons = "K323ОН161RUS Поставищик 3",
                IsDrying = true,
                IsSeparating = true,
                NomenclatureDisplayName = "Пшеница 2020 сорт 1 (Некл.р-н)",
                QuantityPlanned = 23m,
                QuantityActual = 22.84m,
                StartedAt = DateTime.Now.AddHours(-5),
                EndAt = DateTime.Now.AddHours(-4.2),
                Status = TaskStatus.Completed,
                Comment = "Хорошо выполнена"
            });
            historyTasks.Add(new TaskUIItem()
            {
                CreatedAt = DateTime.Now.AddDays(-1),
                Type = TaskType.Receiving,
                ExecuteAt = DateTime.Now.AddHours(-4.1),
                DestUnitName = "Силос 4.1",
                SuppCons = "K3235Н161RUS",
                IsDrying = true,
                NomenclatureDisplayName = "Пшеница 2020 сорт 1 (Некл.р-н)",
                QuantityPlanned = 10,
                QuantityActual = 11m,
                StartedAt = DateTime.Now.AddHours(-4),
                EndAt = DateTime.Now.AddHours(-3.4),
                Status = TaskStatus.Failed,
                Comment = "Некоторый комментарий к задаче"
            });
            HistoryTasks.ActualResult = historyTasks;

            Storages.ActualResult = new StorageUIItem[]
            {
                new StorageUIItem()
                {
                    NomenclatureId = 1,
                    NomenclatureDisplayName = "Рожь 333",
                    UnitId = 1,
                    UnitDisplayName = "№ 4.2",
                    UnitType = UnitType.Silos,
                    Quantity = 140,
                    Capacity = 900
                },
                new StorageUIItem()
                {
                    NomenclatureId = 1,
                    NomenclatureDisplayName = "Пшеница 333",
                    UnitId = 1,
                    UnitDisplayName = "№ 1.1",
                    UnitType = UnitType.Silos,
                    Quantity = 40,
                    Capacity = 940
                }
            };


        }
    }
}
