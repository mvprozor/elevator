﻿using ElevatorMES.Domain.Model.UI;
using ElevatorMES.WPF.Enums;
using System;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using System.ComponentModel;
using ElevatorMES.Utilites.Helpers;
using System.Linq;

namespace ElevatorMES.WPF.Models
{
    [Description("История заданий")]
    public class TasksHistoryResult : MESDataResult<TasksHistoryRequest, List<TaskUIItem>>, ITabViewModel, IPrevSelection, IPrevSorting
    {
        public IList<KeyValuePair<ViewPeriodEnum, string>> ViewPeriods { get; } =
            EnumExtension.ToListKeyDesc<ViewPeriodEnum>();

        public ViewPeriodEnum SelectedViewPeriod 
        {
            get => selectedViewPeriod;
            set
            {
                if (value!=selectedViewPeriod)
                {
                    selectedViewPeriod = value;
                    NewRequest();
                    OnPropertyChanged(nameof(SelectedViewPeriod));
                }
            }
        }

        public DateTime? DateFrom
        {
            get => dateFrom;
            set
            {
                if (dateFrom != value)
                {
                    dateFrom = value;
                    NewRequest();
                    OnPropertyChanged(nameof(DateFrom));
                }
            }
        }

        public DateTime? DateTill
        {
            get => dateTill;
            set
            {
                if (dateTill != value)
                {
                    dateTill = value;
                    NewRequest();
                    OnPropertyChanged(nameof(DateTill));
                }

            }
        }


        public string SearchText
        {
            get => searchText;
            set
            {
                if (String.Compare(searchText, value, true)!=0)
                {
                    searchText = value;
                    NewRequest();
                    OnPropertyChanged(nameof(SearchText));
                }
            }
        }

        public TaskUIItem SelectedTask
        {
            get => selectedTask;
            set
            {
                if (selectedTask!=value)
                {
                    prevSelectedTaskId = selectedTask?.Id;
                    selectedTask = value;
                    OnPropertyChanged(nameof(SelectedTask));
                }
            }
        }

        private ViewPeriodEnum selectedViewPeriod;
        private DateTime? dateFrom;
        private DateTime? dateTill;
        private string searchText;
        private TaskUIItem selectedTask;
        private int? prevSelectedTaskId;
        public string PrevSorting { get; set; }

        public TasksHistoryResult()
        {
            dateFrom = DateTime.Today;
            dateTill = DateTime.Today.AddDays(1);
        }

        protected override TasksHistoryRequest GetRequest()
        {
            SelectedViewPeriod.ToPeriod(DateTill, DateFrom,  out var begin, out var end);
            return new TasksHistoryRequest(begin, end, SearchText);
        }

        public object GetPrevSelection()
        {
            return ActualResult?.Find(m => m.Id == prevSelectedTaskId);
        }

        protected override List<TaskUIItem> PrepareResult(List<TaskUIItem> source)
        {
            return String.IsNullOrWhiteSpace(PrevSorting) ?
                source : source.AsQueryable<TaskUIItem>().OrderBy(PrevSorting).ToList();
        }

        #region ITabViewModel
        #endregion

    }
}
