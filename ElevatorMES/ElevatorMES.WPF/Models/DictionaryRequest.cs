﻿using ElevatorMES.Domain.Model.Args;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Справочники")]
    public class DictionaryRequest : MESDataRequest, IEquatable<DictionaryRequest>
    {
        public bool? IsDeleted { get; }

        public string SearchText { get; }

        public DictionaryRequest(bool? isDeleted = false, string searchText = null)
        {
            IsDeleted = isDeleted;
            SearchText = searchText;
        }

        public bool Equals(DictionaryRequest other)
        {
            return String.Equals(SearchText, other.SearchText, StringComparison.OrdinalIgnoreCase)
               && IsDeleted == other.IsDeleted;
        }

        public GetDictArg ToArg()
        {
            return new GetDictArg()
            {
                IsDeleted = IsDeleted,
                SearchText = SearchText
            };
        }
    }
}