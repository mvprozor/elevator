﻿using System;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Models
{
    public interface IDictionaryView
    {
        DictionaryType Type { get; }
        IDictionaryViewModel GetViewModel(bool isNew, Action onSuccess);
        void Delete();
        void Restore();
        bool NewRequest();
        void ClearSearch();
    }
}
