﻿using ElevatorMES.Domain.Model.Args;
using System;
using System.ComponentModel;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос старта задания")]
    public class TaskStartRequest : MESDataRequest, IEquatable<TaskStartRequest>
    {
        public TaskStartArg Arg { get; }

        public TaskStartRequest(TaskStartArg arg)
        {
            Arg = arg;
        }

        public bool Equals(TaskStartRequest other)
        {
            return Arg.Equals(other.Arg);
        }
    }
}
