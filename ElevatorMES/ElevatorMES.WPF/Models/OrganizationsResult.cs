﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Организации")]
    public class OrganizationsResult : MESDataResult<EmptyRequest, IList<Organization>>
    {
        protected override EmptyRequest GetRequest()
        {
            return new EmptyRequest();
        }
    }
}
