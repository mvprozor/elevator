﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using System;
using System.ComponentModel;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос истории действий с транспортом")]
    public class VehicleHistoryRequest : MESDataRequest, IEquatable<VehicleHistoryRequest>
    {
        public int? VehicleId { get; }

        public VehicleHistoryRequest(int? vehicleId)
        {
            VehicleId = vehicleId;
        }

        public bool Equals(VehicleHistoryRequest other)
        {
            return VehicleId == other.VehicleId;
        }
    }
}
