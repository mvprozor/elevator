﻿using System;
using ElevatorMES.Domain.Model.Args;
using System.ComponentModel;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос заданий для сопоставления")]
    public class TaskMatchRequest : MESDataRequest, IEquatable<TaskMatchRequest>
    {
        public TaskMatchArg Arg { get; }

        public TaskMatchRequest(int taskManualId, int taskERPId)
        {
            Arg = new TaskMatchArg()
            {
                TaskManualId = taskManualId,
                TaskERPId = taskERPId
            };
        }

        public bool Equals(TaskMatchRequest other)
        {
            return Arg.TaskManualId == other.Arg.TaskManualId
                && Arg.TaskERPId == other.Arg.TaskERPId;
        }
    }
}
