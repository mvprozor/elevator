﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class ReportParamItemsModel<TValue> where TValue : struct
    {
        public ReportDataSource DataSource { get; }
        public IList<ReportParamListItemModel<TValue>> Values { get; }

        public ReportParamItemsModel(ReportDataSource dataSource, IList<ReportParamListItemModel<TValue>> values)
        {
            DataSource = dataSource;
            Values = values;
        }
    }
}
