﻿using System;
using ElevatorMES.Domain.Model.Args;
using System.ComponentModel;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос заданий для сопоставления")]
    public class TaskMatchingRequest : MESDataRequest, IEquatable<TaskMatchingRequest>
    {
        public GetTaskMatchArg Arg { get; }

        public TaskMatchingRequest(
            int? taskId, string docId, bool? hasDocId, 
            bool isQueued, TaskType? type, int? nomenclatureId,
            string textFilter, int? taskCount)
        {
            Arg = new GetTaskMatchArg()
            {
                TaskId = taskId,
                DocId = docId,
                IsFromERP = hasDocId,
                TextFilter = textFilter,
                TaskType = type,
                NomenclatureId = nomenclatureId,
                TaskCount = taskCount
            };
        }

        public bool Equals(TaskMatchingRequest other)
        {
            return Arg.TaskId == other.Arg.TaskId
                && String.Equals(Arg.DocId, other.Arg.DocId, StringComparison.OrdinalIgnoreCase)
                && Arg.IsFromERP == other.Arg.IsFromERP
                && Arg.TaskGenStatus == other.Arg.TaskGenStatus
                && String.Equals(Arg.TextFilter, other.Arg.TextFilter, StringComparison.OrdinalIgnoreCase)
                && Arg.TaskType == other.Arg.TaskType
                && Arg.NomenclatureId == other.Arg.NomenclatureId
                && Arg.TaskCount == other.Arg.TaskCount;
        }
    }
}
