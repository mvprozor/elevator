﻿using ElevatorMES.Domain.Model.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Оперативная информация")]
    public class OperInfoResult : MESDataResult<EmptyRequest, OperInfoUI>
    {
        protected override EmptyRequest GetRequest()
        {
            return new EmptyRequest();
        }
    }
}
