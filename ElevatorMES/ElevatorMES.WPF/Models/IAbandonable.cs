﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.WPF.Models
{
    public interface IAbandonable
    {
        bool IsAbandoned { get; }
        
        CancellationToken AbandonToken { get; }

        void Abandon();
    }
}
