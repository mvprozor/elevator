﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Связка подразделений и оборудования")]
    public class UnitEquipmentsResult : MESDataResult<UnitEquipmentsRequest, List<UnitEquipmentUIItem>>, ITabViewModel
    {
        public int? UnitId
        {
            get;
            set;
        }

        public UnitEquipmentUIItem SelectedLink
        {
            get => selectedLink;
            set
            {
                if (selectedLink != value)
                {
                    selectedLink = value;
                    OnPropertyChanged(nameof(SelectedLink));
                }
            }
        }

        private UnitEquipmentUIItem selectedLink;

        protected override UnitEquipmentsRequest GetRequest()
        {
            return new UnitEquipmentsRequest(UnitId);
        }
    }
}
