﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class ReportsListModel : ReportToolModel
    {
        public IList<ReportModel> Values { get; }

        public ReportModel Selected
        {
            get => selected;
            set
            {
                if (selected!=value)
                {
                    selected = value;
                    ReportChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        private ReportModel selected;

        public bool IsEnabled
        {
            get => isEnabled;
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    OnPropertyChanged(nameof(IsEnabled));
                }
            }
        }

        private bool isEnabled;

        public event EventHandler ReportChanged;

        public ReportsListModel() : base("Отчеты")
        {
            isEnabled = true;
            var assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
            Values = new ReportModel[]
            {
                new ReportModel("Tasks", "Задания", $"{assemblyName}.Reports.Tasks.rdl"),
                new ReportModel("SiloState", "Склад", $"{assemblyName}.Reports.SiloState.rdl"),
                new ReportModel("EquipmentStats", "Работа оборудования", $"{assemblyName}.Reports.EquipmentStats.rdl")
            };
        }
    }
}
