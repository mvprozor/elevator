﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.WPF.Models
{
    public class MESDataRequest : IEquatable<MESDataRequest>, IAbandonable
    {
        public bool IsAbandoned => _abandonTokenSource?.IsCancellationRequested ?? false;

        public CancellationToken AbandonToken
        {
            get
            {
                return AbandonTokenSource.Token;
            }
        }

        private CancellationTokenSource AbandonTokenSource
        { 
            get
            {
                _abandonTokenSource = _abandonTokenSource ??
                    new CancellationTokenSource();
                return _abandonTokenSource;
            }
        }


        private CancellationTokenSource _abandonTokenSource;


        public void Abandon()
        {
            AbandonTokenSource.Cancel();
        }

        public bool Equals(MESDataRequest other)
        {
            return true;
        }
    }
}
