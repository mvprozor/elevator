﻿using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System.Collections.Generic;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.Args;

namespace ElevatorMES.WPF.Models
{
    public class UnitEquipmentViewModel : MESDataNoResult<UnitEquipmentUpdateRequest>
    {
        #region Открытые поля

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => isDataRequestEnabled;
            set
            {
                if (isDataRequestEnabled != value)
                {
                    isDataRequestEnabled = value;
                    Units.IsEnabled = value;
                    Equipments.IsEnabled = value;
                    IsEnabled = value;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        public UnitsResult Units { get; }

        public EquipmentsResult Equipments { get; }

        public InputItems<int, Unit> Unit { get; }

        public InputItems<int, Equipment> Equipment { get; }

        public InputItem<bool> IsContainer { get; }

        public InputItem<bool> IsRouteSource { get; }

        public InputItem<bool> IsRouteDestination { get; }

        public string LinkValidationError 
        {
            get => linkValidationError;
            set
            {
                if (String.Compare(value, linkValidationError, StringComparison.OrdinalIgnoreCase)!=0)
                {
                    linkValidationError = value;
                    OnPropertyChanged(nameof(LinkValidationError));
                }
            }
        }

        #endregion


        #region Закрытые поля

        private bool isDataRequestEnabled;

        private string linkValidationError;

        #endregion


        #region Конструкторы и инициализация

        public UnitEquipmentViewModel()
        {
            Units = new UnitsResult();
            Equipments = new EquipmentsResult();
            Units.Processed += UnitsProcessed;
            Equipments.Processed += EquipmentsProcessed;
            Unit = new InputItems<int, Unit>();
            Equipment = new InputItems<int, Equipment>();
            IsContainer = new InputItem<bool>();
            IsRouteSource = new InputItem<bool>();
            IsRouteDestination = new InputItem<bool>();
            IsContainer.SelectedChanged += LinkTypeChanged;
            IsRouteSource.SelectedChanged += LinkTypeChanged;
            IsRouteDestination.SelectedChanged += LinkTypeChanged;
        }

        #endregion


        #region Обрыботка событий

        private void UnitsProcessed(object sender, EventArgs e)
        {
            Unit.FillItems(Units);
        }

        private void EquipmentsProcessed(object sender, EventArgs e)
        {
            Equipment.FillItems(Equipments);
        }

        private void LinkTypeChanged(object sender, EventArgs e)
        {
            LinkValidationError = null;
        }

        #endregion


        #region Проверка ввода и запрос

        public bool ValidateAll()
        {
            var res = true;
            res &= Unit.ValidateItem("Не указано подразделение");
            res &= Equipment.ValidateItem("Не указано оборудование");
            if (!IsContainer.Selected && !IsRouteSource.Selected && !IsRouteDestination.Selected)
            {
                res = false;
                LinkValidationError = "Должен быть указан хотя бы один признак связи";
            }
            return res;
        }

        protected override UnitEquipmentUpdateRequest GetRequest()
        {
            if (ValidateAll())
            {
                return new UnitEquipmentUpdateRequest(
                    new UnitEquipmentUpdateArg()
                    {
                        UnitId = Unit.Selected,
                        EquipmentId = Equipment.Selected,
                        IsContainer = IsContainer.Selected,
                        IsRouteSource = IsRouteSource.Selected,
                        IsRouteDestination = IsRouteDestination.Selected
                    });
            }
            return null;
        }

        #endregion

    }
}
