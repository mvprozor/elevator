﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ElevatorMES.WPF.Models
{
    public class ReportRefreshModel : ReportToolModel
    {
        public bool IsEnabled
        {
            get => isEnabled;
            set
            {
                if (isEnabled!=value)
                {
                    isEnabled = value;
                    OnPropertyChanged(nameof(IsEnabled));
                }
            }
        }

        private bool isEnabled;

        public ReportRefreshModel() : base("Обновить")
        {

     
        }
    }
}
