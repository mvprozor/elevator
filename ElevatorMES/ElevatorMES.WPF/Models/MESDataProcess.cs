﻿using ElevatorMES.Utilites.Helpers;
using NLog;
using System;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;

namespace ElevatorMES.WPF.Models
{
    public abstract class MESDataProcess<TRequest> : INotifyPropertyChanged
        where TRequest : IEquatable<TRequest>, IAbandonable
    {
        #region Закрытые поля

        // описание запрашиваемых данных
        protected readonly string dataDesc;

        // текущий запрос
        protected TRequest currentRequest;

        // отложенный запрос
        protected TRequest queuedRequest;

        // признак процесса загрузки
        private bool isLoading;

        // текст последней ошибки
        private string actualError;

        // флаг разрешения обновления данных
        private bool isEnabled;

        // логирование
        protected static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        #region Открытые свойства и события

        /// <summary>
        /// Признак активности процесса загрузки данных
        /// </summary>
        public bool IsLoading
        {
            get => isLoading;
            set
            {
                if (isLoading != value)
                {
                    isLoading = value;
                    OnPropertyChanged(nameof(IsLoading));
                }
            }
        }

        public string ActualError
        {
            get => actualError;
            set
            {
                if (String.Compare(actualError, value, true) != 0)
                {
                    actualError = value;
                    OnPropertyChanged(nameof(ActualError));
                }
            }
        }

        public bool IsEnabled
        {
            get => isEnabled;
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    OnEnableChanged();
                    OnPropertyChanged(nameof(IsEnabled));
                }
            }
        }

        public event EventHandler Processed;

        #endregion


        #region Конструкторы и иницилизация

        public MESDataProcess()
        {
            var type = GetType();
            dataDesc = type.GetCustomAttribute<DescriptionAttribute>()?.Description ?? type.Name;
        }

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion


        #region Обрыботка запросов

        protected abstract TRequest GetRequest();

        public bool NewRequest()
        {
            TRequest newRequest;
            if (IsEnabled && (newRequest = GetRequest()) != null)
            {
                if (currentRequest == null)
                {
                    // никакой запрос не выполняется, выполняем
                    currentRequest = newRequest;
                    ProcessRequests();
                }
                else
                {
                    // какой-то запрос уже выполняется
                    // если он с другими параметрами,
                    // то отменим старый
                    if (!currentRequest.Equals(newRequest))
                        currentRequest.Abandon();

                    // ставим в очередь всегда, 
                    // когда отменен старый запрос
                    if (currentRequest.IsAbandoned)
                        queuedRequest = newRequest;
                }
                return true;
            }
            else
            {
                queuedRequest = default;
                currentRequest?.Abandon();
                currentRequest = default;
                ActualError = null;
                OnDefaultData();
                return false;
            }
        }

        protected virtual void OnEnableChanged()
        {

        }

        protected abstract void OnDefaultData();

        protected abstract void ProcessRequests();

        protected void OnProcessed()
        {
            Processed?.Invoke(this, EventArgs.Empty);
        }

        #endregion

    }
}
