﻿using NLog;
using System;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System.Collections.Generic;
using ElevatorMES.Domain.Model.UI;

namespace ElevatorMES.WPF.Models
{
    /// <summary>
    /// Описание элемента ввода
    /// </summary>
    /// <typeparam name="TSelected"></typeparam>
    public class InputItem<TSelected> : INotifyPropertyChanged
    {

        #region Открытые поля

        public TSelected Selected
        {
            get => selected;
            set
            {
                if (!comparer.Equals(selected, value))
                {
                    selected = value;
                    ValidationError = null;
                    OnPropertyChanged(nameof(Selected));
                    SelectedChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public string ValidationError
        {
            get => validationError;
            set
            {
                if (String.Compare(validationError, value, StringComparison.OrdinalIgnoreCase) != 0)
                {
                    validationError = value;
                    OnPropertyChanged(nameof(ValidationError));
                }
            }
        }

        public bool IsVisible
        {
            get => isVisible;
            set
            {
                if (isVisible!=value)
                {
                    isVisible = value;
                    OnPropertyChanged(nameof(IsVisible));
                }
            }
        }

        public event EventHandler SelectedChanged;

        #endregion 


        #region Закрытые поля

        private TSelected selected;
        private bool isVisible;
        private string validationError;
        private IEqualityComparer<TSelected> comparer;

        #endregion 


        #region Конструкторы и иницилизация

        public InputItem(IEqualityComparer<TSelected> comparer = null)
        {
            this.comparer = (comparer ?? EqualityComparer<TSelected>.Default) ??
                throw new ArgumentNullException(nameof(comparer));
        }

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
