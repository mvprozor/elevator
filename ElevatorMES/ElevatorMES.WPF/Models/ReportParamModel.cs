﻿using ElevatorMES.WPF.Enums;
using Microsoft.Reporting.Map.WebForms.BingMaps;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ElevatorMES.WPF.Models
{
    public abstract class ReportParamModel : ReportToolModel
    {
        #region Открытые поля и события

        public string Name { get; }

        public abstract object Value { get; }

        public abstract string[] ViewerValues { get; }

        public event EventHandler ValueChanged;

        public abstract bool IsValidValue { get; }

        public IList<ReportParamModel> Dependencies { get; }

        public IList<ReportParamModel> Dependents { get; }

        public bool AllowNull { get; }

        public bool AllowEmpty { get; }

        public bool IsVisible => !String.IsNullOrWhiteSpace(Title);

        #endregion

        private List<ReportParamModel> dependencies;
        private List<ReportParamModel> dependents;


        #region Конструкторы и инициализация

        public ReportParamModel(string name, string title, bool allowNull, bool allowEmpty) : base(title)
        {
            Name = name;
            AllowNull = allowNull;
            AllowEmpty = allowEmpty;
            dependencies = new List<ReportParamModel>(8);
            Dependencies = new ReadOnlyCollection<ReportParamModel>(dependencies);
            dependents = new List<ReportParamModel>(8);
            Dependents = new ReadOnlyCollection<ReportParamModel>(dependents);
        }

        public void AddDependencies(IEnumerable<ReportParamModel> reportParams)
        {
            dependencies.AddRange(reportParams);
            foreach (var reportParam in reportParams)
                reportParam.dependents.Add(this);
        }

        protected void OnValueChanged()
        {
            ValueChanged?.Invoke(this, EventArgs.Empty);
        }

        public override string ToString()
        {
            return Name;
        }

        #endregion

    }
}
