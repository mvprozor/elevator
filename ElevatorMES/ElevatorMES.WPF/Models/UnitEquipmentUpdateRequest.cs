﻿using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос обновления связки подразделения и оборудования")]
    public class UnitEquipmentUpdateRequest : MESDataRequest, IEquatable<UnitEquipmentUpdateRequest>
    {
        public UnitEquipmentUpdateArg Arg { get; }

        public UnitEquipmentUpdateRequest(UnitEquipmentUpdateArg arg)
        {
            Arg = arg;
        }

        public bool Equals(UnitEquipmentUpdateRequest other)
        {
            return Arg.Equals(other.Arg);
        }
    }
    
}
