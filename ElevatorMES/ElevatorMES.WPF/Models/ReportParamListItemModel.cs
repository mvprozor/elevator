﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class ReportParamListItemModel<TValue> where TValue : struct
    {
        public TValue? Value { get; }
        public string Label { get; }
        public bool IsSelected 
        {
            get => isSelected;
            set
            {
                if (isSelected!=value)
                {
                    isSelected = value;
                    SelectedChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }
        private bool isSelected;

        public event EventHandler SelectedChanged;

        public ReportParamListItemModel(TValue? value, string label)
        {
            Value = value;
            Label = label;
        }

        public override string ToString()
        {
            return Label ?? String.Empty;
        }
    }
}
