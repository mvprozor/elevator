﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using System.Linq.Dynamic.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Состояние складов")]
    public class StoragesStateResult : MESDataResult<EmptyRequest, IList<StorageUIItem>>, ITabViewModel, IPrevSelection, IPrevSorting
    {
        public StorageUIItem SelectedStore
        {
            get => selectedStore;
            set
            {
                if (selectedStore != value)
                {
                    prevSelectedUnitId = selectedStore?.UnitId;
                    selectedStore = value;
                    OnPropertyChanged(nameof(SelectedStore));
                }
            }
        }

        private StorageUIItem selectedStore;

        private int? prevSelectedUnitId;

        public string PrevSorting { get; set; }


        protected override EmptyRequest GetRequest()
        {
            return new EmptyRequest();
        }

        public object GetPrevSelection()
        {
            return ActualResult?.FirstOrDefault(m => m.UnitId == prevSelectedUnitId);
        }

        protected override IList<StorageUIItem> PrepareResult(IList<StorageUIItem> source)
        {
            return String.IsNullOrWhiteSpace(PrevSorting) ?
                source : source.AsQueryable<StorageUIItem>().OrderBy(PrevSorting).ToList();
        }

        #region ITabViewModel
        #endregion

    }
}
