﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Журнал автотранспорта")]
    public class VehiclesRequest : MESDataRequest, IEquatable<VehiclesRequest>
    {
        public bool IncludingLeft { get; }
        public string RegNum { get; }

        public VehiclesRequest(bool includingLeft, string regNum)
        {
            IncludingLeft = includingLeft;
            RegNum = regNum;
        }


        public bool Equals(VehiclesRequest other)
        {
            return IncludingLeft==other.IncludingLeft
                && String.Compare(RegNum, other.RegNum, StringComparison.InvariantCultureIgnoreCase) == 0;
        }
    }
}