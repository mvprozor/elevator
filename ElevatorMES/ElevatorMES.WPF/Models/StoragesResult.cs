﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Склады хранения")]
    public class StoragesResult : MESDataResult<EmptyRequest, IList<StorageUIItem>>
    {
        protected override EmptyRequest GetRequest()
        {
            return new EmptyRequest();
        }
    }
}
