﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using System;
using System.ComponentModel;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос действий с транспортом")]
    public class VehicleServeRequest : MESDataRequest, IEquatable<VehicleServeRequest>
    {
        public int VehicleId { get; }
        public VehicleAction Action { get; }
        public DateTime ActionAt { get; set; }
        public int? NomenclatureId { get; }
        public int? UnitId { get; }
        public decimal? Weight { get; set; }
        public string Comment { get; set; }

        public VehicleServeRequest(int vehicleId, VehicleAction action, DateTime actionAt,
             string comment = null, int? nomenclatureId = null, 
             int? unitId = null, decimal? weight = null)
        {
            VehicleId = vehicleId;
            Action = action;
            ActionAt = actionAt;
            Comment = comment;
            NomenclatureId = nomenclatureId;
            UnitId = unitId;
            Weight = weight;
        }

        public bool Equals(VehicleServeRequest other)
        {
            return VehicleId == other.VehicleId && Action == other.Action && ActionAt == other.ActionAt
                && NomenclatureId == other.NomenclatureId && Weight == other.Weight
                && String.Compare(Comment, other.Comment, StringComparison.OrdinalIgnoreCase) == 0;
        }

    }
}
