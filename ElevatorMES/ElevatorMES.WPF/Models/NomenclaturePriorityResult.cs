﻿using System;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using System.Collections.Generic;
using System.ComponentModel;

namespace ElevatorMES.WPF.Models
{
    [Description("Приоритет номенклатур")]
    public class NomenclaturePriorityResult : MESDataResult<EmptyRequest, IList<NomenclaturePriorityUIItem>>, ITabViewModel
    {
        public UnitEquipmentUIItem SelectedPriority
        {
            get => selectedPriority;
            set
            {
                if (selectedPriority != value)
                {
                    selectedPriority = value;
                    OnPropertyChanged(nameof(SelectedPriority));
                }
            }
        }

        private UnitEquipmentUIItem selectedPriority;

        protected override EmptyRequest GetRequest()
        {
            return new EmptyRequest();
        }
    }
}

