﻿using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System.Collections.Generic;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.Args;

namespace ElevatorMES.WPF.Models
{
    [Description("Останов задания")]
    public class TaskStopViewModel : MESDataResult<TaskStopRequest, TaskUIItem>
    {
        #region Открытые поля

        public InputItem<bool> IsFailed { get; }

        public InputItem<bool> IsReturn { get; }

        public InputItem<decimal> Weight { get; }

        public InputItem<string> Comment { get; }

        public TaskUIItem TaskItem 
        {
            get => taskItem;
            set
            {
                if (taskItem!=value)
                {
                    taskItem = value;
                    //Weight.Selected = taskItem?.QuantityPLC ?? decimal.Zero; 
                    //decimal.Zero; taskItem?.QuantityActual ?? taskItem?.QuantityPlanned ?? decimal.Zero;
                    SetWeight();
                    OnPropertyChanged(nameof(TaskItem));
                }
            }
        }

		public decimal? VehicleQuantity
		{
			get => vehicleQuantity;
			set
			{
				if (vehicleQuantity != value)
				{
					vehicleQuantity = value;
					OnPropertyChanged(nameof(VehicleQuantity));
					SetWeight();
				}
			}
		}

		public string VehicleInfo
		{
			get => vehicleInfo;
			set
			{
				if (vehicleInfo != value)
				{
					vehicleInfo = value;
					OnPropertyChanged(nameof(VehicleInfo));
				}
			}
		}

		#endregion


		#region Закрытые поля

		private TaskUIItem taskItem;

		private decimal? vehicleQuantity;

		private string vehicleInfo;

		#endregion


		#region Конструкторы и инициализация

		public TaskStopViewModel() : base(false)
        {
            IsReturn = new InputItem<bool>();
            IsReturn.SelectedChanged += IsReturnSelectedChanged;
            IsFailed = new InputItem<bool>()
            {
                IsVisible = true
            };
            Weight = new InputItem<decimal>()
            { 
                IsVisible = true
            };
            Comment = new InputItem<string>()
            { 
                IsVisible = true
            };
        }

        #endregion


        #region MESDataResult<TaskStopRequest, TaskUIItem>

        protected override TaskStopRequest GetRequest()
        {
            return TaskItem == null ? null : new TaskStopRequest(GetArgs());
        }

        public TaskStopArg GetArgs()
        {
            if (Weight.Selected >= 0)
            {
                return new TaskStopArg()
                {
                    TaskId = TaskItem.Id,
                    IsReturn = IsReturn.Selected,
                    IsCloseTask = !IsReturn.Selected,
                    IsFailed = !IsReturn.Selected && IsFailed.Selected,
                    Weight = Weight.Selected,
                    Comment = Comment.Selected
                };
            }
            else
            {
                Weight.ValidationError = "Некорректный вес";
                return null;
            }
                
        }

        #endregion


        #region События от элементов ввода

        private void IsReturnSelectedChanged(object sender, EventArgs e)
        {
            IsFailed.IsVisible = !IsReturn.Selected;
            SetWeight();
        }

        private void SetWeight()
        {
            if (IsReturn.Selected)
            {
                Weight.Selected = TaskItem.QuantityPLC;
            }
            else
            {
                if (VehicleQuantity.HasValue)
                    Weight.Selected = VehicleQuantity.Value - TaskItem.QuantityActual;
                else
                    Weight.Selected = TaskItem.QuantityPLC;
            }    
        }

        #endregion

    }
}
