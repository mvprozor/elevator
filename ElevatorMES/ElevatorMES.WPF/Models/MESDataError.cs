﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class MESDataError : Exception
    {
        public MESDataError(string message) : base(message) { }
    }
}
