﻿using ElevatorMES.Domain.Model.Args;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос слоев склада")]
    public class UnitLayersRequest : MESDataRequest, IEquatable<UnitLayersRequest>
    {
        public GetUnitLayersArg Arg { get; }

        public UnitLayersRequest(int unitId)
        {
            Arg = new GetUnitLayersArg()
            {
                SiloId = unitId
            };
        }

        public bool Equals(UnitLayersRequest other)
        {
            return Arg.SiloId == other.Arg.SiloId;
        }
    }
}
