﻿using ElevatorMES.Domain.Model.Args;
using System;
using System.ComponentModel;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос смены назначения склада")]
    public class StorageAssignRequest : MESDataRequest, IEquatable<StorageAssignRequest>
    {
        public StorageAssignArg Arg { get; }

        public StorageAssignRequest(int unitId, int? nomenclatureId)
        {
            Arg = new StorageAssignArg()
            {
                UnitId = unitId,
                NomenclatureId = nomenclatureId
            };
        }

        public bool Equals(StorageAssignRequest other)
        {
            return Arg.Equals(other.Arg);
        }
    }
}
