﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Связка подразделений и оборудования")]
    public class UnitEquipmentsRequest : MESDataRequest, IEquatable<UnitEquipmentsRequest>
    {
        public int? UnitId { get; }

        public UnitEquipmentsRequest(int? unitId)
        {
            UnitId = unitId;
        }

        public bool Equals(UnitEquipmentsRequest other)
        {
            return UnitId == other.UnitId;
        }
    }
}
