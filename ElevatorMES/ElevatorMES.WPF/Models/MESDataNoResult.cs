﻿using ElevatorMES.Utilites.Helpers;
using NLog;
using System;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;

namespace ElevatorMES.WPF.Models
{
    public abstract class MESDataNoResult<TRequest> : MESDataProcess<TRequest>
        where TRequest : IEquatable<TRequest>, IAbandonable
    {
        public Func<TRequest, Task> ProcessDataTask { get; set; }

        protected override void OnDefaultData()
        {
            // No Default Data 
        }

        protected async override void ProcessRequests()
        {
            while (currentRequest != null)
            {
                IsLoading = true;
                var request = currentRequest;
                string error = null;
                var timeFrom = DateTime.Now;
                try
                {
                    var processTask = ProcessDataTask?.Invoke(request);
                    if (processTask == null)
                        logger.Debug($"Нет задачи для обработки «{dataDesc}»");
                    else
                    {
                        await processTask;
                        logger.Debug($"Обработка «{dataDesc}» выполнена за {(DateTime.Now - timeFrom).TotalMilliseconds:0.0} мсек");
                    }
                }
                catch (Exception exception)
                {
                    if (!request.IsAbandoned)
                    {
                        var message = $"Ошибка обработки данных «{dataDesc}»";
                        logger.Error(exception, message);
                        error = exception.ExtractMessage();
                    }
                }
                IsLoading = false;
                if (!request.IsAbandoned && queuedRequest == null)
                {
                    ActualError = error;
                }
                else
                {
                    ActualError = null;
                }
                currentRequest = queuedRequest;
                queuedRequest = default;
            }
            OnProcessed();
        }

    }
}
