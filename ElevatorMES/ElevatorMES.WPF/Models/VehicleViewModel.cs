﻿using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System.Collections.Generic;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Data;

namespace ElevatorMES.WPF.Models
{
    [Description("Регистрация транспорта")]
    public class VehicleViewModel : MESDataResult<VehicleRegisterRequest, VehicleUIItem>
    {
        #region Открытые поля

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => isDataRequestEnabled;
            set
            {
                if (isDataRequestEnabled != value)
                {
                    isDataRequestEnabled = value;
                    VehicleTypes.IsEnabled = value;
                    Nomenclatures.IsEnabled = value;
                    VehicleRegNums.IsEnabled = value;
                    IsEnabled = value;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        public VehicleRegNumsResult VehicleRegNums { get; }

        public InputItems<string, VehicleRegNumUIItem> RegNum { get; }

        public VehicleTypesResult VehicleTypes { get; }

        public InputItems<int, VehicleType> VehicleType { get; }

        public NomenclaturesResult Nomenclatures { get; }

        public InputItems<int?, Nomenclature> Nomenclature { get; }

        public InputItem<decimal> Weight { get; }

        public InputDayTime RegisteredAt { get; }

        public InputItem<string> Comment { get; }

        public bool IsForLoadingOnly 
        { 
            get
            {
                return isForLoadingOnly;
            }
            set
            {
                if (isForLoadingOnly != value)
                {
                    isForLoadingOnly = value;
                    Nomenclature.IsVisible = Weight.IsVisible = !value;
                    OnPropertyChanged(nameof(IsForLoadingOnly));
                }
            }
        }

        #endregion


        #region Закрытые поля

        private bool isDataRequestEnabled;

        private bool isForLoadingOnly;

        #endregion


        #region Конструкторы и инициализация

        public VehicleViewModel() : base(false)
        {
            VehicleTypes = new VehicleTypesResult();
            VehicleTypes.Processed += VehicleTypesProcessed;
            VehicleType = new InputItems<int, VehicleType>()
            {
                IsVisible = true
            };
            VehicleRegNums = new VehicleRegNumsResult();
            VehicleRegNums.Processed += VehicleRegNumsProcessed;
            RegNum = new InputItems<string, VehicleRegNumUIItem>()
            {
                IsVisible = true
            };
            Nomenclatures = new NomenclaturesResult();
            Nomenclatures.Processed += NomenclaturesProcessed;
            Nomenclature = new InputItems<int?, Nomenclature>()
            {
                IsVisible = true
            };
            Weight = new InputItem<decimal>()
            {
                IsVisible = true
            };
            RegisteredAt = new InputDayTime()
            {
                IsVisible = true
            };
            Comment = new InputItem<string>()
            {
                IsVisible = true
            };
        }

        #endregion


        #region Обработка событий

        private void NomenclaturesProcessed(object sender, EventArgs e)
        {
            FillNomenclatures();
        }

        private void VehicleTypesProcessed(object sender, EventArgs e)
        {
            FillVehicleTypes();
        }

        private void VehicleRegNumsProcessed(object sender, EventArgs e)
        {
            FillVehicleRegNums();
        }

        #endregion


        #region Заполнение элементов ввода списками

        private void FillNomenclatures()
        {
            Nomenclature.FillItems(Nomenclatures);
        }

        private void FillVehicleTypes()
        {
            VehicleType.FillItems(VehicleTypes);
        }

        private void FillVehicleRegNums()
        {
            RegNum.FillItems(VehicleRegNums);
        }

        #endregion


        #region Проверка данных

        private bool ValidateRegNum()
        {
            if (RegNum.Selected?.Length > 4)
            {
                RegNum.ValidationError = null;
                return true;
            }
            else
            {
                RegNum.ValidationError = "Номер ТС должен состоят хотя бы из 5 символов";
                return false;
            }
        }

        private bool ValidateVehicleType()
        {
            return VehicleType.ValidateItem("Тип транспорта не указан");
        }

        private bool ValidateMultiple(params Func<bool>[] validators)
        {
            bool isValidTotal = true;
            foreach (var validator in validators)
            {
                var isValid = validator();
                isValidTotal = isValidTotal && isValid;
            }
            return isValidTotal;
        }

        private void CleanValidationError()
        {
            RegNum.ValidationError = null;
            VehicleType.ValidationError = null;
            Nomenclature.ValidationError = null;
            Weight.ValidationError = null;
        }

        #endregion


        #region MESDataResult<TaskCreateRequest, TaskUIItem>

        protected override VehicleRegisterRequest GetRequest()
        {
            VehicleRegisterRequest vehicleRequest = null;
            if (ValidateMultiple(ValidateRegNum, ValidateVehicleType))
            {
                if (IsForLoadingOnly)
                {
                    vehicleRequest = new VehicleRegisterRequest(
                        VehicleType.Selected, RegNum.Selected,
                        RegisteredAt.Selected, Comment.Selected);
                }
                else
                {
                    if (Nomenclature.ValidateItem("Не указана номенклатура"))
                    {
                        vehicleRequest = new VehicleRegisterRequest(
                            VehicleType.Selected, RegNum.Selected, 
                            RegisteredAt.Selected, Comment.Selected,
                            Nomenclature.Selected, 
                            Weight.Selected == 0 ? null : (decimal?)Weight.Selected);
                    }
                }    
            }
            return vehicleRequest;
        }

        #endregion
    }
}
