﻿using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System.Collections.Generic;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Data;

namespace ElevatorMES.WPF.Models
{
    [Description("Сопоставление заданий")]
    public class TaskMatchViewModel : MESDataNoResult<TaskMatchRequest>
    {
        #region Открытые поля

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => isDataRequestEnabled;
            set
            {
                if (isDataRequestEnabled != value)
                {
                    isDataRequestEnabled = value;
                    ManualTasks.IsEnabled = value;
                    ERPTasks.IsEnabled = value;
                    IsEnabled = value;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        public TaskMatchingResult ManualTasks { get; }

        public TaskMatchingResult ERPTasks { get; }

        public bool CanMatch
        {
            get => canMatch;
            private set
            {
                if (canMatch!=value)
                {
                    canMatch = value;
                    OnPropertyChanged(nameof(CanMatch));
                }
            }
        }

        #endregion


        #region Закрытые поля

        private bool isDataRequestEnabled;

        private bool canMatch;

        #endregion


        #region Конструкторы и инициализация

        public TaskMatchViewModel() : base()
        {
            ManualTasks = new TaskMatchingResult()
            {
                Description = "Вручную созданные задания.",
                IsQueued = false,
                IsFromERP = false
            };
            ManualTasks.SelectionChanged += ManualTaskSelectionChanged;
            ERPTasks = new TaskMatchingResult()
            {
                Description = "Задания от 1С из очереди.",
                IsQueued = true,
                IsFromERP = true
            };
            ERPTasks.SelectionChanged += ERPTaskSelectionChanged;
        }

        public void RefreshTasks()
        {
            ManualTasks.Refresh();
            ERPTasks.Refresh();
        }

        private void OnTaskSelectionChanged()
        {
            CanMatch =
                ManualTasks.SelectedTask != null && ERPTasks.SelectedTask != null
                && ManualTasks.SelectedTask.NomenclatureId == ERPTasks.SelectedTask.NomenclatureId
                && ManualTasks.SelectedTask.Type == ERPTasks.SelectedTask.Type;
        }

        #endregion


        #region События

        private void ManualTaskSelectionChanged(object sender, EventArgs e)
        {
            OnTaskSelectionChanged();
            if (ManualTasks.SelectedTask != null)
            {
                ERPTasks.TaskType = ManualTasks.SelectedTask.Type;
                ERPTasks.NomenclatureId = ManualTasks.SelectedTask.NomenclatureId;
            }
            else
            {
                ERPTasks.TaskType = null;
                ERPTasks.NomenclatureId = null;
            }
        }

        private void ERPTaskSelectionChanged(object sender, EventArgs e)
        {
            OnTaskSelectionChanged();
        }

        #endregion


        #region MESDataNoResult<TaskMatchRequest>

        protected override TaskMatchRequest GetRequest()
        {
            return CanMatch ?
                new TaskMatchRequest(ManualTasks.SelectedTask.Id, ERPTasks.SelectedTask.Id) : null;
        }

        #endregion
    }
}