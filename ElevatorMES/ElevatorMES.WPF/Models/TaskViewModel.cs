﻿using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System.Collections.Generic;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Data;

namespace ElevatorMES.WPF.Models
{
    [Description("Новое задание")]
    public class TaskViewModel : MESDataResult<TaskCreateRequest, TaskUIItem>
    {

        #region Открытые поля

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => isDataRequestEnabled;
            set
            {
                if (isDataRequestEnabled != value)
                {
                    isDataRequestEnabled = value;
                    Nomenclatures.IsEnabled = value;
                    Organizations.IsEnabled = value;
                    Contragents.IsEnabled = value;
                    Storages.IsEnabled = value;
                    Points.IsEnabled = value;
                    Vehicles.IsEnabled = value;
                    IsEnabled = value;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        public NomenclaturesResult Nomenclatures { get; }

        public OrganizationsResult Organizations { get; }

        public ContragentsResult Contragents { get; }

        public StoragesResult Storages { get; }

        public PointsResult Points { get; }

        public VehiclesResult Vehicles { get; }

        public InputItems<TaskType, KeyValuePair<TaskType, string>> TaskTypeItem { get; }

        public InputItem<bool> Drying { get; }

        public InputItem<bool> Scalping { get; }
        
        public InputItem<bool> Separating { get; }

        public InputItem<string> RegNum { get; }

        public InputItems<int, Nomenclature> Nomenclature { get; }

        public InputItems<int, Organization> Organization { get; }

        public InputItems<int, Contragent> Contragent { get; }

        public InputItems<int, StorageUIItem> StorageSource { get; }

        public InputItems<int, StorageUIItem> StorageDest { get; }

        public InputItems<int, UnitUIItem> LoadingPoint { get; }

        public InputItems<int, UnitUIItem> UnloadingPoint { get; }

        public InputItems<int, VehicleUIItem> Vehicle { get; }

        public InputItem<decimal> Quantity { get; }

        public InputItem<string> Comment { get; }

        #endregion


        #region Закрытые поля

        private bool isDataRequestEnabled;

        private UnitUIItem plant;

		#endregion


		#region Конструкторы и инициализация

		public TaskViewModel() : base(false)
        {
            Nomenclatures = new NomenclaturesResult();
            Nomenclatures.Processed += NomenclaturesProcessed;
            Organizations = new OrganizationsResult();
            Organizations.Processed += OrganizationsProcessed;
            Contragents = new ContragentsResult();
            Contragents.Processed += ContragentsProcessed;
            Storages = new StoragesResult();
            Points = new PointsResult();
            Points.Processed += PointsProcessed;
            TaskTypeItem = new InputItems<TaskType, KeyValuePair<TaskType, string>>()
            {
                IsVisible = true,
                Items = EnumExtension.ToListKeyDesc<TaskType>()
            };
            Vehicles = new VehiclesResult();
            Vehicles.Processed += VehiclesProcessed;
            Drying = new InputItem<bool>();
            Scalping = new InputItem<bool>();
            Separating = new InputItem<bool>();
            RegNum = new InputItem<string>(StringComparer.OrdinalIgnoreCase);
            Vehicle = new InputItems<int, VehicleUIItem>();
            Vehicle.SelectedChanged += VehicleSelectedChanged;
            Nomenclature = new InputItems<int, Nomenclature>();
            Contragent = new InputItems<int, Contragent>();
            Organization = new InputItems<int, Organization>();
            StorageSource = new InputItems<int, StorageUIItem>();
            LoadingPoint = new InputItems<int, UnitUIItem>();
            StorageDest = new InputItems<int, StorageUIItem>();
            UnloadingPoint = new InputItems<int, UnitUIItem>();
            Quantity = new InputItem<decimal>()
            {
                IsVisible = true
            };
            Comment = new InputItem<string>()
            {
                IsVisible = true
            };
            TaskTypeItem.SelectedChanged += TaskTypeChanged;
            Nomenclature.SelectedChanged += NomenclatureChanged;
            StorageDest.SelectedChanged += StorageDestChanged;
            StorageSource.SelectedChanged += StorageSourceChanged;
            TaskTypeItem.Selected = TaskType.Receiving;
        }

        #endregion


        #region Обрыботка событий

        private void TaskTypeChanged(object sender, EventArgs e)
        {
            Drying.IsVisible = Scalping.IsVisible = Separating.IsVisible = true;
            switch (TaskTypeItem.Selected)
            {
                case TaskType.Receiving:
                    RegNum.IsVisible = true;
                    Vehicle.IsVisible = true;
                    Organization.IsVisible = true;
                    Contragent.IsVisible = true;
                    Nomenclature.IsVisible = true;
                    StorageDest.IsVisible = true;
                    UnloadingPoint.IsVisible = true;
                    StorageSource.IsVisible = false;
                    LoadingPoint.IsVisible = false;
                    break;

                case TaskType.ShippingExt:
                    RegNum.IsVisible = true;
                    Vehicle.IsVisible = true;
                    Organization.IsVisible = true;
                    Contragent.IsVisible = true;
                    Nomenclature.IsVisible = true;
                    StorageDest.IsVisible = false;
                    UnloadingPoint.IsVisible = false;
                    StorageSource.IsVisible = true;
                    LoadingPoint.IsVisible = true;
					if (LoadingPoint.SelectedItem == null)
						LoadingPoint.SelectedItem = LoadingPoint.Items.FirstOrDefault();
                    break;

                case TaskType.PlantMoving:
                    RegNum.IsVisible = false;
                    Vehicle.IsVisible = false;
                    Organization.IsVisible = false;
                    Contragent.IsVisible = false;
                    Nomenclature.IsVisible = true;
                    StorageDest.IsVisible = false;
                    UnloadingPoint.IsVisible = false;
                    StorageSource.IsVisible = true;
                    LoadingPoint.IsVisible = false;
                    break;

                case TaskType.Sleeve:
                    RegNum.IsVisible = false;
                    Vehicle.IsVisible = true;
                    Organization.IsVisible = false;
                    Contragent.IsVisible = false;
                    Nomenclature.IsVisible = true;
                    StorageDest.IsVisible = true;
                    UnloadingPoint.IsVisible = false;
                    StorageSource.IsVisible = true;
                    LoadingPoint.IsVisible = true;
                    if (LoadingPoint.SelectedItem==null)
                        LoadingPoint.SelectedItem = LoadingPoint.Items.FirstOrDefault();
                    break;

                case TaskType.SilosMoving:
                    RegNum.IsVisible = false;
                    Vehicle.IsVisible = false;
                    Organization.IsVisible = false;
                    Contragent.IsVisible = false;
                    Nomenclature.IsVisible = false;
                    StorageDest.IsVisible = true;
                    UnloadingPoint.IsVisible = false;
                    StorageSource.IsVisible = true;
                    LoadingPoint.IsVisible = false;
                    break;

            }
            CleanValidationError();
            FillStorages();
        }

        private void NomenclaturesProcessed(object sender, EventArgs e)
        {
            FillNomenclatures();
        }

        private void OrganizationsProcessed(object sender, EventArgs e)
        {
            FillOrganizations();
        }

        private void ContragentsProcessed(object sender, EventArgs e)
        {
            FillContragents();
        }

        private void PointsProcessed(object sender, EventArgs e)
        {
            FillPoints();
        }

        private void VehiclesProcessed(object sender, EventArgs e)
        {
            FillVehicles();
        }

        private void NomenclatureChanged(object sender, EventArgs e)
        {
            FillStorages();
        }

        private void StorageDestChanged(object sender, EventArgs e)
        {
            if (TaskTypeItem.Selected == TaskType.Receiving)
            {
                if (Nomenclature.SelectedItem == null)
                    Nomenclature.Selected = StorageDest.SelectedItem?.NomenclatureId ?? 0;
            }
            else if (TaskTypeItem.Selected == TaskType.SilosMoving)
            {
                StorageSource.FillItems(Storages, m => m.NomenclatureId == StorageDest.SelectedItem?.NomenclatureId);
            }
        }

        private void StorageSourceChanged(object sender, EventArgs e)
        {
            if (TaskTypeItem.Selected == TaskType.ShippingExt 
                || TaskTypeItem.Selected == TaskType.Sleeve
                || TaskTypeItem.Selected == TaskType.PlantMoving)
            {
                if (Nomenclature.SelectedItem == null)
                    Nomenclature.Selected = StorageSource.SelectedItem?.NomenclatureId ?? 0;
            }
            else if (TaskTypeItem.Selected == TaskType.SilosMoving)
            {
                var src = StorageSource.SelectedItem;
                StorageDest.FillItems(Storages, m => src == null ||
                    m.NomenclatureId == src.NomenclatureId);
            }
        }

        private void VehicleSelectedChanged(object sender, EventArgs e)
        {
            var vehicle = Vehicle.SelectedItem;
            if (vehicle != null)
            {
                if (TaskTypeItem.Selected == TaskType.Receiving)
                {
                    if (vehicle.RawId.HasValue)
                    {
                        Nomenclature.Selected = vehicle.RawId.Value;
                        if (vehicle.RawWeight.HasValue)
                            Quantity.Selected = vehicle.RawWeight.Value;
                        Vehicle.ValidationError = null;
                    }
                    else
                        Vehicle.ValidationError = "Выбранный автотранспорт пустой";
                }
            }
        }

        #endregion


        #region Заполнение элементов ввода списками

        private void FillNomenclatures()
        {
            Nomenclature.FillItems(Nomenclatures);
        }

        private void FillOrganizations()
        {
            Organization.FillItems(Organizations);
        }

        private void FillContragents()
        {
            Contragent.FillItems(Contragents);
        }

        private void FillStorages()
        {
            bool noNomenclatureSelected = Nomenclature.SelectedItem == null;
            switch (TaskTypeItem.Selected)
            {
                case TaskType.Receiving:
                    StorageDest.FillItems(Storages, m => (noNomenclatureSelected
                        || m.NomenclatureId == Nomenclature.Selected) && m.UnitType!=UnitType.SleeveStorage);
                    break;

                case TaskType.ShippingExt:
                case TaskType.PlantMoving:
                    StorageSource.FillItems(Storages, m => (noNomenclatureSelected
                        || m.NomenclatureId == Nomenclature.Selected) && m.UnitType != UnitType.SleeveStorage);
                    break;

                case TaskType.SilosMoving:
                    StorageSource.FillItems(Storages, m => m.UnitType!=UnitType.SleeveStorage);
                    var src = StorageSource.SelectedItem;
                    StorageDest.FillItems(Storages, m => (src==null || m.NomenclatureId == src.NomenclatureId)
                        && m.UnitType!=UnitType.SleeveStorage);
                    break;

				case TaskType.Sleeve:
					StorageSource.FillItems(Storages, m => (noNomenclatureSelected
						|| m.NomenclatureId == Nomenclature.Selected) && m.UnitType != UnitType.SleeveStorage);
					StorageDest.FillItems(Storages, m => m.UnitType == UnitType.SleeveStorage);
					break;

			}
		}

        private void FillPoints()
        {
            UnloadingPoint.FillItems(Points, m => m.UnitType == UnitType.UnloadingPoint);
            LoadingPoint.FillItems(Points, m => m.UnitType == UnitType.LoadingPoint);
            plant = Points.ActualResult.FirstOrDefault( m => m.UnitType == UnitType.CompoundFeedPlant);
        }

        private void FillVehicles()
        {
            Vehicle.FillItems(Vehicles);
        }

        #endregion


        #region Проверка данных

        private bool ValidateRegNum()
        {
			if (RegNum.Selected?.Length > 4)
            {
                Vehicle.ValidationError = null;
                return true;
            }
            else
            {
				Vehicle.ValidationError = "Номер ТС должен состоят хотя бы из 5 символов";
                return false;
            }
		}

        private bool ValidateNomenclature()
        {
            return Nomenclature.ValidateItem("Номенклатура не указана");
        }

        private bool ValidateOrganization()
        {
            return Organization.ValidateItem("Организация не указана");
        }

        private bool ValidateContragent()
        {
            return Contragent.ValidateItem("Контрагент не указан");
        }

        private bool ValidateStorageSource()
        {
            if (StorageSource.ValidateItem("Склад-источник не указан"))
            {
                if (!StorageSource.SelectedItem.NomenclatureId.HasValue)
                {
                    StorageSource.ValidationError = "Складу-источнику не назначена номенклатура";
                    return false;
                }
                else
                {
                    StorageSource.ValidationError = null;
                    return true;
                }
            }
            else
                return false;
        }

        private bool ValidateStorageDest()
        {
            if (StorageDest.ValidateItem("Склад-приемник не указан"))
            {
                if (!StorageDest.SelectedItem.NomenclatureId.HasValue)
                {
                    StorageDest.ValidationError = "Складу-приёмнику не назначена номенклатура";
                    return false;
                }
                else
                {
                    StorageDest.ValidationError = null;
                    return true;
                }
            }
            else
                return false;
        }

        private bool ValidateUnloadingPoint()
        {
            return UnloadingPoint.ValidateItem("Точка выгрузки не задана");
        }

        private bool ValidateLoadingPoint()
        {
            return LoadingPoint.ValidateItem("Точка погрузки не задана");
        }

		private bool ValidatePlant()
		{
			if (plant == null)
			{
				StorageSource.ValidationError = "Не найден завод в справочнике";
				return false;
			}
			return true;
		}

		private bool ValidateMultiple(params Func<bool>[] validators)
        {
            bool isValidTotal = true;
            foreach(var validator in validators)
            {
                var isValid = validator();
                isValidTotal = isValidTotal && isValid;
            }
            return isValidTotal;
        }

        private void CleanValidationError()
        {
            RegNum.ValidationError = null;
            Nomenclature.ValidationError = null;
            Organization.ValidationError = null;
            StorageDest.ValidationError = null;
        }

        #endregion


        #region MESDataResult<TaskCreateRequest, TaskUIItem>

        protected override TaskCreateRequest GetRequest()
        {
            TaskCreateRequest taskRequest = null;
            switch (TaskTypeItem.Selected)
            {
                case TaskType.Receiving:
                    if (ValidateMultiple(ValidateRegNum, ValidateNomenclature, ValidateOrganization,
                        ValidateContragent, ValidateUnloadingPoint, ValidateStorageDest))
                        taskRequest = TaskCreateRequest.Receiving(
                            Drying.Selected, Scalping.Selected, Separating.Selected,
                            RegNum.Selected, Nomenclature.Selected, Organization.Selected, 
                            Contragent.Selected, UnloadingPoint.Selected, StorageDest.Selected, 
                            Quantity.Selected, Comment.Selected);
                    break;

                case TaskType.ShippingExt:
                    if (ValidateMultiple(ValidateRegNum, ValidateNomenclature, ValidateOrganization,
                        ValidateContragent, ValidateStorageSource, ValidateLoadingPoint))
                        taskRequest = TaskCreateRequest.Shipping(
                            Drying.Selected, Scalping.Selected, Separating.Selected,
                            RegNum.Selected, Nomenclature.Selected, Organization.Selected,
                            Contragent.Selected, StorageSource.Selected, LoadingPoint.Selected,
                            Quantity.Selected, Comment.Selected);
                    break;

                case TaskType.PlantMoving:
                    if (ValidateMultiple(ValidateNomenclature, ValidateStorageSource, ValidatePlant))
                        taskRequest = TaskCreateRequest.Plant(
                            Drying.Selected, Scalping.Selected, Separating.Selected,
                            Nomenclature.Selected, StorageSource.Selected, plant?.UnitId ?? 0,
                            Quantity.Selected, Comment.Selected);
                    break;

                case TaskType.Sleeve:
                    if (ValidateMultiple(ValidateNomenclature, ValidateStorageSource, ValidateLoadingPoint))
                        taskRequest = TaskCreateRequest.Sleeve(
                            Drying.Selected, Scalping.Selected, Separating.Selected,
							RegNum.Selected, Nomenclature.Selected, StorageSource.Selected,
                            LoadingPoint.Selected, StorageDest.Selected, Quantity.Selected, Comment.Selected);
                    break;

                case TaskType.SilosMoving:
                    if (ValidateMultiple(ValidateStorageSource, ValidateStorageDest))
                        taskRequest = TaskCreateRequest.Silos(
                            Drying.Selected, Scalping.Selected, Separating.Selected,
                            StorageSource.SelectedItem.NomenclatureId.Value, StorageSource.Selected, 
                            StorageDest.Selected, Quantity.Selected, Comment.Selected);
                    break;

                default:
                    ActualError = "Не реализовано сохранение";
                    break;
            }
            return taskRequest;
        }

        #endregion

    }
}
