﻿using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class DumyInventoryViewModel : StorageInventoryViewModel
    {
        public DumyInventoryViewModel() : base(new MainViewModel())
        {
            StorageItem = new StorageUIItem()
            {
                UnitType = UnitType.Silos,
                UnitId = 1,
                UnitDisplayName = "Силос №1",
                Capacity = 1000,
                NomenclatureId = 2,
                NomenclatureDisplayName = "Рожь класс 2 2019",
                Quantity = 320
            };
            Layers.ActualResult = new LayerUIItem[]
            {
                new LayerUIItem()
                {
                    NomenclatureId = 2,
                    NomenclatureName = "Рожь класс 2 2019",
                    CreatedAt = new DateTime(2021,02,17, 19, 49, 33),
                    OrganizationId = 3,
                    OrganizationName = "Поставщик №1 (неизвестная компания)",
                    LotId = 1,
                    LotNo = "ПОСТНК_20201212_12",
                    Quantity = 120
                },
                new LayerUIItem()
                {
                    NomenclatureId = 2,
                    NomenclatureName = "Рожь класс 2 2019",
                    CreatedAt = new DateTime(2021,02,14, 14, 32, 11),
                    OrganizationId = 3,
                    OrganizationName = "Поставщик №2 (известная компания)",
                    LotId = 2,
                    LotNo = "ПОСТИК_20201212_12",
                    Quantity = 20
                },
                new LayerUIItem()
                {
                    NomenclatureId = 2,
                    NomenclatureName = "Рожь класс 2 2019",
                    CreatedAt = new DateTime(2021,02,12, 10, 02, 03),
                    OrganizationId = 3,
                    OrganizationName = "ООО Рога и копыта",
                    LotId = 3,
                    LotNo = "РИК_20201217_12",
                    Quantity = 180
                }
            };
            Layers.SelectedLayer = Layers.ActualResult[1];
        }
    }
}
