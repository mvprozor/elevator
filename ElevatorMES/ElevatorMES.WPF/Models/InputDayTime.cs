﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using ElevatorMES.Utilites.Helpers;

namespace ElevatorMES.WPF.Models
{
    public class InputDayTime : INotifyPropertyChanged
    {
        public InputItem<DateTime> InputDate { get; }

        public InputItems<int, KeyValuePair<int, string>> InputHour { get; }

        public InputItems<int, KeyValuePair<int, string>> InputMinute { get; }

        public bool IsVisible
        {
            get => isVisible;
            set
            {
                if (isVisible!=value)
                {
                    isVisible = value;
                    OnPropertyChanged(nameof(IsVisible));
                }
            }
        }

        public string ValidationError
        {
            get => validationError;
            set
            {
                if (String.Compare(validationError, value, StringComparison.OrdinalIgnoreCase) != 0)
                {
                    validationError = value;
                    OnPropertyChanged(nameof(ValidationError));
                }
            }
        }

        private bool isVisible;
        private string validationError;

        public DateTime Selected
        {
            get 
            {
                return InputDate.Selected.Date.Add(
                    new TimeSpan(0, InputHour.Selected, InputMinute.Selected, 0, 0));
            }
            set
            {
                var now = value.Floor(TimeSpan.FromMinutes(1));
                InputDate.Selected = now.Date;
                InputHour.Selected = now.Hour;
                InputMinute.Selected = now.Minute;
            }
        }

        public InputDayTime()
        {
            InputDate = new InputItem<DateTime>()
            {
                IsVisible = true,
                Selected = DateTime.Now
            };
            InputHour = new InputItems<int, KeyValuePair<int, string>>()
            {
                IsVisible = true,
                Items = Enumerable.Range(0, 24).Select(m => new KeyValuePair<int, string>(m, $"{m:00} час.")).ToList()
            };
            InputMinute = new InputItems<int, KeyValuePair<int, string>>()
            {
                IsVisible = true,
                Items = Enumerable.Range(0, 60)
                    //.Select(m => m * 5)
                    .Select(m => new KeyValuePair<int, string>(m, $"{m:00} мин.")).ToList()
            };
            Selected = DateTime.Now;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
