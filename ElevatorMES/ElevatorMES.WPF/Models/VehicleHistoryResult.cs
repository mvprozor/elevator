﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using System.Linq.Dynamic.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ElevatorMES.WPF.Models
{
    [Description("Журнал истории автотранспорта")]
    public class VehicleHistoryResult : MESDataResult<VehicleHistoryRequest, IList<VehicleActivity>>, ITabViewModel, IPrevSelection, IPrevSorting
    {
        public VehicleActivity SelectedActivity
        {
            get => selectedActivity;
            set
            {
                if (selectedActivity != value)
                {
                    prevSelectedId = selectedActivity?.Id;
                    selectedActivity = value;
                    OnPropertyChanged(nameof(SelectedActivity));
                }
            }
        }

        public string PrevSorting { get; set; }

        private VehicleActivity selectedActivity;

        private int? prevSelectedId;

        private int? vehicelId;

        public int? VehicleId
        {
            get => vehicelId;
            set
            {
                if (vehicelId != value)
                {
                    vehicelId = value;
                    NewRequest();
                    OnPropertyChanged(nameof(VehicleId));
                }
            }
        }

        protected override VehicleHistoryRequest GetRequest()
        {
            return new VehicleHistoryRequest(VehicleId);
        }

        public object GetPrevSelection()
        {
            return ActualResult?.FirstOrDefault(m => m.Id == prevSelectedId);
        }

        protected override IList<VehicleActivity> PrepareResult(IList<VehicleActivity> source)
        {
            return String.IsNullOrWhiteSpace(PrevSorting) ?
                source : source.AsQueryable<VehicleActivity>().OrderBy(PrevSorting).ToList();
        }

        #region ITabViewModel
        #endregion

    }
}
