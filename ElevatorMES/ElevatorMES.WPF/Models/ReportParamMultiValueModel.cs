﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    public class ReportParamMultiValueModel<TValue> : ReportParamListModel<TValue> where TValue : struct, IEquatable<TValue>
    {
        protected override void ItemSelectedChanged(object sender, EventArgs e)
        {
            var isPrevValid = IsValidValue;
            selectedValues = null;
            OnPropertyChanged(nameof(SelectedValues));
            if (IsValidValue!=isPrevValid)
                OnPropertyChanged(nameof(IsValidValue));
            OnValueChanged();
        }

        public TValue?[] SelectedValues 
        { 
            get
            {
                if (selectedValues == null)
                {
                    selectedValues = Values == null ? Array.Empty<TValue?>() :
                        Values.Where(m => m.IsSelected).Select(n => n.Value).ToArray();
                }
                return selectedValues;
            }
        }

        public override object Value => SelectedValues;

        public override bool IsValidValue
        {
            get => AllowNull || SelectedValues.Length>0;
        }

        public override string[] ViewerValues => SelectedValues.Select(m => m.HasValue ? m.ToString() : null).ToArray();

        private TValue?[] selectedValues;

        public ReportParamMultiValueModel(string name, string title, bool allowNull) :
            base(name, title, allowNull)
        {
        }
    }
}
