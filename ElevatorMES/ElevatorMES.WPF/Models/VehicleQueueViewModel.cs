﻿using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Helpers;
using System.Collections.Generic;
using System.ComponentModel;

namespace ElevatorMES.WPF.Models
{
    public class VehicleQueueViewModel : INotifyPropertyChanged
    {
        public VehicleQueue Queue { get; }
        public string Name { get; }
        public int Count 
        {
            get => Items?.Count ?? 0;
        }

        public bool IsSelected
        {
            get => isSelected;
            set
            {
                if (value != isSelected)
                {
                    isSelected = value;
                    OnPropertyChanged(nameof(IsSelected));
                }
            }
        }
        private bool isSelected;

        public IList<VehicleUIItem> Items 
        {
            get => items;
            set
            {
                if (items!=value)
                {
                    items = value;
                    OnPropertyChanged(nameof(Items));
                    OnPropertyChanged(nameof(Count));
                }
            }
        }
        private IList<VehicleUIItem> items;

        public VehicleQueueViewModel(VehicleQueue queue, bool isDefault = false)
        {
            Queue = queue;
            Name = queue.GetDescription();
            IsSelected = isDefault;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
