﻿using ElevatorMES.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос создания задания")]
    public class TaskCreateRequest : MESDataRequest, IEquatable<TaskCreateRequest>
    {
        public TaskType TaskType { get; }
        public bool IsDrying { get; }
        public bool IsScalping { get; }
        public bool IsSeparating { get; }
        public string VehicleRegNum { get; }
        public int NomenclatureId { get; }
        public int OrganizationId { get; }
        public int ContragentId { get; }
        public int UnitDestId { get; }
        public int UnitSourceId { get; }
        public int UnitExtId { get; }
        public decimal Quantity { get; }
        public string Comment { get; }

        private TaskCreateRequest(
            TaskType taskType, bool isDrying, bool isScalping, bool isSeparating,
            string vehicleRegNum, int nomenclatureId, int organizationId, int contragentId,
            int unitSourceId, int unitDestId, int unitExtId, decimal quantity, string comment)
        {
            TaskType = taskType;
            IsDrying = isDrying;
            IsScalping = isScalping;
            IsSeparating = isSeparating;
            VehicleRegNum = vehicleRegNum;
            NomenclatureId = nomenclatureId;
            OrganizationId = organizationId;
            ContragentId = contragentId;
            UnitSourceId = unitSourceId;
            UnitDestId = unitDestId;
            UnitExtId = unitExtId;
            Quantity = quantity;
            Comment = comment;
        }

        public static TaskCreateRequest Receiving(bool isDrying, bool isScalping, bool isSeparating,
            string vehicleRegNum, int nomenclatureId, int organizationId, int contragentId,
            int unloadingPointId, int storageDestId, decimal quantity, string comment)
        {
            return new TaskCreateRequest(TaskType.Receiving, isDrying, isScalping, isSeparating,
                vehicleRegNum, nomenclatureId, organizationId, contragentId, unloadingPointId,
                storageDestId, 0, quantity, comment);
        }

        public static TaskCreateRequest Shipping(bool isDrying, bool isScalping, bool isSeparating,
            string vehicleRegNum, int nomenclatureId, int organizationId, int contragentId,
            int storageSourceId, int loadingPointId, decimal quantity, string comment)
        {
            return new TaskCreateRequest(TaskType.ShippingExt, isDrying, isScalping, isSeparating,
                vehicleRegNum, nomenclatureId, organizationId, contragentId, storageSourceId,
                loadingPointId, 0, quantity, comment);
        }

        public static TaskCreateRequest Plant(bool isDrying, bool isScalping, bool isSeparating,
            int nomenclatureId, int storageSourceId, int loadingPointId, decimal quantity, string comment)
        {
            return new TaskCreateRequest(TaskType.PlantMoving, isDrying, isScalping, isSeparating,
                null, nomenclatureId, 0, 0, storageSourceId, loadingPointId, 0, quantity, comment);
        }

        public static TaskCreateRequest Sleeve(bool isDrying, bool isScalping, bool isSeparating,
			string vehicleRegNum, int nomenclatureId, int storageSourceId, 
            int loadingPointId, int storageSleeveId, decimal quantity, string comment)
        {
            return new TaskCreateRequest(TaskType.Sleeve, isDrying, isScalping, isSeparating,
                vehicleRegNum, nomenclatureId, 0, 0, storageSourceId, loadingPointId, storageSleeveId, quantity, comment);
        }

        public static TaskCreateRequest Silos(bool isDrying, bool isScalping, bool isSeparating,
            int nomenclatureId, int storageSourceId, int storageDestId, decimal quantity, string comment)
        {
            return new TaskCreateRequest(TaskType.SilosMoving, isDrying, isScalping, isSeparating,
                null, nomenclatureId, 0, 0, storageSourceId, storageDestId, 0, quantity, comment);
        }

        public bool Equals(TaskCreateRequest other)
        {
            return TaskType == other.TaskType && IsDrying == other.IsDrying
                && IsScalping == other.IsScalping && IsSeparating == other.IsSeparating
                && String.Compare(VehicleRegNum, other.VehicleRegNum, StringComparison.OrdinalIgnoreCase) == 0
                && NomenclatureId == other.NomenclatureId && OrganizationId == other.OrganizationId
                && ContragentId == other.ContragentId && UnitDestId == other.UnitDestId 
                && UnitSourceId == other.UnitSourceId && UnitExtId == other.UnitExtId
                && String.Compare(Comment, other.Comment, StringComparison.OrdinalIgnoreCase)==0;
        }
    }
}
