﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ElevatorMES.WPF.Models
{
    public class MainViewModel : INotifyPropertyChanged, IRefreshable
    {

        #region Открытые поля

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => _isDataRequestEnabled;
            set
            {
                if (_isDataRequestEnabled!=value)
                {
                    _isDataRequestEnabled = value;
                    OperInfo.IsEnabled = value;
                    ActiveTasks.IsEnabled = value;
                    QueuedTasks.IsEnabled = value;
                    HistoryTasks.IsEnabled = value;
                    Storages.IsEnabled = value;
                    Vehicles.IsEnabled = value;
                    TransmitLog.IsEnabled = value;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        /// <summary>
        /// Сводные оперативные данные
        /// </summary>
        public OperInfoResult OperInfo { get; }

        /// <summary>
        /// Активные (в работе) задания
        /// </summary>
        public TasksActiveResult ActiveTasks { get; }

        /// <summary>
        /// Задания в очереди
        /// </summary>
        public TasksQueuedResult QueuedTasks { get; }

        /// <summary>
        /// Архив заданий
        /// </summary>
        public TasksHistoryResult HistoryTasks { get; }

        /// <summary>
        /// Состояние складов
        /// </summary>
        public StoragesStateResult Storages { get; }

        /// <summary>
        /// Журнал автотранспорта
        /// </summary>
        public VehiclesResult Vehicles { get; }

        /// <summary>
        /// Отчеты
        /// </summary>
        public ReportsViewModel Reports { get; }

        /// <summary>
        /// Журнал отправки в ERP
        /// </summary>
        public TransmitLogResult TransmitLog { get; }

        /// <summary>
        /// Текущая закладка
        /// </summary>
        public ITabViewModel SelectedTab
        {
            get => _selectedTab;
            set
            {
                if (_selectedTab != value)
                {
                    _selectedTab = value;
                    OnPropertyChanged(nameof(SelectedTab));
                    (_selectedTab as IRefreshable)?.Refresh();
                }
            }
        }

        /// <summary>
        /// Признак возможности управления
        /// </summary>
        public bool CanControl { get; private set; }

        /// <summary>
        /// Признак возможности настройки
        /// </summary>
        public bool CanSetup { get; private set; }

        #endregion


        #region Закрытые поля

        private bool _isDataRequestEnabled;

        private ITabViewModel _selectedTab;

        #endregion


        #region Конструкторы и иницилизация

        public MainViewModel()
        {
            OperInfo = new OperInfoResult();
            OperInfo.PropertyChanged += OperInfoPropertyChanged;
            ActiveTasks = new TasksActiveResult();
            QueuedTasks = new TasksQueuedResult();
            HistoryTasks = new TasksHistoryResult();
            Storages = new StoragesStateResult();
            Vehicles = new VehiclesResult();
            Reports = new ReportsViewModel();
            TransmitLog = new TransmitLogResult();
        }

        private void OperInfoPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName==nameof(OperInfo.ActualResult))
            {
                var userRole = OperInfo.ActualResult?.UserRole;
                bool canControl = userRole < UserRole.Reader;
                if (CanControl != canControl)
                {
                    CanControl = canControl;
                    OnPropertyChanged(nameof(CanControl));
                }
                bool canSetup = userRole < UserRole.Operator;
                if (CanSetup != canSetup)
                {
                    CanSetup = canSetup;
                    OnPropertyChanged(nameof(CanSetup));
                }
            }
        }

        #endregion


        #region IRefreshable

        public void Refresh()
        {
            OperInfo.Refresh();
            ActiveTasks.Refresh();
            (SelectedTab as IRefreshable)?.Refresh();
        }

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
