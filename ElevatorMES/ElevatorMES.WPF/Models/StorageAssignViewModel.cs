﻿using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System.Collections.Generic;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Data;

namespace ElevatorMES.WPF.Models
{
    [Description("Новое назначение склада")]
    public class StorageAssignViewModel : MESDataNoResult<StorageAssignRequest>
    {
        #region Открытые поля

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => isDataRequestEnabled;
            set
            {
                if (isDataRequestEnabled != value)
                {
                    isDataRequestEnabled = value;
                    Nomenclatures.IsEnabled = value;
                    IsEnabled = value && storageItem != null;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        public NomenclaturesResult Nomenclatures { get; }

        public InputItems<int, Nomenclature> Nomenclature { get; }

        public InputItem<bool> Unused { get; }

        public StorageUIItem StorageItem
        {
            get => storageItem;
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(StorageItem));
                IsEnabled = isDataRequestEnabled;
                if (storageItem != value)
                {
                    storageItem = value;
                    if (storageItem.NomenclatureId.HasValue)
                    {
                        Unused.Selected = false;
                        Nomenclature.IsVisible = true;
                        Nomenclature.Selected = storageItem.NomenclatureId.Value;
                    }
                    else
                    {
                        Nomenclature.IsVisible = false;
                        Unused.Selected = true;
                    }
                    OnPropertyChanged(nameof(StorageItem));
                }
            }
        }

        #endregion


        #region Закрытые поля

        private bool isDataRequestEnabled;

        private StorageUIItem storageItem;

        #endregion


        #region Конструкторы и инициализация

        public StorageAssignViewModel()
        {
            Nomenclatures = new NomenclaturesResult();
            Nomenclatures.Processed += NomenclaturesProcessed;
            Nomenclature = new InputItems<int, Nomenclature>();
            Unused = new InputItem<bool>();
            Unused.SelectedChanged += UnusedSelectedChanged;
        }

        #endregion


        #region Обрыботка событий

        private void NomenclaturesProcessed(object sender, EventArgs e)
        {
            FillNomenclatures();
        }

        private void UnusedSelectedChanged(object sender, EventArgs e)
        {
            Nomenclature.IsVisible = !Unused.Selected;
        }

        #endregion


        #region Заполнение элементов ввода списками

        private void FillNomenclatures()
        {
            Nomenclature.FillItems(Nomenclatures);
        }

        #endregion


        #region Проверка данных и сохранение

        protected override StorageAssignRequest GetRequest()
        {
            return new StorageAssignRequest(storageItem.UnitId,
                Unused.Selected ? null : (int?)Nomenclature.Selected);
        }

        #endregion
    }
}

