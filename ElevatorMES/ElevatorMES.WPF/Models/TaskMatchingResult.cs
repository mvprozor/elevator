﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.UI;
using System.Linq.Dynamic.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Models
{
    [Description("Задания для сопоставления")]
    public class TaskMatchingResult : MESDataResult<TaskMatchingRequest, IList<TaskMatchUIItem>>, ITabViewModel, IPrevSelection, IPrevSorting
    {
        public event EventHandler SelectionChanged;

        public TaskMatchUIItem SelectedTask
        {
            get => selectedTask;
            set
            {
                if (selectedTask != value)
                {
                    prevSelectedId = selectedTask?.Id;
                    selectedTask = value;
                    OnPropertyChanged(nameof(SelectedTask));
                    SelectionChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        private TaskMatchUIItem selectedTask;

        private int? prevSelectedId;

        public string PrevSorting { get; set; }


        private int? taskId;

        private string docId;

        private bool? isFromERP;

        private bool isQueued;

        private string textFilter;

        private TaskType? taskType;

        private int? nomenclatureId;

        private int? taskCount;

        private string description;

        public string Description
        {
            get => description;
            set
            {
                if (!String.Equals(value, description, StringComparison.OrdinalIgnoreCase))
                {
                    description = value;
                    OnPropertyChanged(nameof(Description));
                }
            }
        }

        public int? TaskId
        {
            get => taskId;
            set
            {
                if (taskId!=value)
                {
                    taskId = value;
                    NewRequest();
                    OnPropertyChanged(nameof(TaskId));
                }
            }
        }

        public string DocId
        {
            get => docId;
            set
            {
                if (!String.Equals(docId, value, StringComparison.OrdinalIgnoreCase))
                {
                    docId = value;
                    NewRequest();
                    OnPropertyChanged(nameof(DocId));
                }
            }
        }

        public bool? IsFromERP
        {
            get => isFromERP;
            set
            {
                if (isFromERP != value)
                {
                    isFromERP = value;
                    NewRequest();
                    OnPropertyChanged(nameof(IsFromERP));
                }
            }
        }

        public string TextFilter
        {
            get => textFilter;
            set
            {
                if (!String.Equals(textFilter, value, StringComparison.OrdinalIgnoreCase))
                {
                    textFilter = value;
                    NewRequest();
                    OnPropertyChanged(nameof(TextFilter));
                }
            }
        }

        public TaskType? TaskType
        {
            get => taskType;
            set
            {
                if (taskType != value)
                {
                    taskType = value;
                    NewRequest();
                    OnPropertyChanged(nameof(TaskType));
                }
            }
        }

        public int? NomenclatureId
        {
            get => nomenclatureId;
            set
            {
                if (nomenclatureId != value)
                {
                    nomenclatureId = value;
                    NewRequest();
                    OnPropertyChanged(nameof(NomenclatureId));
                }
            }
        }

        public int? TaskCount
        {
            get => taskCount;
            set
            {
                if (taskCount != value)
                {
                    taskCount = value;
                    NewRequest();
                    OnPropertyChanged(nameof(TaskCount));
                }
            }
        }

        public bool IsQueued
        {
            get => isQueued;
            set
            {
                if (isQueued != value)
                {
                    isQueued = value;
                    NewRequest();
                    OnPropertyChanged(nameof(IsQueued));
                }
            }
        }

        protected override TaskMatchingRequest GetRequest()
        {
            return new TaskMatchingRequest(TaskId, DocId, IsFromERP, IsQueued, TaskType, NomenclatureId, TextFilter, TaskCount);
        }

        public object GetPrevSelection()
        {
            return ActualResult?.FirstOrDefault(m => m.Id == prevSelectedId);
        }

        protected override IList<TaskMatchUIItem> PrepareResult(IList<TaskMatchUIItem> source)
        {
            return String.IsNullOrWhiteSpace(PrevSorting) ?
                source : source.AsQueryable<TaskMatchUIItem>().OrderBy(PrevSorting).ToList();
        }

        #region ITabViewModel
        #endregion

        public TaskMatchingResult()
        {
        }
    }
}
