﻿using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос останова задания")]
    public class TaskStopRequest : MESDataRequest, IEquatable<TaskStopRequest>
    {
        public TaskStopArg Arg { get; }

        public TaskStopRequest(TaskStopArg arg)
        {
            Arg = arg;
        }

        public bool Equals(TaskStopRequest other)
        {
            return Arg.TaskId == other.Arg.TaskId && Arg.IsCloseTask == other.Arg.IsCloseTask 
                && Arg.Weight == other.Arg.Weight && Arg.IsFailed == other.Arg.IsFailed
                && String.Compare(Arg.Comment, other.Arg.Comment, StringComparison.OrdinalIgnoreCase) == 0;
        }
    }
}
