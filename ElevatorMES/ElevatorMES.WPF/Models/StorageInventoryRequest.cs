﻿using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using System;
using System.ComponentModel;

namespace ElevatorMES.WPF.Models
{
    [Description("Запрос инвентаризации склада")]
    public class StorageInventoryRequest : MESDataRequest, IEquatable<StorageInventoryRequest>
    {
        public StorageInventoryArg Arg { get; }

        public StorageInventoryRequest(int unitId, DateTime? topLayerCreatedAt, decimal topLayerQuantity
            ,DateTime? bottomLayerCreatedAt, decimal bottomLayerQuantity, decimal newQuantity, InventoryMode mode,
            string user, string comment = null)
        {
            Arg = new StorageInventoryArg()
            {
                UnitId = unitId,
                TopLayerCreatedAt = topLayerCreatedAt,
                TopLayerQuantity = topLayerQuantity,
                BottomLayerCreatedAt = bottomLayerCreatedAt,
                BottomLayerQuantity = bottomLayerQuantity,
                NewQuantity = newQuantity,
                Mode = mode,
                User = user,
                Comment = comment
            };
        }

        public bool Equals(StorageInventoryRequest other)
        {
            return Arg.Equals(other.Arg);
        }
    }
}
