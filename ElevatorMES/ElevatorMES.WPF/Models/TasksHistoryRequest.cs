﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Models
{
    [Description("История заданий")]
    public class TasksHistoryRequest : MESDataRequest, IEquatable<TasksHistoryRequest>
    {
        public DateTime? DateFrom { get; }
        public DateTime? DateTill { get; }
        public string SearchText { get; }

        public TasksHistoryRequest(DateTime? dateFrom, DateTime? dateTill, string searchText)
        {
            DateFrom = dateFrom;
            DateTill = dateTill;
            SearchText = searchText;
        }

        public bool Equals(TasksHistoryRequest other)
        {
            return DateFrom==DateFrom && DateTill==other.DateTill 
                && String.Compare(SearchText, other.SearchText, StringComparison.InvariantCultureIgnoreCase)==0;
        }
    }
}
