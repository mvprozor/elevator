﻿using System;
using System.ComponentModel;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF.Models
{
    [Description("Справочник")]
    public class DictionaryUpdateRequest : MESDataRequest, IEquatable<DictionaryUpdateRequest>
    {
        public DictionaryType Type { get; }
        public int? Id { get; }
        public string DisplayName { get; }
        public string Code1C { get; }
        public string Type1C { get; }
        public string CodeParent1C { get; }

        public DictionaryUpdateRequest(DictionaryType type,
            int? id, string displayName, string code1C, string type1C, string codeParent1C)
        {
            Type = type;
            Id = id;
            DisplayName = displayName;
            Code1C = code1C;
            Type1C = type1C;
            CodeParent1C = codeParent1C;
        }

        public bool Equals(DictionaryUpdateRequest other)
        {
            return Type == other.Type && Id == other.Id 
                && String.Equals(DisplayName, other.DisplayName, StringComparison.InvariantCultureIgnoreCase)
                && String.Equals(Code1C, other.Code1C, StringComparison.InvariantCultureIgnoreCase)
                && String.Equals(Type1C, other.Type1C, StringComparison.InvariantCultureIgnoreCase)
                && String.Equals(CodeParent1C, other.CodeParent1C, StringComparison.InvariantCultureIgnoreCase);
        }

        public UpdateDictArg ToArg()
        {
            return new UpdateDictArg()
            {
                Type = Type,
                Id = Id,
                DisplayName = DisplayName,
                Code1C = Code1C,
                Type1C = Type1C,
                CodeParent1C = CodeParent1C
            };
        }
    }
}
