﻿using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System.Collections.Generic;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Data;
using System.Security.Principal;

namespace ElevatorMES.WPF.Models
{
    [Description("Инвентаризация склада")]
    public class StorageInventoryViewModel : MESDataNoResult<StorageInventoryRequest>
    {
        #region Открытые поля

        /// <summary>
        /// Разрешение обновления данных
        /// </summary>
        public bool IsDataRequestEnabled
        {
            get => isDataRequestEnabled;
            set
            {
                if (isDataRequestEnabled != value)
                {
                    isDataRequestEnabled = value;
                    IsEnabled = Layers.IsEnabled = value && storageItem != null;
                    OnPropertyChanged(nameof(IsDataRequestEnabled));
                }
            }
        }

        public InputItem<decimal> Quantity { get; }

        public InputItems<InventoryMode, KeyValuePair<InventoryMode, string>> InventoryModeItem { get; }

        public StorageUIItem StorageItem
        {
            get => storageItem;
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(StorageItem));
                IsEnabled = isDataRequestEnabled;
                if (storageItem != value)
                {
                    storageItem = value;
                    Layers.UnitId = value.UnitId;
                    Layers.IsEnabled = isDataRequestEnabled;
                    Layers.MaxWeight = Convert.ToDouble(value.Capacity);
                    OnPropertyChanged(nameof(StorageItem));
                }
            }
        }

        public UnitLayersResult Layers { get; }

        public MainViewModel MainView { get; }

        #endregion


        #region Закрытые поля

        private bool isDataRequestEnabled;

        private StorageUIItem storageItem;

        private IList<LayerUIItem> storedLayers;

        #endregion


        #region Конструкторы и инициализация

        public StorageInventoryViewModel(MainViewModel mainView)
        {
            MainView = mainView;
            Layers = new UnitLayersResult();
            Layers.Processed += LayersProcessed;
            InventoryModeItem = new InputItems<InventoryMode, KeyValuePair<InventoryMode, string>>()
            {
                IsVisible = true,
                Items = EnumExtension.ToListKeyDesc<InventoryMode>()
            };
            Quantity = new InputItem<decimal>()
            {
                IsVisible = true
            };
            Quantity.SelectedChanged += QuantitySelectedChanged;
        }

        public void RefreshLayers()
        {
            Layers.Refresh();
        }

        #endregion


        #region Обрыботка событий

        private void LayersProcessed(object sender, EventArgs e)
        {
            Quantity.SelectedChanged -= QuantitySelectedChanged;
            Quantity.Selected = Layers.ActualResult?.Sum(m => m.Quantity) ?? decimal.Zero;
            storedLayers = Layers.ActualResult;
            Quantity.SelectedChanged += QuantitySelectedChanged;
        }

        private void QuantitySelectedChanged(object sender, EventArgs e)
        {
            if (Validate())
                UpdateLayers();
        }

        #endregion


        #region Корректировка слоев

        private IList<LayerUIItem> Clone(IList<LayerUIItem> source)
        {
            var res = new List<LayerUIItem>(source.Count);
            res.AddRange(source.Select(m => new LayerUIItem()
            {
                CreatedAt = m.CreatedAt,
                LotId = m.LotId,
                LotNo = m.LotNo,
                NomenclatureId = m.NomenclatureId,
                NomenclatureName = m.NomenclatureName,
                OrganizationId = m.OrganizationId,
                OrganizationName = m.OrganizationName,
                Quantity = m.Quantity
            }));
            return res;
        }

        private void UpdateLayers()
        {
            if (storedLayers == null)
                return;
            var layers = Clone(storedLayers);
            var currQuantity = layers.Sum(m => m.Quantity);
            var newQuantity = Quantity.Selected;
            if (newQuantity <= 0)
                layers.Clear();
            else
            {
                if (layers.Count == 0)
                {
                    // создание нового наполнения
                    var newLayer = new LayerUIItem()
                    {
                        CreatedAt = DateTime.Now,
                        LotId = 0,
                        LotNo = "Новая партия",
                        Quantity = newQuantity
                    };
                    layers.Add(newLayer);
                }
                else
                {
                    switch (InventoryModeItem.Selected)
                    {
                        case InventoryMode.SetBottomLayer:
                            var bottomLayer = layers[layers.Count - 1];
                            if (newQuantity > currQuantity)
                            {
                                bottomLayer.Quantity += newQuantity - currQuantity;
                            }
                            else
                            {
                                while (layers.Count > 0 && currQuantity - layers[layers.Count - 1].Quantity >= newQuantity)
                                {
                                    currQuantity -= layers[layers.Count - 1].Quantity;
                                    layers.RemoveAt(layers.Count - 1);
                                }
                                if (layers.Count > 0)
                                {
                                    bottomLayer = layers[layers.Count - 1];
                                    bottomLayer.Quantity -= currQuantity - newQuantity;
                                }
                            }
                            break;
                        case InventoryMode.SetTopLayer:
                            var topLayer = layers[0];
                            if (newQuantity > currQuantity)
                            {
                                topLayer.Quantity += newQuantity - currQuantity;
                            }
                            else
                            {
                                while (layers.Count > 0 && currQuantity - layers[0].Quantity >= newQuantity)
                                {
                                    currQuantity -= layers[0].Quantity;
                                    layers.RemoveAt(0);
                                }
                                if (layers.Count > 0)
                                {
                                    topLayer = layers[0];
                                    topLayer.Quantity -= currQuantity - newQuantity;
                                }
                            }
                            break;
                    }
                }
            }
            Layers.ActualResult = layers;
        }

        #endregion


        #region Проверка данных и сохранение

        private bool Validate()
        {
            if (storedLayers == null)
                return false;
            if (Quantity.Selected < decimal.Zero)
            {
                Quantity.ValidationError = "Некорректный вес";
                return false;
            }
            if (Quantity.Selected > StorageItem.Capacity)
            {
                Quantity.ValidationError = "Превышение емкости";
                return false;
            }
            return true;
        }

        protected override StorageInventoryRequest GetRequest()
        {
            if (Validate())
            {
                var currentIdentity = WindowsIdentity.GetCurrent();
                var user = currentIdentity?.Name ?? Environment.UserName ?? Environment.MachineName;
                var topLayer = storedLayers.FirstOrDefault();
                var bottomLayer = storedLayers.LastOrDefault();
                return new StorageInventoryRequest(storageItem.UnitId,
                    topLayer?.CreatedAt, topLayer?.Quantity ?? decimal.Zero,
                    bottomLayer?.CreatedAt, bottomLayer?.Quantity ?? decimal.Zero,
                    Quantity.Selected, InventoryModeItem.Selected, user);
            }
            else
                return null;
        }

        #endregion
    }
}
