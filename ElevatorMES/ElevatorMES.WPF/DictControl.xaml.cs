﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ElevatorMES.WPF.Models;
using ElevatorMES.Domain.Data.Base;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Interaction logic for DictControl.xaml
    /// </summary>
    public partial class DictControl : UserControl
    {
        public DictControl()
        {
            InitializeComponent();
        }

        private void NewItemClick(object sender, RoutedEventArgs e)
        {
            ShowDialog(true);
        }

        private void EditItemClick(object sender, RoutedEventArgs e)
        {
            ShowDialog(false);
        }

        private void ShowDialog(bool isNew)
        {
            var dictView = (IDictionaryView)DataContext;
            var dictWindow = new DictWindow()
            {
                Owner = Window.GetWindow(this),
            };
            var viewModel = dictView.GetViewModel(isNew, () =>
            {
                dictWindow.Close();
                dictView.NewRequest();

            });
            dictWindow.DataContext = viewModel;
            dictWindow.Closed += (o, ee) =>
            {
                this.IsEnabled = true;
            };
            this.IsEnabled = false;
            dictWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            dictWindow.Show();
        }

        private void DeleteItemClick(object sender, RoutedEventArgs e)
        {
            var dictView = (IDictionaryView)DataContext;
            dictView.Delete();
        }

        private void RestoreItemClick(object sender, RoutedEventArgs e)
        {
            var dictView = (IDictionaryView)DataContext;
            dictView.Restore();
        }

        private void ClearSearchClick(object sender, RoutedEventArgs e)
        {
            var dictView = (IDictionaryView)DataContext;
            dictView.ClearSearch();
        }
    }
}
