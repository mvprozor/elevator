﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.WPF.Models;
using ElevatorMES.WPF.Utils;
using NLog;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Interaction logic for StorageInventoryWindow.xaml
    /// </summary>
    public partial class StorageInventoryWindow : Window
    {
        #region Открытые поля

        public StorageInventoryViewModel StorageInventoryView
        {
            get => DataContext as StorageInventoryViewModel;
            set => DataContext = value;
        }

        public StorageUIItem StorageItem
        {
            get => StorageInventoryView.StorageItem;
            set
            {
                if (value != StorageInventoryView.StorageItem)
                {
                    StorageInventoryView.StorageItem = value;
                }
            }
        }

        public MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    StorageInventoryView.IsDataRequestEnabled = IsLoaded && value != null;
                }
            }
        }

        public bool IsPerformed { get; private set; }

        #endregion


        #region Закрытые поля

        private MESRepository repository;

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        #region Конструкторы и иницилизация

        public StorageInventoryWindow(MainViewModel mainView)
        {
            StorageInventoryView = new StorageInventoryViewModel(mainView);
            StorageInventoryView.ProcessDataTask = StorageInventoryTask;
            StorageInventoryView.Processed += StorageAssignProcessed;
            StorageInventoryView.Layers.GetDataTask = GetLayersTask;
            InitializeComponent();
            Loaded += WindowLoaded;
            Unloaded += WindowUnloaded;
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Загружена форма инвентаризации склада");
            IsPerformed = false;
            if (Repository == null)
                logger.Info("Ожидание инициализации репозитория MES API для формы назначения склада...");
            else
				StorageInventoryView.IsDataRequestEnabled = true;
        }

        private void WindowUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Выгружена форма инвентаризации склада");
        }

        #endregion


        #region Обновление и сохранение данных

        private Task<IList<LayerUIItem>> GetLayersTask(UnitLayersRequest request)
        {
            return repository.GetUnitLayers(request.Arg, request.AbandonToken);
        }

        private System.Threading.Tasks.Task StorageInventoryTask(StorageInventoryRequest request)
        {
            return Repository.StorageInventory(request.Arg, request.AbandonToken);
        }

        #endregion


        #region Обрыботка событий

        private void ButtonSaveClick(object sender, RoutedEventArgs e)
        {
            StorageInventoryView.StorageItem.IsProcessing = true;
            if (!StorageInventoryView.NewRequest())
                StorageInventoryView.StorageItem.IsProcessing = false;
        }

        private void ButtonCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void StorageAssignProcessed(object sender, EventArgs e)
        {
            StorageInventoryView.StorageItem.IsProcessing = false;
            if (String.IsNullOrWhiteSpace(StorageInventoryView.ActualError))
            {
                IsPerformed = true;
                Close();
            }
            else
            {
                StorageInventoryView.RefreshLayers();
            }
        }

        #endregion
    }
}
