﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Enums
{
    public enum ScalesInputStateEnum
    {
        Initializing = 0,
        Receiving = 1,
        Failed = 2
    }
}
