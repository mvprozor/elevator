﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Enums
{
    public static class EnumExtensions
    {
        public static void ToPeriod(this ViewPeriodEnum viewPeriod, 
            DateTime? boundBegin, DateTime? boundEnd,
            out DateTime? begin, out DateTime? end)
        {
            switch (viewPeriod)
            {
                case ViewPeriodEnum.Last24Hours:
                    end = null;
                    begin = DateTime.Now.AddHours(-24);
                    break;

                case ViewPeriodEnum.Yesterday:
                    end = DateTime.Today;
                    begin = end.Value.AddDays(-1);
                    break;

                case ViewPeriodEnum.Bounds:
                    end = boundBegin;
                    begin = boundEnd;
                    break;

                default:
                    begin = DateTime.Today;
                    end = DateTime.Now;
                    break;
            }
        }
    }
}
