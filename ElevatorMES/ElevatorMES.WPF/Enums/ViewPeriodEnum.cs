﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF.Enums
{
    public enum ViewPeriodEnum
    {
        [Description("Последние 24 часа")]
        Last24Hours = 0,

        [Description("Вчера")]
        Yesterday = 1,

        [Description("Заданные границы")]
        Bounds = 2


    }
}
