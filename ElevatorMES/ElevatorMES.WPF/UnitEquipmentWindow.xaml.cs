﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.WPF.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Interaction logic for UnitEquipmentWindow.xaml
    /// </summary>
    [Browsable(false)]
    public partial class UnitEquipmentWindow : Window
    {
        #region Открытые поля

        public UnitEquipmentViewModel UnitEquipmentView
        {
            get => DataContext as UnitEquipmentViewModel;
            set => DataContext = value;
        }

        public MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    UnitEquipmentView.IsDataRequestEnabled = IsLoaded && value != null;
                }
            }
        }

        public bool IsPerformed { get; private set; }

        #endregion


        #region Закрытые поля

        private MESRepository repository;

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        #region Конструкторы и иницилизация

        public UnitEquipmentWindow()
        {
            UnitEquipmentView = new UnitEquipmentViewModel();
            UnitEquipmentView.Units.GetDataTask = GetUnitsTask;
            UnitEquipmentView.Equipments.GetDataTask = GetEquipmentsTask;
            UnitEquipmentView.ProcessDataTask = UpdateUnitEquipments;
            UnitEquipmentView.Processed += SaveProcessed;
            InitializeComponent();
            Loaded += UnitEquipmenWindowLoaded;
            Unloaded += UnitEquipmenWindowUnloaded;
        }

        private void UnitEquipmenWindowLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Загружена форма связки подразделений и оборудования");
            if (Repository == null)
                logger.Info("Ожидание иницилизации репозитория MES API для формы вязки подразделений и оборудования...");
            else
                UnitEquipmentView.IsDataRequestEnabled = true;
        }

        private void UnitEquipmenWindowUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Выгружена форма связки подразделений и оборудования");
        }

        #endregion


        #region Обрыботка событий

        private void ButtonSaveClick(object sender, RoutedEventArgs e)
        {
            UnitEquipmentView.NewRequest();
        }

        private void ButtonCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SaveProcessed(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(UnitEquipmentView.ActualError))
            {
                if (!UnitEquipmentView.Unit.SelectNextItem() ||
                    !UnitEquipmentView.Equipment.SelectNextItem())
                    Close();
            }
        }

        private System.Threading.Tasks.Task UpdateUnitEquipments(UnitEquipmentUpdateRequest request)
        {
            var res = Repository.UpdateUnitEquipments(request.Arg, request.AbandonToken);
            IsPerformed = true;
            return res;
        }

        #endregion


        #region Обновление данных

        private Task<IList<Unit>> GetUnitsTask(EmptyRequest request)
        {
            return repository.GetUnits(request.AbandonToken);
        }

        private Task<IList<Equipment>> GetEquipmentsTask(EmptyRequest request)
        {
            return repository.GetEquipments(request.AbandonToken);
        }

        #endregion
    }
}
