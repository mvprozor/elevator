﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.WPF.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
namespace ElevatorMES.WPF

{
    /// <summary>
    /// Interaction logic for TaskMatchWindow.xaml
    /// </summary>
    public partial class TaskMatchWindow : Window
    {
        #region Открытые поля

        public TaskMatchViewModel TaskMatchView
        {
            get => DataContext as TaskMatchViewModel;
            set => DataContext = value;
        }

        public MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    TaskMatchView.IsDataRequestEnabled = IsLoaded && value != null;
                }
            }
        }

        public bool IsPerformed { get; private set; }

        #endregion


        #region Закрытые поля

        private MESRepository repository;

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        #region Конструкторы и иницилизация

        public TaskMatchWindow()
        {
            TaskMatchView = new TaskMatchViewModel();
            TaskMatchView.ManualTasks.GetDataTask = GetTaskForMatching;
            TaskMatchView.ERPTasks.GetDataTask = GetTaskForMatching;
            TaskMatchView.ProcessDataTask = MatchTask;
            TaskMatchView.Processed += MatchProcessed;
            InitializeComponent();
            Loaded += WindowLoaded;
            Unloaded += WindowUnloaded;
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Загружена форма сопоставления заданий");
            IsPerformed = false;
            if (Repository == null)
                logger.Info("Ожидание иницилизации репозитория MES API для формы сопоставления заданий...");
            else
                TaskMatchView.IsDataRequestEnabled = true;
        }

        private void WindowUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Выгружена форма сопоставления заданий");
        }

        #endregion


        #region Обновление данных

        private Task<IList<TaskMatchUIItem>> GetTaskForMatching(TaskMatchingRequest request)
        {
            return repository.GetTaskForMatching(request.Arg, request.AbandonToken);
        }

        private System.Threading.Tasks.Task MatchTask(TaskMatchRequest request)
        {
            var currentIdentity = WindowsIdentity.GetCurrent();
            var user = currentIdentity?.Name ?? Environment.UserName ?? Environment.MachineName;
            return repository.MatchTask(request.Arg, request.AbandonToken);
        }

        #endregion


        #region Обрыботка событий

        private void ButtonMatchClick(object sender, RoutedEventArgs e)
        {
            TaskMatchView.NewRequest();
        }

        private void MatchProcessed(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(TaskMatchView.ActualError))
            {
                IsPerformed = true;
                TaskMatchView.RefreshTasks();
            }
        }

        #endregion
    }
}
