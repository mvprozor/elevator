﻿using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.PLC;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorMES.WPF
{
    public class DefaultPLCTasks : IPLCTasks
    {
        #region Закрытые поля

        private class PLCTaskInfo
        { 
            public bool IsRunning { get; set; }
            public decimal PlannedWeight { get; set; }
            public decimal Weight { get; set; }
        }

        private Dictionary<int, PLCTaskInfo> tasks;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        #endregion


        #region Открытые поля

        public readonly static DefaultPLCTasks Instance = new DefaultPLCTasks();

        #endregion


        #region Конструкторы и инициализация

        public DefaultPLCTasks()
        {
            tasks = new Dictionary<int, PLCTaskInfo>(4);
        }

        #endregion


        #region IPLCTasks

        public event EventHandler<PLCTaskStatusEventArgs> TaskStatusReceived;
        
        public event EventHandler<PLCNewTaskEventArgs> NewTaskReceived;

        public void GetTaskStatus(int taskId)
        {
            PLCTaskStatusEventArgs args = null;
            if (tasks.TryGetValue(taskId, out var taskInfo))
            {
                args = new PLCTaskStatusEventArgs(taskId,
                    taskInfo.IsRunning ? PLCTaskStatus.RouteRunning : PLCTaskStatus.RouteStopped, taskInfo.Weight);
                if (taskInfo.IsRunning && taskInfo.Weight < taskInfo.PlannedWeight)
                    taskInfo.Weight += 0.1m;
            }
            else
            {
                args = new PLCTaskStatusEventArgs(taskId, PLCTaskStatus.UnknownTaskId, 0m);
            }
            TaskStatusReceived?.Invoke(this, args);
            //NewTaskReceived?.Invoke(this, new PLCNewTaskEventArgs("14", 30));
        }

        public void StartTask(int taskId, int source, int destination, PLCTaskOperations operations, decimal weight)
        {
            if (!tasks.TryGetValue(taskId, out _))
            {
                tasks.Add(taskId, new PLCTaskInfo() { IsRunning = true, PlannedWeight = weight } );
            }
        }
        
        public void StopTask(int taskId)
        {
            if (tasks.TryGetValue(taskId, out var taskInfo))
            {
                tasks.Remove(taskId);
            }
        }

        public void SuspendTask(int taskId)
        {
            if (tasks.TryGetValue(taskId, out var taskInfo))
            {
                taskInfo.IsRunning = false;
            }
        }

        public void ResumeTask(int taskId)
        {
            if (tasks.TryGetValue(taskId, out var taskInfo))
            {
				taskInfo.Weight = 0.0m;
				taskInfo.IsRunning = true;
            }
        }

        #endregion
    }
}
