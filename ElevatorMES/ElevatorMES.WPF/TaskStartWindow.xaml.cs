﻿using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.PLC;
using ElevatorMES.WPF.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Interaction logic for StartTaskWindow.xaml
    /// </summary>
    [Browsable(false)]
    public partial class TaskStartWindow : Window, INotifyPropertyChanged
    {
        #region Открытые поля

        public TaskStartViewModel TaskStartView
        {
            get => DataContext as TaskStartViewModel;
            set => DataContext = value;
        }

        public TaskUIItem TaskItem
        {
            get => TaskStartView.TaskItem;
            set
            {
                if (value != TaskStartView.TaskItem)
                {
                    TaskStartView.TaskItem = value;
                }
            }
        }

        public MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    TaskStartView.IsDataRequestEnabled = IsLoaded && value != null;
                }
            }
        }

        public IPLCTasks PLCTasks { get; set; }

        public bool IsPerformed { get; private set; }

        #endregion


        #region Закрытые поля

        private MESRepository repository;

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion


        #region Конструкторы и иницилизация

        public TaskStartWindow()
        {
            TaskStartView = new TaskStartViewModel();
            TaskStartView.GetDataTask = StartTask;
            TaskStartView.Processed += TaskStartProcessed;
            TaskStartView.Points.GetDataTask = GetPointsTasks;
            TaskStartView.Storages.GetDataTask = GetStoragesTasks;
            InitializeComponent();
            //Title = isFinished ? "Запуск задания" : "Продолжение задания";
            Loaded += StartWindowLoaded;
            Unloaded += StartWindowUnloaded;
        }

        private void StartWindowLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Загружена форма запуска заданий");
            IsPerformed = false;
            if (Repository == null)
                logger.Info("Ожидание иницилизации репозитория MES API для формы заданий...");
            else
                TaskStartView.IsDataRequestEnabled = true;
        }

        private void StartWindowUnloaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Выгружена форма запуска заданий");
        }

        #endregion


        #region Обновление и сохранение данных

        private Task<IList<UnitUIItem>> GetPointsTasks(EmptyRequest request)
        {
            return repository.GetPoints(request.AbandonToken);
        }

        private Task<IList<StorageUIItem>> GetStoragesTasks(EmptyRequest request)
        {
            return repository.GetStorages(request.AbandonToken);
        }

        private Task<TaskUIItem> StartTask(TaskStartRequest request)
        {
            return Repository.StartTask(request.Arg, request.AbandonToken);
        }

        #endregion


        #region Обрыботка событий

        private void ButtonSaveClick(object sender, RoutedEventArgs e)
        {
            TaskItem.IsProcessing = true;
            if (!TaskStartView.NewRequest())
                TaskItem.IsProcessing = false;
        }

        private void ButtonCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TaskStartProcessed(object sender, EventArgs e)
        {
            TaskItem.IsProcessing = false;
            TaskItem = TaskStartView.ActualResult;
            if (String.IsNullOrWhiteSpace(TaskStartView.ActualError))
            {
                PLCTasks?.StartTask(TaskItem.Id, TaskItem.SourceEquipmentId.Value,
                    TaskItem.DestinationEquipmentId.Value,
                    (TaskItem.IsSeparating ? PLCTaskOperations.Separating : PLCTaskOperations.None)
                    | (TaskItem.IsDrying ? PLCTaskOperations.Drying : PLCTaskOperations.None)
                    | (TaskItem.IsScalping ? PLCTaskOperations.Scalping : PLCTaskOperations.None),
                    TaskItem.QuantityPlanned);
                IsPerformed = true;
                Close();
            }
        }

        #endregion


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
