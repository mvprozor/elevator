﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using ElevatorMES.Domain.PLC;
using ElevatorMES.Domain.Enums;
using System.ComponentModel;
using NLog;

namespace ElevatorMES.WPF
{
    public class MainControlWinCC : UserControl, IPLCTasks
    {
        #region Открытые свойства и события для WinCC

        /// <summary>
        /// Строка подключения к Web API MES
        /// </summary>
        public string MESApi 
        {
            get => mainControl.MESApi;
            set => mainControl.MESApi = value;
        }

        /// <summary>
        /// Уникальный идентификатор задания
        /// </summary>
        public int PLCTaskId { get; private set; }

        /// <summary>
        /// Код-оборудования-источника
        /// </summary>
        public int PLCSource { get; private set; }

        /// <summary>
        /// Код-оборудования-приёмника
        /// </summary>
        public int PLCDestination { get; private set; }

        /// <summary>
        /// Слово операций
        /// </summary>
        /// <remark>
        /// Разряды операций
        ///                     |---|---|---|
        /// Зарезервировано ... |-2-|-1-|-0-|
        ///                     |---|---|---|
        ///                       |   |    |
        ///                       |   |    |-----> Сушка
        ///                       |   |----------> Сортировка
        ///                       |--------------> Скальпирование
        /// </remark>
        public int PLCOperations { get; private set; }

        /// <summary>
        /// Плановый вес по заданию
        /// </summary>
        public double PLCPlannedWeight { get; private set; }

        /// <summary>
        /// Фактический вес по заданию
        /// </summary>
        public double PLCActualWeight { get; set; }

        /// <summary>
        /// Ответ на запрос статуса задания
        /// </summary>
        /// <remarks>
        /// 0 - Неопределенное состояние
        /// 1 - Маршрут остановлен
        /// 2 - Маршрут запущен
        /// 3 - Маршрут останавливается
        /// 4 - Маршрут запускается
        /// 5 - Маршрут остановлен по весу
        /// -1 - Неизвестный ID задания
        /// -2 - Некорректная конфигурация маршрута
        /// -3 - Нет готовности маршрута
        /// -4 - Авария маршрута
        /// </remarks>
        public int PLCStatus { get; set; }


        /// <summary>
        /// Флаг готовности данных по запросу GetTaskStatus
        /// </summary>
        public bool PLCReady
        {
            get => plcReady;
            set
            {
                if (plcReady!=value)
                {
                    plcReady = value;
                    if (plcReady)
                    {
                        logger.Debug($"От ПЛК для задания Id={PLCTaskId} статус: {PLCStatus}, вес: {PLCActualWeight}");
                        TaskStatusReceived?.Invoke(this, new PLCTaskStatusEventArgs(PLCTaskId,
                            (PLCTaskStatus)PLCStatus, Convert.ToDecimal(PLCActualWeight)));
                    }
                }
            }

        }

        /// <summary>
        /// Событие, по которому нужно запустить задание
        /// </summary>
        /// <remarks>
        /// В качестве параметров перед активацией события задаются:
        /// PLCTaskId, PLCSource, PLCDestination, PLCOperations, PLCPlannedWeight
        /// </remarks>
        public event EventHandler PLCStartTask;

        /// <summary>
        /// Событие, по которому нужно завершить задание
        /// </summary>
        /// <remarks>
        /// В качестве параметров перед активацией события задаются:
        /// PLCTaskId
        /// </remarks>
        public event EventHandler PLCStopTask;

        /// <summary>
        /// Событие, по которому нужно приостановить задание
        /// </summary>
        /// <remarks>
        /// В качестве параметров перед активацией события задаются:
        /// PLCTaskId
        /// </remarks>
        public event EventHandler PLCSuspendTask;

        /// <summary>
        /// Событие, по которому нужно продолжить задание
        /// </summary>
        /// <remarks>
        /// PLCTaskId
        /// </remarks>
        public event EventHandler PLCResumeTask;

        /// <summary>
        /// Опрос состояния задания
        /// </summary>
        /// <remarks>
        /// В качестве параметров перед активацией события задаются:
        /// PLCTaskId, PLCTaskStatus = 0
        /// После вызова заполняются следующие поля:
        /// PLCActualWeight - вес, прошедший по конвейрным весам
        /// PLCTaskStatus - статус задания
        /// </remarks>
        public event EventHandler PLCGetTaskStatus;


        /// <summary>
        /// Номенклатура по новому заданию
        /// </summary>
        public string PLCNewTaskNomenclature { get; set; }

        /// <summary>
        /// Плановый вес нового задания
        /// </summary>
        public double PLCNewTaskWeight { get; set; }

        /// <summary>
        /// Флаг готовности данных по запросу GetTaskStatus
        /// </summary>
        public bool PLCNewTaskReady
        {
            get => plcNewTaskReady;
            set
            {
                if (plcNewTaskReady != value)
                {
                    plcNewTaskReady = value;
                    if (plcNewTaskReady)
                    {
                        logger.Debug($"От ПЛК получено новое задание код номенклатуры: {PLCNewTaskNomenclature}, вес: {PLCNewTaskWeight}");
                        NewTaskReceived?.Invoke(this, new PLCNewTaskEventArgs(PLCNewTaskNomenclature, Convert.ToDecimal(PLCNewTaskWeight)));
                    }
                }
            }
        }

        #endregion


        #region Закрытые поля

        private readonly MainControl mainControl;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private bool plcReady;

        private bool plcNewTaskReady;

        #endregion


        #region Конструкторы и иницилизация

        public MainControlWinCC()
        {
            mainControl = new MainControl()
            {
                PLCTasks = this
            };
            var hostControl = new ElementHost()
            {
                Dock = DockStyle.Fill,
                Child = mainControl
            };
            SuspendLayout();
            Size = new Size(640, 480);
            Controls.Add(hostControl);
            ResumeLayout(false);
        }

        #endregion


        #region IPLCTasks

        public event EventHandler<PLCTaskStatusEventArgs> TaskStatusReceived;

        public event EventHandler<PLCNewTaskEventArgs> NewTaskReceived;

        public void StartTask(int taskId, int source, int destination, PLCTaskOperations operations, decimal weight)
        {
            PLCTaskId = taskId;
            PLCSource = source;
            PLCDestination = destination;
            PLCOperations = (int)operations;
            PLCPlannedWeight = Convert.ToDouble(weight);
            logger.Debug($"Запуск в ПЛК задания Id={taskId}...");
            PLCStartTask?.Invoke(this, EventArgs.Empty);
        }

        public void StopTask(int taskId)
        {
            PLCTaskId = taskId;
            logger.Debug($"Останов в ПЛК задания Id={taskId}...");
            PLCStopTask?.Invoke(this, EventArgs.Empty);
        }

        public void SuspendTask(int taskId)
        {
            PLCTaskId = taskId;
            logger.Debug($"Пауза в ПЛК задания Id={taskId}...");
            PLCSuspendTask?.Invoke(this, EventArgs.Empty);
        }

        public void ResumeTask(int taskId)
        {
            PLCTaskId = taskId;
            logger.Debug($"Продолжение в ПЛК задания Id={taskId}...");
            PLCResumeTask?.Invoke(this, EventArgs.Empty);
        }

        public void GetTaskStatus(int taskId)
        {
            PLCTaskId = taskId;
            PLCStatus = (int)PLCTaskStatus.Undetermined;
            PLCActualWeight = 0.0;
            PLCReady = false;
            logger.Debug($"Обновление статуса задания Id={taskId} по данным ПЛК...");
            PLCGetTaskStatus?.Invoke(this, EventArgs.Empty);
        }

        #endregion

    }
}
