﻿using System;
using System.Reflection;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.WPF.Models;
using NLog;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ElevatorMES.ScalesIO;
using System.IO;
using Newtonsoft.Json;

namespace ElevatorMES.WPF
{
    /// <summary>
    /// Логика взаимодействия для VehicleServeWindow.xaml
    /// </summary>
    public partial class VehicleServeWindow : Window
    {
        #region Открытые поля

        public VehicleServeViewModel VehicleServeView
        {
            get => DataContext as VehicleServeViewModel;
            set => DataContext = value;
        }

        public MESRepository Repository
        {
            get => repository;
            set
            {
                if (value != repository)
                {
                    repository = value;
                    VehicleServeView.IsDataRequestEnabled = IsLoaded && value != null;
                }
            }
        }

        public bool IsPerformed { get; private set; }

        #endregion


        #region Закрытые поля

        private MESRepository repository;

        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private const string scalesConfigFileName = "ScalesConfig.json";

        #endregion


        #region Конструкторы и иницилизация

        public VehicleServeWindow(VehicleUIItem vehicle)
        {
            InitializeComponent();
            var scales = LoadScalesConfig();
            VehicleServeView = new VehicleServeViewModel(vehicle, scales);
            VehicleServeView.InputAction.Selected = vehicle.NextAction;
            VehicleServeView.Nomenclatures.GetDataTask = GetNomenclaturesTask;
            VehicleServeView.Points.GetDataTask = GetPoints;
            VehicleServeView.GetDataTask = ServeVehicle;
            VehicleServeView.Processed += VehicleServeProcessed;
            Loaded += WindowLoaded;
            Unloaded += WindowUnloaded;
            
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            logger.Debug("Загружена форма действий с транспортом");
            IsPerformed = false;
            if (Repository == null)
                logger.Info("Ожидание иницилизации репозитория MES API для формы действий с транспортом...");
            else
                VehicleServeView.IsDataRequestEnabled = true;
        }

        private void WindowUnloaded(object sender, RoutedEventArgs e)
        {
            VehicleServeView.ScalesWeights.IsVisible = false;
            logger.Debug("Выгружена форма действий с транспортом");
        }

        private InputScales[] LoadScalesConfig()
        {
            StreamReader sr = null;
            JsonTextReader jr = null;
            logger.Debug($"Загрузка конфигурации весов из файла {scalesConfigFileName}");
            var res = new List<InputScales>(4);
            try
            {
                var scalesConfigPath = scalesConfigFileName;
                if (!File.Exists(scalesConfigPath))
                {
                    var asm = Assembly.GetExecutingAssembly();
                    var location = asm.Location;
                    if (!String.IsNullOrWhiteSpace(location))
                    {
                        scalesConfigPath = Path.Combine(Path.GetDirectoryName(location), scalesConfigFileName);
                        if (!File.Exists(scalesConfigPath))
                            scalesConfigPath = null;
                    }
                    scalesConfigPath = scalesConfigPath ?? Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), scalesConfigFileName);
                }
                sr = new StreamReader(scalesConfigFileName);
                jr = new JsonTextReader(sr);
                var serializer = new JsonSerializer();
                var scalesConfigs = serializer.Deserialize<ScalesConfig[]>(jr);
                for(var i=0; i<scalesConfigs.Length; i++)
                {
                    var scalesConfig = scalesConfigs[i];
                    var scales = scalesConfig.Create(out var validationErrors);
                    scales.DefaultConfig = scalesConfig;
                    if (scales==null)
                    {
                        var errors = String.Join(". ", validationErrors);
                        logger.Error($"Некорректная конфигурации  №{i + 1} весов из файла {scalesConfigFileName}. {errors}");
                    }
                    else
                    {
                        res.Add(new InputScales(scalesConfig.Name, scales));
                    }
                }
            }
            catch(FileNotFoundException)
            {
                logger.Error($"Файл {scalesConfigFileName} конфигурации весов не найден.");
            }
            catch (Exception exception)
            {
                logger.Error(exception, $"При при загрузке конфигурации весов из файла {scalesConfigFileName}");
            }
            finally
            {
                jr?.Close();
                sr?.Close();
            }
            logger.Info($"Загружено весов: {res.Count}");
            return res.ToArray();
        }
        #endregion


        #region Обновление данных

        private Task<IList<Nomenclature>> GetNomenclaturesTask(EmptyRequest request)
        {
            return repository.GetNomenclature(GetDictArg.Default, request.AbandonToken);
        }

        private Task<IList<UnitUIItem>> GetPoints(EmptyRequest request)
        {
            return repository.GetPoints(request.AbandonToken);
        }

        private Task<VehicleUIItem> ServeVehicle(VehicleServeRequest request)
        {
            var currentIdentity = WindowsIdentity.GetCurrent();
            var user = currentIdentity?.Name ?? Environment.UserName ?? Environment.MachineName;
            return repository.ServeVehicle(new VehicleServeArg()
            {
                VehicleId = request.VehicleId,
                Action = request.Action,
                ActionAt = DateTime.Now, //request.ActionAt,
                NomenclatureId = request.NomenclatureId,
                UnitId = request.UnitId,
                Weight = request.Weight,
                User = user,
                Comment = request.Comment
            }, request.AbandonToken);
        }

        #endregion


        #region Обрыботка событий

        private void ButtonSaveClick(object sender, RoutedEventArgs e)
        {
            if (!VehicleServeView.Weight.IsVisible && Validation.GetHasError(tbWeight))
            {
                VehicleServeView.Weight.ValidationError = "Задайте корректный вес";
            }
            else
            {
                VehicleServeView.NewRequest();
            }
        }

        private void ButtonCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void VehicleServeProcessed(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(VehicleServeView.ActualError))
            {
                IsPerformed = true;
                Close();
            }
        }

        private void AcceptWeightClick(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var inputScales = (InputScales)button.DataContext;
            VehicleServeView.Weight.Selected = Math.Round(inputScales.Weight ?? decimal.Zero, 2);
        }

        #endregion
    }
}
