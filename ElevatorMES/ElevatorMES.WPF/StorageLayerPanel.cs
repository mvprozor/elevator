﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ElevatorMES.WPF
{
    public class StorageLayerPanel : Panel
    {
        public static readonly DependencyProperty WeightProperty =
            DependencyProperty.RegisterAttached("Weight", typeof(double), typeof(StorageLayerPanel), 
                new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsParentArrange));

        public static readonly DependencyProperty MaxWeightProperty =
                   DependencyProperty.Register(nameof(MaxWeight), typeof(double), typeof(StorageLayerPanel), 
                       new FrameworkPropertyMetadata(1000.0, FrameworkPropertyMetadataOptions.AffectsMeasure));

        public double Weight
        {
            get { return (double)GetValue(WeightProperty); }
            set { SetValue(WeightProperty, value); }
        }

        public double MaxWeight
        {
            get { return (double)GetValue(MaxWeightProperty); }
            set { SetValue(MaxWeightProperty, value); }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            foreach (UIElement child in Children)
            {
                child.Measure(availableSize);
            }

            return new Size(0, 0);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            var pixelsPerWeight = finalSize.Height / MaxWeight;
            var currentWeight = 0.0;
            for (var i = Children.Count - 1; i >= 0; i--)
                //foreach (UIElement child in Children)
            {
                var child = Children[i];
                var weight = (double)child.GetValue(WeightProperty);
                var height = Math.Max(weight * pixelsPerWeight, 0);
                var offset = finalSize.Height - currentWeight * pixelsPerWeight - height;

                child.Arrange(new Rect(0, offset, finalSize.Width, height));

                currentWeight += weight;
            }

            return finalSize;
        }

    }
}
