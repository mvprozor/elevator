﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Domain.Model.Args;
using System.Threading;
using ElevatorMES.Utilites.Helpers;
using ElevatorMES.Domain.Data;
using System.Net.Http.Formatting;
using ElevatorMES.WPF.Models;
using System.Collections;
using Microsoft.Reporting.Map.WebForms.BingMaps;
using ElevatorMES.Domain.Enums;

namespace ElevatorMES.WPF
{

    public class MESRepository
    {
        private class ErrorContent
        {
            public string Message { get; set; }
        }

        public string BaseAddress => client.BaseAddress.ToString();
        
        private readonly HttpClient client;

        private MediaTypeFormatter mediaTypeFormatter;

        public MESRepository(string baseAddress)
        {
            if (String.IsNullOrWhiteSpace(baseAddress))
                throw new ArgumentNullException(nameof(baseAddress));
            var uri = new Uri(baseAddress);
            HttpClientHandler handler = new HttpClientHandler()
            {
                UseDefaultCredentials = true,
                PreAuthenticate = true
            };
            client = new HttpClient(handler)
            {
                BaseAddress = uri
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            mediaTypeFormatter = new JsonMediaTypeFormatter();
            ServicePointManager.ServerCertificateValidationCallback 
                += (sender, cert, chain, sslPolicyErrors) => true;
        }

        public void Abandon()
        {
            client.CancelPendingRequests();
            client.Dispose();
        }

        private async System.Threading.Tasks.Task<string> GetErrorString(
            string method, HttpResponseMessage response, CancellationToken cancellationToken)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.NotFound:
                    return $"Не найден метод MES API: {method}";

                case HttpStatusCode.Unauthorized:
                    return $"Недостаточно прав для вызова: {method}";

                case HttpStatusCode.InternalServerError:
                    return $"Ошибка при обработка вызова: {method}";

                default:
                    if (response.Content.Headers.ContentType.MediaType == "application/json")
                    {
                        var errorContent = await response.Content.ReadAsAsync<ErrorContent>(cancellationToken);
                        var message = errorContent?.Message;
                        return String.IsNullOrWhiteSpace(message) ?
                            $"MES API: {response.ReasonPhrase}" : message;
                    }
                    return await response.Content.ReadAsStringAsync();
            }
        }

        private async System.Threading.Tasks.Task<TResult> Get<TContent, TResult>(string method, CancellationToken cancellationToken)
            where TContent : TResult
        {
            //System.Diagnostics.Debug.WriteLine($"{DateTime.Now:hh:mm:ss} {method}");
            var response = await client.GetAsync(method, cancellationToken);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<TContent>(cancellationToken);
                return result;
            }
            var errorMessage = await GetErrorString(method, response, cancellationToken);
            throw new MESDataError(errorMessage);
        }

        private async System.Threading.Tasks.Task<TResult> Get<TContent, TResult>(
            string method, string queryParams, CancellationToken cancellationToken) where TContent : TResult
        {
            //System.Diagnostics.Debug.WriteLine($"{DateTime.Now:hh:mm:ss} {method}");
            //var queryParams = QueryParamsHelper.ToUriString(arg);
            var response = await client.GetAsync($"{method}?{queryParams}", cancellationToken);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<TContent>(cancellationToken);
                return result;
            }
            var errorMessage = await GetErrorString(method, response, cancellationToken);
            throw new MESDataError(errorMessage);
        }

        private async System.Threading.Tasks.Task Post<TArg>(string method, TArg arg, CancellationToken cancellationToken)
        {
            var response = await client.PostAsync(method, arg, mediaTypeFormatter, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                var errorMessage = await GetErrorString(method, response, cancellationToken);
                throw new MESDataError(errorMessage);
            }
        }

        private async System.Threading.Tasks.Task<TResult> Post<TArg, TResult>(string method, TArg arg, CancellationToken cancellationToken)
        {
            var response = await client.PostAsync(method, arg, mediaTypeFormatter, cancellationToken);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<TResult>(cancellationToken);
                return result;
            }
            var errorMessage = await GetErrorString(method, response, cancellationToken);
            throw new MESDataError(errorMessage);
        }

        private async System.Threading.Tasks.Task Delete(string method, string queryParams, CancellationToken cancellationToken)
        {
            var response = await client.DeleteAsync($"{method}?{queryParams}", cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                var errorMessage = await GetErrorString(method, response, cancellationToken);
                throw new MESDataError(errorMessage);
            }
        }

        public Task<OperInfoUI> GetOperInfo(CancellationToken cancellationToken)
        {
            return Get<OperInfoUI, OperInfoUI>("OperInfo", cancellationToken);
        }

        public Task<List<TaskUIItem>> GetTasks(GetTaskArg arg, CancellationToken cancellationToken)
        {
            return Get<List<TaskUIItem>, List<TaskUIItem>>("Task", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList> GetTasks(IDictionary<string,object> arg, CancellationToken cancellationToken)
        {
            return Get<List<TaskUIItem>, IList>("Task", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList<VehicleType>> GetVehicleTypes(CancellationToken cancellationToken)
        {
            return Get<VehicleType[], IList<VehicleType>>("Dict/VehicleTypes", cancellationToken);
        }

        public Task<IList<VehicleRegNumUIItem>> GetVehicleRegNums(CancellationToken cancellationToken)
        {
            return Get<VehicleRegNumUIItem[], IList<VehicleRegNumUIItem>>("Vehicle/RegNums", cancellationToken);
        }

        public Task<IList<Nomenclature>> GetNomenclature(GetDictArg arg, CancellationToken cancellationToken)
        {
            return Get<Nomenclature[], IList<Nomenclature>>("Dict/Nomenclatures",
                QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList<Organization>> GetOrganizations(GetDictArg arg, CancellationToken cancellationToken)
        {
           return Get<Organization[], IList<Organization>>("Dict/Organizations", 
               QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList<Contragent>> GetContragents(GetDictArg arg, CancellationToken cancellationToken)
        {
            return Get<Contragent[], IList<Contragent>>("Dict/Contragents",
                 QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList<StorageUIItem>> GetStorages(CancellationToken cancellationToken)
        {
            return Get<StorageUIItem[], IList<StorageUIItem>>("Storage", cancellationToken);
        }

        public Task<IList<UnitUIItem>> GetPoints(CancellationToken cancellationToken)
        {
            return Get<UnitUIItem[], IList<UnitUIItem>>("Points", cancellationToken);
        }

        public Task<TaskUIItem> CreateTask(TaskManualArg arg, CancellationToken cancellationToken)
        {
            return Post<TaskManualArg, TaskUIItem>("Task/CreateManual", arg, cancellationToken);
        }

        public System.Threading.Tasks.Task DeleteTask(int taskId, CancellationToken cancellationToken)
        {
            return Delete("Task/Delete", $"taskId={taskId}", cancellationToken);
        }

		public Task<TaskUIItem> RegWeightTask(TaskWeightArg arg, CancellationToken cancellationToken)
		{
			return Post<TaskWeightArg, TaskUIItem>("Task/Weight", arg, cancellationToken);
		}

		public Task<TaskUIItem> StopTask(TaskStopArg arg, CancellationToken cancellationToken)
        {
            return Post<TaskStopArg, TaskUIItem>("Task/Stop", arg, cancellationToken);
        }

        public Task<TaskUIItem> RunTask(TaskRunArg arg, CancellationToken cancellationToken)
        {
            return Post<TaskRunArg, TaskUIItem>("Task/Run", arg, cancellationToken);
        }

        public Task<TaskUIItem> StartTask(TaskStartArg arg, CancellationToken cancellationToken)
        {
            return Post<TaskStartArg, TaskUIItem>("Task/Start", arg, cancellationToken);
        }

        public System.Threading.Tasks.Task RevertTask(int taskId, CancellationToken cancellationToken)
        {
            return Delete("Task/Revert", $"taskId={taskId}", cancellationToken);
        }

        public Task<List<TransmitLog>> GetTransmitLog(GetTransmitLogArg arg, CancellationToken cancellationToken)
        {
            return Get<List<TransmitLog>, List<TransmitLog>>("TransmitLog", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList> GetSiloStateReport(IDictionary<string, object> arg, CancellationToken cancellationToken)
        {
            return Get<List<LayerUIItem>, IList>("SiloStateReport", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList> GetSiloMoveReport(IDictionary<string, object> arg, CancellationToken cancellationToken)
        {
            return Get<List<MoveUIItem>, IList>("SiloMoveReport", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList> GetEquipmentStatsReport(IDictionary<string, object> arg, CancellationToken cancellationToken)
        {
            return Get<List<EquipmentStatUIItem>, IList>("EquipmentStatsReport", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList<Unit>> GetUnits(CancellationToken cancellationToken)
        {
            return Get<List<Unit>, IList<Unit>>("Units", cancellationToken);
        }

        public Task<List<UnitEquipmentUIItem>> GetUnitEquipments(GetUnitEquipmentsArg arg, CancellationToken cancellationToken)
        {
            return Get<List<UnitEquipmentUIItem>, List<UnitEquipmentUIItem>>("UnitEquipments", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public System.Threading.Tasks.Task UpdateUnitEquipments(UnitEquipmentUpdateArg arg, CancellationToken cancellationToken)
        {
            return Post("UnitEquipments/Update", arg, cancellationToken);
        }

        public Task<IList<Equipment>> GetEquipments(CancellationToken cancellationToken)
        {
            return Get<List<Equipment>, IList<Equipment>>("Equipments", cancellationToken);
        }

        public System.Threading.Tasks.Task StorageAssign(StorageAssignArg arg, CancellationToken cancellationToken)
        {
            return Post("Storage/Assign", arg, cancellationToken);
        }

        public Task<IList<LayerUIItem>> GetUnitLayers(GetUnitLayersArg arg, CancellationToken cancellationToken)
        {
            return Get<List<LayerUIItem>, IList<LayerUIItem>>("SiloStateReport", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public System.Threading.Tasks.Task StorageInventory(StorageInventoryArg arg, CancellationToken cancellationToken)
        {
            return Post("Storage/Inventory", arg, cancellationToken);
        }

        public Task<TaskUIItem> CreateTaskPLC(TaskPLCArg arg, CancellationToken cancellationToken)
        {
            return Post<TaskPLCArg, TaskUIItem>("Task/CreatePLC", arg, cancellationToken);
        }

        public Task<IList<VehicleUIItem>> GetVehicles(GetVehicleArg arg, CancellationToken cancellationToken)
        {
            return Get<List<VehicleUIItem>, IList<VehicleUIItem>>("Vehicles", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList<VehicleActivity>> GetVehicleHistory(int vehicleId, CancellationToken cancellationToken)
        {
            return Get<List<VehicleActivity>, IList<VehicleActivity>>("Vehicle/History", $"vehicleId={vehicleId}", cancellationToken);
        }

        public Task<VehicleUIItem> RegisterVehicle(VehicleRegisterArg arg, CancellationToken cancellationToken)
        {
            return Post<VehicleRegisterArg, VehicleUIItem>("Vehicle/Register", arg, cancellationToken);
        }

        public Task<VehicleUIItem> ServeVehicle(VehicleServeArg arg, CancellationToken cancellationToken)
        {
            return Post<VehicleServeArg, VehicleUIItem>("Vehicle/Serve", arg, cancellationToken);
        }

        public System.Threading.Tasks.Task DeleteVehicle(int vehicleId, CancellationToken cancellationToken)
        {
            return Delete("Vehicle/Delete", $"vehicleId={vehicleId}", cancellationToken);
        }

        public System.Threading.Tasks.Task DeleteVehicleHistory(VehicleHistoryDeleteArg arg, CancellationToken cancellationToken)
        {
            return Delete("Vehicle/History/Delete", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public Task<IList<NomenclaturePriorityUIItem>> GetNomenclaturePriority(CancellationToken cancellationToken)
        {
            return Get<List<NomenclaturePriorityUIItem>, IList<NomenclaturePriorityUIItem>>(
                "Settings/NomenclaturePriority", cancellationToken);
        }

        public System.Threading.Tasks.Task UpdateNomenclaturePriority(NomenclaturePriorityUpdateArg arg, CancellationToken cancellationToken)
        {
            return Post("Settings/NomenclaturePriority", arg, cancellationToken);
        }

        public Task<IList<TaskMatchUIItem>> GetTaskForMatching(GetTaskMatchArg arg, CancellationToken cancellationToken)
        {
            return Get<List<TaskMatchUIItem>, IList<TaskMatchUIItem>>("Task/Match", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public System.Threading.Tasks.Task MatchTask(TaskMatchArg arg, CancellationToken cancellationToken)
        {
            return Post("Task/Match", arg, cancellationToken);
        }

        public System.Threading.Tasks.Task UpdateDictionary(UpdateDictArg arg, CancellationToken cancellationToken)
        {
            return Post("Dict/Update", arg, cancellationToken);
        }

        public System.Threading.Tasks.Task DeleteDictionary(DeleteDictArg arg, CancellationToken cancellationToken)
        {
            return Delete("Dict/Delete", QueryParamsHelper.ToUriString(arg), cancellationToken);
        }

        public System.Threading.Tasks.Task RestoreDictionary(DeleteDictArg arg, CancellationToken cancellationToken)
        {
            return Post("Dict/Restore", arg, cancellationToken);
        }

    }
}
