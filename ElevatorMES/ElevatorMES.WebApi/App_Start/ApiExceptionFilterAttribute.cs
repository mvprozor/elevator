﻿using ElevatorMES.Utilites.Exceptions;
using ElevatorMES.Utilites.Helpers;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using Newtonsoft.Json;

namespace ElevatorMES.WebApi
{
    public class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly NLog.ILogger logger = NLog.LogManager.GetCurrentClassLogger();

        public override void OnException(HttpActionExecutedContext context)
        {
            var message = context.Exception.ExtractMessage();
            var args = new System.IO.StringWriter();
            var serializer = Newtonsoft.Json.JsonSerializer.CreateDefault();
            foreach (var kvp in context.ActionContext.ActionArguments)
            {
                args.WriteLine($"{kvp.Key}");
                serializer.Serialize(args, kvp.Value);
            }
            var logMessage = $"{context.Request.RequestUri}. Arguments: {args}";
            
            if (context.Exception is LogicException)
            {
                context.Response = context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, message);
                
                logger.Warn(context.Exception, logMessage);
            }
            else
            {
                context.Response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, 
                    $"Ошибка при обработке запроса: {message}");
                logger.Error(context.Exception, logMessage);
            }


            //HandleException(ex);

            //if (ex is ClientException)
            //{
            //    context.Response = context.Request.CreateErrorResponse(HttpStatusCode.Forbidden, ex.Message);
            //}
            //else if (ex is LogicException logicException)
            //{
            //    if (ex.Message != null && !ex.Message.Contains("^^^"))
            //    {
            //        context.Response = context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            //    }
            //    else
            //    {
            //        context.Response = CreateClientResponse(context.Request, ex.Message);
            //    }
            //}
            //else
            //{
            //    context.Response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Ошибка при обработке запроса: " + context.Exception);
            //}
        }

        private HttpResponseMessage CreateClientResponse(HttpRequestMessage request, string exMessage)
        {
            return request.CreateResponse(HttpStatusCode.BadRequest);
        }

        private void HandleException(Exception ex)
        {
            //if (ex is OperationCanceledException)
            //    return;

            //if (ex is LogicException)
            //    return;

            //LogManager.GetCurrentClassLogger().Log(LogLevel.Debug, ex.GetFullDescription());
        }

    }
}
