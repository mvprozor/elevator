﻿using ElevatorMES.Domain.Enums;
using ElevatorMES.Utilites.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;

namespace ElevatorMES.WebApi
{
    public class DomainRoleProvider : RoleProvider
    {
        private readonly WindowsTokenRoleProvider windowsRoleProvider;

        private readonly KeyValuePair<UserRole, string>[] roleMapping;

        protected readonly NLog.ILogger logger = NLog.LogManager.GetCurrentClassLogger();

        public DomainRoleProvider()
        {
            windowsRoleProvider = new WindowsTokenRoleProvider();
            var settings = System.Configuration.ConfigurationManager.AppSettings;
            roleMapping = EnumExtension.ToValues<UserRole>().OrderBy(m => m)
                .Select(m => new KeyValuePair<UserRole, string>(m, settings[$"DomainGroup{m}"]))
                .ToArray();
        }

        public override string ApplicationName { get; set; }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            var windowsRoles = new HashSet<string>(windowsRoleProvider.GetRolesForUser(username), StringComparer.OrdinalIgnoreCase);
            var skipped = roleMapping.SkipWhile(m => !windowsRoles.Contains(m.Value));
            var mesRoles = skipped.Select(m => m.Key.ToString()).ToArray();
            logger.Debug($"User {username} groups: {String.Join(", ", windowsRoles)}");
            return mesRoles;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}