﻿using ElevatorMES.Domain.Enums;
using System;
using System.Linq;
using System.Web.Http;

namespace ElevatorMES.WebApi
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params UserRole[] roles)
        {
            this.Roles = String.Join(",", roles.Select(m => m.ToString()));
        }
    }
}