﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;

namespace ElevatorMES.WebApi.Controllers
{
    /// <summary>
    /// Операции
    /// </summary>
    public class TaskController : BaseController
    {
        private TaskManager TaskManager => new TaskManager(UnitOfWork);
        public TaskController()
        {
           
        }

        /// <summary>
        /// Поступление информации о ТС от 1С
        /// </summary>
        /// <param name="regVehicleArg"></param>
        [HttpPost]
        [Route("Task/RowIncome/RegVehicle")]
        [AuthorizeRoles(UserRole.Operator)]
        public string RegVehicle(RegVehicleArg regVehicleArg)
        {
            return TaskManager.RegVehicle(regVehicleArg);
        }

        /// <summary>
        /// Поступление задания на приемку сырья от 1С
        /// </summary>
        /// <param name="rowAcceptArg"></param>
        [HttpPost]
        [Route("Task/RowIncome/Accept")]
        [AuthorizeRoles(UserRole.Operator)]
        public string Accept(RowAcceptArg rowAcceptArg)
        {
            return TaskManager.RowAccept(rowAcceptArg);
        }

        /// <summary>
        /// Отгрузка сырья внешним потребителям
        /// </summary>
        /// <param name="shippingArg"></param>
        [HttpPost]
        [Route("Task/Shipping")]
        [AuthorizeRoles(UserRole.Operator)]
        public string Shipping(ShippingArg shippingArg)
        {
            return TaskManager.Shipping(shippingArg);
        }

        /// <summary>
        /// Ручное создание задания
        /// </summary>
        /// <param name="arg"></param>
        [HttpPost]
        [Route("Task/CreateManual")]
        [AuthorizeRoles(UserRole.Operator)]
        public TaskUIItem CreateManual(TaskManualArg arg)
        {
            arg.User = GetUserName();
            return TaskManager.CreateManual(arg);
        }

        /// <summary>
        /// Cоздание задания по данным ПЛК
        /// </summary>
        /// <param name="arg"></param>
        [HttpPost]
        [Route("Task/CreatePLC")]
        [AuthorizeRoles(UserRole.Operator)]
        public TaskUIItem CreatePLC(TaskPLCArg arg)
        {
            arg.User = GetUserName();
            return TaskManager.CreatePLC(arg);
        }

        /// <summary>
        /// Удаление задания
        /// </summary>
        /// <param name="taskId"></param>
        [HttpDelete]
        [Route("Task/Delete")]
        [AuthorizeRoles(UserRole.Operator)]
        public void TaskDelete(int taskId)
        {
            TaskManager.Delete(taskId);
        }

        /// <summary>
        /// Получение списка заданий
        /// </summary>
        /// <param name="getTaskArg"></param>
        [HttpGet]
        [Route("Task")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<TaskUIItem> GetTaskUI([FromUri] GetTaskArg getTaskArg)
        {
            return TaskManager.GetTaskUI(getTaskArg);
        }

        /// <summary>
        /// Запуск (продолжение) задания
        /// </summary>
        /// <param name="taskStartArg"></param>
        [HttpPost]
        [Route("Task/Start")]
        [AuthorizeRoles(UserRole.Operator)]
        public TaskUIItem TaskStart(TaskStartArg taskStartArg)
        {
            taskStartArg.User = GetUserName();
            return TaskManager.Start(taskStartArg);
        }

        /// <summary>
        /// Перевод задания в состояние "выполняется"
        /// </summary>
        /// <param name="taskRunArg"></param>
        [HttpPost]
        [Route("Task/Run")]
        [AuthorizeRoles(UserRole.Operator)]
        public TaskUIItem TaskRun(TaskRunArg taskRunArg)
        {
            taskRunArg.User = GetUserName();
            return TaskManager.Run(taskRunArg);
        }

		/// <summary>
		/// Фиксация веса по заданию
		/// </summary>
		/// <param name="taskWeightArg"></param>
		[HttpPost]
		[Route("Task/Weight")]
		[AuthorizeRoles(UserRole.Operator)]
		public TaskUIItem TaskWeight(TaskWeightArg taskWeightArg)
		{
			taskWeightArg.User = GetUserName();
			return TaskManager.RegWeight(taskWeightArg);
		}

		/// <summary>
		/// Завершение (приостановка) задания
		/// </summary>
		/// <param name="taskStopArg"></param>
		[HttpPost]
        [Route("Task/Stop")]
        [AuthorizeRoles(UserRole.Operator)]
        public TaskUIItem TaskStop(TaskStopArg taskStopArg)
        {
            taskStopArg.User = GetUserName();
            return TaskManager.Stop(taskStopArg);
        }

        /// <summary>
        /// Задание приоритета приемки сырья
        /// </summary>
        /// <param name="priorityArg"></param>
        [HttpPost]
        [Route("Task/Priority")]
        [AuthorizeRoles(UserRole.Operator)]
        public string Priority(PriorityArg priorityArg)
        {
            return TaskManager.UpdatePriority(priorityArg);
        }


        /// <summary>
        /// Получение списка заданий для сопоставления
        /// </summary>
        /// <param name="getTaskArg"></param>
        [HttpGet]
        [Route("Task/Match")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<TaskMatchUIItem> GetTaskForMatching([FromUri] GetTaskMatchArg getTaskMatchArg)
        {
            return TaskManager.GetTaskForMatching(getTaskMatchArg);
        }

        /// <summary>
        /// Сопоставление заданий
        /// </summary>
        /// <param name="getTaskArg"></param>
        [HttpPost]
        [Route("Task/Match")]
        [AuthorizeRoles(UserRole.Reader)]
        public void MatchTask(TaskMatchArg arg)
        {
            TaskManager.MatchTask(arg);
        }

        /// <summary>
        /// Отмена выполнения задания
        /// </summary>
        /// <param name="taskId"></param>
        [HttpDelete]
        [Route("Task/Revert")]
        [AuthorizeRoles(UserRole.Operator)]
        public void TaskRevert(int taskId)
        {
            TaskManager.Revert(taskId);
        }

    }
}
