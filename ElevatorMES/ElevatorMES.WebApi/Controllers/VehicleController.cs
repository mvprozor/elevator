﻿using System.Collections.Generic;
using System.Web.Http;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
namespace ElevatorMES.WebApi.Controllers
{
    public class VehicleController : BaseController
    {
        private VehicleManager vehicleManager => new VehicleManager(UnitOfWork);

        /// <summary>
        /// Получение списка транспорта
        /// </summary>
        [HttpGet]
        [Route("Vehicles")]
        [AuthorizeRoles(UserRole.Reader)]
        [AuthorizeRoles(UserRole.Operator)]
        public List<VehicleUIItem> GetVehicles([FromUri] GetVehicleArg arg)
        {
            return vehicleManager.GetVehicles(arg);
        }

        /// <summary>
        /// Получение истории транспорта
        /// </summary>
        [HttpGet]
        [Route("Vehicle/History")]
        [AuthorizeRoles(UserRole.Reader)]
        [AuthorizeRoles(UserRole.Operator)]
        public List<VehicleActivity> GetVehicleHistory(int vehicleId)
        {
            return vehicleManager.GetVehicleHistory(vehicleId);
        }

        /// <summary>
        /// Регистрация автотранспорта
        /// </summary>
        [HttpPost]
        [Route("Vehicle/Register")]
        [AuthorizeRoles(UserRole.Operator)]
        public VehicleUIItem RegisterVehicle(VehicleRegisterArg arg)
        {
            return vehicleManager.RegisterVehicle(arg);
        }

        /// <summary>
        /// Действия с автотранспортом
        /// </summary>
        [HttpPost]
        [Route("Vehicle/Serve")]
        [AuthorizeRoles(UserRole.Operator)]
        public VehicleUIItem ServeVehicle(VehicleServeArg arg)
        {
            return vehicleManager.Serve(arg);
        }

        /// <summary>
        /// Удаление задания
        /// </summary>
        /// <param name="VehicleId"></param>
        [HttpDelete]
        [Route("Vehicle/Delete")]
        [AuthorizeRoles(UserRole.Operator)]
        public void VehicleDelete(int vehicleId)
        {
            vehicleManager.Delete(vehicleId);
        }

        /// <summary>
        /// Удаление действие
        /// </summary>
        /// <param name="activityId"></param>
        [HttpDelete]
        [Route("Vehicle/History/Delete")]
        [AuthorizeRoles(UserRole.Operator)]
        public void VehicleHistoryDelete([FromUri] VehicleHistoryDeleteArg arg)
        {
            vehicleManager.DeleteHistory(arg);
        }

        /// <summary>
        /// Выбор номеров
        /// </summary>
        [HttpGet]
        [Route("Vehicle/RegNums")]
        [AuthorizeRoles(UserRole.Operator)]
        public List<VehicleRegNumUIItem> VehicleRegNums()
        {
            return vehicleManager.GetRegNums();
        }

    }
}