﻿using System;
using System.Collections.Generic;
using System.Web.Http;

namespace ElevatorMES.WebApi.Controllers
{
    public class PingController : ApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [Route("Ping")]
        public DateTime Ping()
        {
            return DateTime.Now;
        }
    }
}
