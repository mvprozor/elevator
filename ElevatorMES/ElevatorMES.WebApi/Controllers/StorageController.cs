﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ElevatorMES.Data;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;

namespace ElevatorMES.WebApi.Controllers
{
    /// <summary>
    /// Операции
    /// </summary>
    public class StorageController : BaseController
    {
        private StorageManager StorageManager => new StorageManager(UnitOfWork);
        
        public StorageController()
        {
           
        }

        /// <summary>
        /// Получение списка складов, их номенклатуры и состояний
        /// </summary>
        [HttpGet]
        [Route("Storage")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<StorageUIItem> GetStorages()
        {
            return StorageManager.GetStorages();
        }

        /// <summary>
        /// Назначение склада
        /// </summary>
        [HttpPost]
        [Route("Storage/Assign")]
        [AuthorizeRoles(UserRole.Operator)]
        public void StorageAssign(StorageAssignArg arg)
        {
            StorageManager.StorageAssign(arg);
        }

        /// <summary>
        /// Инвентаризация склада
        /// </summary>
        [HttpPost]
        [Route("Storage/Inventory")]
        [AuthorizeRoles(UserRole.Operator)]
        public void StorageInventory(StorageInventoryArg arg)
        {
            StorageManager.StorageInventory(arg);
        }

        /// <summary>
        /// Получение слоев на складе
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("SiloStateReport")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<LayerUIItem> GetSiloStateReport(int siloId)
        {
            return StorageManager.GetSiloStateReport(siloId);
        }

        /// <summary>
        /// Получение движений по складу
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("SiloMoveReport")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<MoveUIItem> GetSiloMoveReport(int siloId, DateTime? timeFrom = null)
        {
            return StorageManager.GetSiloMoveReport(siloId, timeFrom);
        }

    }
}
