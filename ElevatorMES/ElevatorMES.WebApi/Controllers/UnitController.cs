﻿using System.Collections.Generic;
using System.Web.Http;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;

namespace ElevatorMES.WebApi.Controllers
{
    public class UnitController : BaseController
    {
        private UnitManager UnitManager => new UnitManager(UnitOfWork);

        /// <summary>
        /// Получение списка подразделений
        /// </summary>
        [HttpGet]
        [Route("Units")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<Unit> GetUnits()
        {
            return UnitManager.GetUnits();
        }

        /// <summary>
        /// Получение связки подразделение-оборудование
        /// </summary>
        /// <param name="arg"></param>
        [HttpGet]
        [Route("UnitEquipments")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<UnitEquipmentUIItem> GetUnitEquipments([FromUri] GetUnitEquipmentsArg arg)
        {
            return UnitManager.GetUnitEquipments(arg);
        }

        /// <summary>
        /// Обновление связок подразделение-оборудование
        /// </summary>
        /// <param name="arg"></param>
        [HttpPost]
        [Route("UnitEquipments/Update")]
        [AuthorizeRoles(UserRole.Operator)]
        public void UpdateUnitEquipments(UnitEquipmentUpdateArg arg)
        {
            UnitManager.UpdateUnitEquipments(arg);
        }

        /// <summary>
        /// Получение списка точек погрузки, отгрузки
        /// </summary>
        [HttpGet]
        [Route("Points")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<UnitUIItem> GetPoints()
        {
            return UnitManager.GetPoints();
        }
    }
}
