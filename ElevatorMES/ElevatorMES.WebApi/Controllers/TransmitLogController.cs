﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ElevatorMES.Data;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;

namespace ElevatorMES.WebApi.Controllers
{
    public class TransmitLogController : BaseController
    {
        private TransmitLogManager TransmitLogManager => new TransmitLogManager(UnitOfWork);

        public TransmitLogController()
        {

        }

        /// <summary>
        /// Получение журнала отправки
        /// </summary>
        [HttpGet]
        [Route("TransmitLog")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<TransmitLog> GetTransmitLog([FromUri] GetTransmitLogArg arg)
        {
            return TransmitLogManager.GetTransmitLog(arg);
        }
    }
}