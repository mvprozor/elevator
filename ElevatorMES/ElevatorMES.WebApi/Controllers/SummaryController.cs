﻿using System;
using System.Web.Http;
using ElevatorMES.Data.Managers;
using System.Linq;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;
using ElevatorMES.Utilites.Helpers;
using ElevatorMES.Data.Managers.Base;

namespace ElevatorMES.WebApi.Controllers
{
    public class SummaryController : BaseController
    {
        private SummaryManager summaryManager => new SummaryManager(UnitOfWork);

        /// <summary>
        /// Получение оперативной информации
        /// </summary>
        [HttpGet]
        [Route("OperInfo")]
        [AuthorizeRoles(UserRole.Reader)]
        public OperInfoUI GetOperInfo()
        {
            var res = summaryManager.GetOperInfo();
            res.UserName = User.Identity.Name;
            res.UserRole = EnumExtension.ToValues<UserRole>().First(m => User.IsInRole(m.ToString()));
            res.Is1CAvailable = true;
            return res;
        }
    }
}
