﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model.Args;

namespace ElevatorMES.WebApi.Controllers
{
    /// <summary>
    /// Справочники
    /// </summary>
    public class DictController : BaseController
    {
        private DictManager DictManager => new DictManager(UnitOfWork);
        private SyncManager SyncManager => new SyncManager(UnitOfWork);

        public DictController()
        {
           
        }

        /// <summary>
        /// Пользователи
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Dict/Users")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<User> GetUsers()
        {
            var result = DictManager.GetUsers().ToList();
            return result;
        }

        /// <summary>
        /// Организации
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Dict/Organizations")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<Organization> GetOrganizations([FromUri] GetDictArg arg)
        {
            return DictManager.GetOrganizations(arg).OrderBy(m => m.DisplayName).ToList();
        }

        /// <summary>
        /// Контрагенты
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Dict/Contragents")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<Contragent> GetContragents([FromUri] GetDictArg arg)
        {
            return DictManager.GetContragents(arg).OrderBy(m => m.DisplayName).ToList();
        }

        /// <summary>
        /// Номенклатура сырья и продукции
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Dict/Nomenclatures")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<Nomenclature> GetNomenclatures([FromUri] GetDictArg arg)
        {
            return DictManager.GetNomenclatures(arg).OrderBy(m => m.DisplayName).ToList();
        }

        /// <summary>
        /// Силосы
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Dict/Silos")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<Unit> GetSilos()
        {
            return DictManager.GetSilos().ToList();
        }

        /// <summary>
        /// Склады
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Dict/Storages")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<Unit> GetStorages()
        {
            return DictManager.GetStorages().ToList();
        }


        /// <summary>
        /// Транспортные средства
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Dict/Vehicles")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<Vehicle> GetVehicles()
        {
            return DictManager.GetVehicles().ToList();
        }

        /// <summary>
        /// Типы транспортных средств
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Dict/VehicleTypes")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<VehicleType> GetVehicleTypes()
        {
            return DictManager.GetVehicleTypes().ToList();
        }

        /// <summary>
        /// Синхронизируемые с 1С таблицы
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Dict/GetSyncTables")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<string> GetSyncEntities()
        {
            return SyncManager.GetSyncEntities().ToList(); ;
        }

        /// <summary>
        /// Синхронизировать
        /// </summary>
        /// <param name="dictSyncArg"></param>
        [HttpPost]
        [Route("Dict/Sync")]
        [AuthorizeRoles(UserRole.Operator)]
        public void Sync(DictSyncArg dictSyncArg)
        {
            SyncManager.Sync(dictSyncArg);
        }


        /// <summary>
        /// Поучение логов 1С
        /// </summary>
        /// <param name="logEvent1CArg"></param>
        [HttpPost]
        [Route("Dict/LogEvent1C")]
        [AuthorizeRoles(UserRole.Reader)]
        public string AddLogEvent1C(LogEvent1CArg logEvent1CArg)
        {
            return SyncManager.AddLogEvent1C(logEvent1CArg);
        }

        /// <summary>
        /// Обновление/создание
        /// </summary>
        /// <param name="arg"></param>
        [HttpPost]
        [Route("Dict/Update")]
        [AuthorizeRoles(UserRole.Operator)]
        public void UpdateDictionary(UpdateDictArg arg)
        {
            DictManager.UpdateDictionary(arg);
        }

        /// <summary>
        /// Удаление
        /// </summary>
        /// <param name="arg"></param>
        [HttpDelete]
        [Route("Dict/Delete")]
        [AuthorizeRoles(UserRole.Operator)]
        public void DeleteDictionary([FromUri] DeleteDictArg arg)
        {
            DictManager.DeleteDictionary(arg);
        }

        /// <summary>
        /// Восстановление
        /// </summary>
        /// <param name="arg"></param>
        [HttpPost]
        [Route("Dict/Restore")]
        [AuthorizeRoles(UserRole.Operator)]
        public void RestoreDictionary(DeleteDictArg arg)
        {
            DictManager.RestoreDictionary(arg);
        }

    }
}
