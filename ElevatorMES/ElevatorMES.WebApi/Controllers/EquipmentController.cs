﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;

namespace ElevatorMES.WebApi.Controllers
{
    public class EquipmentController : BaseController
    {
        private EquipmentManager EquipmentManager => new EquipmentManager(UnitOfWork);

        /// <summary>
        /// Получение списка подразделений
        /// </summary>
        [HttpGet]
        [Route("Equipments")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<Equipment> GetEquipments()
        {
            return EquipmentManager.GetEquipments();
        }

        /// <summary>
        /// Статистика по работе оборудования
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("EquipmentStatsReport")]
        [AuthorizeRoles(UserRole.Reader)]
        public List<EquipmentStatUIItem> GetEquipmentStatsReport(string equipmentId, DateTime begin, DateTime end)
        {
            return EquipmentManager.GetEquipmentStats(equipmentId, begin, end);
        }
    }
}
