﻿using System.Web.Http;
using ElevatorMES.Data;
using System.Threading;

namespace ElevatorMES.WebApi.Controllers
{
    public class BaseController : ApiController
    {
        public readonly UnitOfWork UnitOfWork;

        protected readonly NLog.ILogger logger = NLog.LogManager.GetCurrentClassLogger();

        public BaseController()
        {
            var ctx = new DatabaseContext();
            UnitOfWork = new UnitOfWork(ctx);
            //logger.Trace($"Контроллер \"{this.GetType().FullName}\" создан. ThreadId: {Thread.CurrentThread.ManagedThreadId}");
        }

        protected string GetUserName()
        {
            return User?.Identity?.Name ?? "Anonymous";
        }
    }
}
