﻿using System.Collections.Generic;
using System.Web.Http;
using ElevatorMES.Data.Managers;
using ElevatorMES.Domain.Data;
using ElevatorMES.Domain.Enums;
using ElevatorMES.Domain.Model;
using ElevatorMES.Domain.Model.Args;
using ElevatorMES.Domain.Model.UI;

namespace ElevatorMES.WebApi.Controllers
{
    public class SettingsController : BaseController
    {
        private SettingsManager settingsManager => new SettingsManager(UnitOfWork);

        /// <summary>
        /// Получение приоритетов сырья
        /// </summary>
        [HttpGet]
        [Route("Settings/NomenclaturePriority")]
        [AuthorizeRoles(UserRole.Reader)]
        [AuthorizeRoles(UserRole.Operator)]
        public List<NomenclaturePriorityUIItem> GetNomenclaturePriority()
        {
            return settingsManager.GetNomenclaturePriority();
        }

        /// <summary>
        /// Обновление приоритетов сырья
        /// </summary>
        [HttpPost]
        [Route("Settings/NomenclaturePriority")]
        [AuthorizeRoles(UserRole.Reader)]
        [AuthorizeRoles(UserRole.Operator)]
        public void UpdateNomenclaturePriority(NomenclaturePriorityUpdateArg arg)
        {
            settingsManager.UpdateNomenclaturePriority(arg);
        }
    }
}