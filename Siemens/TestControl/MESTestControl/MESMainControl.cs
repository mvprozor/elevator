﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MESTestControl
{
    public partial class MESMainControl: UserControl
    {
        public event EventHandler TaskStart;

        public int CodeFrom
        { 
            get 
            {
                int.TryParse(tbCodeFrom.Text, out var res);
                return res;
            }
            set
            {
                tbCodeFrom.Text = value.ToString();
            }
        }

        public int CodeTo
        {
            get
            {
                int.TryParse(tbCodeTo.Text, out var res);
                return res;
            }
            set
            {
                tbCodeTo.Text = value.ToString();
            }
        }

        public int Result
        {
            get
            {
                int.TryParse(tbResult.Text, out var res);
                return res;
            }
            set
            {
                IsExecuting = false;
                tbResult.Text = value.ToString();
            }
        }

        public string Comment
        {
            get => tbComment.Text;
            set
            {
                tbComment.Text = value;
            }
        }

        public bool IsExecuting
        {
            get => isExecuting;
            set
            {
                if (isExecuting != value)
                {
                    isExecuting = value;
                    if (value)
                    {
                        btnStart.Enabled = false;
                        TaskStart?.Invoke(this, EventArgs.Empty);
                    }
                    else
                    {
                        btnStart.Enabled = true;
                    }
                }
            }
        }

        private bool isExecuting;


        public MESMainControl()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            IsExecuting = true;
            
        }
    }
}
