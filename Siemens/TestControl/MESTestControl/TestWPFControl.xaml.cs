﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MESTestControl
{
    /// <summary>
    /// Interaction logic for TestWPFControl.xaml
    /// </summary>
    [Browsable(false)]
    public partial class TestWPFControl : UserControl, INotifyPropertyChanged
    {
        public int Numeric
        {
            get => numeric;
            set
            {
                if (value!=numeric)
                {
                    numeric = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Numeric)));
                }

            }
        }

        private int numeric;



        public TestWPFControl()
        {
            InitializeComponent();
            DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
