﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MESTestControl
{
    public partial class WPFContainer : UserControl
    {
        [Description("Numeric value")]
        [Category("Setup")]
        public int Numeric
        {
            get => testWPFControl1.Numeric;
            set => testWPFControl1.Numeric = value;
        }

        [Description("Numeric values")]
        [Category("Setup")]
        public int[] Numerics
        {
            get;
            set;
        }

        public WPFContainer()
        {
            InitializeComponent();
            Numerics = new int[4];
        }
    }
}
