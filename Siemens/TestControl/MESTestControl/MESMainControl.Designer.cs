﻿namespace MESTestControl
{
    partial class MESMainControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbCodeFrom = new System.Windows.Forms.TextBox();
            this.lbFrom = new System.Windows.Forms.Label();
            this.tbCodeTo = new System.Windows.Forms.TextBox();
            this.lbTo = new System.Windows.Forms.Label();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.tbComment = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbCodeFrom
            // 
            this.tbCodeFrom.Location = new System.Drawing.Point(87, 22);
            this.tbCodeFrom.Name = "tbCodeFrom";
            this.tbCodeFrom.Size = new System.Drawing.Size(100, 20);
            this.tbCodeFrom.TabIndex = 0;
            // 
            // lbFrom
            // 
            this.lbFrom.AutoSize = true;
            this.lbFrom.Location = new System.Drawing.Point(8, 25);
            this.lbFrom.Name = "lbFrom";
            this.lbFrom.Size = new System.Drawing.Size(43, 13);
            this.lbFrom.TabIndex = 1;
            this.lbFrom.Text = "Откуда";
            // 
            // tbCodeTo
            // 
            this.tbCodeTo.Location = new System.Drawing.Point(87, 67);
            this.tbCodeTo.Name = "tbCodeTo";
            this.tbCodeTo.Size = new System.Drawing.Size(100, 20);
            this.tbCodeTo.TabIndex = 0;
            // 
            // lbTo
            // 
            this.lbTo.AutoSize = true;
            this.lbTo.Location = new System.Drawing.Point(8, 74);
            this.lbTo.Name = "lbTo";
            this.lbTo.Size = new System.Drawing.Size(31, 13);
            this.lbTo.TabIndex = 1;
            this.lbTo.Text = "Куда";
            // 
            // tbResult
            // 
            this.tbResult.Location = new System.Drawing.Point(87, 168);
            this.tbResult.Name = "tbResult";
            this.tbResult.Size = new System.Drawing.Size(100, 20);
            this.tbResult.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 171);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Результат";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(87, 112);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Запуск";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tbComment
            // 
            this.tbComment.Location = new System.Drawing.Point(87, 217);
            this.tbComment.Name = "tbComment";
            this.tbComment.Size = new System.Drawing.Size(100, 20);
            this.tbComment.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 220);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Примечание";
            // 
            // MESMainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbTo);
            this.Controls.Add(this.lbFrom);
            this.Controls.Add(this.tbComment);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.tbCodeTo);
            this.Controls.Add(this.tbCodeFrom);
            this.Name = "MESMainControl";
            this.Size = new System.Drawing.Size(239, 273);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbCodeFrom;
        private System.Windows.Forms.Label lbFrom;
        private System.Windows.Forms.TextBox tbCodeTo;
        private System.Windows.Forms.Label lbTo;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox tbComment;
        private System.Windows.Forms.Label label2;
    }
}
