USE [master]
GO
/****** Object:  Database [RUSAGRO]    Script Date: 27.04.2022 12:31:57 ******/
CREATE DATABASE [RUSAGRO]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RUSAGRO', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.WINCCPLUSMIG2014\MSSQL\DATA\RUSAGRO.mdf' , SIZE = 17408KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'RUSAGRO_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.WINCCPLUSMIG2014\MSSQL\DATA\RUSAGRO_log.ldf' , SIZE = 2816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [RUSAGRO] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RUSAGRO].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RUSAGRO] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RUSAGRO] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RUSAGRO] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RUSAGRO] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RUSAGRO] SET ARITHABORT OFF 
GO
ALTER DATABASE [RUSAGRO] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RUSAGRO] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RUSAGRO] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RUSAGRO] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RUSAGRO] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RUSAGRO] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RUSAGRO] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RUSAGRO] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RUSAGRO] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RUSAGRO] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RUSAGRO] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RUSAGRO] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RUSAGRO] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RUSAGRO] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RUSAGRO] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RUSAGRO] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RUSAGRO] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RUSAGRO] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RUSAGRO] SET  MULTI_USER 
GO
ALTER DATABASE [RUSAGRO] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RUSAGRO] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RUSAGRO] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RUSAGRO] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [RUSAGRO] SET DELAYED_DURABILITY = DISABLED 
GO
USE [RUSAGRO]
GO
/****** Object:  User [SQL_S71500]    Script Date: 27.04.2022 12:31:57 ******/
CREATE USER [SQL_S71500] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MT25-W-00100\SIMATIC HMI]    Script Date: 27.04.2022 12:31:57 ******/
CREATE USER [MT25-W-00100\SIMATIC HMI]
GO
ALTER ROLE [db_owner] ADD MEMBER [SQL_S71500]
GO
ALTER ROLE [db_owner] ADD MEMBER [MT25-W-00100\SIMATIC HMI]
GO
/****** Object:  Table [dbo].[CURRENT]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CURRENT](
	[ID] [float] NULL,
	[HhCur] [float] NULL,
	[WarnCur] [float] NULL,
	[EmerCur] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CURRENTnew]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CURRENTnew](
	[ID] [float] NULL,
	[HhCur] [float] NULL,
	[WarnCur] [float] NULL,
	[EmerCur] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ID]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ID](
	[ID] [float] NULL,
	[GV_Dev] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MH]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MH](
	[DevId] [int] NULL,
	[TaskId] [int] NULL,
	[DateTime] [datetime] NULL,
	[MotorHours] [bigint] NULL,
	[Power] [real] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[REPORT]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REPORT](
	[TaskId] [int] NULL,
	[StartTime] [datetime] NULL,
	[StopTime] [datetime] NULL,
	[Source] [nvarchar](50) NULL,
	[Destination] [nvarchar](50) NULL,
	[AllDevs] [nvarchar](400) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ROUTES]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ROUTES](
	[IDrt] [int] NULL,
	[IDsubrt] [int] NULL,
	[IDdev] [int] NULL,
	[VALdev] [int] NULL,
	[VALcmd] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TERMOdata]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TERMOdata](
	[DateTime] [datetime] NULL,
	[idSL] [int] NULL,
	[idTC] [int] NULL,
	[Sns_0] [float] NULL,
	[Sns_1] [float] NULL,
	[Sns_2] [float] NULL,
	[Sns_3] [float] NULL,
	[Sns_4] [float] NULL,
	[Sns_5] [float] NULL,
	[Sns_6] [float] NULL,
	[Sns_7] [float] NULL,
	[Sns_8] [float] NULL,
	[Sns_9] [float] NULL,
	[Sns_10] [float] NULL,
	[Sns_11] [float] NULL,
	[Sns_12] [float] NULL,
	[Sns_13] [float] NULL,
	[Sns_14] [float] NULL,
	[Sns_15] [float] NULL,
	[Sns_16] [float] NULL,
	[Sns_17] [float] NULL,
	[Sns_18] [float] NULL,
	[Sns_19] [float] NULL,
	[Sns_20] [float] NULL,
	[Sns_21] [float] NULL,
	[Sns_22] [float] NULL,
	[Sns_23] [float] NULL,
	[Sns_24] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TERMOparams]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TERMOparams](
	[idSL] [float] NULL,
	[idTC] [float] NULL,
	[WarnSP] [float] NULL,
	[EmergSP] [float] NULL,
	[Status] [float] NULL,
	[adrTC] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TERMOparams1]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TERMOparams1](
	[idSL] [int] NULL,
	[idTC] [int] NULL,
	[WarnSP] [int] NULL,
	[EmergSP] [int] NULL,
	[Status] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TERMOsl]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TERMOsl](
	[idSL] [int] NULL,
	[nmSL] [nvarchar](255) NULL,
	[WarnSP] [int] NULL,
	[EmergSP] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[MESSilosStates]    Script Date: 27.04.2022 12:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[MESSilosStates]
AS
SELECT 
	un.Id,
	un.Code1C,
	un.DisplayName,
	us.NomenclatureId,
	nm.Code1C NomenclatureCode1C,
	nm.DisplayName NomenclatureDisplayName,
	un.Capacity,
	ISNULL(uq.Quantity, 0.0) Quantity,
	CAST(ROUND(ISNULL(uq.Quantity*100.000/NULLIF(un.Capacity,0.0), 0.0),3) AS DECIMAL(18,3)) FillPercent
FROM [192.168.0.58].ElevatorMES.dbo.Units un
LEFT JOIN [192.168.0.58].ElevatorMES.dbo.UnitSettings us ON us.UnitId=un.Id
LEFT JOIN [192.168.0.58].ElevatorMES.dbo.Nomenclatures nm ON nm.Id=us.NomenclatureId
LEFT JOIN
(
	SELECT 
		ul.UnitId,
		SUM(Quantity) Quantity
	FROM [192.168.0.58].ElevatorMES.dbo.UnitLayers ul
	GROUP BY ul.UnitId
) uq ON uq.UnitId = un.Id
WHERE un.Type=3 OR un.Type=5 OR un.Type=7

GO
USE [master]
GO
ALTER DATABASE [RUSAGRO] SET  READ_WRITE 
GO
