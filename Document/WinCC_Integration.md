# Статусы задач MES

* Queued - поставлено в очередь на выполнение. В этом состоянии задания оказываются после получения из 1С или после создания вручуню оператором.
* Running - задание выполняется, его маршрут работает.
* Suspended - задание выполняется, но его маршрут не работает (ожидание готовности и.т.п.).
* Completed - задание успешно завершено.
* Failed - задание завершилось со сбоем (но при этом могли частично переместить вес).
* Deleted - задание удалено (применяется только для заданий в состоянии Queued)/

![Диаграмма переходов статусов](db1.png "Диаграмма переходов статусов")

# Интерфейс обмена (C#)

```csharp
    public interface IWinCCTasks
    {
        /// <summary>
        /// Уникальный идентификатор задания
        /// </summary>
        int PLCTaskId { get; set; }

        /// <summary>
        /// Код-оборудования-источника
        /// </summary>
        int PLCSource { get; set; }

        /// <summary>
        /// Код-оборудования-приёмника
        /// </summary>
        int PLCDestination { get; set; }

        /// <summary>
        /// Слово операций
        /// </summary>
        /// <remarks>
        ///                            разряды
        ///                     |---|---|---|
        /// Зарезервировано ... | 2 |-1-|-0-|
        ///                     |---|---|---|
        ///                       |   |    |
        ///  	                  |   |    |-----> Сушка
        ///                       |   |----------> Сортировка
        ///                       |--------------> Скальпирование
        /// </remarks>
        int PLCOperations { get; set; }

        /// <summary>
        /// Плановый вес по заданию
        /// </summary>
        double PLCPlannedWeight { get; set; }

        /// <summary>
        /// Фактический вес по заданию
        /// </summary>
        double PLCActualWeight { get; set; }

        /// <summary>
        /// Ответ на запрос статуса задания
        /// </summary>
        ///  2 - маршрут работает
        ///  1 - маршрут по заданию стоит (готовится к запуску, остановился из-за аварии или завершения и.т.п.)
        ///  0 - нет связи с WinCC (исходное состояние перед запросом статуса)
        /// -1 - неизвестный ID задания (задание не запускалось или заверишилось и были запущены другие задания)
        /// -2 - некорректный ID источника или приемника
        /// -3 - задание не может быть запущено
        int PLCTaskStatus { get; set; }

        /// <summary>
        /// Событие, по которому нужно запустить задание
        /// </summary>
        /// <remarks>
        /// В качестве параметров перед активацией события задаются:
        /// PLCTaskId, PLCSource, PLCDestination, PLCOperations, PLCPlannedWeight
        /// </remarks>
        event EventHandler StartTask;

        /// <summary>
        /// Событие, по которому нужно завершить задание
        /// </summary>
        /// <remarks>
        /// В качестве параметров перед активацией события задаются:
        /// PLCTaskId
        /// </remarks>
        event EventHandler StopTask;

        /// <summary>
        /// Событие, по которому нужно приостановить задание
        /// </summary>
        /// <remarks>
        /// В качестве параметров перед активацией события задаются:
        /// PLCTaskId
        /// </remarks>
        event EventHandler SuspendTask;

        /// <summary>
        /// Опрос состояния задания
        /// </summary>
        /// <remarks>
        /// В качестве параметров перед активацией события задаются:
        /// PLCTaskId, PLCTaskStatus = 0
        /// После вызова заполняются следующие поля:
        /// PLCActualWeight - вес, прошедший по конвейрным весам
        /// PLCTaskStatus - статус задания
        /// </remarks>
        event EventHandler GetTaskStatus;
```

# Описание обмена

Удобно, чтобы в WinCC хранилась следующая структура:

```c
struct TaskInfo
{
	int Id;		// PLCTaskId для MES
	int RouteId; // <внутренний Id выбранного маршрута для задания> - для MES не нужен, нужен для проверок
	int Status;	// PLCTaskStatus для MES
	double PlannedWeight; // PLCPlannedWeight для MES
	double ActualWeight;  // PLCActualWeight для MES
}
```

Для описания активных маршрутов используются массив структур TaskInfo.


## Опрос состояния заданий

Все задания в статусе "Suspended" и "Running" периодически опрашиваются на предмет изменения состояния.
На каждом цикле опроса выполняются следюущие действия:

* [MES] Устанавливает следуюшие поля интерфейса обмена: PLCTaskId.
* [MES] Вызывает событие GetTaskStatus.
* [SCADA] Проверяет, есть среди активных маршруты с таким PLCTaskId. Если нет, то PLCTaskStatus=-1, выходим.
* [SCADA] Выдает расчитанное текущее состояние задания-маршрута в PLCTaskStatus, вес в PLCActualWeight из TaskInfo.


## Запуск задания

* [MES] Пользователь выбрал задание нажал кнопку "Пуск".
* [MES] Выбранное задание переводится в состояние "Suspended".
* [MES] Устанавливает следуюшие поля интерфейса обмена: PLCTaskId, PLCSource, PLCDestination, PLCOperations, PLCPlannedWeight.
* [MES] Вызывает событие StartTask.
* [SCADA] Получает событие StartTask.
* [SCADA] Создает структуру taskInfo для запрашиваемого задания. Инициализируются поля: taskInfo.Id=PLCTaskId, taskInfo.PlannedWeight=PLCPlannedWeight, taskInfo.Status=1.
* [SCADA] Проверяет идентификаторы PLCSource источника и PLCDestination приемника. Если они некорректны, то taskInfo.Status=-2, выходим.
* [SCADA] Вызывает окно выбора маршрутов. 
* [SCADA] При отказе от выбора (нет доступных свободных устройств в нужных маршрутах) выдаем статус taskInfo.Status=-3, выходим.
* [SCADA] Id выбранного маршрута запусывается в taskInfo.RouteId. 
* [SCADA] После выбора маршрута выполняется его подготовка и проверка готовности. В случае неготовности выдаем статус taskInfo.Status=-3, выходим.
* [SCADA] После запуска устанавливаем статус taskInfo.Status=2.







