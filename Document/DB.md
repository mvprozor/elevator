# Таблицы
## Справочники
### Пользователи (Users)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* DisplayName NVARCHAR(255) NOT NULL отображаемое имя
* Deleted BIT NOT NULL DEFAULT 0 - признак удаления

### Номенклатура сырья и продукции (Nomenclature)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* Deleted BIT NOT NULL - признак удаления
* DisplayName NVARCHAR(255) NOT NULL отображаемое имя
* Code1C NVARCHAR(64) NOT NULL UNIQUE код для 1С 
* Type1C NVARCHAR(36) NULL код типа в 1С
* CodeParent1C NVARCHAR(36) NULL код родительского элемента в 1С

### Организации (Organizations)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* Deleted BIT NOT NULL - признак удаления
* DisplayName NVARCHAR(255) NOT NULL отображаемое имя
* Code1C NVARCHAR(64) NOT NULL UNIQUE код для 1С 
* Type1C NVARCHAR(36) NULL код типа в 1С
* CodeParent1C NVARCHAR(36) NULL код родительского элемента в 1С

### Контрагенты (Contragents)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* Deleted BIT NOT NULL - признак удаления
* DisplayName NVARCHAR(255) NOT NULL отображаемое имя
* Code1C NVARCHAR(64) NOT NULL UNIQUE код для 1С 
* Type1C NVARCHAR(36) NULL код типа в 1С
* CodeParent1C NVARCHAR(36) NULL код родительского элемента в 1С

### Типы транспортных средста (VehicleTypes)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* Deleted BIT NOT NULL - признак удаления
* Class TINYINT NOT NULL - класс транспортного средства (enum: Truck=1)
* DisplayName NVARCHAR(255) NOT NULL отображаемое имя
* Code1C NVARCHAR(64) NOT NULL UNIQUE код для 1С 
* Type1C NVARCHAR(36) NULL код типа в 1С
* CodeParent1C NVARCHAR(36) NULL код родительского элемента в 1С

### Узлы (Units)

Описывает силосы, точки выгрузки, промежуточные склады и.т.п.

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* Deleted BIT NOT NULL - признак удаления
* Type TINYINT NOT NULL - тип узла 
    * Базовый тип склада = 1
    	* Силос = 3
    	* Бункер 5
    	* Сушилка = 9
    * Базовый тип точки погрузки/отгрузки 16 
    	* Точка погрузки = 48
    	* Точка выгрузки = 80
* Capacity DECIMAL(18,3) NOT NULL емкость в тоннах
* DisplayName NVARCHAR(255) NOT NULL отображаемое имя
* Code1C NVARCHAR(64) NOT NULL UNIQUE код для 1С 
* Type1C NVARCHAR(36) NULL код типа в 1С
* CodeParent1C NVARCHAR(36) NULL код родительского элемента в 1С

### Оборудование (Equipments)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* EquipmentType TINYINT NOT NULL - тип оборудования
    * Common = 1
    * Noria = 2
    * Conveyor = 3
    * Fan = 4
    * Airlock = 5
    * VibratingPlate = 6
    * Scalpel = 7
    * Separator = 8
    * CarUnloader = 9
    * Screw = 10
    * HopperScales = 11
    * GrainDryer = 12
    * Siren = 13
    * GateValve = 14
    * TwoWayValve = 15
    * ThreeWayValve = 16
    * Bunker = 17
* SysName NVARCHAR(128) NOT NULL отображаемое имя
* DisplayName NVARCHAR(128) NOT NULL отображаемое имя

### Связка узлов и оборудования (UnitEquipmentLinks)

* UnitsId INT NOT NULL PK FK - идентификатор узла
* EquipmentId INT NOT NULL PK FK - идентификатор оборудования
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* LinkType INT NOT - тип связки
    * Нет связи = 0
    * Контейнер = 1
    * Начальная точка маршрута = 2
    * Конечная точка маршрута = 4

## Журналы учета

### Партии (Lots)
Отражает партию как сущность - некоторое количество определенного сырья, полученное от одного поставщика в одно время на одной или нескольких транспортных единицах. Обладает уникальным номером партии.

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания партии
* LotNo NVARCHAR(64) NOT NULL UNIQUE - уникальный номер партии
* NomenclatureId INT NOT NULL FK - какое сырье
* OrganizationId INT NULL FK - поставщик

### Назначение склада (UnitsSettings)
Какая номенклатура лежит в каком силосе. 

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* UnitsId INT NOT NULL PK FK - идентификатор силоса
* NomenclatureId INT NULL FK - ссылка на сырье
* UpdatedAt DATETIME2 NOT NULL - дата обновления

### Слои силоса (UnitsLayers)
В эту таблицу пишется послойное состояние силоса. По датам слоя определяется в каком они порядке. C точки зрения подачи в силос и выдачи из него слои организованы в виде FIFO (подается сверху, выдается снизу). В слое хранится определенное количество из одной партии. Партии в слоях могут повторяться (прогнали половину слоя и положили сверху обратно).

* UnitsId INT NOT NULL PK FK - идентификатор силоса
* CreatedAt DATETIME2 NOT NULL - когда положили слой, определяет порядок FIFO
* LotId INT NOT NULL FK - ссылка на партию
* Quantity DECIMAL(18,3) NOT NULL - сколько в слое тонн лежит

### Журнал регистрации транспорта (Vehicles)
Транспорт, поступивший на предприятие под погрузку/отгрузку.

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* VehicleTypeId INT NOT NULL FK - тип транспортного средства
* RegNum NVARCHAR(36) NOT NULL - номер транспортного средства
* LeftAt DATETIME2 NULL - время покидания территории
* DocId NVARCHAR(36) NULL - идентификатор документа (1C)
* Sync1CAt DATETIME2 NULL - дата передачи/приема в 1С
* UserId INT FK NULL - автор (диспетчер или пользователь в 1С)

### Журнал активности транспорта на территории VehiclesActivities
Что происходит с транспортом, поступившим на предприятие под погрузку/отгрузку.

* CreatedAt PK DATETIME2 NOT NULL - время создания записи
* VehicleId PK INT NOT NULL FK - ссылка на журнал регистрации
* Action TINYINT NOT NULL - действие
    * Registering = 1 - регистрация
    * Weighting = 2 - взвешивание
    * Unloading = 3 - выгрузка
    * Loading = 4 - погрузка
* NomenclatureId INT NULL FK - что в транспоре в зависимоти от действия
* UnitId INT NULL FK - где происходило
* Weight DECIMAL(18,3) NULL - вес в зависимости от действия
* Comment NVARCHAR(255) NULL - комментарий к действию

### Задания (Tasks)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания задания
* Type TINYINT - тип задания 
    * Приемка = 1
    * Перемещение между силосами = 2
    * Перемещение на комб. завод = 3
    * Отрузка внешнему потребителю = 4
    * Отрузка в рукава = 5
* NomenclatureId INT NOT NULL FK - продукция
* SourceLotId INT FK NULL FK - исходная партия
* SourceUnitId INT NULL FK - источник
* DestLotId INT NULL FK - конечная партия
* DestUnitId INT NULL FK - применик
* OrganizationId INT NULL FK - откуда или кому (NULL для комбикормового завода и рукавов, перемещений)
* ContragentId INT NULL FK - идентификатор контрагента
* VehicleId INT NULL FK - транспортное средство
* IsDrying BIT NOT NULL - сушка
* IsScalping BIT NOT NULL - очистка скальператором
* IsSeparating BIT NOT NULL - сепарация
* ExecuteAt DATE NULL - требуемая дата исполнения
* Quantity DECIMAL(18,3) NOT NULL - количество
* Status TINYINT NOT NULL - статус задания
	* В очереди (Queued)= 1
	* Удалено (Deleted) = 2
	* Выполняется (Running) = 3
	* Завершено (Completed) = 4
	* Сбой (failed) = 5
	* Ожидание (Pending) = 6
	* Приостановлено (Suspended) = 7
* Comment NVARCHAR(2000) NULL - комментарий к заданию
* UserId INT FK NULL - автор (диспетчер или пользователь в 1С)
* DocId NVARCHAR(36) NULL - идентификатор документа в 1C
* Sync1CAt DATETIME2 NULL - дата передачи/приема в 1С

Диаграмма переходов между статусами представлена на рисунке.


![Диаграмма переходов статусов](db1.png "Диаграмма переходов статусов")


### Журнал фактического выполнения заданий (TasksExec)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* TaskId FK NOT NULL - ссылка на таблицу заданий
* Start DATETIME2 NOT NULL - фактическая дата начала
* End DATETIME2 NULL - фактическая дата завершения
* Status TINYINT NOT NULL - статус задания (из списка статусов заданий)
* Comment NVARCHAR(2000) NULL - комментарий к выполнению
* UserId INT NULLFK - диспетчер

### Перемещения при выполнении задания (TasksExecMove)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* TasksExecId FK NOT NULL - ссылка на таблицу фактического выполнения заданий
* LotId INT NOT NULL FK - ссылка на партию
* SourceUnitId INT NULL FK силос-источник
* SourceVehicleId INT NULL FK транспорт-источник
* DestUnitId INT NULL FK силос-приемник
* DestVehicleId INT NULL FK транспорт-приемник
* MovedAt DATETIME2 NOT NULL - дата окончания перемещения
* Quantity DECIMAL(18,3) NOT NULL - перемещенное количество

### Журнал приоритета сырья

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* DocId NVARCHAR(36) NULL - идентификатор документа в 1C
* NomenclatureId INT NOT NULL FK - сырье
* Priority INT NOT NULL FK - приоритет
* UserId INT FK NULL - автор (диспетчер или пользователь в 1С)

### Журнал получения данным от 1С (LogEvent1C)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* DocId NVARCHAR(36) NULL - идентификатор документа в 1C
* EventType NVARCHAR(MAX) NULL - тип события
* Message NVARCHAR(MAX) NULL - сообщение 1С
* DateMessage NVARCHAR(MAX) NULL - данные к сообщению 1С

### Журнал отправки данных в 1С (TransmitLogs)

* Id INT PK
* CreatedAt DATETIME2 NOT NULL - дата создания записи
* xMessage NVARCHAR(MAX) NULL - сообщение для отправки в 1С
* MessageType INT NOT NULL - типа сообщения
    * Выполнение задания по отгрузке вмешнему потребителю = 1,
    * Выполнение задания по отгрузке в рукава = 2,
	* Выполнение задания по отгрузке на КЗ = 3,
	* Взвешивание транспорта = 4,
	* Состояние склада = 5,
	* Завершение перемещения = 6,
	* Завершение приёмки сырья 7
* MessageTypeRus NVARCHAR(MAX) NULL - описание типа сообщения
* Result NVARCHAR(MAX) NULL - результат
* IsSucces BIT NULL - признак успешности передачи
* CountError INT NILL - счетчик ошибок
* DateTransmit DATETIME2 NULL - дата передачи
* LastTryDate DATETIME2 NULL - дата последней попытки повторной отправки
* NextTryDate DATETIME2 NULL - дата следюущей попытки повторной отправки
* ErrorType INT NILL - тип ошибки при передаче (0 - логическая, 1 - сетевая)

# Программные элементы

## Хранимые процедуры

### GetEquipments
Получает список оборудования.
Параметры:

* @EquipmentName		NVARCHAR(255) = NULL - шаблон имени

### GetEquipmentStats
Отчет о работе оборудования.
Параметры:

* @EquipmentId NVARCHAR(255) = NULL - перечень идентификаторов оборудования через ","
* @Begin DATETIME = NULL - время начала отчета
* @End DATETIME = NULL - время окончания отчета

### GetOperInfo
Получает текущие оперативные данные для вкладки на СКАДе.
Параметры: нет

### GetPoints
Получает текущие актуальные точки погрузки/выгрузки.
Параметры: нет

### GetRecvReport
Отчет о приемке сырья.
Параметры:

* @Begin DATETIME = NULL - время начала отчета
* @End DATETIME = NULL - время окончания отчета
* @TextFilter NVARCHAR(300) = NULL - фильтр по текстовым полям

### GetSiloMoveReport
Отчет по движениям на складе (силосе или промежуточного бункера).
Параметры:

* @SiloId INT - идентификатор склада (UnitId)
* @TimeFrom	DATETIME = NULL - с какого момента времени

### GetSiloStateReport
Отчет по состоянию склада (силоса или промежуточного бункера).
Параметры:

* @SiloId INT - идентификатор склада (UnitId)

### GetStorages
Получает текущие актуальные склады.
Параметры: нет

### GetTask
Отчет по заданиям.
Параметры:

* @TaskId INT = NULL - идентификатор задания
* @TaskStatus INT = 0, обобщенный статус заданий:
	* 0 - все
	* 1 - в очереди
	* 2 - активные
	* 3 - история
* @Begin DATETIME = NULL - время начала отчета
* @End DATETIME = NULL - время окончания отчета
* @TextFilter NVARCHAR(300) = NULL - фильтр по текстовым полям

### GetUnitsForReports
Получается списки узлов для отчетов

* @Type INT = NULL - тип узла.

### GetVehicleReport
Отчет по транспортным средствам.
Параметры:

* @Begin DATETIME = NULL - время начала отчета
* @End DATETIME = NULL - время окончания отчета
* @RegNum NVARCHAR(16) = NULL - фильтр по номера тр. ср-ва
* @IsWithin BIT = 1 - которые сейчас на территории
